

# <center>Fab Academy Week 10</center>
# <center>Input Devices</center>

<center>![](assets/)</center>


##### **Resources:**
* http://academy.cba.mit.edu/classes/input_devices/index.html
* http://fab.academany.org/2019/labs/barcelona/local/wa/week10/
* http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/inputdevices/


  <!---  -->
  <!---  -->
  <!---  -->
  <!---  -->

<!---
Demonstrate workflows used in circuit board design and fabrication
  -->
  <!---  
Described your design and fabrication process of your OWN DESIGNED BOARD
"explain the programming process you used and how the datasheet helped you
-->
<!---
Cut/solder/programm one of the examples to understand how it works and modify one or make a completely new one.
Described your problems and how you fixed
include your design files and photos  -->
  <!---  -
  Described your design and fabrication process using words/images/screenshots. (eagle, milling, soldering)
It would be nice to see your comments about neil’s code. How do you understand it, or if you modified it.
You explained some changes you did in other to adapt to the new python version and your usb port, but you need to explain deeper how neil’s code works. How is structured and how do you understand the logics of the code.
If you didn’t change the Neils code you need to explain deeper how it works. How is structured and how do you understand the logic of the code.
The download link seems to be broken.
I really like the idea of potentiometer as and input. Would be nice to measure the range resistance it has. Normally is 10K ohms but you should double check with datasheet or voltmeter.
Explain what are the multiple tests you did with Arduino? What code did you use? What conclusions did you get?
Described your design and fabrication process using words / images / screenshots. (eagle, milling, soldering)
Explain the code with which you programmed your pcb. Did you use the neil code?
Explains the programming process. Did you use Arduino IDE or Command line?
Missing eagle and code files
Super nice explanation about fuses.
Add Guillem fabacademy webpage link.
I didn’t know about Platformio maybe would be nice to explain a bit more about how the program works, and how do you use it for the assignment.
Maybe you show in networking week but will be nice to have a short video that shows how you read the sensor.
I know maybe it’s obvious but would be nice to include some pictures of programming process  -->
  <!---  -->

## Sensors

Strategic use of cheap and available devices as sensors is as important as knowing how to make one from scratch.

I decided to use a sonar distance sensor because this will allow me to test ultrasonic metamaterials which are easy to fabricate in a fab lab.

## Why we need an extra board

The HC-SR04 has its own microcontrollers on board to store data and process it to make it meaningful as measures of distance.  

It is possible to get readings from the sensor by connecting it directly to an Arduino as outlined [in this tutorial](http://www.ardumotive.com/how-to-use-the-hc-sr04-ultrasonic-sensor-en.html)
However the board has FTDI and ISP headers to enable communication between devices to be embedded in a larger system.

The Fab Academy website provided an example of [a small board](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.png) designed by Neil Gershenfeld to convert the output of the sensor to return a reading to an interface?

![](assets/hcsr04.png)


## Fabricating an HC-SR04 Ultrasound distance sensor board

Neil has kindly shared with us the traces, outline and .c and .py files for the board:

![](assets/sonartrace.PNG) ![](assets/sonaroutline.PNG)

### Milling the board

I put the trace and the outline .PNG files into the fabmodules application because for milling with the Roland SRM20 Milling machine this is a much easier process for me than using the MODS application as there are less steps to remember.

![](assets/fabmodulesoptions.png)

I uploaded the traces board provided and selected top output to a .rml file to make a 1/64 trace. I checked the speed was 4mm/s and the zjog was 12mm and clicked calculate for fabmodules to calculate the toolpath as before.
The computer in the fab lab is able to send the file to a locatio that the machine can read.

I taped my board to the platform using double sided tape and inserted the endmill into the machine using the allen key being careful not to tighten it too hard.

In the machine interface I used the arrows to move the mill end to the point where I wanted the trace to start. I clicked the X/Y button on the right hand side of the control button which sets the X, Y origin.


#### Setting the Z origin

using the machines control panel I slowly lowered the mill until it was about 5mm above the board. i loosened the holding screw and lowed the mill end with my fingers until it touched the board. With my other hand I pushed the board down slightly as there was a very small amount of cushioning from the tape. With this hand pusshing down on the board I let the mill fall to this slightly lower level by releasing it and then held it tightly again. I left go of the board with my other hand and screwed the mill into place. This gave a good connection between the copper and the mill. This was my Z so in the panel I set this by click the Z button in the set origin section on the right hand side.

With origins set I clicked Cut and was prompted to choose the file to cut from a location where I had saved my .rml file from fabmodules.

I selected my file and the machine started milling.

#### Cutting the outline

Once the trace had been cut I brushed away the dust and was careful not to reset the XY in the control panel or move the board.
I remembered to change the endmill to a 1/32 and clicked cut and selected the outline file.

The machine cut out the board and the satisfaction of this drummed home the message to REMEMBER TO CHANGE THE END MILL FOR CUTTING OUT THE BOARD.


### Soldering components

Below are the components needed for the board

| Label  | Component name| Part number | Purpose| Inputs | Outputs |
| ------------- |-------------| -----| -----| -----| -----|
| HC-SR04 | Ultrasonic Module HC-SR04 Distance Sensor | HC-SR04 |Sends out ultrasound and receives reflected ultrasound to calculate distances | VCC from FTD, Trigger signal from PB3 on microcontroller | Echo signal back to PB4 on microcontroller, GND. THis device also has  |
| IC1t45 | [ATtiny45/V Microcontroller](http://www.farnell.com/datasheets/1698186.pdf) | ATtiny45/V |Can run small scripts via 8 pins | VCC, RST, SCK/Rx, MISO, MOSI, PB4 (Echo) | GND, PB3 (trigger) |
| R1 |  10k Pull-up Resistor |  1002 | Ensures zero is really zeroed out for the off state going into the MCU | VCC | RST |
| C1 |1uF capacitor| | between VCC and ground to remove voltage or signal spikes  | -----| -----|
| J1 ISP | 6-pin programming header |  | for programming using 'serial peripheral interface' protocol from an ISP (In-system programmer)  | VCC, GND, MOSI, MISO, SCK, RST| |
| J2 FTDI  | FTDI header | none | a 6 pin connector for power and data - to be used with the FTDI patented microchip which works through USB - needs drivers to decode information coming in. | Rx from microprocessor, Tx from programmer | The VCC and GND to the whole board, Tx to microprocessor, Rx to programmer |

Here is my board. I am not proud of the soldering but look forward to improving this. I am enjoying soldering even though it can be frustrating.

<center>![](assets/solderedsonar.jpg)</center>

## Programming the board to read and display

<!---  
Programming
- First of all i downloaded the C, Make, and Python files.
- Open the terminal
go to the specific folder and run
the Hex file
// make -f hello.load.45.make
http://academy.cba.mit.edu/classes/input_devices/motion/hello.HC-SR501.make
- Program the tiny 45 //
make -f hello.load.45.make program-usbtiny
- Download pyserial 2.6, unarchive, and place in a known directory (desktop, documents, etc.)
- Go to the pyserial folder and type // python setup.py install
- Run the python file: // python hello.load.45.py /dev/com3
-->

<!---
## Choosing a microprocessor
 Anything that works on an Arduino works on a 328
has serial built in - does not need serial programming.
## Pin capabilities of the [ATtiny45/V microprocessor](http://www.digikey.com/product-detail/en/ATTINY45V)
I was really interested to hear that pin capabilities are related to their physicality

| Board Schematic | ATtiny45/V pin number| Arduino  pin| Capability of pin  |
| ------------- |-------------| | -----|
| PB2      | Pin 5 | | Analogue input, Analogue output from digital inputs |
| PA7 | Pin 6 | | Analogue input, Analogue output from digital inputs |
| PA2 | Pin 11 | | Analogue input |
| PA3 | Pin 10 | | Analogue input |
## Measure something
## Using the ISP as external pins
## Programming the ATtiny45/VMicroprocessor
## Future projects
- making Metamaterials as sensor s[Metamaterials Application in Sensing Tao Chen 1,*, Suyan Li 2 and Hui Sun](https://www.researchgate.net/publication/228071785_Metamaterials_Application_in_Sensing)
-->

### Toolchain for programming the ATTiny44 on the sensor board

| Component | Purpose | Language needed | Environment for programming| code |
| ------------- |-------------| -----|----|----|
| Sensor | to receive voltage pulses in its RX pin and convert these into sonar signals, receive echoes and convert these into voltages and transmit these through its TX pin | it does not need programming| | |
| Sensor Interface board with ATTiny45 microcontroller| To send the trigger pulses to the sensor, receive the echo and make the result useful so if the sensor breaks you don't have to reprogram it | contains programme in binary code | Programming is written in .c, converted to .hex by a compiler | |
| Bootload burner software that has drivers for  the programmer (Arduino IDE) | To set the fuses? the functions (configurations) of the pins on the ATTiny45 |  | Arduino IDE with support library for the microprocessor | not known |
| Programmer | To turn hex code into binary code and send to the microcontroller | |  | http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.c /   http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.make|
| Compiler software | to turn C code of what the ATTin45 should do into hex code and an .out code send to the Programmer along with commands (in ?) to programme the ATTiny45 | .make files  | make commands in terminal or hyper mode | |

Another amazing How to Make Almost Anything student Francesca has made this [really useful documentation page](http://fabacademy.org/archives/2015/eu/students/perona.francesca/htm/07_week.html) with this schematic of embedded programming using the FabISP and an Arduino that includes environments, hardware, software and most importantly ACTIONS! - Thank you Francesca!

![](assets/pipsdiag.PNG)

We checked if Fifa's FabISP was soldered properly by using a multimeter to check the connections

<center>![](assets/checkfabisp.jpg)</center>

After asking Xavi in the lab we decided to use Fifa's FabISP programmer and [Neil's interface circuit for the HC-SR04 that I made](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.png) giving the following toolchain:

![](assets/sensortoolch.jpg)

[HCSR04 Sensor](https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf) - bare minimum physical set up for taking readings

We connected the toolchain as follows:

1. Female to female wires connecting the sensor to VCC, Ground, Trigger and Echo and on the Programmable board
2. Programmable board - to power the sensor, to time when to send the trigger and receive the echo voltages and to process it into a meaningful value.
3. 6 pin ISP header connected with 6 wire ribbon  to connect the programmable board to the programmer (FabISP or Avr-isp-MK2).
4. The programmer to transfer .hex code from it's own ATTiny 44 microprocessor straight into the ATTiny45 on the programmable board. We got an error message when we tried to do this so we swapped in with Avr-isp-MK2 programmer found in the lab.
5. A miniUSB cable to interface the FabISP(with firmware) with a mac laptop with drivers installed. We ended up using a Avr-isp-MK2 programmer with a normal USB port to the laptop.
6. Arduino IDE with the [ATtiny support library installed](http://highlowtech.org/?p=1695)to command (the programmer? the FTDI?) to burn the bootloader of [ATTiny45](http://www.farnell.com/datasheets/1698186.pdf) on the programmable board.
7. [Neil's hello.HC-SR04.make file ](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.make) called up in "hyper" mode in mac os to compile [Neil's hello.HC-SR04.c code for the ATTiny45](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.c) to generate a .hex and .out file and send it to the programmer
8.  Arduino to read the sensor board, convert the voltages to cm and display in the serial monitor (i think!)

We used this [excellent documentation by Nicolo Gnecchi](http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/input-devices/) to guide us through programming the ATTTiny45 on the sensor board.

#### Steps 1-5: Set up for burning bootloader and programming the ATTiny 45 on the sensor board

<center>![](assets/sonarprogramming.jpg)</center>

#### Step 6: Burning the bootloader of the ATTiny45

* 6.1  We downloaded [Neil's .c file](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.c) and [.make file](http://academy.cba.mit.edu/classes/input_devices/sonar/hello.HC-SR04.make) for the controller board's ATTiny45 and saved them in the same folder.
* 6.2. The .make file is saved as a .txt file by the system so we changed the suffix to a .make file
* 6.3. In 'hyper' in mac os we used use "cd" to go to the file with the downloads
* 6.4. In hyper entered the command share??..
        avr script
        mk2 avr progr
        make - f hello.mag
        program-avrisp2: $(PROJECT)


#### **PROBLEM!** we downloaded the wrong files and used the wrong command to programme the ATTiny45

As we were following Niccolo's documentation we also entered his commands!

**TIP!: make sure you don't blindly follow instructions especially code when you don't know what they are doing**

#### Step 7: Call the .make and .c files

Call the .make file hello.HC-SR04.make to compile the hello.HC-SR04.c file to generate .hex and .out files

          make -f hello.HC-SR04.make

Program the board with programmer

          make -f hello.HC-SR04.make program-avrisp2 (as defined in the make file)

#### Step 8: Reading the sensor using the Arduino IDE Serial Monitor

Xavi found this Arduino simpleread code for HC-SR04 distance sensor online that turns voltage readings into centimetres.
I found [this source for Arduino libraries for interfacing with the HC-SR04 sensor](https://www.arduinolibraries.info/libraries/ultrasonic)

<!---in the correct c and  the one we loaded the pin definitions were the same:
      #define serial_port PORTB
      #define serial_direction DDRB
      #define serial_pin_out (1 << PB2)
      #define out_pins PINB
      #define out_pin (1 << PB4) -->


       #include <SoftwareSerial.h>

       #define RX 5
       #define TX 6

       SoftwareSerial mySerial(6,2); // rx, tx

       #define echoPin 4
       #define trigPin 3
       #define LEDPin 13

       int maximumRange = 200;
       int minimumRange = 0;
       long duration, distance;
       int redPin = 8;
       int greenPin = 9;
       int bluePin = 10;

       void setup()
       {
        mySerial.begin (9600);
        pinMode(trigPin, OUTPUT);
        pinMode(echoPin, INPUT);
        pinMode(redPin, OUTPUT);
       }

       void loop()
       {
        digitalWrite(trigPin, LOW);
        delayMicroseconds(2);
        digitalWrite(trigPin, HIGH);
        delayMicroseconds(10);
        digitalWrite(trigPin, LOW);
        duration = pulseIn(echoPin, HIGH);
        distance = duration/58.2;
       mySerial.println("Hello:");
       mySerial.println(distance);
       delay(100);
       }

The serial port in Arduino can be read and coded into Processing.



Xavi found code to turn the reading from the Arduino serial port into Processing here(https://github.com/processing/processing/blob/master/java/libraries/serial/examples/SimpleRead/SimpleRead.pde)


      /**
       * Simple Read
       *
       * Read data from the serial port and change the color of a rectangle
       * when a switch connected to a Wiring or Arduino board is pressed and released.
       * This example works with the Wiring / Arduino program that follows below.
       */


      import processing.serial.*;

      Serial myPort;  // Create object from Serial class
      String val;      // Data received from the serial port
      int number = 0;
      void setup()
      {
        size(200, 200);
        // I know that the first port in the serial list on my mac
        // is always my  FTDI adaptor, so I open Serial.list()[0].
        // On Windows machines, this generally opens COM1.
        // Open whatever port is the one you're using.
        String portName = Serial.list()[0];
        myPort = new Serial(this, "/dev/ttyUSB0", 9600);
      }

      void draw()
      {
        if ( myPort.available() > 0) {  // If data is available,
          val = myPort.readString();         // read it and store it in val
          number = int(val);
          println(number);
        }
        background(255);             // Set background to white
        if (number <= 0) {              // If the serial value is 0,
          fill(0);                   // set fill to black
        }
        else {                       // If the serial value is not 0,
          fill(204);                 // set fill to light gray
        }
        rect(50, 50, 100, 100);
        delay(100);
      }



## Getting a decent analogue signal from a MAX4466 sound sensor, breadboard and Arduino

In Networking week I tried to get an analogue signal out of a sound sensor - this was not easy as the sensor had to be tuned very finely. I bought a MAX4466 sound sensor to see if this might be more effective.

Using a breadboard and an Arduino, I loaded the code straight into the Arduino via Arduino IDE.

<iframe width="560" height="315" src="https://www.youtube.com/embed/v9y2teN-EmE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Other Wifi antenna and recievers
