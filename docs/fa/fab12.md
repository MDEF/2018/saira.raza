

# <center>Fab Academy Week 12 Applications & Implications</center>

<center>![](assets/)</center>

<!---     --><!---     --><!---     --><!---     -->
<!---    
define the scope of your project
develop a project plant
what will do?
what has done beforehand? investigation
what materials and components will be required?where they come from?what it is the cost? BOM(Bill of materials detailed)
what parts and systems will be made ? what processes used?
what taks need to be completed?
what questions need to be answered
what is the schedule? real vs ideal
how will it be evaluated?
 -->


## Scope of My Project

The scope of my project is to visualise sound fields in 2 dimensional planes as 2 dimensional visuals.<!---     -->

The purpose of visualising sound fields is part of my masters project on collaborating with objects to experience our environment and design efficient things.

It seeks to demonstrate how fab labs, opensource and learning through making contribute to
1. documenting, representing and sharing experiences
2. finding hidden contextual and interaction information for designing this apparatus through embodied design.
3. demonstrating the degree to which a complex field of energy can be simplified and represented to be useful to test and design  metamaterials


## The task
<!---  what will do?   -->

The task is to set up the apparatus for measuring sound fields to enable visualisation of the effect of acoustic metamaterials. This breaks down into the following sub-tasks:

### Sensing sound amplitudes

The sound fields will be measured as average maximum voltages from a sound sensor representing amplitude of the sound. This requires a sample rate which will determine the range of frequencies that the sensor can detect.

### Making sounds of specific frequencies

If I can make the system without sound propagation I will be happy but sound propagation means I can demonstrate some known metamaterials.


### Processing signals into meaningful values

To make sense of the fluctuating voltages a processor will have to take fluctuating readings, sample them in periods set by code, store the values and select the peak value and then average these every period for as long as we take a reading.

### Moving the sensor in 2 dimensions

The sensor will scan a 2D plane and sensor readings will be stored tagged with an X and Y value to enable a 2D map of sound amplitude.


<!---  
Resources:

*   -->

### Storing sensor and location values

The data processed by the processor will need to be sent to a repository alond with its X and Y positions.

### Visualising sensor values in 2 dimensions

The data stored will need to be processed into a visualisation of the average sound amplitudes in a 2d shape such that it can be analysed by an AI.

### Making a set of 3D metamaterial devices to test

## Materials and Components
<!---
what materials and components will be required?where they come from?what it is the cost?  -->

I will need..

- sound sensor
- microphone
- a salvaged 2d printer stage
- a closed box with minimised gaps (3d printed if its small possibly case if larger)
- sensor board and microphone board
- x and y stepper motors
- wires or wireless communication from sensor to microphone and data storage device
- data storage server - either a raspberry pi or cloud
- software to read from wireless transmitter or wired in put
- software to transform data into a 2D visualisation

Fabrication Equipment
- 3d printer
- Laser cutting for housing frame (at least)
- Milling machine for boards for sensor and emitter
- Computer for coding and hosting software
- Peripherals for display (screen at least)


### Sensors

Currently sound sensors output a voltage based on the movement of something - a reed - a piezo (a solid material that reacts proportionally to sound) or a mem (very small mechanical structures). There is a physical limit to how fast these structures can respond to sound waves.

Sensors therefore need to be matched to the frequencies they need to measure. Ambient sound obviously depends on where you are. The audible range of sound frequencies is from 200 to 20,000Hz. There are ultra and infra sound frequencies all around us too.

The sound frequencies will determine the size of the apparatus

#### User inputs for measurement

- sample period for sensor
- sample period for average reading
- and maybe x and y values if movement is done manually.

## References of what has been done before
<!---    what has done beforehand? investigation -->

### High end

Laser sound visualisation

<iframe width="560" height="315" src="https://www.youtube.com/embed/VRq1vc00R7s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Big scale sonar

<iframe width="560" height="315" src="https://www.youtube.com/embed/SXZ1YTsmzzI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### "Mid" budget

<iframe width="560" height="315" src="https://www.youtube.com/embed/-VmPZeYx2II" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Low-tech Cymatics

DIY Analogue Sound Experiments

[Science Friday Science Club experiment](https://www.sciencefriday.com/educational-resources/make-a-model-eardrum-to-detect-sound-waves/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/k_rCL0r4OAo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Makers visualising EM fields (acoustics are more complicated!)

Using a CNC platform to do the scanning...

<iframe width="560" height="315" src="https://www.youtube.com/embed/aqqEYz38ens" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Low tech with microcontrollers

Acoustic input to light output seems to be the go-to strategy for mapping sounds and waves in general. This assembly gets an RGB LED to indicate phase changes between two fixed signals and records them on a long exposure photograph like the example above.  Might this do for our purposes?

**[PhaseBrush](https://www.doc-diy.net/electronics/phasebrush/)** uses a sound sensor and an ATtiny24 to detect phase shifts between two static microphone signals. It displays phase shift to LEDs which indicate different bands of shift. The phase brush is manually scanned across a translucent 2D surface and the light effects are recorded using long exposure photography records the movement of the PhaseBrush.

Last year Xiang et al built a [2D acoustic sacanner using 2 stepper motors controlled by an Arduino](https://asa.scitation.org/doi/pdf/10.1121/1.5040886?class=pdf) to investigate acoustics of scaled models of spaces.


## Strategy

<iframe
  src="https://embed.kumu.io/6d17fc977f89ccf543fc44e127a4d7d4"
  width="940" height="600" frameborder="0"></iframe>


##  Systems and Parts to make

<!--- What parts and systems will be made ? what processes used? -->

### 1. Propagating sound
 - Emitter
 - Frequency generating code
 - on/ off control
 - could try to use a 3d printed parabolic dish to make the waves straight

#### Progress




### 2. Sensing sound
 - microphone
 - signal amplification and possible calibration

 The sound sensor will be mounted either on the plotter pen holding head

#### Progress
I have bought a [MAX4466 sound sensor](https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels) and a piezo buzzer which I have combined into this code in Arduino IDE which cause the piezo to buzz whilst the sensor listens


        int piezopin = 8;
        const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
        unsigned int sample;

        void setup() {
            // for the sensor open serial port and set data rates to 9600 bps (bits-per-second)
           // this lets us communicate to/from the arduino
           Serial.begin(9600);
        }

        void loop() {

         // The tone function for the buzzer - can be between 0 and 65,535 - 2000 and 5000 for audible,
         // for 1000= 2484 is picked up by the sensor (strange value)?  2000= 1992 hz i think, 4000=3984, 6000= 6000 hz. So the range that seems to corrlelate to microphone readings for this code is 2000 - 6000

        tone(piezopin, 2000);
        // tone(pin, frequency, duration) tone function is independent of the delay function - the duration is the length 500 milliseconds
        //- from the arduino clock in parralell with the delay. 1000 duration is almost nonstop
        // pins 3 and 11 uses the same clock so dont control from these dont use analogwrite
        // you can only use the tone one pin at a time
        //delay(1000);

        //for the sensor
           unsigned long startMillis= millis();  // Start of sample window
           unsigned int peakToPeak = 0;   // peak-to-peak level
           unsigned int signalMax = 0;
           unsigned int signalMin = 1024;

        // sensor to collect data for 50 mS
           while (millis() - startMillis < sampleWindow)
           {
              sample = analogRead(0);
              if (sample < 1024)  // toss out spurious readings
              {
                 if (sample > signalMax)
                 {
                    signalMax = sample;  // save just the max levels
                 }
                 else if (sample < signalMin)
                 {
                    signalMin = sample;  // save just the min levels
                 }
              }
           }
           peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
           double volts = (peakToPeak * 5.0) / 1024;  // convert to volts

           Serial.println(volts);
        }

### 3. Sensor or emitter Position control
    - x and y stepper motors connected to a rigid frame that will enable the sensor to move in controlled, measured and recorded increments

    Here is a scanning platform in the fab lab that is small and sturdy - could I borrow and adapt this?

    ![](assets/scanner2.jpg)

    ![](assets/scanner1.jpg)

    I spent a week getting the plotter to move and am at the last step of loading GCode I have prepared into its Arduino via Chilipeppr tinyG environment but I am stuck on the file type required and how to convert a .tx file to whatever this might be.

#### Existing solutions

The **Machines That Make** group at MIT has a number of open source solutions for machines you can make in a Fab LAb. These [Reconfigurable Stages](http://mtm.cba.mit.edu/machines/stages/) run with the pyGestalt controller framework so that they can take input from different software interfaces. This requires steppers set up with [Gestalt Single Stepper Node boards],a power source, FTDI and AVR programmer and [the pygestalt library]((https://github.com/nadya/pygestalt).

[OpenEM – Electromagnetic field mapping robot for microwave and RF measurements
Benjamin Wanga Rayan Suda Michael Leungb Mimi Yang b Jesse A.RodriguezaR ickyLeea MarkCappellia https://doi.org/10.1016/j.ohx.2019.e00062
links ArduinoIDE to the CNC xPRO Controller V3

https://ars.els-cdn.com/content/image/1-s2.0-S2468067218300622-ga1.jpg

http://diy3dprinting.blogspot.com/2014/12/

#### Progress

I currently have a [28byj-48 5vdc stepper motor](http://robocraft.ru/files/datasheet/28BYJ-48.pdf) and a driver board. I managed to make a start on programming this through an Arduino but struggled to find a way to code stops into the code.

Oscar pointed out that I should take advantage of the accuracy and time saving offered in an existing plotter in the fab lab which can be programmed by plugging it's onboard microcontroller to my laptop and sending GCODE to it through my terminal

G Code is able to describe the path of the scan accurately for that machine and is easy to understand and write.

I followed [this tutorial](https://www.instructables.com/id/How-to-write-G-code-basics) to write the following code to put into the scanner

      %
      OSCAN //Name of task
      G00  
      G90, for absolute position mode.
      G80, to cancel any previously used canned cycles.
      G28, to rapid to the home position.
      G17, to select the x, y circular motion field.
      G21 - Millimeters as units
      G40, to cancel cutter compensation.
      G49, to cancel the cutter height compensation.

        G17 - Select XY plane (default
        G53 - Set absolute coordinate system
        G92 X0 Y0(set current position to origin <x,y>=<0,0>)

        G1 X10 Y0 F100.0 (Move to postion <x,y>=<10,-0> at speed 100.0)
        G4 P5000 (wait for 5000 milliseconds)
        G1 X20 Y0 F100.0
        G4 P5000
        G1 X30 Y0 F100.0
        G4 P5000
        G1 X40 Y0 F100.0
        G4 P5000
        G1 X50 Y0 F100.0
        G4 P5000
        G1 X60 Y0 F100.0
        G4 P5000
        G1 X70 Y0 F100.0
        G4 P5000
        G1 X80 Y0 F100.0
        G4 P5000
        G1 X90 Y0 F100.0
        G4 P5000
        G1 X100 Y0 F100.0
        G4 P5000
        G1 X110 Y0 F100.0
        G4 P5000
        G1 X120 Y0 F100.0
        G4 P5000
        G1 X10 Y10 F100.0
        G4 P5000
        G1 X20 Y10 F100.0
        G4 P5000
        G1 X30 Y10 F100.0
        G4 P5000
        G1 X40 Y10 F100.0
        G4 P5000
        G1 X50 Y10 F100.0
        G4 P5000
        G1 X60 Y10 F100.0
        G4 P5000
        G1 X70 Y10 F100.0
        G4 P5000
        G1 X80 Y10 F100.0
        G4 P5000
        G1 X90 Y10 F100.0
        G4 P5000
        G1 X100 Y10 F100.0
        G4 P5000
        G1 X110 Y10 F100.0
        G4 P5000
        G1 X120 Y10 F100.0

I wrote it using this Excel file

Santi kindly allowed me to use the plotter in the lab. It currently does not have any power so I cannot test my code.

I have been looking at the mechanism and have decided to attach a texting box with some sound proofing and a
I the meantime I have drawn a piece to

### 4. Communication from sensor, emitter, and motor / distance measurement to a readable server

Code to listen and respond in real time to any pattern using GCODE and Arduino [here](https://www.marginallyclever.com/2013/08/how-to-build-an-2-axis-arduino-cnc-gcode-interpreter/)

For this we can use the serialprint or serial read on the arduino

### 4.5 Imaging

https://sites.google.com/site/ncadarduino/serial-communication
> This example uses Three potentiometers connected to Analog pins A0 A1 A2 to create a drawing  in Processing.
The processing code reads three analog sensors via serial
One sensor reading controls x axis position of a circle
One sensor reading controls y axis position of a circle
One sensor reading controls tone of the circle


in arduino

              void setup()
              {
                Serial.begin(9600);
              }

              void loop()
              {
                delay(500);
                Serial.print(analogRead(A0));
                Serial.print(",");
                Serial.print(analogRead(A1));
                Serial.print(",");
                Serial.println(analogRead(A2));

              }

in processing

        import processing.serial.*;
        float xValue = 0;
        float yValue = 0;
        float toneValue = 0;
        boolean pressed = false;
        Serial myPort;
        void setup()
        {
        //creates a window 500p x 500p high
        size(500, 500);
        smooth();
        println(Serial.list());
        myPort = new Serial(this, Serial.list()[0], 9600);
        myPort.bufferUntil('\n');
        }
        void draw()
        {
        //Will fill based on the sensor's input
        fill(toneValue);
        //creates an ellipse r = 30 w/ centerpoint at the x and y coordinates
        ellipse(xValue, yValue, 30, 30);
        }
        void serialEvent(Serial myPort)
        {
        String inString = myPort.readStringUntil('\n');
        float[] coordinates = float(split(inString, ","));
        if(coordinates.length >= 3)
        {
        //maps the range of values of the potentiometer to the
        //values of the size of the screen. You
        //may wish to reverse the 0 and 500 to
        //get the knobs to go up and down according to a different //twisting direction.
        //dont think the arduino feeding the x and y
        xValue = map(coordinates[0], 0, 1023, 0, 500);
        yValue = map(coordinates[1], 0, 1023, 0, 500);
        //maps the range of values of the photoresistor to the
        //values of fill.
        toneValue = map(coordinates[2], 0, 1023, 0, 255);
        }
        }


### 5. Housing and soundproofing
  - soundproof foam - could make a mini anechoic chamber
  - sealed or 5 sided housing -- could even make this as a 3d printed metamaterial cloak!
### 6. Control interface if time
### 7. Ideally a selection of 3D printed metamaterials for the sensor frequency range


https://sites.google.com/site/ncadarduino/serial-communication


## Tasks, BOM, Progress and Schedules

![](assets/projprog.png)


<!---
## BOM


|  |  |  |  |
|--|--|--|--|
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  |
|  |  |  |  | |

-->

## Answers needed

<!---what questions need to be answered     -->
- What communication methods can be set up in time i.e will the system need to be completed wired into an Arduino or can we make some elements wireless?


- How can I program microcontrollers with the equipment I've got - what am I doing wrong that is not letting me use my Arduino as an ISP programmer?


- Is there a shortcut to making the scanning assembly from scratch? can I use a broken printer I have been given?


I am storing links of information that may come in handy in this **[project information dump](/fabprojinfodump)**


<!--- what is the schedule? real vs ideal    -->


## Evaluation
<!---  how will it be evaluated?   -->

The aim of the exercise is to produce a DIY way of getting repeatable shapes that correspond to the same acoustic energy state. Of course this is a big ask because of the issue of ambient noise but I will aim to try and get enough resolution so that the variations in acoustics for the same emitter frequency and amplitude sound field produce the same shape at least 7 out of 10 times.
