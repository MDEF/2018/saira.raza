

# <center>Fab Academy Week 18 Intellectual Property</center>


<center>![]()</center>
<center>![]()</center>

This week we learned the range of ways that ideas can be shared by licensing and patenting. 


My project centers around how making informs the building of technologies and how in the age of artificial intelligence, collective problem solving, designing and fabricating could make us more mindful of materials and the potential they contain.

Quite a long winded point I know but in the context of this copyleft ideals are very important.

By documenting my project online in all its various states completion and direction the things I make and the ideas I write (for what they are worth) belong to everyone. As far as I see it they were never mine to own.

As my project has ended up borrowing a lot of existing technology (Arduino, Formbytes) I am unable to say the design is my own as it would be too specific and not innovative enough to patent.

However in the name of collaboration I want to discourage me and others being shut out of conversations that I am pushing to be more transparent and easy to understand. Therefore I would not want anyone to materialise any ideas and make their work exclusive.

Currently I am looking at how we learning and can make in a fab lab materials that are currently being researched in universities and getting permission to use researchers work is important to me as this starts a conversation on linking academics to makers.



<!---   
  -->

  <!---     --><!---     --><!---     --><!---     -->
<!---   -->
