

# <center>Fab Academy Week 10 inputs
 /center>

<center>![](assets/)</center>

http://academy.cba.mit.edu/classes/input_devices/index.html

http://fab.academany.org/2019/labs/barcelona/local/wa/week10/


https://hackmd.io/uEv4u6B3QPG2QpZk6vOCeg?both#Electronics-concepts



touch pads
musical instrucments


individual assignment:
      measure something: add a sensor to a microcontroller board
      that you have designed and read it

      make a new one

  the hello board has input at the bottom and LED output

  work in pairs
  replicate one of bneils boards


## Sensors
neurons
perform logic
bioindicators
sensing the environemnt
closing loops with feedback to monitor and control

sensors are getting cheaper -  think of the sensors on your phone-

sensor fusion - gyroscope and accelerometer gives relative information - location service + GPS

modelling equivalent parameters to reduce the number of sensors

* measure the environment
* data fusion - combining sensors
* we can remove some once you  see how the system runs

relating signals to one another - building a picture
MATLAB can be used to relate signals to one another to build mathematical models
we use python



### So what sensor do i use?
* survey what sensors you have an models that exist
* devices are off the shelf like a webcam, mobile phone, apps - eg 3d scanner + skanect. but these require a lot of coding to extract and use the data. we can connect many to a raspberry pi.
* components are the things we tend to use in fab aca. capacitive sensors are like antenna - go for the one that has the best documentation so you can calibrate better - try adafruit - check the frequency of sampling rate as this has to match your use. Accuracy, range
* sensor interfaces like skanect


## Assignment ideas

1. measure something - sound or ultrasound (ping sensor arduino) for acoustic metamaterials, wifi for milled antenna
2. add a sensor to a microcontroller board - the hello board has input at the bottom and LED output that you have designed
3. read it

### metamaterial sensors
https://www.researchgate.net/publication/228071785_Metamaterials_Application_in_Sensing


MEMS
tiny moving parts within  - accelerometers - compass - that help us measure things

Microcontrollers
for processing different signals

SEED studio
ready made components and breakout boards
sold by mouser

Switch

### Headers
FTDI - serial to usb converter on a cable -
USB to serial protocol
output connector

ISP
usb to ISP protocal
programming connector

read something from the environemtn ussing a neil example sensor
process using microcontroller
send through ftdi that will be converted to usb
visualise using the serial port?

http://academy.cba.mit.edu/classes/input_devices/index.html
for each sensor

python scrip to visualise the data

Sonar
ultrasound sender reciever  - distance meter - noisy
HC-SR04
 - looks like a boombox

 magentic field sensors  - used in speedomters

 resistors that change with temperature
needs a voltage divider

we can download the eagle file from adafruit



sound MEMS
 20 hz to 20khz is human voice


 vibration - piezo

 IMage

 camera  -recieve pixels  whic each measure 8 bits 255

 raspberry pi has connectors for their own camera


SPI signals

## workshop

what pins

highlowtech
neil uses c right now


http://highlowtech.org/?p=1695

do a blink test with an LED
1. button is a digital pin in pinmode(7, output)led
pinmode (3, input) button

arduino atttiny44 boards in arduino window

set clock to - external 20mhz  (external to the processor)

2. LED arduino pin equivalent

3. fuses: tools - board bootloader - sets speed of arduini - 20 mhz

void loop(){
  digitalwrite(7,high)
  delay(1000)
  digitalwrite(7,low);
  delay(1000);
}

ftdi cables are 5v and 3.3v

chips have dedicated sections othat only listen out for certain things through - mechanics?


ftdi reads high low signals digital


install library

sketch include library softwareserial
examples - softwareserial

#include <software serial.h>

softwareserial myserial(0,1);//rx,tx  telling the receiever and transmitter pins

RXD TXD

button serial-hello44

FTDI needs drivers

red cable is 5v power cable o  fdti

https://github.com/fablabbcn/arduino-workshops/tree/master/Hello%2044



debounce delay - defines milliseconds that an action needs to be done

https://github.com/fablabbcn/arduino-workshops/tree/master/Hello%2044/debounce_hello44

programming sequences of button presses.

Python go to your sketch in python
you can find the python port name of your attiny from the arduino

button.py
python is muc more simpler
debouncing can use

you cannot open a serial port from more than one application at a time

python can run raspberry pi

neil has written python scripts for each sensor



PWM allows ???


analogue sensors
external pins - the pIScp (ISP 6 pin) pins can be used as external pins by connecting to the attiny 44aa   - miso to miso etc

some sensors have tiny processors inside them  - eg DHT sensors

voltage gnd and communication -
can use serial protocol
libraries exist for sensors - check the datasheets and then in aduino sketch.



One pin on hello board



Connect the

we dont have serial in hardware on attiny
we need to load the software serial and have every perial pin replaced by our pins



check clock 20 mhz


include library for the sensor
you might need to install adafruit library on top if your sensr is from adafruit

check bpard and clock

if sketch is too large - you might need to use atmega328 instead of atttiny


attiny - fine for buttons and leds

MOUs and digital sensors like dht 22 sensors - instead of building a build - yuse a more powerful chip

bigger chips we have
xmega 1328 - same as uno? - similar to hello board - better for this week.
needs ftdi - 32 flashspace?

atmella 32 u 4 - has usb built in

you can use lots of them - each hello board  will have a usb - software to connect - you hcan have boards talking to eachother before


multiple sensors - use a powerful chip easy enough to code

risky option - go through eagle

sdafer - fabacademy fabduino website has mega 328 in the middle - looks like an arduino
