

# <center>Fab Academy Week 2</center>

<center>![](assets/)</center>



##### Resources:

* http://fabacademy.org/2019/
* http://fab.academany.org/2019/
* http://fab.academany.org/2018/labs/barcelona/local/general/
* http://academy.cba.mit.edu/classes/computer_design/index.html



model your final project and





- Gimp - can do boolean functions, can clone objects so if one changes the other changes.
- Photoshop
- Mypaint

-imagemagick - can change formats, vector to raster to achieve 10s-100s kB for images for website.

- bimp -

- inkscape (lines not pixels)
Illustrator

bluejeans -

freeCAD tracks relationships - you can constrain relationships you can assihn dimensions.
you can promote 2d to 3d yby extrusion. you can draw in 2d onto 3d and extrude or cut.


what was that thesis he showed us?

### 3d design
- function reps - 3d drawing more powerful than boundary definition.
- imperitive - design what you want
- declaritive - computer works out what yyou want
- generative design - iterative - optimisation
- multidisciplinaty optimisation - designs structures and subsystems (like BIM)

**Programs:**

- sketup nad tinkercad
- blender - powerful rendering (genertive), animation, video, not god at cad style but does great realistic renderuing  -with node editer can be parametric.
- rhino
- grasshoper is a node editor - graph to show flow of info into rhuno
- **FreeCAD again** - opencascade - a geometry engine. you can work with boolean objects AFTER the cut/ join. can link spreadsheets to drawing dimensions. you can select faces of objects and draw on them (bit like sketchup) Constraints

linux lets you download freecas from terminal

Autocad and fusion - cad cam and modelling integrated - not free - but is for for students - needs cloud connection.. they have workbenches - eg pointclouds, rendering everything is extensinle in PYTHON and people contribute addons.

- solidworks - only runs in windows  - engineering - large scale engineering designs with teams. support fablabs. **send request to uisse tracker for lisense.**
- x-design looks likes solid works but runs in browser - back end has Catia engine (scalable)
- Onshape cloud editor running in cloud for collabs. bit like googledocs. free educational programme.
- Maya to houdidnin-  rendering but not CAD tools per se
- Catia, creo etc - hi end cad. hard to lear for major complex lifecycle of projects.
- openscad - uses formula - represents object as a function not patches of surfaces.
OpenJSCAD
- [Matt Keeter](https://www.mattkeeter.com/projects/antimony/3/)
theses  fabmodules antimony libfive  - functional representation solver.. comes with a node editor - it makes 3D graphs - nodes for varioables or functions. the object is a function that i can manipulate. you can morph between solids - objects
- neil wrote mods uses GPU with nodes in browser
- grabcad and viewer?


### Interchanging between tools.

- stl really simple represents triangles you lose design info
- obj fairly good
- **STEP is recommended** - retains design intention
- alembr - more CG

catia is the most scalable (boeing use it)



### Game engines
**Unreal and unity** - free versions run in browsers - both have full graphics environments - model geometry , render, interact and interfaces into physical devises - you can use them to make hollywood quality. you can do everything in the game engine -  its imersve it can be vr and ar - good way to compose  then could move to cad to designthe actual thing.


### Simulation

- COMSOL - multiphysics tool - modelling forces and heat and airflow and FIELDS!
- simulation in solidworks
- Fusion has a really good modelling environment built in
-open dynamics engine -
- modelica - opensource

### Audio / Video

- Audacity (have)

Vid:
Kdenlive from linux - non linear vid editor -

Changing file types:
ffmpeg avconv mencoder


HTML5 MP4 ffmpeg encoding - added audio and video

play in browsers without plugins
they can play in browsers. shrinks size of video
cheat sheet here: http://academy.cba.mit.edu/classes/computer_design/video.html
bit rate - fixed or variable because?

formats
https://developer.mozilla.org/en-US/docs/Web/HTML/Supported_media_formats

Animated gifs - image quality is very poor - ok for 2 discrete frame - also huge files.

## This week's assignment

Try as many of these CAD programms as possible focus
focus on
- fusion (free we have tutorials), solidworks(issue tracker request), xdesign (issue tracker request), Onshape(free)
- freecad

2d tool
  - gimp / inkscape

3d including a parametric package
  - freecad, fusion for workflow, solidworks / xdesign? for industrial strength

next week we are going to make a construction kit so it needs to be parametric. This week learn one that can propagate parameters.
- look at game engines and sim environments
set up video
- take a potential project and represent in many different ways that you can  - render, 2d, 3d, represent in as many different ways as possible digitally.
get these programmes installed so you are ready for  the rest of the course.
- dont spend all week learning one tool do a little of all.
- document as you go on.
- try to make a 3d model for your project for local evaluation

next week we will share what we did and what we found (2d, 3d simulations, video etc) no recitation next week.

Vector v ratser
http://fab.academany.org/2019/labs/barcelona/local/wa/week2extra/vector-raster/

screens are rasterising everything as pixels

resolution and ppi
more pixels more definition
web is about **72 ppi**
printer at home 100 ppi
only for huge enlargements do you use 600ppi - requires a hasselblad

nits and uz?


srgb is what your data is stored as on the internet - so images on atom?

Printing should be done in RGB - rgb has more colours - best screens represent 80% of percievable colours.. most laptops are 40% - YOu need a screen!

jpgs LOOSE DATA! every time you copy them 15mb
ong cannot stor cmyw
tiff allow layered images - good for cameras
psd - photoshop file - highly editable
raw-nef - raw data - like a film but digital contains colour grading data - really big
the tech pf post processing determines how good an iphone is - compared to oter phones 150 mb ten times as large

raster
http://drawsvg.org/
https://pixlr.com/
https://vectr.com/


photoshop Raster tips
dark interfaces can misconfigure your eyes.. work in a light interface to edit colors.


augment your ram memory with any graphics heavy stuff.

increase history of raster image

srgb for online online
jpg 8bit
16 bit only most professional cameras
phones 3-- ppi from phones


### 3d programs

many are vectors in 3d space making surfaces
(rhino and fusion)

- nurbs - outside surfaces - we dont know what happens inside
- meshes - pointclouds -triangulation of dots - big files - we dont know what happens ninsdie. blender
- voxel representation - 3D pixel - represented with cubes - its a location of the centre of a cube - lighter files - eg that metamaterial interface - you can maipulate the inside  eg Monolith  - envelopes
MagicaVoxel more fun version. you can do 3d imaging witgh this.
video - to 3d model.


WOW!

tinker cad is paint of 3d
https://www.tinkercad.com/dashboard
voxels  - looks like its for kids but its not
you can program your own


Shapeshifter.io also on autodesk

onshope - enough for the things that we need to do - assembling parts etc.
https://www.onshape.com/
onshape has VERSION CONTROL



- pictorial


Fusion 360
metal and acrylic we can use in the lab - oscar did this

generative design is coming to fusion
generates solutions to connect pieces and

like in that exhobition - overkill maybe?


patch environment lets you sculpt

'Create' workspace is in 3D.
'Manufacture' is the CAM environment - intuitative
Open systems help faster design processes with more user testing
'Simulation' Environment



ways to use 3d
- sketchung - Blender lets you select surfaces to extrude like sketchup.

-

## FUSION demo

sketch first
add restraints
parametricise later

bodies are just shapes
Escape to stop
bottom left hand corner contains history that you can go back to and change
a panel appears whenever you select from the left hand list of bodies
modify  and sigma sign to get parameters

bodies make components and components make assemblies

sketch palette has construction lines too
dont use centre button for gimball just normal click


 parametrics can iterate quickly

 rhino is freer but fusion can code in relationships with greater flexibility. This is important in fabrication because materials and tools change parameters. it lets you build a shape using different tools and materials  - which is key in design.


 file types between rhino and fusion
 .dxf - 3d shapes
 .obj (solid plus skin) and .stl is a mesh
 STEP SVG STL OBJ files for cad machines records relations


In fusion you can show assembled models from parts by linking parameters

## Grasshopper and gcode?
like a GPS coding for a machine
with grasshoper we can translate gcode equations of shapes into movements of the machine

can create graphs

visualisation tool!

http://fab.academany.org/2019/labs/barcelona/local/wa/week2extra/param/

http://fab.academany.org/2019/labs/barcelona/local/wa/week2extra/rhino-grass/#simple-geometry-example-script

Machine learning plugins to learn how materials behave to model / simulate

REPETITION ITERATION



Plugins
food4rhino
https://www.food4rhino.com/
Voronoid shapes - plugins - phyllomachine


type grasshoper into command line to open


never press enter n a panel

param menu
line


save document one because grasshopper does backups often

doubleclick for commandbox

panel componeent for tracking data
Connecting points in rhino to grasshopper:
add component
rightclick set as..
then draw in rhino

if you work on grasshopper without setting in rhino the process is happening IN GH not rhino - you wont see it in rhino

button component allows setting to on or off

toggle - like a button

grasshopper repeats the last set point if you doint set them
press shift to connect multiple points to one input

series component
step=size between numbers
c=count

divide curve component

loft - surface between two curves

curve curve>


set
list panel componets

dispatch separates a list based on conditions

to deconstruct a shape into faces


you can link indexes to surfaces and points and use a slider to select through them.

evaluate surface - gives normal as a vector
md slider

reparametrise -
degrees - translates angles component in radians to degrees

geometry component


black parametric components just host information
e.g geometry

maths
graph mapper
graphtypes
can connect to series plitter

series component to graph mapper
graph type can be changed to cos sin etc.

polygon (input)
explode
evaluate curves
vector components

## Blender

free and built by a community

**viral licence** - if you change it you have to opensource it

focus on cinema!

good render engine
rendering is about light interactions
creating images based on information defining geometry.
complex calculations on how light refracts and reflects through lenses and against materials.
- inputs can be:
  - nurbs
  - meshes
  - pointclouds

- you can work almost without a mouse based on static geometries.
- can sculpt as if with your hands - using pen with pressure sensor (wacom). left hand on keyboard

- Solution tracking for vfx changes ontop of a shot video. tracks a moving image. follows patterns in pixels. it tracks the movement of pixels

node system - like a flow diag - linking layers of moving things

Rigging - creating skeletns for movements - eg robot axes of movements.
some video editing

Views
views at top
- compositing is the nodes on
- animation
- motion tracking
- scripting for python and a text editor
- uv - texture editing
- you can do your own and save by adding a view at the top dropdown

macro menu is on the left with tabs
main changes done on menu on right click plus sign to open
how you see your scene is on the bottom left of the editing space
also MODES are here
timeline at the bottom
Change are recorded using key frames here

top right shows whats visible

you can control your objects - like the layer managed in photoshop


left hand side
above the timeline there is a cube dropdown which contains:
- graph editor
- dope sheet
- add - addons  - includes things like bolts

community learning and tools
