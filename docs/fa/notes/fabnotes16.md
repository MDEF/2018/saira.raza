

# <center>Fab Academy Week 15 Interfaces
 </center>

<center>![](assets/)</center>


http://academy.cba.mit.edu/classes/interface_application_programming/index.html

## languages
   hello world
   C, GCC, C++, GDB, DDD
   .NET, C#, Mono
   Java, OpenJDK, IcedTea, Android
   APL, Haskell, Scala
   Processing, Wiring, Arduino, p5.js
   LabVIEW, Simulink, Max, Pd, Scratch, App Inventor, Grasshopper, Firefly

   LabVIEW, Simulink, Max, Pd, Scratch, App Inventor, Grasshopper, Firefly - you make  picture first and it generates script?


   ### Bash, Tcl - lets you write in the terminal

### Recommend Python
descendent of list, c, synthesises many languages and very well documented pearl and ruby  but it has performance issues
   Python tutorial, Perl, Ruby
   python is interpretted


### javascript started as a now obsolete web browser grown into a better language now  . java does it in real time  - compiles as it executes - matches time of underlying C code   javascript is a compilers. all you need is a web brower. so
LEARN javascript.
   JavaScript tutorial, Node.js, V8,
   npm, asm.js,
   a new thing WebAssembly,let you use other languages
    CoffeeScript is compatible with javascript.

runs in a browser based on V8 engine  -
   Node-RED, mods

mods is a wrapper around javascript.

mozilla runs MDN  has really good docs and tutorials



## device interfaces

### python and node into serial devices ...serial interfaces

browsers cant talk to serial yet
   RS/EIA/TIA 232/422/485, pySerial, serialport,
   going from browser to serial may change serial

### lower level bit programming:
   FTDI, libFTDI, python-ftdi, ftdi

### how you exchange information   
   Firmata

### usb interfaces  
  speak to usb USB, PyUSB, usb

### python to infrred
   IrDA, python-irda

### talking to instruments that speak visa?
   GPIB, VISA, PyVISA

### protocols coordinating devices
   MQTT, clients and servers where you offere serve data and they can subscribe and publish to eachother wel supported in 8266 or esp 32s needs IP connectivity though

   XMPP, does  that too

   IFTTT, standard way to build workflows

   UPnP, how devices discover their capabilities?

   Wemo belkin product line like smart appliances does upnp - goes to amazon alexa ..

### lower level network programming.   
socket, python to the internet
dgram net,
ws websockets open a browser and let you exchange data between client and server lets you
open web socket to a server puts up a graphical element and

sets up a websocket on one side on the other side it works on the serial side//
lots of chat in the lesson about this!


      hello.mag.45.html hello.mag.45.js video mods
mods can link a server in node to serial devices you can put  a bar graph in mods too!

use mods for your project



## data interfaces

if you have less than 1gb you dont need a database.. if data exceed computer memory you can use openoffice andgoogle docs.
python versions exist.
sql opensource database handle much more data than you fit on your commputer
the virtues of spreadsheets!


   flat files

   Calc, Sheets

   Pyspread, Pandas

   MySQL, MongoDB



## user interfaces


ncurses - powerful puts characters on your screen  - just needs text terminal curses good for low re disply! yes!

youtube  - vid running in terminal - whole game done in curses



### Tk,http://www.tcl.tk/ Tkinter
standard for building interfaces in python using tk for graphics
hello.load.45.py
import serial module and serial port
put up a window on the screen
set up graphics
idle
python reads data and updates display and calls idle routine
tk to do graphics for python



### Wx
is similar to tk drawing elements sliders, check boxes - aims to look native to the application .
   wxWidgets, wxPython
      panel_png_path.py

cad.ty predecessor to mods was built with wx

### others
widget libraries
   Qt, PyQt nokia

   GTK, PyGTK - linux

   Clutter, PyClutter

   Kivy

### UI for javascript
   forms
mods uses forms for UI
javascript calls to html standards to forms
lowest commoon denominator any browser can use it
looks like traditional web




### javascript UI tools
   jQuery sits on top of javascript not html script and widgets ,
   dat.GUI another javascript set,
   Bootstrap also javascript,
   Flat UI do not look like tradition websites
   pulls in libraries



### mutliple libraries frameworks from javascript modules
   Backbone, Require, Angular, Handlebars, Ember

### start in javascript build an app for desktop , mobiles - one javascript thing executable in different environmetns inclusing natve

   Meteor, Babel, React, React Native, Cordova, Ionic, Electron, Blynk



## graphics

###   X Windows
ancient stardard base of linux . you can write to dirctly  - calling x windows from c

      xline.c, video, ximage.c, video
         $ gcc xline.c -o xline -lm -lX11

### graphics libraries in javascript ysing canvas standard


  #### AWT, JFC, Swing
      JavaImage.java, JavaImage.html, video
####   Canvas
   turning on pixels on the screen good for image data
         JavaLine.java, JavaLine.html, video

     canvasline.html, canvasimage.html
####   SVG
   lines and dots
      svgline.html
  points keep identity once theuy are on the screen  - better for movelement


####   WebGL
standard for graphics on the web
messy code
webgl is a web version of open gl  - standard built into browser
      webglline.html

whole rendering pipellines describedd here

versio nof opengl from pythn you can call directly

doesnt run oini browser

the c prigramm calls directly into gl.


#### Three.js
   3d graphics runing in webbowser. non standard needs library - you have to put a camera in sceene , materila propoerties, lights m rendering , updtae geometry and ask to animate very owerful cad environment s!

      threejsline.html, threejsimage.html, threejssurf.html


####   OpenGL, GLUT, PyOpenGL
      glimage.c, video, glsurf.c, video
         $ gcc glsurf.c -o glsurf -lm -lGL -lGLU -lglut
      glimage.py, glsurf.py



####   RenderMan,- pixar  cgkit  - renderman ish


####   VTK,very powerful for medical data flow simulation complex data visualisations nic epython biinding to this
    pyvtk,
    Mayavi



####   OpenVDB
   good for complex scenes

   Unity, Unreal
   game engines free versions really powerful 3d engines

## multimedia - audio


  ### SDL, Pygame - python wraps round sdl

  ###   openFrameworks, ofpython media framework  - python binding


  ### HTML5 - new multimedia standards
      Web Audio
         audioline.html plays what you see  - in javascript ascing for an audio deveice bufering and playing out fom browser

      WebRTC - sstandard for video over the web
         video.html video - asks fo a camera  - gets video in and do processing on it in javascript asked for a camera askefdd for frames and then little loops that loop over the data.



## VR/AR/MR/XR
using headsets  - rapidly evolving
   ### WebVR THREE.VRController - vr on the web three js has an interface to vr controller s so learn three js to do 3d programming!

   ARCore - out of google

   autodesk..
   Forge

   three.ar.js three.xr.js

   AR.js

   A-Frame

## mathS


### Netlib, BLAS are underlying routines used to do maths, LINPACK, LAPACK

### MATLAB very popular for analysing a data born as a scripting language , Octave is an opensource verssion

### Libraries in python
NumPy extension of python to add maths ,
   SciPy building on nump to add libraries on top

   matplotlib graphing library in python really simple coding you can do it in realtime, Seaborn

      e.g line.py, video, lines.py, image.py, video, surface.py

### python notesbooks
in broswer make a notebook to run it

   Anaconda, IPython, Jupyter
      plot.ipynb plot.html

### cross platform     
   R lots of stats tools, RPy, ggplot2

   Julia

   Mathematica - symbolic math , SymPy uses python ,

   Sage, Scilab (math with formula as well as numbers )

### using javascript

   libraries for javascript

   Math.js, Science.js, numbers.js

   Plotly nice ploting library javascript interfaace  import and sits over java script Python JavaScript

  ### Data visualtion live wensites

   D3 - visualisation - really expressive
   jqPlot, Highcharts,
    Chart.js, mpld3

### machine learing
packages for machine learning defines a machine learning frameworks to train data to make predictions
all desk top.. lots to know..
hard to evaluate  might not work well on something its not seen - needto test it

    usually pythin
    tensorflow js is javascript


   Theano, python on numbpi

   PyTorch, Keras, TensorFlow,

   TensorFlow.js

   classes on this - READ
   signal processing,
   mathematical modeling - book by neil

  VM ware - virtual machines

## performance

   pi.py, numpi.py - a
      Cython, Numba


   pi.html
      JIT, typed arrays, web workers, file readers
for parralel operations
for speeding up java script



### for c
   pi.c, pim.c
      $ gcc pi.c -o pi -lm -O3 -ffast-math

   mpipi.c - an extension to c - do multiple things at once from c  - for supercpomputers for trains aof pes per secon

      MPICH Open MPI MVAPICH
      $ mpicc mpipi.c -o mpipi -O3 -ffast-math
      $ mpirun -np 4 ./mpipi


### for using GPU
gpu s can do maths very efficient  - these extensions let you spedd up maths on the GPU
webGL graphics calls the GPU to do the maths f

   cudapi.cu
      CUDA, PyCUDA
      OpenCL, PyOpenCL
      GPUMath.js

## deploy - where do you put your data?

   Amazon AWS EC2 Lambda - run a programme without a server
   Google Cloud
   Microsoft Azure
   DigitalOcean - towards linux

   Linode
   Heroku - platform platform for serving aplpications  - virtual computer?

   Docker - open - container with apps that then run on their server

    Kubernetes
     Auto
     DevOps - how you manage development - like gitlab



## assignment

   individual assignment:
      ### write an application that interfaces with an input &/or output device and user!
      that you made

      can run on anything - desktop , cloud, a rasberry pi
learn how to make graphics , multimedia vr, maths performance , deploy, build something between user and devices - interafaces for your machine? your final project? or just experiment? take sensor data and do mad stuff witth that.


   group assignment:

      #### compare as many tool options as possible

      make a chart?

### suggestions

  Processing is the easiest lots of serial examples
  python ! fo web
  grasshopper qiwith firefly


  BXavi tomorrow witll make it easier
   simplify with a button  - dont even need arduino?
   grasshopper
