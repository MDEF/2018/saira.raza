

# <center>Fab Academy Week 8 Embedded Programming
 /center>

<center>![](assets/)</center>

http://academy.cba.mit.edu/classes/embedded_programming/index.html


octopart shows part availability of parts
digikey

## AVRs
ATtiny10 - tiny size of rice
  ATtiny45V
  ATtiny44A
  ATtiny814
  ATmega328P
  ATmega16U2 - arduino?
  ATxmega16E5
  ATxmega16A4U

data sheets http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf

  look for the register?
  electric characteristics

clocks
resistor and capacitors can be uused as clocks
ceramic resonator
quartz crystal
 - slower clock speed saves power - faster gets more done

 data sheets
 you can overclock these and make processes run faster

 ## in system programming
 header programming
 put pads on withut soldering

 long pcb is a prgrammers to load programmes into smaller chips


 ## languages
 ### HEX codes

 instructions on data sheet status register
 16 bit operating code

 ### c

 GCC - opensource compiler
 avc is a compiler
 dont use win avr

 download tool chain

 rs232 - simplest way to talk to a computer
  this weeks board uses this
  host communication



steps neil used
http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.program.png


http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c

interrupt command
least amount possible in interrupt routine

assembly code written like c code


arduino is actually c - under the hood is the C compiler -

we are making our own arduino

change something in neils c code


input wek
output week
pcb week

you could save time by making something you only need a shield for.



Documented what you learned from reading a microcontroller datasheet.
What questions do you have? What would you like to learn more about?
Programmed your board.
Described the programming process/es you used.
Included your code.

individual assignment:
      read a microcontroller data sheet
      program your board to do something,
         with as many different programming languages
         and programming environments as possible
   group assignment:
      compare the performance and development workflows
         for other architectures

## Workshop

## Bits

bit masking is lazy bit code


## Libraries


adding libraries uses up memory into the chip better to call them from bagger chips


 we have 20megahertz microcontroller so it does this many cycles per second?


the order of complexity is
- assembly most basic langage
then  -  c next level - but still close to arranging bits onto the chip -
then python - fast learning curve - less microcontrollers that work with python
then -  arduino - most of us will use this - but arduino uses libraries? you have to install a lot of things onto your chip.
then


it takes memory to interpret what the port is doing


| tool from  | tool to | file type | Purpose|  ||
| ------------- |-------------| -----| -----| -----| -----|
|  |  |  |  | |
| R1 |  10k Pull-up Resistor |  1002 | Ensures zero is really zeroed out for the off state going into the MCU | VCC | Rest of board |
| C1 |1uF capacitor| | between VCC and ground in parallel with the resonator.  Capacitors are often used as filter devices to remove voltage or signal spikes in electronic circuits | -----| -----|
