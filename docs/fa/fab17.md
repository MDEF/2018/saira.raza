

# <center>Fab Academy Week 17 wildcard </center>


<center>![]()</center>
<center>![]()</center>

## Welding

This week Mikel taught us how to do MIG welding. This uses CO2 and argon and a filament with electricity running through it.

### Safety

<center>![](assets/helmets.jpg)</center>


There were many safety precautions we had to take.

1. wear flame proof jacket
2. wear a light protective helmet
3. wear leather burn-proof gloves
4. make sure everyone has their helmet on when you are about to weld by calling out and getting a response.


### Welding Operation
1. I check the filament - it should be about 1cm out of the nozzle. We cut any balls of melted metal on it with clippers.
2. switch on the gas and power
2. To push the filament out I press the trigger momentarily.
3. clamp the two pieces of metal in place
4. clip the ground of the welding machine to the clamp to create a circuit - this will not electrocute the entire table
5. make sure everyone has their helmet on when you are about to weld
6. Put the tip of the filament on the metal and push the trigger
7. keep the filament touching the metal and make small circles that and try to stay in that circle until it goes red.


#### PROBLEM!: I could not see the end of the tip of the filament so I lost contact and caused this patchy area

TIP: hold the gun to the side so you can see past the nozzle

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pk7ZBG48FyU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

6. Unclamp and move the piece and repeat  if you can't access the whole joint.

<center>![](assets/halfweld.jpg)</center>

### Hero shot

Not too heroic this time. I started off well but the filament kept jumping when I couldn't see it. It's quite a light touch and the gun is heavy so you need to remember.


<center>![](assets/fullweld.jpg)</center>

## Grinding


### Safety

### Operation









<!---   
someything we havent covered

document the ssignment

include everything needed fir sieone else to produce it  comp

something you havent done in another week
assignment
   Design and produce something with a digital fabrication process
   (incorporating computer-aided design and manufacturing) not covered
   in another assignment, documenting the requirements that your
   assignment meets, and including everything necessary to reproduce
   it. Possibilities include (but are not limited to) composites, textiles,
   biotechnology, robotics, folding, and cooking.

   functional structures - to programme in fabric

   extending existing machines

sam kalish - 3d cutting to make cuts in 2d to make a 3d form
folding
erik demaine . origami. curve crease

digital fabrcation and gastronomy
maybe do the lignlsing again
cmposite materials

- have you made a big 3d mould?
- have you used fibre composite on it?
- select and apply suitable materials and processes to - - - create a composite part
- does it look awesome?
- Described your problems and how you fixed
- learn about msds and safety concerns
- include your design files and photos

  -->

  <!---     --><!---     --><!---     --><!---     -->
<!---   -->

##### Resources



## LASER INDUCED GRAPHENE

Earlier this year I was reading about Laser Induced graphene which researcher in... have been experimenting with.

The author of this paper which was able to pin down some parameters that we could safely test in a CO2 laser available in Fab Labs.

 Ruquan Ye, the author of [this paper on using making laser induced graphene on organic substances like bread, potatoes, coconuts and pine](https://pubs.acs.org/doi/abs/10.1021/acsnano.7b08539) was kind enough to share it with us for free (thanks Ruquan!).

The theory that Ruquan and his colleagues found was that by using the raster laser function on a CO2 laser at low power and an out of focus laser enables a two stage transformation of carbon based materials that are high in lignin.

In February Mikel our Laser expert did some tests with me on a potato - which did not look like it was supposed to  - not dark enough.

So we have prepare now a piece of pure pine would which should work. The reference paper recommends a pre-rastered area before a second raster. The result should be a blackened area with a slightly lighter looking image on top (which should be the graphene)

Here is a file I prepared for this experiment with two layers. The initi

<center>![](assets/rasterpine.png)</center>

Obviously there are safety issues.
