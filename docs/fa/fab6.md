

<!---<h1 style="color: #f2ab8a;"</h1>>-->

# <center>Fab Academy Week 6</center>
# <center> Electronics design</center>


<center>![](assets/)</center>


##### **Resources:**

* http://academy.cba.mit.edu/classes/electronics_design/index.html
* [Fab Lab Barcelona's electronics club material](http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/electronics2/)
* [Fab Academy tutorial http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html

<!---

individual assignment:
redraw the echo hello-world board,
add (at least) a button and LED (with current-limiting resistor)Resistor calculator
check the design rules, make it, and test it
extra credit: measure its operation
extra extra credit: simulate its operation

 Group assignment: explain any of the test equipment of the Lab, for example the multimeter.
Did you try Kicad ? Add links to Eagle and Kicad.
Explain the workflow after design your pcb in Eagle with words and screenshots: .png (traces, outlines), milling and soldering.
Add a screenshot of your pcb.
Could you add some screenshots of the routing in Kicad ?
Could you add some screenshots of the milling process you used with the Modela MDX-20 ?
Did you test it ? Explain how you did it.
Upload your files and add a link to them.
Missing Eagle files and png files.
Even if you have explained in the electronic production week it would be nice to remember how you generated the cam files for the SRM-20. Also, include those files in your archive.
No problems soldering? No comments on that part?
Explain how did you calculate the resistor for the LED.
I really like the conclusion part! Nice.
Did you test it ? Explain how you did it.
Upload your files and add a link to them.
Explain basic workflow of Eagle.
Explained how you worked with design rules for milling (DRC in EagleCad and KiCad)
16mil is not only to be sure, it is also because it corresponds to the tool diameter 1/16 and thus we guarantee that we will be able to pass everywhere when we do the traces
I love the Frankenstein of your first Hello board intent! congrats
The download link seems to be broken, or you moved the files to another side.
Explain how did you add the fab library in eagle   -->


## Redrawing the echo hello-world board adding a button and an LED

The original hello-world ftdi board is shown below.

![hello world board](assets/hello.ftdi.44.png)

*http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.components.jpg*

The components parts are shown in the following photograph

![hello world board](assets/helloworldftdicomps.PNG)

I am not entirely sure what the hello-world board was meant to do when I started this task but making it has helped me unpick some key principles in designing an electronic board and in how programming fits in to the physical board.

#### Components

First of all I needed to identify the components I needed.

check this
http://archive.fabacademy.org/2018/labs/fablabdassault/students/saba-ghole/exercise06.html

It took 2 iterations of board designs for me to compile the complete list.

| Label on original Hello World board  | Component name| Part number | Purpose| Inputs | Outputs |
| ------------- |-------------| -----| -----| -----| -----|
| IC1 | [Microcontroller](https://www.digikey.com/product-detail/en/ATTINY44A-SSU/ATTINY44A-SSU-ND) | ATtiny 44A |Can run small scripts via 12 pins | VCC, MISO, Rx, SCK, RST,  | GND, Resonator signal, MOSI, Tx |
| R1 |  10k Pull-up Resistor |  1002 | Ensures zero is really zeroed out for the off state going into the MCU | VCC | Rest of board |
| C1 |1uF capacitor| | between VCC and ground in parallel with the resonator.  Capacitors are often used as filter devices to remove voltage or signal spikes in electronic circuits | -----| -----|
| XTAL| 20MHz resonator |  | Synchronises the packaging of bits | | |
| J1 ISP | 6-pin programming header |  | for programming using 'serial peripheral interface' protocol from an ISP (In-system programmer)  | VCC, GND, MOSI, MISO, SCK, RST| |
| J2 FTDI  | FTDI header | none | a 6 pin connector for power and data - to be used with the FTDI patented microchip which works through USB - needs drivers to decode information coming in. | Rx from microprocessor, Tx from programmer | The VCC and GND to the whole board, Tx to microprocessor, Rx to programmer |


#### Lines between components

**Power - VCC and GND**

This powers the circuit where VCC has high potential (charge) and GND (ground) has theoretically zero potential - this difference of electrical potential causes a flow of electrons through the circuit.

**Serial Peripheral Interface (SPI)** is a synchronous serial data protocol used by microcontrollers for communicating with one or more peripheral devices quickly over short distances. It can also be used for communication between two microcontrollers. It uses one master device (usually a microcontroller) controlling peripheral devices. Typically there are three lines common to all the devices:

* **MISO (Master In Slave Out)** - The Slave line for sending data to the master,
* **MOSI (Master Out Slave In)** - The Master line for sending data to the peripherals,
* **SCK (Serial Clock)** - The clock pulses which synchronize data transmission generated by the master
We can use these pins to communicate with


**Tx and Rx**

These are data transmission and reception lines. When connecting devices, the transmission line connects to the reception line unlike with MOSI and MISO. A driver may be needed to decode the signal.


#### Calculating the current limiting resistor for LED

To find an optimal resistance for the current limiting resistor for the LED I looked at this datasheet for a typical 1260 green resistor:
https://cdn.sparkfun.com/datasheets/Components/LED/S150ANG4.pdf

* The source voltage from a laptop or Arduino is typically 5V
* [A 1206 LED has a typical forward voltage of: 3.2 V
* The preferred current through the LED : =20mA](https://cdn.sparkfun.com/datasheets/Components/LED/S150ANG4.pdf)

The calculator here http://led.linear1.org/1led.wiz used these values to recommended a 1/8W or greater 100 ohm resistor have chosen the next up which is 488 ohms

* 01 x 1206 LED (has polarity as is a diode)
* 01 x 499 ohms current limiting resistor
* 01 x Push Button (6mm SMD)

|Label on original Hello World board  | Component name| Part number | Purpose| Inputs | Outputs |
| ------------- |-------------| -----| -----| -----| -----|
| LED| LED | 1206 | |  VCC from Resistor | GND |
| R2 |  488 ohm Resistor | 4990  | stops the LED from burning out | VCC | GND |
| S1 | 6mm button| | breaks circuit so VCC does not flow to components | | | |



### Choosing pins on the microcontroller to connect the LED
To add  a button and LED (with a current-limiting resistor), I need to check what pins on the microcontroller are available and suitable for the signals required.

On the schematic pins  PB2 and PA7 on the microcontroller are free.
These are both capable of performing the PWM technique to generate analogue output signals using digital signals.

![attiny44a pins](assets/ATTiny44apins.PNG)

| Pins available on Hello World Board Schematic | ATTiny44a pin number| Capability of pin  |
| ------------- |-------------| -----|
| PB2      | Pin 5 | Analogue input, Analogue output from digital inputs |
| PA7 | Pin 6 | Analogue input, Analogue output from digital inputs |
| PA2 | Pin 11 | Analogue input |
| PA3 | Pin 10 | Analogue input |


<!---![attiny44a and arduino pins](assets/ATTiny44arduino.png)

PB2 = Pin 5 on  ATTiny44a = Pin 8 on Arduino = Analogue input, Analogue output from digital inputs

PA7 = Pin 6 on ATTiny44a = Pin 7 on Arduino = Analogue input, Analogue output from digital inputs
PA2 = Pin 11 on ATTiny44a = Pin 2 on Arduino  = Analogue input
PA3 = Pin 10 on ATTiny44a = Pin 3 on Arduino  = Analogue input    -->

I chose Pin 6 for my LED
and Pin 10 for the button


### Drawing a new schematic on Eagle

Eagle has a [schematic (SCH) mode](https://learn.sparkfun.com/tutorials/using-eagle-schematic/all) that enables you to draw a schematic by picking from a library of components. When you create a new project this view is where you start.

Conveniently **Fab academy has a downloadable Eagle components library called **eagle_fab** which contains all the parts we will be working with**.

You can label which data or electrical line (VCC, GND, SCK, RST, MOSI and MISO) you want the pins of each component to go to or you can connect them by eye by drawing connections.

**For the LED**, we need a current-limiting resistor  in series before the  LED to protect it from burning out.

**For the button** we can include the pull up resistor before the button.
The VCC 5v voltage from the FTDI header goes to to the microprocessor which runs a programme depending on what programme is inputted from MISO  out puts to the LED depending on what the  will go to the microprocessor

Below is the schematic
![Schematic of ATTiny44a with LED and Button](assets/LEDbuttonschem.PNG)

YOU CAN SEE MY EAGLE Schematic file [HERE](assets/w6 ATTINY LED Button Header.sch)

Clicking 'Board view' connects the components in a tangled way..

![](tangledboard.PNG)

You can click on components and use the move and rotate buttons to untangle crossed lines or you can use the "ratsnest" command which tried to do this for you but I enjoyed laying out the pieces to see how small I could make the board.

### Routing the connections

There is an autorouter button or a manual routing button which you can click to start drawing the shapes you want the connections to be.

I found that I got a bit carried away with trying to keep the board small at this stage because we were advised by our classmate Rutvij that **the width of the connections needs to be size 16**.

<!---
To change the width of the traces in the top menu select > File > switch to board
-->

You can also use 0 ohm resistors to allow one line to cross another.

Below is the rearranged board route 9after much readjusting.

![Finished routed circuit](assets/circuit.png)

YOU CAN SEE MY EAGLE Board file [HERE](assets/W6 board LED button.brd)

## Checking the design rules in Eagle

There is a convenient Design Rule Check (DRC) button in Eagle located in the Tools menu.

Clicking this shows any errors in the drawing that may lead to poor operation of the circuit including "airwires" where the circuit has not been joined properly.


## Making the circuit

### Producing the trace and outline in Eagle

For the trace (engrave)
* Click the Visible Layers button
* deselect all layers
* select Dimension and Top layers
* Go to File > Export > Image
* select monochrome
* choose folder to save trace in
* change resolution to 1000
 * click ok

For the board outline (full cut)

* Click the Visible Layers button
* deselect all layers
* select only Dimension layer
* Go to File > Export > Image
* select monochrome
* choose folder to save outline in
* change resolution to 1000
 * click ok

![Trace](assets/trace.jpg)

## Milling the board

### Converting .PNG files to . rml files in fabmodules

This time I converted my trace file to a .rml file using [fabmodules](http://fabmodules.org/) as it is much easier to set up for our Roland SRM 20 Milling machine because there are less steps that you can forget to do.




Fabmodules asks you what your input file is and what output file you want then asks you what process you want to run in the milling machine.

![](fabmodulesoptions.PNG)


For the trace:
* I selected Trace 1/64 for the trace
In the right hand panel that appeared:
* I selected the SRM20 as the machine
* checked that the speed was set to 4mm/s
* checked that the zjog (the distance the mill moves up before cuts) was set to 12mm
* clicked the Calculate button

Fabmodules then calculates the toolpath.

I repeated the process with the outline but selecting the 1/32 PCB outline process instead of the 1/64 trace process.

I saved the files onto the computer and proceded to prepare the board, mill and origins and cut the trace.

### Forgetting to change the endmill

I ran into trouble with cutting the outline of the board as I forgot to change the endmill. The result was an engraved outline. I considered changing the mill and going over the board again but I thought that it might not be good for the endmill or the board so I left the board uncut.


## Soldering the board - a new technique

My classmate Laura showed me a trick to make soldering these tiny components a bit easier - this was to solder half the pads on the board of each component without the component first. Then lay the component on top of the hardened soldered pads and heat the pins with the solder iron until they melt the solder and sit inside the liquid solder. Then there is a second or two to adjust the component so all pins are sitting on their pads and the solder solidifies and holds them in place. Then the remaining pins can be soldered by holding the solder iron against the pins and touching the solder wire to the pin as well.

I got much shiner smoother solders and didn't need to keep heating the pins and adjust the components as many times as I did before.

Here is the board...

<center>![](assets/LEDboard.jpg)</center>




### Vinyl cutting a circuit on CutStudio

By this time I had made 3 iterations of drawing a circuitboard so I decided to try cutting a circuit on a copper adhesive on a vinyl substrate and to vinyl cut this.

I tried to vinyl cut copper tape to place onto a PVC base.
CutStudio uses .jpgs to perform cuts.

In Eagle the trace file needs to be exported as a .jpg and scaled down.
This is done by checking the size in Eagle by going to Tools > statistics, finding the x and y dimensions and in Cutstudio **manually resizing** the image.

Then I loaded the vinyl cutter according to the fab lab instructions here
http://wiki.fablab.is/wiki/How_to_use_the_Roland_GX-24

http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week03_computer_controlled_cutting/rolandgx24.html

http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html


I cut the piece so it was small enough to fit in the maximum piece length.
I loaded the sheet under both feed wheels and selected the cutter to cut "piece". the cutter measure the piece and then immediately said "sheet unloaded"

It took a while to learn that the Roland machine requires pieces to be of a certain length and that rolls of material should be loaded from the back of the machine. With some help from more experienced people we performed some tests. 110 force and 5cm/s seemed to cut just about well enough to cut the tape and not life the tape off its backing.

I am a fan of labelling machines used by lots of people if it might save time in explaining or figuring basic functions out.


### Picking materials

I want to experiment with unusual conductive materials.
With one of the earlier iterations I tried [a recent method developed by Yieu Chyan et al from Rice University and University of the Negev of producing graphene on organic substrates using a CO2 laser cutter](https://pubs.acs.org/doi/10.1021/acsnano.7b08539)

![cork circuit](assets/corkcircuit.png)

Unfortunately this one trial did not work when I tested it with a multi-meter and I was unsure how I would attach the tiny components to the thin graphene layer without destroying it.
