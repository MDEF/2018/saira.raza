

# <center>Fab Academy Week 13 Networking</center>

<center>![](assets/cyandipole.gif)</center>

<!---  
<!--  -> <!--  ->
-->


<!--- assignment
MDEF in pairs: read a sensor value and send to your team made
breakout sensors Arduino boards and sensors - communicate between
try something other than wifi
   individual assignment:
  - design, build, and connect wired or wireless node(s)
      with network or bus addresses dont use a store bought board - design a board that interfaces t a radio board!
  Pick a processor interface with a network that has an address.

   group assignment:
      send a message between two projects    

in pairs  - one make a sensor -  other person gets an emitter  does..?

- implement and interpret networking protocols
Described your design and fabrication process of your OWN DESIGNED BOARD
explained the programming process you used and how the protocol you use works
Outlined problems and how you fixed them
Described your problems and how you fixed
included original design files (Eagle, KiCad, Inkscape - whatever)

      -->


##### **Resources:**

* https://www.maxbotix.com/documents/LV-MaxSonar-EZ_Datasheet.pdf
* http://academy.cba.mit.edu/classes/networking_communications/index.html
* [Fab Lab Barcelona turoisls](https://hackmd.io/s/B15x5dn9V)
* https://lastminuteengineers.com/nrf24l01-arduino-wireless-communication/
* https://www.deviceplus.com/how-tos/arduino-guide/nrf24l01-rf-module-tutorial/

This week was very relevant to the realisation of my project as we learned about how to modulate wireless signals which are essentially waves.

I found it really interesting to learn how we manipulate often massless fields of energy to communicate. Even when we talk to each other we do a similar thing I suppose but our trained brains pick out and interpret the signals. Its fascinating to me how we have learned to work with the geometry of these fields - through frequencies, wave-shapes and amplitudes to code and decode information.

The other thing that I loved learning about was the use of geometry to improve antenna signals by understanding the geometry of the fields  in the environment.

I look forward to adapting [the fractal patch antenna I made in week 2](fab3.md)

It has been useful to learn the terminology of this craft.


## Networking terminology

First of all a **"bus"** is a communication system that transfers data between components inside a computer, or between computers.

A **Serial bus**  is where each board takes turns to send messages and requires a master to be in charge of when the clients talk

A **node** is where the commands end up (slave) and input or an output of the network

A **Bridge** passes information through (two way)

http://academy.cba.mit.edu/classes/networking_communications/bus/bus.jpg

<!---
serial interfaces mtbi is doing rs232 - between client and host
rs422 and rs45 are multidrop -->

<!---
### Bridge board
### Nodes board
### Coding a bus in C
http://academy.cba.mit.edu/classes/networking_communications/bus/hello.bus.45.c

Names of what is Tx and Rx are always done from the host side

IVC are the connectors to boards - a ribbon feeds through these

Each board is getting power from tx and rx.
For power on receiver each pin (on the processor)can be low high or tri state

### I2c also TWI (two wire interface) bus

bus used for connecting between processors and devices
this is synchronous rather than asynchronous communication.
One of the pins is a clock pin and the other is a data - the clock tells you when to read the data.
 i2c commonly used between processors and proc and smart devices

universal interface.
protocol is so simple you can do it in software - library exists on arduibno

most of the data sheet is about programming i2d - in the accelerometer data sheet frominput week..
bit banging - in i2c? what?

lots of different devices speak i2 c and is very easy protocol to implement

## SPI
in system programming
about as common as i2c
generally supported by what you want to talk to

memory cards speak spi
http://academy.cba.mit.edu/classes/networking_communications/SD/hello.uSD.44.jpg
with a dedicated protocal

slower but easier to implement
its easy to implement for your proj

more data than you can fit in your processor - you can store data for environmental sensing
mosi and miso
3 pins used for spi interface
example is
you can have multiplpe devices connected to spi ( check what he says about pins in the videos.
  micro code
  serial comms on the fti side
  spi  )
http://academy.cba.mit.edu/classes/networking_communications/SD/hello.uSD.44.read.c

alice in wonderland on the card
 http://academy.cba.mit.edu/classes/networking_communications/SD/hello.uSD.44.read.txt
 the input
 th output
 http://academy.cba.mit.edu/classes/networking_communications/SD/hello.uSD.44.read.c
 count<80 changes clock speed
 put- flash string pstr sd command
 loop to initialise the card
 until card is ready to talk to yiu

 file system
 https://en.opensuse.org/SDB:Basics_of_partitions,_filesystems,_mount_points
 find how it is partitioned
partitions - ike a shelf
directory - where and when to read the file  - all over the drive - you have to read a chucnk of data to read the next
sectors
 block
 read the first partition table
 can
 for industrial contro;

 ## usb
 in mega family you can get versions with usb
 one xtra module calle dhardware usb
AVR has its library for using these

HARDWARE USB
higher through put more data
LUFA is nicer for usb devices - AUDIO
http://www.fourwalledcubicle.com/LUFA.php

run this library so you device can appear as a mouse, keyboard or whaterve

you can make your device lok like a device it already knoe=ws how to talk
bitbangs

 SOFTWARE USB
implements usb in siftware
https://www.obdev.at/products/vusb/index.html
vsb - is usb1 at lowest datarate -
isb is very demanding for the timing. rc timing on processor is not close enough
vusb listens to the data rathe the host is talking at it adjusts its clock to the host
slow data rate

## OSI layers
osi is the standards body
network how you direct messages
transport when to start and stop
presentation

## Physical media
capacity )of data)
you can send more data if you have more frequesncies power
and LESS NOISE

### wired networks
## Wireless communication
radio
ism
shows you who has what part of the frequency spectrum
government control
within there are ism bands
industrial scientific mediaical - historically not designed for comms
2.4ghz band was created for microwave oven protection
802 - wifi
802.15 - another protocol
blu tooth is in 2,4 ghz
light infrared  
blink lights quickly - lif - fi lights in a room
sound sending data through that
sonar - modulate that
## how to put message in  - modulation
PCm - measuring amploitude
ppm - measure timing of pules
ook - turn signal on and off
fsk - 2 frequencies for low or high
bsk - change the phase of gheh signal - time reference ot measurethe phase
qam - frequency amplitude and phse
fdm - divide frequencies
spread - make it look randome spread over the who signal
baas above with band

## channel sharing
fdma - listen to frequencies
csma - when channel is available - you check if there is a colleision and back off if someone is in the queue - this is whow the internet works
cdma - the codes tell people apart like phones
mimo - multiple int=put - spearing beams at people

## errors
for detetcion and correction
##  Internet protocols
 requests for comments
 https://www.ietf.org/rfc/rfc2324.txt
ipv4 for the internet
 https://www.ietf.org/rfc/rfc0791.txt
 ipv6 for internet
 yon need to make an IP packet to talk on the internet
  DNS - how
IP address is like an address on an envelope
recipient
ports   UDP(short and real time performace is good), TCP(big messages)
     HTTP sits on top of tcp
  full stack:
     RFCs
     IPv4, IPv6
     DNS DHCP NAT private
     UDP, TCP
     HTTP
     BGP AODV ROLL
     socket
  wireshark
  handy for debugging and spyying
  serial ports can support internet
video turning light on or off based o address

## RADIO
  handbook
  oscillator
  mixer to modulate
  power amp to send out
  low noise to recieve
  frequency stage to demodulation

  demodulated signal
  filter to present
  antenna
  https://www.wiley.com/en-us/Antenna+Theory%3A+Analysis+and+Design%2C+4th+Edition-p-9781118642061
  impedance matching - match to the impedance of space - send in every direction sends more power in one direction
  explosion of chips that contain everything he's describing
  single chip radios
  nordic datasheeet

    PCm - measuring amploitude
    ppm - measure timing of pules
    ook - turn signal on and off
    fsk - 2 frequencies for low or high
    bsk - change the phase of gheh signal - time reference ot measurethe phase
    qam - frequency amplitude and phse
    fdm - divide frequencies
    spread - make it look randome spread over the who signal
    baas above with band

   -->
## Networking protocols


### Serial and Parallel communication

Serial transmission is good for long distance communications, whereas parallel is designed for short distances or when very high transmission rates are required.
https://learn.sparkfun.com/tutorials/serial-communication



### Synchronous (SPI) and Asynchronous (TX/RX) protocols


The below information is distilled from [this Sparkfun page](https://learn.sparkfun.com/tutorials/serial-communication)


#### Asynchronous

* data is transferred without support from an external clock signal.
* perfect for minimizing the required wires and I/O pins,
* extra effort needed for  reliably transferring and receiving data.     ... * * The asynchronous serial protocol has built-in rulesthat help ensure robust and error-free data transfers:
 - Data bits,
- Synchronization bits,
- Parity bits,
- and Baud rate.

#### Synchronous serial interface:

*  always pairs its data line(s) with a clock signal, so all devices on a synchronous serial bus share a common clock.
*  more straightforward, often faster serial transfer,
* also requires at least one extra wire between communicating devices.
* Examples of synchronous interfaces include SPI, and I2C.


 Asynchronous serial connections add extra start and stop bits to each byte help the receiver sync up to data as it arrives. has a lot of overhead in both the extra start and stop bits sent with every byte, and the complex hardware required

 SPI is a "synchronous" data bus, it uses separate lines for data and a "clock" that keeps both sides in perfect sync. The clock is an oscillating signal that tells the receiver exactly when to sample the bits on the data line. This could be the rising (low to high) or falling (high to low) edge of the clock signal; it's a simpler (and cheaper!) piece of hardware than a UART (Universal Asynchronous Receiver / Transmitter) that asynchronous serial requires. In SPI, only one side generates the clock signal (usually called CLK or SCK for Serial ClocK).

### Wired and Wireless

The benefits of wired and wireless communication is quite obvious but the mobility needed of the microprocessor and the distances needed for communication as well as cost will play a part.

#### Frequencies of wireless communication

 <!-- <center>![](assets/emspectrum.gif)</center>  -->

<center>![](assets/EMspec.GIF)</center>

I was unable to find a tool to help to compare these different protocols to help match protocols to applications so I tried to compile one here.


| Serial /Parallel |  Set up | Protocol name | Synchronous / asynchronous protocol |  Frequency of transmission line | Communication capabilities | Arduino Libraries | Libraries for Python |
|---|  ----------- |-------------| -----| ------| -----------|--- |--- |
| |      |  | | | | ||
| Serial | Wired only two wires, apart from a common ground | RX/TX   | Asynchronous  | | |||
| serial |  wired    | Serial Peripheral Interface (SPI)   | synchronous |  | short-distance communication, primarily in embedded systems. It uses a master-slave architecture with a single master. The master device originates the frame for reading and writing. Multiple slave-devices are supported through selection with individual slave select (SS) lines.|  SPI , device libraries from Adafruit, Sparkfun, Arduino | |
| SERIAL | Wired | I2c also TWI (two wire interface) bus | synchronous | | only meant for short distance communications within a single device.  | Wire, TinyWireS | sm.BUS |
| Serial  | Wired UART hardware or bitbanged non-UART chips (using pins to send the data as voltage levels very quickly) |  UART | Asynchronous | | | Serial and SoftwareSerialfor bitbanging  | pyserial |
| Differential pair circuit  | Wired  The data lines require 3.3V  so need to limit the voltage coming from many USB ports (computers, chargers, etc) Using an LDO in the Vcc or  zener diodes in the D+, D- |  V-USB   | Asynchronous | | V-USB will create an USB 1.1 compliant low-speed device. | | V-USB is a library  |
|  | Wireless ontrol of the module is done through the SPI bus, so it is easy to control it from a  Arduino.  | NRF24  |  | 2400 to 2525 MHz, being able to choose between 125 channels spaced at the rate of 1MHz. It is recommended to use the frequencies from 2501 to 2525 MHz to avoid interference with Wi-Fi networks.   | configurable between 250 Kbps, 1Mbps, and 2 Mbps and allows simultaneous connection with up to 6 devices.   | RF24 library |  |
| Serial |  Wireless  |  HC-12 is a half-duplex  module  AT Serial Commands   | Synchronous | 433.4-473.0 MHz range    | capable of transmitting up to 1 km when Paired with an external antenna and possibly slightly beyond 1 km in the open and are more than adequate for providing coverage throughout a typical house. 100 channels  . x 20 dBm (100 mW) transmitter paired with a receiver that has -117 dBm (2×10-15 W) sensitivity at 5000 bps  | Wire||
|  |  Wireless  radio device modulates and transmits signal. a digital controller, consisting of a CPU, a (DSP - Digital Signal Processor) called Link Controller (or Link Controller) and interfaces with the host device.  | HC08-Communication Bluetooth  using AT Commands. Link Controller  processes ARQ and FEC protocols of the physical layer | Link Controller handles both asynchronous and synchronous transfer functions.   | radio - Adaptive Frequency Hopping (AFH) detects interference and automatically blacklists channels with interference . | BlueTooth devices can act as Masters or as Slaves.  a BlueTooth Slave can only connect to a master and nobody else,  a BlueTooth master, can connect to several Slaves or allow them to connect and receive and request information from all of them, (up to a maximum of 7 Slaves).| FTDI link enables AT Ccommands and python in Arduino environment | |
|   |    |  | | | | | |


## Assignment

>in pairs get two devices to connect and respond to each other

I worked in a team with [Jess](https://mdef.gitlab.io/jessica.guy/fabacademy/networking_communications/), [Veronica](https://mdef.gitlab.io/veronica.tran/fab-academy/week-13/) and [Fifa](https://mdef.gitlab.io/fifa.jonsdottir/fabacademy/week-13/.

### Designing a communication channel

We had available to us:

* 2 x RF - NRF24L01 transceivers
* 2 x Arduinos  
* 2 x Arduino USB cables for power and programming
* 1 x basic sound sensor

We thought with these we would try to send readings from the microphone using the radio transceiver (as a transmitter) to another transceiver (as a receiver) and display the data in the serial monitor of a connected Arduino .

To do this we needed to make and code the following transfers of data:

1. A Sensor that sends data read by an Arduino
2. An Arduino that sends data to a transceiver (as a transmitter)
3. A RF communication line from the transceiver to another transceiver (as a receiver)
4. An Arduino that receives data from the second transceiver and can display this on its serial monitor


### A Sound sensor sending analogue voltage readings to an Ardunio

#### Circuit

I connected an hw484 sound sensor to an Arduino according to the diagram shown [here](http://osoyoo.com/2017/07/Arduino-lesson-sound-detection-sensor/).

<centre>![](assets/microphone.PNG)</centre>

[Here's more about the sensor](https://randomnerdtutorials.com/guide-for-microphone-sound-sensor-with-Arduino/).
It is a [MEM sensor](https://www.cui.com/blog/comparing-mems-and-electret-condenser-microphones)

The sensor has 4 labelled pins: VCC, GND, Digital output and Analogue output which can be used for different applications.
For my final project I want to be able to sense changes in amplitude in sound across a 2D plane so I looked for ways to capture this.

I spent some time looking for examples of analogue output code. I used [this sound level sensor code from Adafruit](https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels) which returns the maximum value per reading cycle.
Sound waves vary continuously so one way to package the information is to monitor the maximum or minimum voltage of the analogue signal coming from this sensor as the MEM sensor vibrates. To do this we have to set a time period from which the code can pick the maximum reading. The code uses 50ms. This is enough to capture the maximum sounds of any frequency above 1kHz.


Here is the code from Adafruit:

    const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
    unsigned int sample;

    void setup()
    {
       Serial.begin(9600);
    }


    void loop()
    {
       unsigned long startMillis= millis();  // Start of sample window
       unsigned int peakToPeak = 0;   // peak-to-peak level

       unsigned int signalMax = 0;
       unsigned int signalMin = 1024;

       // collect data for 50 mS
       while (millis() - startMillis < sampleWindow)
       {
          sample = analogRead(0);
          if (sample < 1024)  // toss out spurious readings
          {
             if (sample > signalMax)
             {
                signalMax = sample;  // save just the max levels
             }
             else if (sample < signalMin)
             {
                signalMin = sample;  // save just the min levels
             }
          }
       }
       peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
       double volts = (peakToPeak * 5.0) / 1024;  // convert to volts

       Serial.println(volts);
    }


#### PROBLEM!

The reading would not change from a low reading. I tried changing the cycle period to 100ms and it made no difference.

**SOLUTION**

I turned the screw on the sensor (which is a potentiometer) anti-clockwise to increase the voltage output so the Arduino could read the signal fluctuations. It's a small range of turning that produces a reading that is actually readable so its amazing when you found it.


I must note that a looked through a number of possible codes but most of them converted readings into digital outputs - probably because the window that the Arduino can actually sense analogue changes is so narrow.

<centre>![](assets/response.jpeg)</center>

<centre>![](assets/testingsensor.jpeg)</center>

### Sending a signal from an Ardunio to another Arduino through 2 NRF24L01 transceivers

https://lastminuteengineers.com/nrf24l01-Arduino-wireless-communication/

A Transceiver is not able to send and recieve information simultateously , it can recieve or transmitt but not do both because the tw circuits are not isolated from eachother.



#### Circuit to connect NRF24L01 transceivers to Arduino

The module communicates using SPI protocol. Therefore, you should make looking at the Arduino pin connector model SPI pins.

we hooked up the tranceiver as in https://hackmd.io/s/B15x5dn9V



Here is how we connected the NRF24 to the Arduino Uno

VCC  to  3.3 V
GND to  GND
CS  to   7
CE   to  8
MOSI to  11
MISO to  12
SCK  to  13



![](assets/wiring.PNG)   ![](assets/connectingtransceiver.jpg)



#### Code for Sending data from an Arduino through a transciever (as a transmitter)

I downloaded RF24 version 1.3.2 Library from tools>manage libraries>


<!--  
https://www.factoryforward.com/nrf24l01-rf-transceiver-module-communication-Arduino/

      #include <SPI.h>
      #include <nRF24L01.h>
      #include <RF24.h>

      RF24 radio(7, 8); // CE, CSN
      const byte address[6] = "00001";

      void setup() {
      radio.begin();
      radio.openWritingPipe(ardreceiverboard);
      radio.setPALevel(RF24_PA_MIN);
      radio.stopListening();
      }

      void loop() {
      const char text[] = "ON";
      radio.write(&text, sizeof(text));
      delay(500);   
      }

-->

We took this basic NRF24 radio sender code from [here](https://hackmd.io/s/B15x5dn9V)

            #include <nRF24L01.h>
            #include <RF24.h>
            #include <RF24_config.h>
            #include <SPI.h>

            const int pinCE = 9;
            const int pinCSN = 10;
            RF24 radio(pinCE, pinCSN);

            // Single radio pipe address for the 2 nodes to communicate.
            const uint64_t pipe = 0xE8E8F0F0E1LL;

            char data[16]="Hola mundo" ;

            void setup(void)
            {
               radio.begin();
               radio.openWritingPipe(pipe);
            }

            void loop(void)
            {
               radio.write(data, sizeof data);
               delay(1000);
            }




##### PROBLEM!

I had trouble uploading the NFR24 sender code to the Arduino because of board settings / library problem with the Arduino IDE.

**TIP:** It turns out it is not a good idea to upload every library with the words NRF24 in it!

I tidied up my libraries so I only had the NRF libraries that I needed for the task.
I did this by going into Sketch>Libraries>Manage Libraries and searching for NRF

The [fix for the bug from the Arduino community](https://forum.Arduino.cc/index.php?topic=568731.0) was:

  - Tools > Board > Boards Manager
  - Wait for downloads to finish.
  - When you move the mouse pointer over "Arduino AVR Boards", you will see a "Select version" dropdown menu appear. Select "1.6.21".
  - Click "Install".
  - Wait for installation to finish.
  - Click "Close".

I was reminded of the  Important things to check before uploading code in general :

  - check the board
  - check the programmer setting
  - check the output port

**TIP: When you are using libraries:  check you have the latest version on all nodes / bridges.  Also make sure Arduino updates are all installed as this can affect the use of libraries too.**



#### Receiver code

This code was also taken from [the same site](https://hackmd.io/s/B15x5dn9V) and programmed onto another Arduino

      #include <nRF24L01.h>
      #include <RF24.h>
      #include <RF24_config.h>
      #include <SPI.h>

      const int pinCE = 9;
      const int pinCSN = 10;
      RF24 radio(pinCE, pinCSN);

      // Single radio pipe address for the 2 nodes to communicate.
      const uint64_t pipe = 0xE8E8F0F0E1LL;

      char data[16];

      void setup(void)
      {
         Serial.begin(9600);
         radio.begin();
         radio.openReadingPipe(1,pipe);
         radio.startListening();
      }

      void loop(void)
      {
         if (radio.available())
         {
            int done = radio.read(data, sizeof data);
            Serial.println(data);
         }
      }




#### Coding in the NRF24 connection test

to test the connection between the two nrf24s/ Arduinos we coded into the Ardunios the digital channels 9 and 10 as communication pins:

    const int pinCE = 9; // This pin is used to set the nRF24 to standby (0) or active mode (1)
    const int pinCSN = 10; //This pin is used to tell the nRF24 whether the SPI communication is a command or message to send out
    RF24 wirelessSPI(pinCE, pinCSN);
    RF24 radio(pinCE, pinCSN); // Radio pipe addresses for the 2 nodes to communicate. Tells one to listen (standby) when the other sends (is in communication)

and we gave our channel a name 0x000003 - an 8 digit character corresponding to 8 bits

    const uint64_t pipe = 0x000003;

and coded a message
    char data[16]="Hola mundo" ;

##### PROBLEM!

Jess also had problems with a line of code which was related to the NRF library. The compiling error read "Void value not ignored as it ought to be"

**SOLUTION**

Jess changed the receiver code from

        int done = radio.read(data, sizeof data);

to:
        radio.read(data, sizeof data);

We are not sure why the format of the code has changed.

### COMPILING SOUND SENSOR PLUS NRF24 SENDER CODE

I next added the sound sensor code to the NRF24 sending code.
I gathered together:

* the libraries at the top
* the consts to declare
* the setup loop
* and the void loops

and we listed these sections of compiled code in order.

We left in the test text message and uploaded this to the sender Arduino.

We checked both sender and receiver serial monitors.

##### PROBLEM!

We found that Jess only received the text message not the sensor readings.

<center>![](assets/holamundo.png)</center>

**SOLUTION**

Eduardo changed the voltage value which was a number into a text string array which we called "b"

    char b[16];
    String str;
    str=String(volts);
    str.toCharArray(b,16);

**IMPORTANT: NRF24 modules cannot send floats or integers only character strings!**

Ilja also  advised us to just start using the name "b" without declaring it beforehand as this confuses the Arduino.

<center>![](assets/testingsensor.jpg)</center>

It worked hooray!


<center>![](assets/receivedcode.png)</center>


### Code File

You can see the sender file is [here](assets/soundsensornrt24.ino)

and the modified receiver code is as follows:

      #include <nRF24L01.h>
      #include <RF24.h>
      #include <RF24_config.h>
      #include <SPI.h>

      const int pinCE = 9;
      const int pinCSN = 10;
      RF24 radio(pinCE, pinCSN);

      // The Address name can be anything you want.
      // We simply exchanged the 8 to a 6 in the Address.
      // Single radio pipe address for the 2 nodes to communicate.
      const uint64_t pipe = 0xE6E8F0F0E1LL;

      char data[16]="Hola mundo" ;

      //Adding the speed for reading the Serial monitor
      void setup(void)
      {
       Serial.begin(9600);
         radio.begin();
         radio.openWritingPipe(pipe);
      }

      //Adding the Serial Print ln
      void loop(void)
      {
         radio.write(data, sizeof data);
         Serial.println(data);
         delay(1000);
      }


## Connecting Ramps 1.4 and/or sound sensor to laptop to give serial writes

### Getting x and y positions for each reading

For my project I need to get sensor, and position readings. I would have to programme an Arduino to do this normally - is there a way to use the Arduino on the plotter? Looking at the plotter the Arduino and ramps board is inaccessible.

 I could infer X and Y coordinate for every sound reading  sequentially as I already know what they will be? I could make a map of sensor readings by using geometric similarity to the path I have coded - i.e I could back-code the readings.

### Sensor to Arduino to Laptop

If so then I can connect the sensor to an arduino and programme it to read the sensor and write the value of each reading on a new line.
I have by now written GCODE for my scan area and it takes 144 readings.
This serial output can then be sent by serial port to a laptop and read by a processing sketch or on firefly which could (I hope) be coded to display readings sequentially in 12 rows of 12 units in a zig-zag pattern to create a heatmap.

### ESP 8266

The ESP8266 NodeMcu comes with a firmware that lets you program the chip with the Lua scripting language.

https://www.freecodecamp.org/news/how-to-get-started-with-freertos-and-esp8266-7a16035ddd71/


### sensor to RF  to rf to arduino / raspberry pi - processing sketch

not sure if this will need a laptop plugged in

### sensor to ESP8266 to MQTT to wifi router to platforms IO to laptop

looks promising

https://www.hackster.io/mtashiro/temp-sensor-connected-to-esp8266-and-upload-data-using-mqtt-5e05c9

Adafruit MQTT library  https://github.com/adafruit/Adafruit_MQTT_Library

ESP8266 library for arduino  https://github.com/esp8266/Arduino

### sensor-arduino-esp8266-web-laptop

Allows some distance between display and scan


#### Arduino device manager - talks to an arduino via the esp - canned

https://create.arduino.cc/devices/
https://create.arduino.cc/getting-started

needs linux based devices? don't know if my Arduino IE will change the code correctly. I'm  not sure this adds any value for the application

### Sensor - esp8266 - web - laptop  (no arduino)

removes bulky arduino from assemby and allows laptop to be far from the scanner

#### Instructibles method - libraries not working
https://www.instructables.com/id/Esp8266-Firebase-Connection/

What you need:

* ArduinoJson library by benoit blanchon
* firebase library here https://github.com/FirebaseExtended/firebase-arduino/releases

* esp8266 Board manager http://arduino.esp8266.com/stable/package_esp8266com_index.json - **a board manager is not the same as a library**

* CP210x driver for Esp8266  https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers

* WIFI_SSID, WIFI_PASSWORD
* firebase webpage and authentication key

        #define WIFI_SSID "IAAC-WIFI"
        #define WIFI_PASSWORD "enteriaac2013"
        #define FIREBASE_HOST "soundsensor-6ab6b.firebaseio.com"
        #define FIREBASE_AUTH "3DzUtG6WP3yliTwopMrYzcTlBwDj6KGLN2Jyxdcc"

To use Functions, you need to install Firebase command line tools using npm (Node.js
  this requires javascript i think

#### Circuitdigest method - canned

https://circuitdigest.com/microcontroller-projects/sending-temperature-and-humidity-data-to-firebase-database-using-esp8266
- needs a library for the sensor

#### Microcontroller kits method - canned
http://microcontrollerkits.blogspot.com/2016/12/esp8266-firebase.html

Arduino Library
Firebase Arduino Library
WiFi Connector
DHT Sensor
Adafriut GFX ( OLED Display )

also needs sensor software

#### Hackanons method using Arduino NodeMCU - libraries not working

https://www.hackanons.com/2018/02/iot-with-firebase-connecting-arduino.html
https://medium.com/@o.lourme/our-iot-journey-through-esp8266-firebase-angular-and-plotly-js-part-1-a07db495ac5f

software:

- Install Arduino ESP8266 core
- Download FirebaseArduino library
- Click Sketch > Include Library > Add .ZIP Library...
- Choose firebase-arduino-master.zip downloaded in step 3.

looks like a different module

"Here i will suggest to only use arduino ide 1.6.9 version on Linux machine then only this tutorial will work like charm otherwise you will get error."

needs linux - not hopeful


## sensor-esp8266-raspberry pi server

replaces laptop with a smaller device which can be far from the scanner

Not vital at this stage as understanding how to use a pi will take ages
Can't see the raspberry pi.


## radio sniffing!

https://www.bitcraze.io/2015/06/sniffing-crazyflies-radio-with-hackrf-blue/

http://travisgoodspeed.blogspot.com/2011/02/promiscuity-is-nrf24l01s-duty.html

poor mans wifi scanner with
https://forum.arduino.cc/index.php/topic,54795.0.html


      #include <SPI.h>

      // Poor Man's Wireless 2.4GHz Scanner
      //
      // uses an nRF24L01p connected to an Arduino
      //
      // Cables are:
      //     SS       -> 10
      //     MOSI     -> 11
      //     MISO     -> 12
      //     SCK      -> 13
      //
      // and CE       ->  9
      //
      // created March 2011 by Rolf Henkel
      //

      #define CE  9

      // Array to hold Channel data
      #define CHANNELS  64
      int channel[CHANNELS];

      // greyscale mapping
      int  line;
      char grey[] = " .:-=+*aRW";

      // nRF24L01P registers we need
      #define _NRF24_CONFIG      0x00
      #define _NRF24_EN_AA       0x01
      #define _NRF24_RF_CH       0x05
      #define _NRF24_RF_SETUP    0x06
      #define _NRF24_RPD         0x09

      // get the value of a nRF24L01p register
      byte getRegister(byte r)
      {
       byte c;

       PORTB &=~_BV(2);
       c = SPI.transfer(r&0x1F);
       c = SPI.transfer(0);  
       PORTB |= _BV(2);

       return(c);
      }

      // set the value of a nRF24L01p register
      void setRegister(byte r, byte v)
      {
       PORTB &=~_BV(2);
       SPI.transfer((r&0x1F)|0x20);
       SPI.transfer(v);
       PORTB |= _BV(2);
      }

      // power up the nRF24L01p chip
      void powerUp(void)
      {
       setRegister(_NRF24_CONFIG,getRegister(_NRF24_CONFIG)|0x02);
       delayMicroseconds(130);
      }

      // switch nRF24L01p off
      void powerDown(void)
      {
       setRegister(_NRF24_CONFIG,getRegister(_NRF24_CONFIG)&~0x02);
      }

      // enable RX
      void enable(void)
      {
         PORTB |= _BV(1);
      }

      // disable RX
      void disable(void)
      {
         PORTB &=~_BV(1);
      }

      // setup RX-Mode of nRF24L01p
      void setRX(void)
      {
       setRegister(_NRF24_CONFIG,getRegister(_NRF24_CONFIG)|0x01);
       enable();
       // this is slightly shorter than
       // the recommended delay of 130 usec
       // - but it works for me and speeds things up a little...
       delayMicroseconds(100);
      }

      // scanning all channels in the 2.4GHz band
      void scanChannels(void)
      {
       disable();
       for( int j=0 ; j<200  ; j++)
       {
         for( int i=0 ; i<CHANNELS ; i++)
         {
           // select a new channel
           setRegister(_NRF24_RF_CH,(128*i)/CHANNELS);

           // switch on RX
           setRX();

           // wait enough for RX-things to settle
           delayMicroseconds(40);

           // this is actually the point where the RPD-flag
           // is set, when CE goes low
           disable();

           // read out RPD flag; set to 1 if
           // received power > -64dBm
           if( getRegister(_NRF24_RPD)>0 )   channel[i]++;
         }
       }
      }

      // outputs channel data as a simple grey map
      void outputChannels(void)
      {
       int norm = 0;

       // find the maximal count in channel array
       for( int i=0 ; i<CHANNELS ; i++)
         if( channel[i]>norm ) norm = channel[i];

       // now output the data
       Serial.print('|');
       for( int i=0 ; i<CHANNELS ; i++)
       {
         int pos;

         // calculate grey value position
         if( norm!=0 ) pos = (channel[i]*10)/norm;
         else          pos = 0;

         // boost low values
         if( pos==0 && channel[i]>0 ) pos++;

         // clamp large values
         if( pos>9 ) pos = 9;

         // print it out
         Serial.print(grey[pos]);
         channel[i] = 0;
       }

       // indicate overall power
       Serial.print("| ");
       Serial.println(norm);
      }

      // give a visual reference between WLAN-channels and displayed data
      void printChannels(void)
      {
       // output approximate positions of WLAN-channels
       Serial.println(">      1 2  3 4  5  6 7 8  9 10 11 12 13  14                     <");
      }

      void setup()
      {
       Serial.begin(57600);

       Serial.println("Starting Poor Man's Wireless 2.4GHz Scanner ...");
       Serial.println();

       // Channel Layout
       // 0         1         2         3         4         5         6
       // 0123456789012345678901234567890123456789012345678901234567890123
       //       1 2  3 4  5  6 7 8  9 10 11 12 13  14                     |
       //
       Serial.println("Channel Layout");
       printChannels();

       // Setup SPI
       SPI.begin();
       SPI.setDataMode(SPI_MODE0);
       SPI.setClockDivider(SPI_CLOCK_DIV2);
       SPI.setBitOrder(MSBFIRST);

       // Activate Chip Enable
       pinMode(CE,OUTPUT);
       disable();

       // now start receiver
       powerUp();

       // switch off Shockburst
       setRegister(_NRF24_EN_AA,0x0);

       // make sure RF-section is set properly
       // - just write default value...
       setRegister(_NRF24_RF_SETUP,0x0F);

       // reset line counter
       line = 0;
      }

      void loop()
      {
       // do the scan
       scanChannels();

       // output the result
       outputChannels();

       // output WLAN-channel reference every 12th line
       if( line++>12 )
       {
         printChannels();
         line = 0;
       }
      }

it works !

57600 baud rate on serial print






airpsys all over the world  https://airspy.com/directory/


https://www.youtube.com/watch?v=EDbS-zlLPxw

AstroRadio SL
Roca i Roca 69
Terrassa, 08226 Barcelona
Spain
Teléfono:
93.7353456
Teléfono móvil:
727780727
Skype:
astro-radio
Email:

Horario:

### antennas

>The pcb antenna is calculated to match with the tcvr, and you just can't measure it's length! There are a lot of things like shortening factors, imedance, and other stuffs you have to calculate in. With this mod, i'm sure you unmatch the impedance, and at a higher transmit powers (like PA+LNA version) it could kill your transmitter!
If you want to do the correct way, you should disconnect the entire antenna with a cut after 2-3mm, and solder a wire antenna on. The proper size should be l=((C/F)/4)*0.96, where C is 300, F is the freq, like 2450MHz (center of the band), and 0.96 is the free air velocity factor: l=(300/2450)/4*0.96 gives you to cut 29.4mm bare wire for a quarter wave antenna.
You can read more about here:
https://en.wikipedia.org/wiki/Velocity_factor
https://en.wikipedia.org/wiki/Monopole_antenna

--

https://www.instructables.com/id/Enhanced-NRF24L01/


https://forum.arduino.cc/index.php?topic=54795.0

https://www.mysensors.org/


<!--
https://www.youtube.com/watch?v=2ZK6dcBTeT0
microwave radar doppler modules
### Ultrasound
https://www.theengineeringprojects.com/2016/08/new-proteus-libraries-engineering-students.html
-->
