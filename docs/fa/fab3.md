

# <center>Fab Academy Week 3</center>
# <center> Laser and Vinyl cutting</center>

<center>![](assets/)</center>

## Assignment: Cut something on a vinyl cutter

##### **Resources:**

* http://fabacademy.org/2019/labs/barcelona/local/wa/week3/



<!---
Group assignment: You should write the power, speed, frequency setting for every test… But in general very good!
Group assignment: List your group team, and add the links to their webpage
Group assignment: Missing files.
Group assignment: Explain the conclusions
Group assignment: List the basic characteristic of the machine you are working with (name, dimension, power, etc..). Add link to software and machine webpage. Also to the manual.
Group assignment: I like how you are starting to document! Well done! I like the combination between text and picture. I’m missing your own learning comments.
Group assignment: Fabmodules link it’s not working
Group assignment: Add link to the test files (pngs) or link to the archive page.
Group assignment: Change
Group assignment: Add pictures of the results
Group assignment: You should extract conclusion about kerf, cut settings, etc… But in general very good!
Missing parametric design.
Explain better how are the steps in the camstudio from the vinyl cutter (adjust blade, set the material, update dimensions through the serial port….)
Your design is not parametric. Re-design and explain how you parametrically design the files.
Explain you did group assignment alone.
External Files should be linked directly.
Missing photos of “laser cut frame”
Group assignment: Add link to software, machine or people you mention. Ej: Speedy 400.
I understand you will add later but you are missing all pictures ;)
Group assignment: Nice machine documentation. I like you add a link to the user manual, but are super hard to see! can you change you css to make more visible the link ;)
Group assignment: Missing keft laser cut test.
Missing Vinyl assignment
Missing Pressfit construction kit
Good job explaining the problems with freecad.
Remember to upload the final version of the files that you have been doing for the different programs. Even if they are test or not definitive.
Add a screenshot of the Changing parameters to show how did you paramtrize your design.
Missing a final picture with your pressfit construction kit....
You must explain the key commands and tools you used to generate the Ilustrator design, steps, etc.
Design files must be present in the archive in generically readable forms such as DXFs, STLs, etc., as well as in the original editable format (antimony, solidworks, eagle, CAD, etc). Is missing Fusion file and the svg or dxf version of AI file.
Is your design parametric? Explain how you parametrically design the files
Which settings you use to cut your press fit design. Which Kerf you have?
  -->

<!---  -->
### A transformable metamaterial
The project that came to mind were transformable / kinematic surfaces formulated by [Studio Overvelde](http://www.studioovervelde.com/). I think this is really interesting as it touches on the field of soft robotics and mutifunctionailities of a surface / space which harness digital fabrication tools.



#### Drawing the file
 I used this example from this paper by [Johannes T. B. Overvelde et al (2016) Reconfigurable origami-inspired acoustic waveguides](https://www.researchgate.netpublication/312508299_Rational_design_of_reconfigurable_prismatic_architected_materials)

 <center>![](assets/overveldefig2.jpg)</center>




 I drew this figure by drawing surfaces by eye in Rhino in 3d and then used the "Unroll Surface" function to reveal a 2D net that I could fold to  assemble. It was quite hard to visualise how this would work without playing with model of this but I made an attempt anyway. I tried unrolling an assembly of 4 units to see if I could reduce the cuts but it got too complicated (the pink shape) to see how to fold it so I stuck to using one shape per unit (shown in green).

<center>![](assets/unrolling.jpg)</center>

I used the "Scale2D" command to scale up the shape so that each square was 2 x 2 cm. I traced the edges using a "Polyline" and added lines where cuts were needed inside the shape. I added tabs to the entire outside edge using the "polyline" tool again  so I could join the outer edges.

<center>![](assets/cuts.jpg)</center>

#### Formatting for import to  GX-24 Camm Servo Vinyl Cutter

I struggled to find a way around using Adobe Illustrator without having to manually resize the image. This is a recurring problem with drawing on Rhino - it does not export scales to 2D to many image formats. In the end I used a trial version of Illustrator.

#### <center>The .ai file I used for the cut can be downloaded from **[here](https://gitlab.com/MDEF/saira.raza/raw/master/docs/fa/assets/Overvelde16xAcousMetaatoms.AI?inline=false)**</center>

#### Loading the Vinyl cutter GX-24 Camm Servo

http://wiki.fablab.is/wiki/How_to_use_the_Roland_GX-24


* It took hours for me to find out that you need to load rolls of material through the back of the cutter.
* You have to push the holding lever down to losen it. I put the roll edge through.
* I adjusted the rollers so they were at maximum width whilst remaining in the designated space.
* I pulled the lever up to hold the roll in place.
* I selected "roll" on the panel in the cutter


#### Setting the CutStudio software to cut

I set up the cut as per the instructions in [this Fab Academy tutorial:](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week03_computer_controlled_cutting/rolandgx24.html)

I imported my .ai file into CutStudio.

I went to CUT SETUP panel and I imported the width of the roll in the CUT SET UP panel.

I had to manually adjust the height of the cutting area as this stage - it took 2 attempts before I realised what was happening.

**TIP: IN the CUT SETUP panel make sure you manually enter the height of the cutting area so your drawing fits into it. Otherwise the cutter will not completely cut your drawing**

With the cut area saved I clicked CUTTING panel and confirmed the cut.

After 2 attempts I finally managed to cut these 16 pieces in one roll.

<center>![](assets/roll.jpg)</center>

#### Hero shot!

Here are 4 acoustic metaatoms assembled  by sticking the tabs of each shape together.

I have 12 more to make!

<center>![](assets/vinyl.jpg)</center>

**References**


* [Johannes T.B. Overvelde etal(2017) Rational design of reconfigurable prismatic architected materials](https://www.researchgate.net/publication/312508299_Rational_design_of_reconfigurable_prismatic_architected_materials/figures?lo=1)
* [Chiroptical kirigami-based metamaterials](https://www.nature.com/articles/s41427-018-0082-x)
* [A three-dimensional actuated origami-inspired transformable metamaterial with multiple degrees of freedom](https://www.nature.com/articles/ncomms10929)
* [DNA origami for silica nanostructures](https://www.researchgate.net/publication/328773939_DNA_Origami_Templated_Silica_Growth_by_Sol-gel_Chemistry)
* [Reconfigurable origami-inspired acoustic waveguides
Sahab Babaee1, Johannes T. B. Overvelde1,2, Elizabeth R. Chen1, Vincent Tournat1,3 and Katia Bertoldi1](https://www.researchgate.net/publication/310766212_Reconfigurable_origami-inspired_acoustic_waveguides)
* [Kirigami metamaterials for reconfigurable toroidal circular dichroism](https://www.nature.com/articles/s41427-018-0082-x)

### Dual band fractal radio antenna

For my masters project I am focussing on using fab lab equipment to explore how we can use metamaterials as maker and I wanted to try to make this [Sierpinski fractal antenna designed by Sika Shrestha et al](https://www.researchgate.net/publication/270625606_A_New_Fractal-Based_Miniaturized_Dual_Band_Patch_Antenna_for_RF_Energy_Harvesting) that is designed to harvest energy from radio waves at two frequencies, 2.45 and 5.8 GHz.  

<!---
>an arrayed fractal antenna can be designed to increase overall gain, and a stacked Sierpinski carpet fractal antenna can be designed to obtain a wider bandwidth than a single antenna. Such antennas can be integrated with a rectifier circuit generating rectennas in order to harvest RF energy at dual frequencies.

  *- [Sika Shrestha, Seong Ro, Lee Dong-You Choi (2014) A New Fractal-Based Miniaturized Dual Band Patch Antenna for RF Energy Harvesting](https://www.researchgate.net/publication/270625606_A_New_Fractal-Based_Miniaturized_Dual_Band_Patch_Antenna_for_RF_Energy_Harvesting)* -->

#### Drawing the file

I copied this image onto Rhino again to ensure the dimensions were correct.

<center>![](assets/fracant.png)</center>

#### <center>The Rhino file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/fractalantenna.3dm)**</center>

#### Converting the file to .ai

I saved my Rhino file directly from Rhino as an Illustrator .ai file

#### <center>The .ai file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/raw/master/docs/fa/assets/Fractal%20Antenna%20Outline.AI?inline=false)**</center>

#### Cutting copper tape on PVC with the vinyl cutter

I followed the [Fab Academy tutorial on cutting a PBC using a vinyl cutter](http://archive.fabacademy.org/archives/2016/doc/vinylpcb.html) using copper adhesive tape on PVC but did not use Kapton tape.

The copper tape hade to be smoothed out as I was sticking it on to avoid bubbles.

I used the Roland cutter again.

This time I had to carefully peel off any copper I did not need on the part leaving the finished product. This leaves tape open to the elements so the piece is not very durable. I will see if I can measure some sort of current coming off it at some point.

The pressure and the speed of the tool can sometimes drag the tape off the vinyl as in this instance of one of my classmates test cuts.

<center>![](assets/mangledcircuit.jpg)</center>

Depending on the material it is wise to test varying these.


#### Hero shot!

<center>![](assets/fractalantenna.jpg)</center>

<!---
#### Circuit for solar panel - to stick onto a glass - titanium oxide blackberry juice

#### Flexible circuit on a transparent base (solar panels?)
  -->



## Laser Cutters

##### **Resources**

* http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week3_computer_controlled_cutting/press_fit.html

I've had a whirlwind tour of laser cutting as part of the MDEF masters so far and there was a bit of a traffic jam to cut this week but we managed to get a few things done and revisit the machines a bit later on.

### Key general things I learned about laser cutting:

1. They do vector cutting, vector engraving (scoring) or raster engraving - with raster engraving the resolution of the image determines how closely the laser burns the dots together. Laser cutters can use vector graphics and raster graphics files and I was surprised to learn that the [laser cutters in our fab lab can even work with Word and Excel files!](https://www.troteclaser.com/en/knowledge/faqs/compatible-graphics-programs/)
2. **Many materials are not safe to cut in a laser cutter** including ABS used in 3D printing, PVC and polystyrene so always check you know what material you are cutting. IN relation to safety from fumes **ventilation should always be switched on when using the laser**.
3. The protocol for using the laser cutter in our fab Lab are [here](https://drive.google.com/file/d/15n6L-T7Xf9T2ejN9rBHu0hW_4uYi5NBh/view)
4. The **kerf** is the portion of material that the laser burns away when it cuts through sand it varies even in the same piece of material sometimes so its a good idea to do a test if the accuracy of the cuts are important to avoid having to do iterations.
5. Engraving is tricky to do so testing is definitely a good Idea. The laser can interpret a greyscale image into a raster engraving even if you draw embed it into rhino as a vector image. Selecting a Raster Output from the laser makes a stronger interpretation of the grey scale (the darks are much darker.)


### Assignment: Cut something on the laser cutter

#### Kerf / Engrave test
We tried to cut a laser test in a group using 6.5mm cardboard to find an optimum slot width for our press kits.

<center>![](assets/lasingtest.jpg)</center>

But it  didn't work. Unfortunately we had not measured the width properly and it was much thicker than we thought and we had the wrong material settings on  the laser. I think at that stage we didn't quite appreciate how this information would affect the subsequent task!

<center>![](assets/failedlase.jpg)</center>



### Laser engraving to induce graphene

Mikel (who is the fab lab expert on laser cutting) and I wanted to try to make graphene on some organic materials using a defocussed laser. Ruquan Ye, the author of [this paper on using making laser induced graphene on organic substances like bread, potatos, coconuts and pine](https://pubs.acs.org/doi/abs/10.1021/acsnano.7b08539) was kind enough to share it with us for free (thanks Ruquan!).

#### Drawing the files

The file had to be engraved so used a circuit I had previously drawn and in Eagle imported this into Rhino, I used the "OFFSET" command to thicken the lines, closed the lines and  joined them using the JOIN command to make a closed curve. I used the HATCH command in Rhino to fill these hollow curves and moved the hatches to a separate layer colored red.
<center>![](assets/potatohatch.png)</center>

#### <center>You can download the Rhino file for the drawing **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/LIGdrawing.3dm)**</center>

Importantly this drawing was effectively imported into the Trotec plugin as a VECTOR image as Rhino files work on a VECTOR NURBS system.

#### Practice

We defocussed the laser so that each lase would give multiple overlaps of low intensity light.

The settings we used were:

- Power: 1
- Speed: 10%
- DPI (resolution): 1000
- Defocus: 1mm
- Passes: 3

We didn't have much time to think it over but I think we got the theory a bit wrong. I think we need to try a RASTER image because I think the laser moves in dots in this setting hence the intensity is measured in dpi rather than hertz. Anyway we got something of a result but it looked quite pale compared to the photos in the paper:

![](assets/laserpotato.jpg)  ![](assets/potato.jpg)

The circuit held a small current but I don't think it was graphene.

I hope to have another go with pine one day because pine has a lot of lignin. We should lase the pine with a prep layer first (blue in the file above) and then print the circuit over it (red layer again).


## Assignment: Cut an assemble-able and reconfigurable press fit kit

### A Tetrahedron press fit kit

I've read a couple of things this term about why tetrahedrons are an interesting shape - including a proposal that it is [the smallest, indivisible unit in 3D quasicrystalline reality](https://www.quantumgravityresearch.org/lay-person-overview)(?!). A lot of fab academy students have made press kits with tetrahedrons before. I really like these ones by [Ahmed Helal](http://fab.cba.mit.edu/classes/863.10/people/ahmed.helal/Week%202/pressfit.html) and [Joanthan Bobrow](http://fab.cba.mit.edu/classes/863.14/people/jonathan_bobrow/projects/7/jpeg-4/)

Ahmed managed to make his tetrahedrons tesselate by using a clip. I wondered if it was possible to make an internal skeleton to push the faces onto.

I  wanted to try a snap fit joint too however these need to be able to take very localised stress and need some plasticity so I am expecting that they wont work well with corrugated cardboard. My suspicions were confirmed by [Chris Saulnier's experiments that found that cardboard snap joints get mangled easily.](http://www.mit.edu/~saulnier/mas.863/pages/pressfit.htm)

As I am interested in transformable metamaterials so I also considered using hinges made out of a more plastic material than cardboard and found these examples of hinged polyhedra by [Gerard Westendorp](https://westy31.home.xs4all.nl/Hingedpolyhedra/Hingedpolyhedra.html) who references the Johannes Overvelde's toolkit for transformable metamaterials that I explored through vinyl cutting. Overvelde's group have been making these beautiful transformable [structures using hinged card board pieces](https://amolf.nl/wp-content/uploads/2017/01/1701_bas_overvelde_nature_s.jpg) but it is not clear what holds them together.

### Drawing in Rhino

I started by drawing a 5cm edge tetrahedron in Rhino, Using the **Pyramid tool**. I calculated the height of the tetrahedron/ pyramid using [this online calculator](https://rechneronline.de/pi/tetrahedron.php)

I traced triangles on the faces of the tetrahedron using the **Polyline tool** and **Offset** these by 5mm inwards (allowing for the attached faces), and **extruded** them 5mm inwards towards the centre of the tetrahedron to see the size each face piece would be. I sliced this structure with a 5mm thick block to visualise how the symmetry of the piece would work. I decided to make corner braces of the tetrahedron parallel to the plane opposite because this would give an equilateral triangle that would be easy to parametricise.

<center>![](assets/testtet.png)</center>

I did a **BOOLEAN SPLIT** operation to see how big this corner piece could be without hitting another cornerpiece. I found a reasonable size of the corner piece and traced around it using the POLYLINE tool. Using the **3 POINT RECTANGLE** tool I added pegs to each side of this triangle corner piece - these will plug into 3 faces at each corner of the tetrahedron. The pegs were drawn 5mm deep so that they hold the full thickness of the cardboard faces. I then traced around the triangle and 3 pegs to form the outline for the corner brace piece.


<center>![](assets/corner.png)</center>


I tried to reassemble the faces with the corner pieces in Rhino which was tricky as I was not working in right angles. I did this to see where the holes for the pegs should be in the faces of the tetrahedrons.


I laid out the faces and pegs and then tried to draw this in Grasshopper to parametricise the kit so it could be adjusted for different thicknesses of material.


#### Some learnings about Grasshopper

Grasshopper has a really great interface that uses machine analogy which helps to conceptualise operations HOWEVER there are MANY components you can use and many are not on the toolbar. I found myself trying some mind-bending workarounds for certain things like drawing the pegs. Owing to time constraints I had to abandon Grasshopper.

I managed to draw the face and pegs but not the actual notches on the pegs.

![](assets/Grasshopper.png)

I had made the design for 5mm thick card board so I thought I'd see if I could tweak the dimensions on Grasshopper rather than draw the thing from grasshopper.

I had used the Polygon to generate the Triangle face and the proportional corner piece base however Santi showed me that **Divide curve component can be used to define positions on the outline of a shape** and that the **Polyline Curve tool can be used to draw shapes between defined points**. this was a game changer.

![](assets/ghtriangle.png)
![](assets/ghtriangleview.png)

I was surprised to not be able to find instantly what the Boolean Toggle component does but I believe its a very fast reactive yes/no switch.


### Preparing the file for the laser cutter

The next day I arranged 16 faces and 16 corner braces next to each other and put them in a new layer which I coloured blue.

**PROBLEM!: File drama**

When I opened the Rhino file there was no grasshopper file. I have no idea why - it saved it so I assumed it was linked - one for the UX designers.

**TIP: Grasshopper files have to be saved separately to Rhino files so remember to save both before closing your drawing.**

So now after about 3 days of drawing and learning how to do things I resorted to blindly drawing in Rhino.

#### Resizing

For some reason my triangles had shrunk overnight so I resized my shapes quickly using the 2d Scale function in Rhino.

**TIP: save your files with meaningful names so you don't lose them!**

The Speedy 100R holds 30cm x 61cm
The Speedy 400 holds a bigger size

My total cut area was only 271 x 139 mm so I used the Speedy 100R. I was able to use scrap cardboard.

**PROBLEM: Cardboard was not 5mm and the holes and pegs all needed changing.**

I measured this with calipers to be 4mm and found that the cardboard available was 4mm not 5mm so I had to adjust the hole widths and peg heights to 4.1mm and 4mm respectively without Grasshopper. I really wished I had sorted that out earlier!

![](assets/tetracutpath.jpg)

#### <center>The Rhino 5 file for this kit can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/terahedroncut1.3dm)**
</center>
### Finally cutting on the Trotec Speedy 100

<!--- http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/computer-controlled-cutting/  -->

1. First of all I turned on the power and then the big ventilation switch on the back wall.
2. Then I switched on the machine
3. I put in my piece to cut in the top left hand corner and **set the laser focus** by placing the metal focus measure  labelled Trotec 100 which you hold from the red knob and place on the notch above the laser. I raised the platform until the stick fell of its notch.
4. I downloaded [my Rhino 5 file](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/terahedroncut1.3dm) from my Googledrive and opened my file in Rhino 5 on the computer in the fab lab and made sure that everything I wanted to cut was set in the blue layer and everything else was hidden. Mikel gave us a tip that it's handy to put cuts in the blue layer and engraving in the red layer as this is the order they come up in the setup for the machine.
5. For a new material make a test cut - a square  - and click PRINT.
6. The PRINT SETUP menu appears and here you make sure the correct machine is selected.
7. I clicked SET WINDOW to highlight where I wanted the cut area to start. This area should be treated like a shape in Rhino so I typed Move and selected and moved the highlighted cutting area leaving a small margin. I set the window and press print.
8. In PRINT SETUP under OI selected my output as VECTOR (you can choose RASTER here for engraving. Hit enter
9. I deleted the last job in the Trotec software memory so only my file was showing. I dragged it onto the canvas and clicked CONNECT.
10. I set the laser start point manually using the arrows on the machine control panel and selected my drawing in the software and used the MOVE command to move to the top left corner.
11. I clicked on MATERIAL TEMPLATE Settings. The software has a colour legend that can be set to specified layers and jobs. I set the blue layer to:
  - POWER = 30
  - SPEED = 0.9
  - Frequency = 1000 Hz
10. Send to CUT


**! PROBLEM: Weird cutting path**

Even though I tried to minimise cuts by tessellating, this didn't stop the laser from going over every single shape individually in the file in the order it was drawn.

**TIP: Check for duplicate shapes and remove them. You can use the Vector order tool under menu to ensure cutting doesn't pass over lines twice.**


#### The moment of truth

![](assets/tetrafail.jpg)

![](assets/tetrafail2.jpg)


Nope. Not this time. The flush finish combined with corrugated cardboard interior to not feel intuitive to fit together and the faces kept popping off.

 Despite the laser kerf I forgot to give extra room in the holes so the fit was too tight and as the pegs did not go into the holes perpendicularly it was not intuitive to fit together.

 Also these resized little stubby corners don't work nearly as well as a thinner longer one would.

### Using parametric OpenSCAD building blocks by Christian Schmidt

As a quick attempt at trying parametric I tried the [Parametric building blocks for OpenSCAD.](http://fab.academany.org/2019/labs/aachen/students/christian-schmidt/project_files/building_blocks.scad) that Fab Academy student [Christain Schmidt](http://fab.academany.org/2019/labs/aachen/students/christian-schmidt/index.html) produced this year (thanks Christian).

I tried changing the parameters but it took me a while to see how to use the buttons at the bottom of the view screen.

The RENDER button (bottom of view screen second from the left ) finally implemented my changes.

#### What I changed

* I added the line:
      nslots = 4;

as I think the nslots parameter definition was missing.

* I tweaked the "size" parameter to a larger size in comparison to the slots (70).
* I changed the thickness parameter to 4 to relate to the cardboard I know we have in the fab lab

![](assets/para.png)


![](assets/parametricpk.jpg)
I tried to export this as an .svg file so it could be used with the Trotec lasers in our fab lab

#### <center>The .scad file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/parametricpressfit%20-%20Copy.scad)**</center>

#### <center>The .svg file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/parametricpressfit.svg)**</center>


<!--- GIMP
 FOR PRESS FIT - boolean function - clone and object to creates a  a link...


 ## FreeCAD

 make a square and cut out in the square  - trim intersections

 constraints

-->

#### Some tools for drawing joints for laser cutters

* https://www.instructables.com/id/The-Ultimate-Guide-to-Laser-cut-Box-Generators/
* https://kyub.com/


#### Other Ideas for press kits:

* DNA origami - I briefly looked at DNA origami last term in my project on Geometry.
Some former student of How to Build Almost Anything has already attempted DNA press kits
    * http://fab.cba.mit.edu/classes/863.15/section.Harvard/people/McNamara/week1.html
    * http://fab.cba.mit.edu/classes/863.16/section.CBA/people/Smith/week1.html
* Speculative Circuit for a physical code generator (https://www.nature.com/articles/s41699-018-0073-3)
  * http://cba.mit.edu/docs/papers/17.04.11.SelfAssemSpacecraft.pdf


<!---

#### Acoustic metamaterial kits

3D - 2d modelling

IAAC students  Shambayati, R., Tankal, E., and Baseta, E. (2016) have also worked on mechanical surfaces, interestingly this was to alter acoustic properties - another field I have been looking at.
  -->

### Cutting a Metasurface one way acoustic tunnel

I decided I should try to make [this design of a 2d acoustic diode](https://aip.scitation.org/doi/10.1063/1.4930300) as it can form part of an experiment for my final project.

#### Drawing the files

I had already drawn the cross section in Rhino as this enables easy scaled drawings and also because we have a plugin for the the Trotec Speedy 400 laser cutter that we use in the lab.
  **I documented this for [week 2 here where I added the files too](fab2).**

I used Rhino to draw a shape and created different layers for engraving and cutting however I decided not to engrave on this occasion as the piece forms part of an experiment where the surface has to be straight.



#### <center>You can download the Rhino file I used to cut this piece **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/Lasertunnel.3dm)**</center>

Next I had to find the material to make this tunnel unfortunately (or fortunately) we found a big piece of transparent acrylic and Mikel said it would be best to use the bigger laser cutter.

![](assets/biglasercut.jpg)
![](assets/biglaserresult.jpg)
