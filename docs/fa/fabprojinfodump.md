

# <center>Fab Academy Project Information Dump/center>

https://www.researchgate.net/profile/Stefan_Punzet/publication/323430011_Design_and_construction_of_an_Acoustic_Scanning_Stage/links/5a95c3bc0f7e9ba42972cd52/Design-and-construction-of-an-Acoustic-Scanning-Stage.pdf

https://aip.scitation.org/doi/pdf/10.1063/1.4989995


Economical Miniaturized Arduino-Based Metamaterial Setup



A Perspective of Non-Fiber-Optical Metamaterial and
Piezoelectric Material Sensing in Automated
Structural Health Monitoring
Venu Gopal Madhav Annamdas 1,2,* and Chee Kiong Soh 2

## Buzzer / sensor rig

### buzzer

from: https://learn.adafruit.com/adafruit-arduino-lesson-10-making-sounds/playing-a-scale

Pins:
GND
Digital data pin (eg 2)

          /*
          Adafruit Arduino - Lesson 10. Simple Sounds
          */

          int speakerPin = 12;

          int numTones = 10;
          int tones[] = {261, 277, 294, 311, 330, 349, 370, 392, 415, 440};
          //            mid C  C#   D    D#   E    F    F#   G    G#   A

          void setup()
          {
            for (int i = 0; i < numTones; i++)
            {
              tone(speakerPin, tones[i]);
              delay(500);
            }
            noTone(speakerPin);
          }

          void loop()
          {
          }

To play a note of a particular pitch, you specify the frequency. See the following section on sound. The different frequencies for each note are kept in an array. An array is like a list. So, a scale can be played by playing each of the notes in the list in turn.

The 'for' loop will count from 0 to 9 using the variable 'i'. To get the frequency of the note to play at each step, we use 'tone[i]'. This means, the value in the 'tones' array at position 'i'. So, for example, 'tones[0]' is 261, 'tones[1]' is 277 etc.

The Arduino command 'tone' takes two parameters, the first is the pin to play the tone on and the second is the frequency of the tone to play.

When all the notes have been played, the 'noTone' command stops that pin playing any tone.

We could have put the tone playing code into 'loop' rather than 'setup', but frankly the same scale being played over and over again gets a bit tiresome. So in this case, 'loop' is empty.

To play the tune again, just press the reset button.


### The hw-484 sensor

https://blog.yavilevich.com/2016/08/arduino-sound-level-meter-and-spectrum-analyzer/


https://randomnerdtutorials.com/guide-for-microphone-sound-sensor-with-arduino/


is hw-484

Pin	Wiring to Arduino
A0	Analog pins
D0	Digital pins
GND	GND
VCC	5V


          /*
           * Rui Santos
           * Complete Project Details https://randomnerdtutorials.com
          */

          int sensorPin=7;
          boolean val =0;

          void setup(){
            pinMode(sensorPin, INPUT);
            Serial.begin (9600);
          }

          void loop (){
            val =digitalRead(sensorPin);
            Serial.println (val);

          }

## EXcel and ardunio



## Scanning movement

## Machines that make

The Squid Works project

### 2 stepper motors

Making XY Plotter using Arduino UNO and 28BYJ-48 stepper Motor
https://www.youtube.com/watch?v=_bfjivNyQDM


https://electronoobs.com/eng_arduino_tut30.php


>I use grbl with the Arduino to send G-Code to the printer and a really great way to create G-Code is with the totally free software Inkscape, which has a built-in feature, G-Code tools. Inkscape will convert your drawings to G-Code which grbl will interpret and send instructions to the plotter.

https://www.instructables.com/id/X-Y-Plotter-1/

An open source, embedded, high performance g-code-parser and CNC milling controller written in optimized C that will run on a straight Arduino https://github.com/gnea/grbl/wiki  https://github.com/grbl/grbl

rduino, 2 L293DN H-Bridges, 2 stepper motors from old CD Drives and lots of hot glue.
https://www.youtube.com/watch?v=iI4WkqwxDSY
