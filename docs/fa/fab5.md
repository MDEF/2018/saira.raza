# <center>Fab Academy Week 5</center>
# <center> 3D printing and scanning</center>
<center>![](assets/)</center>
<!---     -->
##### **Resources:**
* http://fab.academany.org/2019/labs/barcelona/local/wa/week5/


<!---  Group assignment: List the basic characteristic of the machine you are working with (name, dimension, power, etc..). Add link to software and machine webpage. Also to the manual. If you don’t find the webpage you can link to the Fablabbcn wiki
Group assignment: How did you send the GCODE to the printer? Did you send by serial, SD card, or wifi? If it’s wifi, which software did you use to communicate with the 3d printers? Add a link Octoprint
Group assignment: Explain the advantages and limitations of 3D printing.
Group assignment: List your group team, and add the links to their webpage.
Design files are missing
Scanning process is missing
3D printing documentation is missing ( just screenshots )
Show how you designed and made your object and explained why it could not be made subtractively is missing.
Explain the advantages and limitations of 3D printing.
For your individual assignment: nice exercise but for this assignment you should design your 3d printing object not one from thingiverse.
Explain why your 3d object could not be made subtractively .
Upload your design files to the gitlab repository.
Add some screenshots of the CURA process.
Explain the advantages and limitations of the scanning technology.
Did you use Blender ( or other software like Meshmixer ) for work on the mesh after the scanning process ?
We already commented but I really love your design. Congrats! Also, the light is an extra point!! ;)
Show how you designed and made your object (screenshots of fusion) and explain why it could not be made subtractively is missing.
Upload your design files to the gitlab repository.
Add some screenshots of the CURA process and explain the basic settings: Infill, raft, supports, Layer hight, etc… (You just explain layer height and speed)
Explain basic configuration and workflow with skanect. How did you post process your mesh?
Explain the basic CURA settings configuration you use to print your miniyou.
explains the basic configuration you used for printing your object (layer height, speed, infill, structural support, etc …
Extra credit: Explain how it works Octroprint
Upload your design files to the gitlab repository, in the original editable format (antimony, solidworks, eagle, CAD, etc) and readable forms such as DXFs, STLs, etc.

Described what you learned by testing the 3D printers
Shown how you designed/made your object and explained why it could not be made subtractively  
Scanned an object
Outlined problems and how you fixed them
Included your design files and ‘hero shot’ photos of the scan and the final object

  -->




## Additive manufacturing

Additive manufacturing refers to manufacturing by layering material on top of other layers. To build an object these "sliced" layers are horizontal cross-sections of the object.


## 3d Printers

There a different strategies that 3d printers use to form objects from layers of shapes. These include:

* Extrusion - the most widely used 3d printing method through the Fusion Deposition Modelling (FDM) technology. Lines of solid thermoplastic filaments are melted and extruded and laid down by a nozzle path.
* stereolithography - uses a platform submerged in a tank filled with liquid polymer resin and a laser maps layers which solidify.
* Powder bed
* selective laser sintering
### Software (Slicers)
Software calculates the path of the nozzle by slicing the object into horizontal layers the slices from an imported CAD file. Slicer software commonly can import .stl format files but also .obj. and .vrml and .plpy

These softwares transform CAD files into .gcode - a code that is used to control motion of the printer (and other machines like CNC machines use gcode too)

<!---

   -->

### 3D Printing Guidelines

* Everything in a CAD drawing needs a thickness - a 0.5mm minimum is recommended. Clean the drawing so there are no shapes within shapes.
* The model must be made water tight with no bare unjoined edges. You can use [Meshmixer](http://www.meshmixer.com/) to analyse if your drawing is watertight.
* Avoid intersecting geometry because this adds work for the printer. a solid model has no coincident or intersecting geometry because it is its outer surfaces, once they are completely joined.
* The CAM software slices the model into horizontal layers that are printed in 2D and the height of these needs to be specified in the CAM software. Layer heights are usually between 0.15mm and 0.3mm but the initial layers are made thicker to ensure a sturdy base.
* Layer height and nozzle size effect resolution. Resolution effects the print time and the amount of material used with high resolution using more material and taking more time.
* The materials used depend on the machine but there are a few options per machine which should be matched to the application. The two most common materials are PLA and ABS. PLA  is easy to print and can be made from renewable sources. ABS works at higher temperatures and is stronger. Nylon is is more flexible and gives less friction
* The filament manufacturer will state what the temperature needs to be for the filament to be printable.
* You need to draw in support structures for any overhangs where the material curves out more than 45 degrees to the vertical.
* The thickness of the wall is called the shell and this allows programming of a good finish surface to a lighter internal structure - which will save time and material. This can also produce very light yet sturdy objects.
* Infill is the material inside the model and usually makes up 20-90% of the object and affects the strength of the object, the amount of material used and the print time. There are different infill options that the slicer can set.



## Design and 3D print a small object that cannot be made subtractively.


<!---  
complex hollow custom three-dimensional structure is required it can only be made by additive manufacturing.    -->

3D printing is helping researchers to test structures that remained theoretical until 3D printing made it cost effective to test these structures out.

One area of research that is benefitting from 3D printing is acoustic metamaterials - these solid repeating shapes cause bulk effects that the material does not demonstrate otherwise such as negative refractive index (bending waves in the opposite direction to what they normally do), cloaking and acoustic diodes that only allow sounds to pass in one direction.

>Encouraged by the burgeoning 3D printing, topology optimization have successively accomplished the metamaterials design and then reached the desired performances

*[Hao-Wen Dong et al (2018) Systematic design and realization of double-negative acoustic metamaterials by topology optimization
](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf)*


I thought I would try and 3D print an acoustic metamaterial this week because 3D printing provides a **cost effective** and ** fast** way to **prototype** these intricate shapes and I will be investigating these for my final project.


### Space-coiling acoustic metamaterial by Hao-Wen Dong et al

Space-coiling  metamaterials have intricate cross sections and require precision. These could be milled in theory but would need very precise milling tools and strong (expensive) material that would not vibrate too much during cutting. Labyrinthine coiling configurations of small scales would also be hard to cast as they require some very sharp edges.

I thought that eventually I would make a completely enclosed metamaterial acoustic device 3D printed in one piece which form a tamperproof structure that would be hard to open.

I tried to print a small set of space-coiling meta-atoms designed in a [paper by Hao-Wen Dong et al (2018)](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf) as this paper specified most of the dimensions needed and could be tested in the audible range of 2200 Hz. The paper also documents that the structure has been printed in PLA

### Drawing the model

I used Rhino to draw the model because I know how to draw things to scale in Rhino and a scale replica of the design would enable me to replicate the experiment in the paper. The dimensions of  the cross section of the meta-atom (unit of a metamaterial) are given below

<center>![](assets/metaatom.png)</center>

*<center>[Dong et al (2018) Systematic design and realization of double-negative acoustic metamaterials by
topology optimization](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf)</center>*

I drew an irregular grid on Rhino to represent the dimensions I knew and traced the outline of one basic shape of the cross section and used the CLOSE CURVE command to ensure the piece would not have any open curves.

![](assets/grid.png)

I realised that I could rotate a basic "K" shape to complete the cross section  so I used the JOIN tool to join all the lines in the K shape and used the ROTATE tool to rotate copies of this unit to speed up the drawing process.

![](assets/metaunit.png)

#### Choosing the size of the shape

Once I had this completed cross section I made an array of atoms as shown in the paper by copying it six times and moving them into a grid. I chose 6 shapes as Mikel our 3D printing expert advised that this would take around 4 hours - which I felt was about as long as I could justify for a piece that was the first one I ever did. Also I did not want to waste a lot of material on something I did not know would work.

#### Extruding into 3D

Next to extrude this shape into I removed all the square guides, selected all the shapes in the array and used the EXTRUDE CURVE command to extrude the cross section into the z axis by 5cm specified in the design.

<center>![](assets/printprocess.png)</center>

#### Making it watertight

I then closed the curves using the CLOSE CURVE command

I selected the polysurfaces created by the extrusion and moved them away from the base shapes and used the SHOWEDGES command and selected “naked edges". This revealed no naked edges so the polysurfaces were watertight.

![](assets/edgewireframe.png)

#### Format for Export
I saved the file as a .stl file

![](assets/scoilobj.png)

#### <center>You can download the .stl file from **<font color="lime" background-color: lime>[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/acousticscoiling.stl)</font>**</center>

### Preparing the print in Cura for the 3D printer

I used the **[Creality CR-10S5](https://www.creality3d.cn/creality3d-cr-10-s5-p00099p1.html) printer** which:

* uses FDM (filament extrusion) printing and
* with 1.75mm filaments
* and a nozzle diameter of  0.4mm.

![](assets/Creality.jpg)

In the Fab Lab it is controlled with [Cura software](https://ultimaker.com/en/products/ultimaker-cura-software)

I used PLA plastic filament as specified in the design.

Ultimaker Cura supports the following 3D file formats: 3MF, OBJ and STL. The 2D image file types supported are: BMP, GIF, JPG and PNG.

I opened my STL file in Cura. The print setup panel in Cura has several dropdowns for settings.
including:

* Quality
    - Layer height: 0.2mm (for a finer finish)
    - Line width: 0.4mm (set by the nozzle of the printer)
    - Initial layer height: 0.3mm
* Shell
    - Wall line count: 2
* Infill
    - Infill: 10%
* Print Set up
    - Printing temperature: 205degC
* Speed
    - Travel Speed: 120mm/s
    - Print speed: 60mm/s
- Flow 100%
- Retraction: 6.5mm

Click PREPARE to see the visualisation of the settings

![](assets/layers.png)

Click SAVE to file to save the settings which is a .gcode file ready with instructions for the machine.

Click PRINT to send  the Gcode to the printer.

### Printing the object

**TIP: The platform was sprayed with hairspray to improve it's stickiness for the important first layer on top of which the entire print rests**


</center>![](assets/acoustic.jpg) ![](assets/acoustic2.jpg)</center>


#### Afterthoughts

I realised after the print that really I should have printed the entire casing of the material to hold the piece into place and to maximise the the ability for 3D printing to created such enclosed spaces.


#### Other acoustic metamaterials for future experiments

* I thought I could try modelling this [ultrasonic brick array kit by a team in Surrey University](https://www.nature.com/articles/ncomms14608) parametrically using Grasshopper but I did not manage to draw this because I don't yet understand how to control curves with Grasshopper. Progress on this research from the team [here](https://www.researchgate.net/publication/322143783_Review_of_Progress_in_Acoustic_Levitation/figures?lo=1
http://docplayer.net/48975283-Building-acoustic-landscapes-with-meta-bricks.html)
* Acoustic voxels - I want to try using the Monolith plugin for grasshopper to draw [acoustic voxels as made by this team from Columbia university](http://www.cs.columbia.edu/cg/lego/acoustic-voxels-siggraph-2016-li-et-al.pdf)
* Sound absorbers. Some tried 3D printing methods highlighted here
    * https://github.com/juliendorra/3D-printable-sound-absorbers
    * [This one by Bin Liang et al is easy to draw](https://www.degruyter.com/view/j/nanoph.2018.7.issue-6/nanoph-2017-0122/nanoph-2017-0122.xml)  
* [Digital metamaterials (metamaterials that compute waves or forces) are amazing!](https://pdfs.semanticscholar.org/47de/919aefe501e3b90ec61cbb80fb2c5e399ee6.pdf). They use a material shape to compute a specific type of equation on an incoming wave. I wonder if we can use them to transform information from the noisy acoustic waves that surround us.



## 3D Scanning an object with Kinect camera and Skanect software
My classmate Veronica helped to scan me using a Kinect camera. This is an infrared laser and camera and we plugged it into a computer and read it with [Skanect](https://skanect.occipital.com/) scanning software.

I was interested to hear that the Kinect device is due to have [a redesign "to create systems that use .. basically tech, sensors and things that are some way away from a central server or cloud."](https://www.theinquirer.net/inquirer/news/3071580/microsoft-azure-kinect-official)

<center>![](assets/Veronica.jpg)</center>

### The process was:

1. Plug the Kinect into power socket and a  USB port in the computer.
2. Open Skanect software and prepare the settings **in the Prepare tab**:
    * I input the scene (what the camera should highlight)
    * the size of the bounding box of the scan area (1 x 1 x 1)
    * I also chose a place to save my file by clicking the PATH button.

<center>![](assets/prepare.jpg)</center>

3. **In the Record tab:**
  You click RECORD and you can see live how your object looks. the aim of the scan is to try to keep the pixels green. You need to move slowly and ensure the camera has been rotated around the entire object or the object is completely rotated. In my case Veronica held the camera and I twirled around very slowly.


  <center>![](assets/mebeingscanned.jpg)</center>


4. I Clicked into the **RECONSTRUCT tab** to piece all the data together. At this stage you can see whether or not the camera has read your object as you want it to look (more or less).

<center>![](assets/reconstruct.jpg)</center>



5. In the **SHARE tab** you can save the file as .svg or .obj in the file specified.


###  Design files

#### <center>You can see and download my .obj file **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/scanofme.obj)**</center>

### Hero shots

I opened the file in Rhino for these images

<center>![](assets/scanviews.jpg)</center>

<center>![](assets/3dscan.jpg)</center>

I was especially pleased with how well the fabric of my sweatshirt scanned

<center>![](assets/fabric.png)</center>

### PROBLEMS!

As you can see I have a hole in my head - which sums up how I feel today so I'm ok with the result. I could use the file to make a plant pot or something. I am told that I can  fix the generated mesh using a programme called [Meshmixer](http://www.meshmixer.com/)

<!---   
### Further ideas to explore
* https://www.nature.com/articles/srep42650  -->
