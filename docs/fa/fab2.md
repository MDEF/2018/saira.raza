

# <center>Fab Academy Week 2 </center>
# <center>Computer-Aided Design</center>

<center>![](assets/)</center>

<!---  --> <!---  -->

<!---  Missing 2d design files.
 what did you test, what you learn, and what are your conclusions. For example, why Solidworks is a parametric 3D software?
key commands and tools you used to generate the files, steps, etc.
I like a lot your programs analisis! Very good.
Missing files.
Would be nice to include the image & Illustrator file, as well as in the original editable format (antimony, solidworks, eagle, CAD, svg, etc).
Not clear what did you design during week.
No 2D software?? You should document at list one.
What happen with Grasshopper? If you are not gonna update, better remove
All your design and fabrication files should be in the archive (Gitlab). Google drive it’s not allowed as a storage service

Modelled experimental objects/part of a possible project in 2D and 3D software
Shown how you did it with words/images/screenshots
Included your original design files

  -->

<!---   -->


This week was really interesting for me as Interfaces form a branch of enquiry for my final project and we were looking at the Computer Aided design interfaces.
We went on a  safari of interfaces with Neil and to greater depth with Santi which included:

**Voxel software** - allows assembly of 3d structures using 3D pixels known as voxels which can be defined within a cube shape. This would be especially useful for designing metamaterials.

**Fusion 360** - Playing with material and tool parameters and simulation

**Grasshopper** - Parametric design. The intuitive UI uses a machine analogy.

**Blender** - a rendering tool

Hearing about these different tools made me think of the amazing the amount of observation that goes into these programs. Neil Gershenfeld shared with us this [thesis by Matthew Keeter on ways of representing objects for digital fabrication](http://cba.mit.edu/docs/theses/13.05.Keeter.pdf) which gives a good grounding in how we use geometry to interface with the environment and tools.

Victor explained how the designs should lead the tools (software / languages ) you use.


<!---
## Assignment


### Try as many CAD programmes as possible Learn one that can propagate parameters.
Focus on:
- fusion (free we have tutorials),
- solidworks(issue tracker request),
- xdesign (issue tracker request),
- Onshape(free)
- freecad
- look at game engines and sim environments
set up video?

### Take a potential project and represent in many different ways that you can  - render, 2d, 3d, represent in as many different ways as possible digitally.

-->

## 2D images

### Drawing by hand

Neil explained how hand drawings can be digitised easily and can bring expressivity to designs.

### Raster images

Raster graphics or bitmap images are composed as a matrix representing a generally rectangular grid of pixels (points of colour).
Common raster image file extensions include .BMP, .TIF, .JPG, .GIF, and .PNG.

### Vector images

Vector images are made of thin lines and curves (paths)  defined through mathematical theory.  this intricate wireframe-type includes defined node positions, node locations, line lengths and curves. Each image can scaled without losing resolution.
Common vector image file extensions include .EPS, .AI, and .SVG.


## 3D images

There are a number of strategies to describe geometry in 3D:
3D models are typically comprised of polygons and verticies and that create the model's shape. They may also include textures as well as lighting and shading information.

Common 3D image file extensions include .OBJ, .STP, and .MA files.



Neil took us through some other strategies for 3d cad programmes including

* Constructive Solid Geometry, - ability to do logic on shapes
* constrained, - constraint solvers can tangency and distance
* hierarchical, - allows changes of many instance of a sgape
* parametric, - parameters can be changed and everything adats
* procedural, - algorithms do the design
* algorithmic
* boundary (b-rep), represent surfaces - more po
* function (f-rep) representations - rep objects rather than surfaces
* GUIs, scripting,  - writing code to describe shapes
* hardware description languages
* imperative,
* **declarative, describe what you want the design to do**
* generative, what you cant do by hand -
* optimization,  highly iteratuve optimisation  - -search
*  **Multidisciplinary Design Optimization - modelling includied**

#### Highlighted programmes include

* Free Cad - 2D with constraints and some 3D
* Blender - powerful rendering - organic drawing and animation
* Grasshopper  - node editer - information editor for parametric for Rhino
* Kangaroo for Grasshopper  constraint tools
* FreeCAD - based Open cascade Gemometry engine - getting very powerful does boolean  - does linked copies of objects - like in inkscape - you can add a SPREADSHEET with formulae which basically work like grasshopper if you know how to do functions in spradsheets (or you could manually adjust the spreadsheet)
* Inkscape dows 2D booleans


### Polygon meshes
These are defined by a collection of vertices, edges and faces. .obj files are meshes and 3D printer software uses these to translate into movements in 2d layers. 3D Scanners are also able to translate data into this format.

### NURBS
This is modelling using maths - Rhino uses this which is probably why there are so many plugins for it.


## Rhino - NURBs

### Acoustic tunnel

From [this design of a 2d acoustic diode](https://aip.scitation.org/doi/10.1063/1.4930300), I made a 3D acoustic tunnel and tried to see if I could make them into headphones

I started off by drawing in the XY plane and using the RECTANGLE tool to ensure the footprint was the right dimensions. Then  I drew an outline around this and joined the curve using the POLYLINE tool.

![](assets/tunnelprof.png)

Then I polar extruded the cross section along a circle using EXTRUDE CURVE ALONG CURVE command. I had to draw a vertical circle to do this

![](assets/polarlines.png) ![](assets/polarextrude.jpg)


I drew a box shape roughly the dimensions of my head using the RECTANGLE tool and the EXTRUDE CURVE tool (shown in pink below)
![](assets/headphones.jpg)
![](assets/ghosted.jpg)

#### <center>The Rhino file for these drawings can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/acoustictunnel.3dm)**</center>

<!---  
check also
Acoustic Black Holes in Structural Design for Vibration and Noise Control
Chenhui ZhaoOrcID and Marehalli G. Prasad *

-->

#### Afterthought
This tunnel is designed for operation at around 6000 Hz (a very high annoying pitch). Lower frequencies will likely need even bigger dimensions.

However recently this team produced a 3D printed tunnel which works lower frequencies



### Transformable meta-atom by Johannes T.B. Overvelde

[This paper by Johannes T.B. Overvelde et al shows how 2D moduls can be used to make 3d transformable structures](https://www.researchgate.net/publication/312508299_Rational_design_of_reconfigurable_prismatic_architected_materials).

I copied one of these by eye by drawing 2D 2cm squares using the RECTANGLE command.

More details in [machine cutting week documentation](fab3.md).



## Illustrator - Vector drawing

I drew a spiral for the vinyl cutter in Illustrator. I find Illustrator's UI a bit bewildering - Adobe in general seems to design UI to leave people out rather than make things obvious. This also keeps their software pretty samey over the years. Also the price of these design-standard formats has createde new design cultures based on opensource software.

#### <center>The .ai file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/spiral.AI)**</center>

## Monolith - Voxel drawing

I wanted to try using a Voxel CAD package so I tried Monolith by Autodesk.
It uses exotic files such as .lith, .vol, .exr, .vti.

Like with Fusion - I wasn't quite sure what to do with my mouse to make things happen.






## Grasshopper - Parametric modelling

I really wanted to understand how to use Grasshopper as I really like the machine analogy interface however I found that finding the components is the main barrier to using it. I tried to make this metamaterial brick kit by [Memoli et al](https://www.nature.com/articles/ncomms14608) but could not find a clean way to draw the curve.

I did however manage to make a series of rectangles whose dimensions I could vary with a slider.

#### <center>The Grasshopper setup file can be downloaded **[here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/rectangleextrusions.gh)**</center>

There were some basic UI problems I had with Grasshopper such as how do you remove a component from your work area. I think it will take a number of examples for me to feel comfortable with this but I hope I get one chance to use it for my project.

Some weeks later Santi showed me that **DIVIDE CURVE component can be used to define positions on the outline of a shape** and that the **POLYLINE CURVE tool can be used to draw shapes between defined points**.

![](assets/ghtriangle.png)
![](assets/ghtriangleview.png)

I will experiment with this component and intend to do some tutorials on Grasshopper.
