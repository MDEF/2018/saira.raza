

# <center>Fab Academy Week 11 Outputs</center>

<center>![](assets/bmotor.gif)</center>
<center>![](assets/steppermotor.gif)</center>

<!---  Missing eagle files
If you didn’t change the Neils code you need to explain deeper how it works. How is structured and how do you understand the logic of the code. It’s not enough to replicate the others work. You should add something.
I know you work in the LCD screen output, can you document, please?
Same comment as before. Direct the download links to the specific folders, please!
Explain the key point of your code. If you used Neil’s code, explain what did you change or how did you understand the code.
Do not repeat information (no sense). You already add the information on Attiny 44 in the Embedded Programming week , it will be enough to put the link to that week.
It would be good if you specified what information was relevant to you to program the motor Stepper.
Show some pictures of how did you design it(eagle).
Explain a bit more how did you program the board. Seems you did by terminal (Neil’s code) and another with arduino? Did you use Fabisp or AvrmkII? Also what did you learn from Neil’s code?
Did you have problems?
I am sorry if I made your task difficult during the assignment, due to the lack of mosfets N. ;(
Only as a detail, it seems that the link of the last photo is broken. Check it out please.  

OUtput something  - replicate a neils board
Demonstrate workflows used in circuit board design and fabrication
Described your design and fabrication process of your OWN DESIGNED BOARD
explain the programming process you used and how the datasheet helped you
Cut/solder/programm one of the examples to understand how it works and modify one or make a completely new one.
Described your problems and how you fixed
include your design files and photos

 -->

##### **Resources:**
* http://fab.academany.org/2019/labs/barcelona/local/wa/week11/
* http://academy.cba.mit.edu/classes/output_devices/index.html
* http://fab.academany.org/2019/labs/barcelona/local/clubs/electroclub/outputdevices/

This week we were taken through some options for outputs from circuits that we build. We learned how voltages can:
- produce electromagnetism to move magnets and produce rotational and linear movements.
- emit photons
- cause piezoelectric vibrations in materials like quartz

We learned about some control strategies used in these

| Strategy | Output | Component | Control methods |
| --- |-----|---|---|
| Electromagnetic | | Stepper Motor| 1 h bridge control for unipolar, 2 for bipolar often come as driver boards|
| Electromagnetic | | Servo motor | always have a control chip and use a potentiometer to measure the position of the gears. It uses 50 pulses/ s instruction trigger and a library to send an angle converted to a pulse with a specific "on" length |
| Electromagnetic | Sound | Speaker | |
| Emit photons through a diode | Light of a given colour | LED | |
|Emit photons through 3leds | | Addressable LEDs or Neopixels - ws2812 | a small computer through one wire it receives information. there is a library called fast led that you can use with attiny |
| Quartz-Based Piezoelectric Materials which vibrate with voltages | Sound | Piezo buzzers | the tone function in arduino can control the frequence of the piezo : tone(pin, freq) |
| Liquid crystals which orientate magnetically| displays | Liquid crystal display | |
| Electroluminescence and bioluminescent | | | ||


## Controlling current

We can control an electronic current by setting on and off conditions in microcontrollers eg using digitalWrite (HIGH) or digitalWrite (LOW) in Arduino.

OR using **Pulse Width Modulation (PWM)** which is controlling the width of pulses of signals. This enables more power to be transferred in a smaller circuit with less heat. Dimmers for LEDs work like this.
where analogueWrite can be a point in the middle of high and low by calling

    analogWrite(pin, value)

where
pin int: The PWM output pin number
value int: a value in the range 0-255 representing the current pulse width that is used

control coil of dc motor - can be done with a PWM coding

<!---
## Some types of outputs
### Using Coils of current and magnetic fields for movement
We can use coils of electrical current to produce electromagnetic fields to move objects.
This is commonly used in motors which are controlled using different configurations of static mounts and bearings to produce rotating parts. different types of control characterise different motors including:
* DC motors
* Brushless motors
* Servo motors are small DC motors connected with a potentiometer which varies resistance.. you can know the position of the motor - small chip read potentiometers and turns on and off
* Stepper motors - tiny micro stepping motors controlled by the amount of voltage
Electromagnetism can als be harnessed to move in 1 dimension with a number of application
* Relays, solenoids, valves, etc use a current through a small coil typically producing a digital on or off state.
The current can be used to move a magnet attached to cone to produce sound in speakers.
The standard audio sample rate is 44khz normal (with audible rate between 20 to 20,000 Hz)
### Turning current to photons
An electric current can be used to emit photons to produce light as in LEDS.
### Addressable LEDs - Neopixels - ws2812
3leds and a small computer through one wire it recieves information - the protocol to send data is complicated but there is a library called fastled that you can use with attiny - addr
### LCDs
Liquid crystal display
sandwich of layers contain liquid crystals - microorientation from positions
https://www.researchgate.net/publication/235415638_Electro-optical_control_in_a_plasmonic_metamaterial_hybridised_with_a_liquid-crystal_cell/figures?lo=1
PAL TV
Video - like phono lead
outputting digital video is not possible from the attiny
Electrolumniscent  - bioluminescent
### -->


### XY scanning stage

I am trying to build a 2D cartesian xy scanning mechanism to mount my sound sensor.

I first looked MITs [Machines that Make projects](http://mtm.cba.mit.edu/index.html) as it looks at ways to make machines in a fab lab.

I found this [Cardboard CNC by By James Coleman and Nadya Peek](http://archive.monograph.io/james/m-mtm) that is controlled by a [Gestalt Node] - a small electronic control board that can be easily networked connected to a Fabnet USB board which can plug into a laptop. Gestalt framework is a high level Python code that powers it all, you can see it on Gestalt GitHub. The Gestalt Framework for Virtual Machine Control uses a Python interface the calls a remote interface through http requests.

I still don't "speak" Python so I decided to try using an Arduino for starters (again!)


## Arduino, ready made driver board and bipolar stepper motor

I used a cheap [28byj-48 5vdc stepper motor](http://robocraft.ru/files/datasheet/28BYJ-48.pdf) and a driver that came with my Arduino kit and managed to get it to turn once in one direction and then in another however I could not work out how to make it turn a little, stop for a measurement and then continue.

Arduino IDE has a some libraries you can use to control stepper motor drivers.

I also found an library that converts GCODE code to Arduino (c?) codec called
GRBL

## Coding stepper motors using GCODE via Ramps 1.4 and Marlin

Oscar suggested that I could try and use GCODE to run a ready made plotter in the lab which runs on the RAMPS 1.4 3D Printer Controller Board. This controller board has a serial port that you can plug into your computer. The board does not have the pin adapter so I ordered one online.

I wrote this code to make the "nozzle" of the plotter scan a 12cm x 12cm area, 1cm at a time.

reprap gcode is here
https://reprap.org/wiki/G-code


      %
      OSCAN //Name of task
      G00  
      G90, for absolute position mode.
      G28, to rapid to the home position. - you havent set the origin yet
      G21 - Millimeters as units
        G53 - move in absolute coordinate system
        G92 X0 Y0 (set current position to origin <x,y>=<0,0>) - not sure this is always the case  - its for temporary offsets i think
        G1 X10 Y0 F100.0 (Move to postion <x,y>=<10,-0> at speed 100.0)
        G4 P5000 (wait for 5000 milliseconds)
        G1 X20 Y0 F100.0
        G4 P5000
        G1 X30 Y0 F100.0
        G4 P5000
        G1 X40 Y0 F100.0
        G4 P5000
        G1 X50 Y0 F100.0
        G4 P5000
        G1 X60 Y0 F100.0
        G4 P5000
        G1 X70 Y0 F100.0
        G4 P5000
        G1 X80 Y0 F100.0
        G4 P5000
        G1 X90 Y0 F100.0
        G4 P5000
        G1 X100 Y0 F100.0
        G4 P5000
        G1 X110 Y0 F100.0
        G4 P5000
        G1 X120 Y0 F100.0
        G4 P5000
        G1 X10 Y10 F100.0
        G4 P5000
        G1 X20 Y10 F100.0
        G4 P5000
        G1 X30 Y10 F100.0
        G4 P5000
        G1 X40 Y10 F100.0
        G4 P5000
        G1 X50 Y10 F100.0
        G4 P5000
        G1 X60 Y10 F100.0
        G4 P5000
        G1 X70 Y10 F100.0
        G4 P5000
        G1 X80 Y10 F100.0
        G4 P5000
        G1 X90 Y10 F100.0
        G4 P5000
        G1 X100 Y10 F100.0
        G4 P5000
        G1 X110 Y10 F100.0
        G4 P5000
        G1 X120 Y10 F100.0

I think this gcode is for marlin

#### PROBLEM
There was no molex connector to the 12V mains power supply

**SOLUTION:**
I ordered one online and after a few days I got power to the Ramps 1.4 board and Arduino.

The display board says it runs on marlin

Oscar mentioned something Marlin which I found out is firmware [here](https://solidutopia.com/marlin-firmware-user-guide-basic/)

>Marlin is an open source firmware for the RepRap family of replicating rapid prototypers — popularly known as “3D printers.” ..
One key to Marlin’s popularity is that it runs on inexpensive 8-bit Atmel AVR micro-controllers. These chips are at the center of the popular open source Arduino/Genuino platform. The reference platform for Marlin is an Arduino Mega2560 with RAMPS 1.4.

http://marlinfw.org/meta/gcode/

This firmware sets the baudrate and parameters of your CNC machines like temperature settings, extrusions, mechanical settings so I did not want to change these.

Then I was directed towards a [Calibration Guide](https://reprap.org/wiki/Triffid_Hunter%27s_Calibration_Guide) by RepRap

>RepRap takes the form of a free desktop 3D printer capable of printing plastic objects.

There is a RepRap display on the plotter so I assume that the plotter I have is a RepRap machine.  Reprap has its own firmware called RepRapFirmware. It turns out that the firmware interprets certain GCODE commands differently so I am not sure if the GCODE I have written is ok to send it.

It was still not clear how you put Gcode into the Arduino/Reprap. I think I can just upload it but I was a bit worried that I might wipe the Marlin off it.

>Marlin uses G10/G11 for executing a retraction/unretraction move. Smoothie uses G10 for retract and G10 Ln for setting workspace coordinates. RepRapFirmware interprets a G10 command with no P or L parameter as a retraction command.

>There are a few different ways to prepare GCode for a printer. One method would be to use a slicing program such as Slic3r, Skeinforge or Cura. These programs import a CAD model, slice it into layers, and output the GCode required to print each layer. Slicers are the easiest way to go from a 3D model to a printed part, however the user sacrifices some flexibility when using them. Another option for GCode generation is to use a lower level library like mecode. Libraries like mecode give you precise control over the tool path, and thus are useful if you have a complex print that is not suitable for naive slicing. The final option is to just write the GCode yourself. This may be the best choice if you just need to run a few test lines while calibrating your printer.

>Gcode can also be stored in files on SD cards. A file containing RepRap Gcode usually has the extension .g, .gco or .gcode. Files for BFB/RapMan have the extension .bfb. Gcode stored in file or produced by a slicer might look like this:
https://reprap.org/wiki/G-code

I think I'm supposed to use Python?

Then I noticed a sign for Formbytes came up on the display. When I looked that up I found [the plotter I had!](https://www.formbytes.com/en/). I found the User Manual [here](https://drive.google.com/file/d/10dFhXFs6bAP5BKTeYH-WeHd4rqeMLLr1/view). Its in Spanish. It tells you to touch the touch screen but the screen does not respond.

Then I found the [documentation for the machine I have in the Fab Lab Barcelona wiki](https://wiki.fablabbcn.org/Formbytes#Turning_on_the_machine). It says to insert an SD card to the command board and it will ask you which file you want to read.


#### Problem
Unfortunately I don't have a way of transfering my code to an SD card.

Then I asked Oscar if there were alternatives and he advised that I could use:
1. python script from my terminal
2. my Arduino IDE but as the onboard Arduino is quite old it would probably complain.
3. Use http://chilipeppr.com/ which runs commands remotely and has a graphical interface. It has Serial port and Terminal and even Grbl workspaces to send code.

So I thought I would try Chilipeppr.

I decided to test my GCODE out in this [online simulator](https://nraynaud.github.io/webgcode/) first.

#### PROBLEM

When I connected the Formbytes plotter to my laptop chilipeppr did not recognise anything . I used the TinyG workspace.

I looked for a way to tell Chillipeppr what port I was using but nothing was coming up in the serial port section except..

>Download the Serial Port JSON Server (SPJS) from inside the "Serial Port Widget" in lower right.

https://github.com/synthetos/TinyG/wiki/Chilipeppr

I can't see this anywhere on that widget.
There is a tab called "Your Servers" which had a download link. I thought this might be something to do with wireless control because there was a lot of references to internet of things for the code.

Next I tried http://chilipeppr.com/serialport
It lets you edit your workspace but in code. I have no idea what I'm looking at

![](assets/chilippr.png)

No widget dropdown, no workspace menu, just hard code I think to build your own serial communication  

>This is a workspace for a generic Serial Port console for ChiliPeppr's Hardware Fiddle. This workspace depends on ChiliPeppr's Serial Port JSON Server.

I look for software to connect my USP port to the Formbytes machine

I think this is it https://github.com/chilipeppr/widget-tinyg

These instructions are for coders

      // You should right-click and choose "Run" inside Cloud9 to run this
      // Node.js server script. Then choose "Preview" to load the main HTML page
      // of the script in a new tab.

      // When you run the main HTML page of this script it does all sorts
      // of convenient stuff for you like generate documenation, generate
      // your final auto-generated-widget.html file, and push your latest
      // changes to your backing github repo.

So I give up because I don't understand any of the instructions.

(at this point it has taken 3 hours to figure out how to send GCODE through my  laptop to the  machine )

Eduardo suggests to use Firefox

I try the Serial port workspace in Chilipepper and its the same

I get an SD Card and a USB adapter in and [here](https://wiki.fablabbcn.org/)Formbytes#Installation
I learned to push the knob click it twice to move the menu.

https://forum.formlabs.com/t/does-form1-support-support-sd-card-or-other-connections-besides-usb/42

#### PROBLEM
ReRap is not reading the sd card directly

Eduardo tried to interface through his laptop through a software called Repetier-host which you have to download. it has a controller in the environment - so you can test without putting in gcode.

**SOLUTION: Check power wiring of the printer is not faulty**

He found that the power switch had a lose wire

Once he fixed this he was able to control the plotter from Repetier.

**SOLUTION: Change baud rate of printer to 115200 in controller**
He had to change the plotters communication rate to 115200 as this is the baud rate that the RepRap works at for these sorts of codes.


SOLUTION: For chilipeppr Download JSON Server (SPJS) to communicate via server

He also tried chilipepper with tinyg and found the  download the Serial Port JSON Server (SPJS)  that I saw earlier. This actually was the software that would detect hardware on serial ports and runs the code on a server from your computer. It detects the Adruino of the plotter and and lets you communicate to it.

I can manually put GCODE into Chillipepper now but I need to send a file .

I put my GCODE into a .txt file and uploaded it to Chillipepper tinyg with the arduino hardware plugged in and detected - it gives a bunch of errors bout checksums of line numbers.

I googled "upload GCODE files chilipeppr" and I got this
https://github.com/synthetos/TinyG/wiki/Chilipeppr - reams of reading to get a simple answer. and no answre just more about coding.

This is what I get in the GCODE section
https://github.com/synthetos/TinyG/wiki/Gcode-Support

Santi suggested I could try renaming the file as a .nc file.

#### PROBLEM - cannot upload a file of GCODE into chilipeppr

I could not do this in wordpad and when I did it manually it didn't word like it does with .md files.

I don't know what a .nc file is so I searched online how to turn make a .nc file - no simple answer - more coding and something to do with autodesk.

![](assets/plottertc3.png)

I found this:
>GCODE files are created by slicing programs, such as Simplify3D and Slic3r, that translate CAD drawings into G-Code, which a 3D printer can read.

https://fileinfo.com/extension/gcode

I think you cannot just write in text which i thought was is strange because you can write it straight into control software
https://www.simplify3d.com/software/features/

I finally found the technique for changing a .txt file to a .g file:

**TIP: when you want to change a text file's extension from .txt** click save as select 'all types' from file type dropdown and type the xtension (.g) at the end of the file name

![](assets/gcodesave.png)

i opened this in repetier and cura and it seems to look right

![](assets/scanpath.png)

and on chilipepr
![](assets/chiliscan.png)

Connecting the software was problematic again,
neither software was connecting to my port.
Repetier also has a server connection option which I tried but couldnt get to work.

**TIP:** Only have one software trying to communicate with your serial port at a time.

**TIP:** Don't forget to change the baud rate of your port in your operating systems Device Manager if you change it in software communicating through that port.

IT WORKS!

So the
#### Procedure for sending GCODE to Repetier
-  write your gcode in .txt save your.txt file without a .g on the end at this point
- test it in...
- now save as select 'All files' from the drop down and put a "".g" at the end of your file name and save.
- power the printer
- connect to computer
- go to device manager>ports in windows
- find your port and change baud rate to 115200
- open Repetier
- go to printer settings on the top right and change baud rate to 115200
- in Repetier load your gcode in .g format by clicking the load button in the top left.
- you can check the file path again by going to the Print Preview tab in the right hand panel and tick 'show travel moves'
- still in the print preview tab, click the edit gcode button to check the GCODE. This will open the 'GCODE Editor' tab, here you can check you have the code you want. You can click 'New Text' to clear the
- click connect on the top left
- click print

#### Adjusting the code

The speed is really slow between measurement points so I took it out.

I also took out G28 which makes the sensor go to the origin

The final scan takes around a minute and a half which I think is acceptable for a demonstration.


The final GCODE for the scan is [here](https://gitlab.com/MDEF/saira.raza/blob/master/docs/fa/assets/120x120x100mmSCAN.g)


<!---
[This paper by a postgraduate student from Sinhgad Academy of Engineering](http://www.ijirset.com/upload/2015/september/21_Design.pdf) uses two bipolar Stepper Motors coupled with a lead screw as a feed drive by means of aluminium flexible coupling.
Motors are further interfaced with PC via Arduino Microcontroller board and use the Arduino Motor Shield board to control the movement of XY Scanning Stage.


Making XY Plotter
- 2x ULN2003 stepper driver + 5volt stepper
- Arduino UNO
- Micro servo
- Breadboard
- Jumper cables
- Fishing cable
- 4x 25cm 8mm axis
### Arduino Stepper one stepatatime example
http://robocraft.ru/files/datasheet/28BYJ-48.pdf
Due to the gear reduction ratio of *approximately* 64:1 it offers decent torque for its size at speeds of about 15 rotations per minute (RPM).
      #include <Stepper.h>
      const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
      // for your motor
      // initialize the stepper library on pins 8 through 11:
      Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);
      int stepCount = 0;         // number of steps the motor has taken
      void setup() {
        // initialize the serial port:
        Serial.begin(9600);
      }

      void loop() {
        // step one step:
        myStepper.step(1);
        Serial.print("steps:");
        Serial.println(stepCount);
        stepCount++;
        delay(500);
      }


Inkscape - GCode - Processing  - Arduino
Hardwar
https://www.arduinolibraries.info/libraries/cheap-stepper
added to code
https://anthscomputercave.com/recycling/stepper/use_stepper_motors_from_printers.html

https://courses.ideate.cmu.edu/16-375/f2017/text/ex/steppers.html
-->


## Piezo Buzzer control using Tone in Arduino

For my project I made a simple breadboard circuit using an Arduino and a Piezo buzzer.

I used the Tone function which lets you set the frequency that the buzzer vibrates.


## Ultrasound

### UST-40T Transmitter

These need a lot of power to work

### Producing highly directional sound from 40kHz ultrasonic transducers

Again a lot of power is needed for the kinds of signals we need to send.
This is too dangerous for me - I destroyed one USB in my laptop already for connecting a motor with an external power source to my laptop.


https://hackaday.io/project/9085-ultrasonic-directional-speaker-v1

>this speaker works by pumping out a 40kHz square wave, then varying the duty cycle. The human ear would respond to the varying duty cycle. This means that the ultrasonic transducers are continuously emitting full power.

https://www.instructables.com/id/Body-ultrasound-Sonography-With-Arduino/

- requires two 9V/1A power supplies for the symmetric +9/GND/-9V supply
- a step-up-converter for the needed 100V for 5 USD: 100V boost converter
