

# <center>Atlas of Weak Signals</center>

<center>![](assets/)</center>


> The Atlas of Weak Signals is a navigating and analysis tool that will help to document weak signals of future trends based on topics curated from the MDEF course content. each week we will focus on a different set of weak signals and with our tutors from Elisava we will learn tools to analyse data sources of information on these topics.


## Mapping signals

### From data scraped from selected urls using beautiful soup python library

<iframe
  src="https://embed.kumu.io/8fa88b7808d463b52c19989fa56a78f3"
  width="940" height="600" frameborder="0"></iframe>

### From Keywords mapped by discussion (work in progress)

<iframe
  src="https://embed.kumu.io/7932579a85ffb4c1591ea999b80cbd85"
  width="940" height="600" frameborder="0"></iframe>


### Connecting weak signals to MDEF Intervention groups using assigned keywords and most prevalent scraped keywords

<iframe src="https://embed.kumu.io/f772ffb47ea13c4ec45a8a5af0434889" width="940" height="600" frameborder="0"></iframe>

## Weak signals for my project

<iframe
  src="https://embed.kumu.io/410e6a9bd835cb698cf8ad29fbdbe485"
  width="940" height="600" frameborder="0"></iframe>


#### Long termism
Metadesign as an adaptive expansive culture implicitly addresses the long term effects and needs of design. Tools are built with emerging needs and technologies in mind and it does this by promoting more and more open knowledge, technological and social systems.

#### Technology for equality

The drive and message behind my project is the belief that everyone is a a designer, an artist, a scientist or a maker. Jose Luis presented one speculation that with UBI and more automation and changing notions of work that there may be a renaissance of creativity. This idea is what metadesign envisages and encourages. A reason I find this interesting is because of my experience of learning through making this year and the therapeutic effects of making on mental health.

Another facet of equality is inclusion of people by using technologies that make information accessible to people with different learning preferences and literacies. I hope to design an interface that is engaging and accessible to people who have limited literacy or technical literacy.

#### Online identities
I found in trying to build a narrative for my project that technology mediates out relationships with material things and other people and this in turn changes who we think we are. I would like to communicate my project in a way that reflects this profound aspect of technology. I look forward to looking at emerging technocultures  and their storytelling techniques which I might be able to use as inspiration for storytelling techniques.

#### Carbon neutral lifestyles - incentives to habits and ways of living
My project aims to help people to connect with material realities as makers and not as consumers to allow them to experience how material design decisions are another way of organising our resources rather than being directed by corporate, top-down, capitalist, competing machineries.



## Weak signals that impact me and my future

#### Attention protection from the erosion of words
I rely heavily on technology to help me process information. I feel in the future tools that improve our ability to understand and use links and patterns in information.

#### The truth wars

I see evidence that the internet is enabling an acceleration of the misuse, spreading and erosion of the meanings of words. We are losing complexity encoded in words as they get reduced to dog whistles to separate people into teams and memes to recruit people into broad, vague, mostly online cultures.

The internet is also enabling communities to develop of people who don't trust the scientific method, journalists, experts or the need to establish facts to guide political decisions.

This team-sports attitude towards politics which is mostly led by faith already affects my ability to study or work in Europe and has promoted amongst British people a scapegoating attitude towards immigrants and minorities.

The higher education system in the UK is not affordable for most people and is not considered a right but a privilege.

There is a desperate need for a tool or a narrative to set out the ideologies and power structures at play. It's an important time for storytelling.


#### Exploring identity
The last few years have been an interesting time for me to notice changing attitudes towards recognising and representing factors of identity.

<!--- Nobody prepares you for getting older than 40 - and nobody prepares you for

##### Non heteropatriarchical innovation

<!---    The most obvious one I have noticed has been the struggle to articulate and represent feminism against a tide of resistance. The word feminism is being used to mean different things by different people. -->


##### Non-western centric futures

##### Disrupt ageism


##### Welfare state 2.0
