<html>
<head>
<title></title>
<link rel="shortcut icon" href="../../img/mandala3.png">
<style>


.overlay {
position: fixed;
top: 0px;
left: 0px;
pointer-events: none;
z-index: 50;
}

.overlay2 {
position: fixed;
top: 800px;
left: 800px;
pointer-events: none;
z-index: 10;
}

img.sticky {
  position: -webkit-sticky;
  position: sticky;
  top: 0;
  width: 200px;
}

.box {
position: relative;
width: 400px;
height: 300px;
float: left;
padding: 0px 0px 0px 0px;
vertical-align: none;
border-style: none;
box-shadow: none;

}
.screenshot {
width: 400px;
height: 300px;
overflow: hidden;
}

.medbox{
  display: block;
  position: relative;
  width: 150px;
  height: 150px;
  float: left;
  padding: 0px 0px 0px 0px;
  margin: 0px 0px 0px 0px;
  vertical-align: none;
  box-shadow: none;
  }

.smbox{
display: block;
position: relative;
width: 82px;
height: 86px;
float: left;
padding: 0px 0px 0px 0px;
vertical-align: none;
box-shadow: none;
}
.centeredtxt {
position: absolute;
text-align: center;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
background-color: black;
color: white;
}

.centeredtxt:hover{
background-color: black;
z-index: 50;
color: lime;
font-weight: bold;
text-decoration: underline;
}

.smbox:hover{
border: 2px solid lime;
}
.medbox:hover{
border: 2px solid lime;
}


</style>
</head>

<body>

<div class="overlay">
<img src="assets/transpheadlineextra.png/" style="width:100%; height:100%;">
</div>

<center>
<a href="https://mdef.gitlab.io/2018/saira.raza/index0">
<img src="assets/rgb-additive-colors.gif" width="300" height="300" style="vertical-align:middle;margin:0px 0px" title="̰d̶̡͈̮̻̺̝̖̝̈͊̒́̕ȍ̸̹͕̺̣͖̠̦̩͙̲̎͊̉͋̑̔̇͝n̶͍̭͍̮̞̱̩̓̏'̶̲̹̋̈́͌̑̏͝͝͝ẗ̴̞͉̜͙̠́ ̸̢̨̟̙̼̦̬̙̝̽͒̒̈ḓ̵͛͗̕ơ̴̛̩͔̩̫͙͑̅̄̏́̔͛͘ ̴̧̨̬̼̻̙̠͈̀̃͂̍̾̚ͅi̷̡̪̩̱̍̓̃͑t̶͎̱͍̭̫̐̂  🅳🅾🅽'🆃 🅳🅾 🅸🆃 alt="colours">
</a>
</center>

<!-- <center><a href="https://mdef.gitlab.io/saira.raza"><img src="assets/led.gif" width="498" height="500" title="Led" alt="colours"></a></center>--->

<center>
<div class="box">
<a href="https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4">
<div class="centeredtxt">PROJECT</div>
<img src="https://www.av8n.com/physics/img48/sphere-ylm-3.gif" width="300" height="300" style="vertical-align:middle;margin:0px 0px" title="PROJECT">
</a>
</div>

<div class="box">
<center><font size="2">This website is a log of my reflections from the 9 month Masters in Design for Emergent Futures course at IAAC and Elisava.
The appearance and content of this website develops with my skills and time to reflect.
</font></center>
</div>
</center>
<!--
<div class="box">
<div class="screenshot">
<a href="https://mdef.gitlab.io/saira.raza/fa/fab1/"><img src="assets/3dprinting.gif" style="width:400px;"/></a>
</div>
<p>Fab Academy</p>
</div>

<a href="https://mdef.gitlab.io/2018/saira.raza/PROJECTDOCS/REPORT0/"><img src="assets/inversedesign.gif" width="150" height="150" title="PROJECT" alt="PROJECT"></a>

<center><a href="https://mdef.gitlab.io/2018/saira.raza/about"><img src="https://media0.giphy.com/media/t0XXqlRWJ8uek/source.gif" width="150" height="150" title="About me" alt="colours"></a></center>
<br>
--->

<br>
<br>

<center>

<div class="medbox">
<a href="https://mdef.gitlab.io/2018/saira.raza/wa/week1/">
<div class="centeredtxt">TERM 1</div>    
<img src="assets/mars.gif" width="150" height="150" style="vertical-align:middle;margin:0px 0px" title="TERM 1">    
</a>
</div>

<div class="medbox">
<a href="https://mdef.gitlab.io/2018/saira.raza/mdd/materialdrivendesign/">
<div class="centeredtxt">TERM 2</div>
<img src="assets/smallBrainReco.gif" width="150" height="150" style="vertical-align:middle;margin:0px 0px" title="TERM 2" alt="TERM 2">
</a>
</div>

<div class="medbox">
<a href="https://mdef.gitlab.io/2018/saira.raza/fa/fab1/">
<div class="centeredtxt">FAB ACADEMY</div>
<img src="assets/3dprinting.gif" width="150" height="150" style="vertical-align:middle;margin:0px 0px" title="FAB ACADEMY" alt="FAB ACADEMY">
</a>
</div>

<div class="medbox">
<a href="https://mdef.gitlab.io/2018/saira.raza/designforthenew/">
<div class="centeredtxt">TERM 3</div>
<img src="assets/inversedesign.gif" width="150" height="150" style="vertical-align:middle;margin:0px 0px" title="TERM 3" alt="TERM 3">
</a>
</div>

<div class="medbox">
<a href="https://mdef.gitlab.io/2018/saira.raza/resources/">
<div class="centeredtxt">RESOURCES</div>
<img src="assets/links.gif" width="150" height="150" style="vertical-align:middle;margin:0px 0px" title="RESOURCES" alt="Resources">
</a>
</div>

</center>

<br>


<center>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week1/">
<div class="centeredtxt">T1 W1</div>
<img src="assets/mars.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="MDEF Bootcamp" alt="Week 1"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week2/"><div class="centeredtxt">T1 W2</div><img src="assets/dna.png" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Biology Zero" alt="Week 2"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week3/"><div class="centeredtxt">T1 W3</div><img src="assets/tool.png" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Design for the Real Digital World" alt="Week 3"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week4/"><div class="centeredtxt">T1 W4</div><img src="assets/colorcube.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Exploring Hybrid Profiles" alt="Week 4"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week5/"><div class="centeredtxt">T1 W5</div><img src="https://media.giphy.com/media/nHyt5zmBphwl2/200w_d.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Navigating Uncertainty" alt="Week 5"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week6/"><div class="centeredtxt">T1 W6</div><img src="assets/ainumber.png" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Design with Extended Intelligence" alt="Week 6"></a></div>
</center>

<center>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week7/"><div class="centeredtxt">T1 W7</div><img src="assets/solenoid.jpg" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="The Way Things Work" alt="Week 7"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week8/"><div class="centeredtxt">T1 W8</div><img src="assets/wavelet.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Living with Ideas" alt="Week 8"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week9/"><div class="centeredtxt">T1 W9</div><img src="assets/currentcoil.png" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Engaging Narratives" alt="Week 9"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week10/"><div class="centeredtxt">T1 W10</div><img src="wa/assets/lightcone.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="An Introduction to Futures" alt="Week 10"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week11/"><div class="centeredtxt">T1 W11</div><img src="assets/vortex.gif" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="From Bits to Atoms" alt="Week 11"></a></div>
<div class="smbox"><a href="https://mdef.gitlab.io/2018/saira.raza/wa/week12/"><div class="centeredtxt">T1 W12</div><img src="assets/field poles.png" width="82" height="86" style="vertical-align:middle;margin:0px 0px" title="Design Dialogues" alt="Week 12"></a></div>
</center>
<br>
<br>

<!--
<center><a href="https://mdef.gitlab.io/saira.raza/interests/"><img src="assets/cyandipole.gif" width="82" height="86" title="My interests" alt="interests"></a> <a href="https://mdef.gitlab.io/saira.raza/fictions/"><img src="assets/colourwave.gif" width="82" height="86" title="Fictions" alt="fictions"></a> <a href="https://mdef.gitlab.io/saira.raza/proposals/"><img src="assets/field.gif" width="82" height="86" title="Proposals" alt="Proposals"></a></center>
</div>

--->

</body>
</html>
