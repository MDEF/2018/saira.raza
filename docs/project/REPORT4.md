<!--- <center><font size="7" color="blue">Making from noise</font></center>
<center><font size="4" color="blue">Investigating metamaterial design as a novice maker</font></center>
<br>-->

**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦</font></center>
<br>

# <center>Speculating futures of making</center>

  <!--- --> <!--- -->

>“The future cannot be predicted, but it can be envisioned and brought lovingly into being. Systems cannot be controlled, but they can be designed and re-designed” (Meadows, 2008, p. 169). Rittel and Webber reach a nearly identical formulation, “Social problems are never solved. At best they are re-solved— over and over again” (Rittel & Webber, 1973, p. 160).”

[Jamer Hunt (2019)](https://jfsdigital.org/articles-and-essays/vol-23-no-3-march-2019/anticipating-future-system-states/)

<!---
## Impacting social practices

Social practices are a kind of cultural system and are made up of:

**Material things** including people’s bodies and brains –matter

**Actions, transformation of information** – social norms or codes of behaviour and action

**Socially shared ideas or concepts** associated with the practice that give meaning to it - social values and meanings behind these things and actions surrounding them.
>
‘Practices emerge, persist and disappear as links between their defining elements are made and broken’

[Shove, Pantzar, & Watson, (2012)](https://www.researchgate.net/publication/312035382_Shove_Pantzar_and_Watson_The_dynamics_of_social_practice_everyday_life_and_how_it_changes)

Links between these elements are important for understanding change in practices.  Making and designing actively contribute materials, information, politics and interactions into cultures. They bring actants of social practices into being and also links towards changes in social practices.

Social Practice theory encourages mapping to find links and prototyping or acting out how people would behave around them. Devices used to do this can include speculative objects, fictional storyboards, and performance improvisation.

## Intervening a system

>“A system is a set of things—people, cells, molecules, or whatever—interconnected in such a way that they produce their own pattern of behavior over time”

[Meadows, 2008](https://www.amazon.co.uk/Thinking-Systems-Primer-Diana-Wright/dp/1844077268)

-->

## A speculative future social practice of Making

### Inclusive, decentralised systems of design

Meta design principles propose that design should keep opening up to become more decentralised and inclusive - so as to utilise as many experiences and minds as possible to find ideas that inspire us to build the kinds of systems we want.

### Transdisciplinary roles

To do this, meta design encourages embodied design – using our unique bodies as instruments of learning to gain information through practical material knowledge on how to make things, constructivist experiences on what qualities information has and constructionist making of artefacts out of what you know.

I envisage this would encourage people to develop transdisciplinary roles – with people feeling they have permission, access to knowledge and training and agency to be part of the macro-systems of not only design but making, science, art and humanities. A total knowledge-practice network.




### Human – Non-Human relationships

Non-anthropocentric philosophies challenge us to envisage incorporating the agency and impact that non-human things and non-living things have to our culture and systems.

I imagine if humans could work with tools and materials with a sense of guardianship and responsibility. This kind of interaction seems to happen with our relationships with transport vehicles: cars, boats, bicycles and even trains and aeroplanes. We maintain them, we invest in them we grow with them, we see the world with them. At the end of their life we may even feel a sadness of losing them. We are physically close to these machines and we journey with them.

Personal computers play a strangely similar role, especially laptops which travel with us but they are also our tools of work. But how could we feel about a tool with a layer of intelligence and access to amounts of data we could not perceive?

What would be the everydayness of a multidisciplinary designer/ maker/ measurer human “node” in a network of working with other human “nodes” and non-human “nodes?”

<iframe
  src="https://embed.kumu.io/366fc18c3198462188332e87111fa2b5"
  width="940" height="600" frameborder="0"></iframe>


<!---
## Transitioning to an inclusive, non-anthropocentric maker/ codesign culture

We can try to link the elements of social practice now somepared to those of the past and those of desired future practices.

<iframe
  src="https://embed.kumu.io/9e183a8f453c06e75df45e127dbc8511"
  width="940" height="600" frameborder="0"></iframe>
-->

## A Prototype human-non human design interface

<center><iframe width="600" height="400" src="https://sraza.kumu.io/i-am-the-shape-machine" allowfullscreen></iframe></center>



<!--## The everydayness of future open co-design cultures

### Materials, Things, Technologies
  - interfaces to collaborate with AI algorithms and data banks and other humans
  - Sensor network to monitor material resources, needs and flows
  - Biodegradable food containers are standard.
  - More food is grown locally in or near cities


### Meanings

  - A data, science, art and design network of human and non-human nodes that information sensors, transmitters, generators, receivers, artists, scientists and designers
  - the link is a unit of value as well as the byte or the atom.
      - Systems that favour of reward  co-design teams - rewarding building links in an innovation network
  - Feelings of care and of being custodians of materials and non-human - co-designers as we might for living things.
  - Material Burial instead of landfill
  - coevolution of technology and life
  - Maintenance rights - duty of care towards others when you buy material and power/ information intensive things like cars
  - Licenses / agreement to maintain computers, plastics like a cars


### Competencies
  - Uploading data in exchange for computations
  - exchanging shapes with Ai
  - Tuning AIs
  - Making links with sensors, other humans and computers
  - Caring for non-humans
  - Caring for materials




### Open co-design culture today

#### Materials
- Personal environment data sensors
- simple easy to learn and design interfaces

#### Meanings
- measuring, codesigning and artistic expression as facets of everyone's life
- Design, science and art welcome everyone

#### Competencies
- making
- expressing
- fixing
 -problem solving

### The path to the future (in progress)

-->








<!--

## The future of human AI relationships in design

### Resolution - iterations - detail

### Using quantum states

## The future of making material things

### Design environments

### Non anthropocentric social norms

## The future of feeling human -->
