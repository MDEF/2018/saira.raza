<head>

<style>

.biblist {
  border: 0px outset red;
}


.biblist > h2, h3, .h2, .h3 {
margin-inline-start: -40px;
}

.biblist > h4, .h4 {
margin-inline-start: -30px;  
}

.biblist > h5, h6, h7, h8, .h5, .h6, .h7, .h8 {
margin-inline-start: -20px;  
}

.biblist ul {
}

.biblist li {
list-style-type: disc;
margin-bottom:0.4em;
padding-left: 0em;
}

.biblist li::before {
  padding-right: 0px;
           color: blue;
}

ul.biblist li {
  padding-left: 0px;
  margin-left: 0rem;
  margin-bottom: 0.4em;
  position: relative;
  list-style-position: outside;
  display: list-item;
}

ul.biblist li::marker {
color: blue;
margin: 0px;
}

ul.biblist li:hover::marker {
font-weight: 400;
}



.biblist details {
  padding-bottom: 0em;
  padding-left: 0px;
  margin-left: 0em;
  list-style-position: outside;
  display: list-item;
  list-style: none;
}

.biblist details[open] {
background-color:  #e5e5ff;
padding-top:1em;
padding-right:1em;
padding-left: 0em;
padding-bottom:0.1em;
margin:0.4em;
margin-left:0em;
}

.biblist details summary {
  cursor: pointer;
  padding-bottom: 0em;
  padding-left: 0em;
  margin-bottom: 0.4em;
  margin-left: -16px;
  outline: 0px;
  outline-color: transparent;
  display: list-item;
}

.biblist summary:hover {
outline: 0px;
padding-bottom: -10px;
margin-left: -18px;
margin-bottom: 0.4em;
color: ;
outline-color:lime ;
outline-style: solid;
outline-width: 2px;
background-color: transparent;
}

.biblist details summary::marker {
color: lime;
padding-bottom: 0px;
padding-left: 0px;
padding-right: 0px;
}
.biblist summary::-webkit-details-marker {
color: lime;
margin-right: 0px;
}
.biblist details[open] summary {
  background-color: #e5e5ff;
  margin-left: -18px;
  padding-left: 0em;
  padding-bottom: 0em;
  outline-color: transparent;
}
.biblist details[open] summary::marker {
color: lime;
padding-bottom: 0px;
padding-left: 0px;
padding-right: 0px;
}



#bibtable{
border-collapse: collapse;
width: 100%;
max-width:1000px;
font-size: 0.7rem;
}
#bibtable td, #bibtable th {
  border: 1px solid #ddd;
  padding-top: 4px;
  padding-right: 4px;
  padding-bottom: 4px;
  padding-left: 4px;
  min-width: 110px;
}

#bibtable th {
  text-align:center;
}

#bibtable td {
text-align: left;
padding-left: 8px;
}



.biblist blockquote {
margin: auto;
margin-bottom: 50px;
margin-top: 20px;
padding: 30px 30px 30px 30px;
width: 80%;
max-width: 800px;
font-size: 0.8rem;
font-style: italic;
border: solid 1px;
background-color: rgba(0, 0, 255, 0.1);
box-shadow: 15px 15px 0 0 blue;
}

.biblist blockquote-footer {
  padding-right:30px;
  text-align: right;
  font-style: normal;
  color: black;
  font-size: 100%;
}


</style>
</head>



**<font face color="blue" size="5"><center>[░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)</center></font>**
<center><font size="3" color="blue">

[𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)
</font></center>
<br>

# Bibliography / Source material



## Innovation and evolution

<ul class="biblist">

<details><summary><a href=http://www.kspjournals.org/index.php/JEB/article/view/1650>Mario Coccia (2018) <cite>Classification of innovation considering technological interaction</cite></a></summary><figure><blockquote class='blockquote'><p>"Sahal (1985, p.64) argues that technological innovations can be: ..structural innovations that arise from a process of differential growth; whereby the parts  and  the  whole  of  a  system  do  not  grow  at  the  same rate.  
Second,  we  have what may  be  called  the material innovations that are necessitated  in an  attempt to meet  the  requisite  changes  in  the  criteria  of  technological  construction  as  a consequence  of  changes  in  the  scale  of  the  object.  
Finally,  we  have  what  may  be called the systems innovations that arise from integration of two or more symbiotic technologies in an attempt to simplify the outline of the overall structure‛. This trilogy  can  generate the  emergence  of  various  techniques  including  revolutionary innovations  in  a  variety  of technological  and scientific fields(cf., Sahal,  1981; Coccia, 2016, 2016a)"</p><figcaption class='blockquote-footer'>Mario Coccia<cite title=Classification of innovation considering technological interaction>, Classification of innovation considering technological interaction</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.amazon.co.uk/Patterns-Technological-Innovation-Devendra-Sahal/dp/0201066300>Sahal, D.  (1981) <cite>Patterns of Technological Innovation</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/260029656_Driving_forces_of_technological_change_The_relation_between_population_growth_and_technological_innovation_Analysis_of_the_optimal_interaction_across_countries>Mario Coccia  (2014) <cite>Driving forces of technological change: The relation between population growth and technological innovation</cite></a><br></li>
<li><a href=https://technosphere-magazine.hkw.de/p/The-Material-Order-4gK5EMpZ3SzB79aTePfJo7>Daniel Niles and Sander Van der Leeuw (2018) <cite>The Material Order - Do cultural ideas order our material worlds and technologies, or is it the other way around?</cite></a><br></li>
<li><a href=https://futurism.com/is-technological-evolution-infinite-or-finite>Unknown   (2013) <cite>Is Technological Evolution Infinite or Finite? - From Quarks to Quasars for Futury</cite></a><br></li>
<li><a href=https://pdfs.semanticscholar.org/1bb3/17a5487af7fbe69d274b0d0bead519af735a.pdf>Edited by John Ziman  (2000) <cite>Technological Innovation as an Evolutionary Process - 92000</cite></a><br></li>
<li><a href=https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5842246/>Krist Vaesen and Wybo Houkes  (2017) <cite>Complexity and technological evolution: What everybody knows?</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/327782016_Controllability_of_Technosphere_and_Paradigm_of_Global_Evolutionism>Natalia Popkova  (2018) <cite>Controllability of Technosphere and Paradigm of Global Evolutionism</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/282716418_The_syntheses_of_technological_materials_as_emergences_in_complex_systems>Jiri Bila  (2014) <cite>The syntheses of technological materials as emergences in complex systems</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/272730549_DesignerMaker_The_Rise_of_Additive_Manufacturing_Domestic-Scale_Production_and_the_Possible_Implications_for_the_Automotive_Industry>Mark Richardson  (2012) <cite>Designer/Maker: The Rise of Additive Manufacturing, Domestic-Scale Production and the Possible Implications for the Automotive Industry</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/256739352_Is_There_Techne_in_My_Logos_On_the_Origins_and_Evolution_of_the_Ideographic_Term-Technology>Ron Tulley  (2008) <cite>Is There Techne in My Logos? On the Origins and Evolution of the Ideographic Term—Technology</cite></a><br></li>
<li><a href=http://www.glass-bead.org/research-platform/diagramming-horizons-art-techne-artifactual-elaboration-mind/?lang=enview>Keith Tilford <cite>Diagramming Horizons Between Art, Techne, and the Artifactual Elaboration of Mind </cite></a><br></li>

<details><summary><a href=https://www.researchgate.net/publication/266887969_Technoculture_and_its_democratization_noise_limits_and_opportunities_of_the_labs>Ramon Sangüesa (2010) <cite>Technoculture and its democratization: noise, limits and opportunities of the "labs"</cite></a></summary><figure><blockquote class='blockquote'><p>"Since matter  can be  considered  as code  and code  as  matter,  the  latter  also  has  become programmable  (Ratto,  2010),  (Sangüesa,  2009,  2010). The  same  algorithms  that   guide  3D  laser  printers  in FabLabs  (Gershenfeld,  2005)  are  used  to "print"  organic  matter  and  build  new  human  organs  (Ringeisen,  2010).  In  a  symmetrical  move, living  matter becomes the  substratum  of computing  processes:  living  cells are interconnected to act as  computers  (Regot,  2011),  (Bray,  2011).  The  boldest   champions  of  the  technocultural  logic envision  as  very  close  the  moment  when  we,  humans,  as  digitized  information,  will  be  able  to change our supports (a new body) and become "immortal" (Kurzweil, 2006)."</p><figcaption class='blockquote-footer'>Ramon Sangüesa<cite title=Technoculture and its democratization: noise, limits and opportunities of the "labs">, Technoculture and its democratization: noise, limits and opportunities of the "labs"</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://www.susanblackmore.uk/memetics/genes-memes-and-temestremes/>  Susan Blackmore (accessed 2018) <cite>Genes, memes, and temes/tremes, www.susanblackmore.uk</cite></a></summary><figure><blockquote class='blockquote'><p>"Earth now has three replicators – genes (the basis of life), memes (the basis of human culture) and temes (the basis of technology).. formation copied by books, phones, computers and the Internet is the beginning of this third replicator and consequent new evolutionary process."</p><figcaption class='blockquote-footer'>  Susan Blackmore<cite title=Genes, memes, and temes/tremes, www.susanblackmore.uk>, Genes, memes, and temes/tremes, www.susanblackmore.uk</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.mdpi.com/1099-4300/21/9/864/htm>Stuart Kauffman  (2019) <cite>Innovation and The Evolution of the Economic Web</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/325793905_Social_organization_The_thermodynamic_basis>Umit Gunes, Adrian Bejan, Marcelo Errera,B. Sahin  (2018) <cite>Social organization: The thermodynamic basis</cite></a><br></li>
<li><a href=http://www.autonomatic.org.uk/archive/team/jm/JMcraft-techpaper.pdf>Justin Marshall (2002) <cite>Craft and Technology</cite></a><br></li>
<details><summary><a href= >Douglas M. Davis (1968) <cite>Art & Technology – Conversations</cite></a></summary><figure><blockquote class='blockquote'><p>"Technology today does not simply imply a physical implement, ‘a machine’ mechanical or electronic, but a systematic, disciplined, collaborative approach to a chosen objective. There is a new technology that Daniel Bell has called ‘intellectual technology’ – that is what artists must accept and understand. The medium, in this case technology, is not itself the message; it becomes a message when it is in a vital dialogue with our most authentic contemporary needs."</p><figcaption class='blockquote-footer'>Gyorgy Kepes interviewed by Douglas M. Davis<cite title=Art & Technology – Conversations>, Art & Technology – Conversations</cite> </figcaption></blockquote></figure></details>

<li><a href=>Gyorgy Kepes (1970) <cite>Toward Civic Art</cite></a><br></li>

<details><summary><a href=https://rauterberg.employee.id.tue.nl/presentations/2014-Design-Philosophy-paper.pdf>Matthias RAUTERBERG  (2014) <cite>Philosophical Foundation for the Design of Interactive Systems</cite></a></summary><figure><blockquote class='blockquote'><p>"But based on the work of Ihde, Latour and Borgmann, Verbeek presents a modern view how artifacts are reflecting our culture but at the same time shaping our existence and experience"</p><figcaption class='blockquote-footer'>Matthias RAUTERBERG <cite title=Philosophical Foundation for the Design of Interactive Systems>, Philosophical Foundation for the Design of Interactive Systems</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://foucault.info/documents/foucault.technologiesOfSelf.en/>Foucault  (1988) <cite>Technologies of the Self</cite></a></summary><figure><blockquote class='blockquote'><p>"We must understand that there are four major types of these ‘technologies’ each a matrix of practical reason: (1) technologies of production, which permit us to produce, transform, or manipulate things; (2) technologies of sign systems, which permit us to use signs, meanings, symbols, or signification; (3) technologies of power, which determine the conduct of individuals and submit them to certain ends or domination, an objectivizing of the subject; (4) technologies of the self, which permit individuals to effect by their own means or with the help of others a certain number of operations on their own bodies and souls, thoughts, conduct, and way of being, so as to transform themselves in order to obtain a certain state of happiness, purity, wisdom, perfection, or immortality"</p><figcaption class='blockquote-footer'>Foucault <cite title=Technologies of the Self>, Technologies of the Self</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.tandfonline.com/doi/figure/10.1080/1751696X.2018.1505815?scroll=top&needAccess=true>Nathalie Gontier  (2018) <cite>Cosmological and phenomenological transitions into how humans conceptualize and experience time</cite></a><br></li>

<h3> Paradigm shifts</h3>

<li><a href=https://medium.com/@amber_r_murray/spatial-paradigm-shifts-from-euclidean-geometry-to-ar-mr-vr-54252aa82d3a>Amber R Murray  (2016) <cite>Article on Medium: SPATIAL PARADIGM SHIFTS — FROM EUCLIDEAN GEOMETRY TO AR/MR/VR</cite></a><br></li>
<li><a href=https://store.moma.org/books/books/the-universitas-project-pb/70-70.html>Emilio Ambasz (Foreword), Jean Baudrillard, Octavio Paz  (2006) <cite>The Universitas Project: Solutions for a Post-Technological Society</cite></a><br></li>

<h3> Information technology</h3>

<li><a href=https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4958943/>Sergi Valverde (2016) <cite>Major transitions in information technology</cite></a><br></li>
<li><a href=https://iopscience.iop.org/article/10.1088/1757-899X/392/6/062197>Zhimiao Fang1, Zhenghan Wu2 and Le Luo  (2018) <cite>Research on Computer Information Processing Technology in the "Big Data" Era</cite></a><br></li>
<li><a href=https://mitpress.mit.edu/books/what-algorithms-want>Nofre, D.  (2018) <cite>What Algorithms Want: Imagination in the Age of Computing by Ed Finn. Technology and Culture</cite></a><br></li>
<li><a href=>Blanchette, J. (2011) <cite>A material history of bits</cite></a><br></li>
<li><a href=https://www.cc.gatech.edu/~beki/cs4001/Winner.pdf>L WINNER  (1980) <cite>Do Artifacts Have Politics?</cite></a><br></li>

<h3> Quantum Computing</h3>

</ul>

## Building Knowledge

<ul class="biblist">

<li><a href=https://www.tandfonline.com/doi/abs/10.1080/0304379031000078960>Antonio S.C. Fernandes and Pedro Melo Mendes (2003) <cite>Technology as culture and embodied knowledge</cite></a><br>
</li>
<li><a href=https://quod.lib.umich.edu/cgi/t/text/text-idx?c=did;rgn=main;view=text;idno=did2222.0001.084>Diderot, Denis.Translated by Richard N. Schwab and Walter E. Rex  (2009) <cite>Detailed explanation of the system of human knowledge.</cite></a><br></li>
<li><a href=https://www.materialworldblog.com/2010/12/a-material-history-of-bits/>Haidy Geismar  (2010) <cite>A Material History of Bits, materialworldblog.com</cite></a><br></li>


<h3> Practical Knowledge</h3>

<details><summary><a href=https://link.springer.com/chapter/10.1007/978-3-319-45671-3_1>Valleriani M.  (2017) <cite>The Epistemology of Practical Knowledge. In: Valleriani M. (eds</cite></a></summary><figure><blockquote class='blockquote'><p>"In recent years, the increasing focus on the material aspects of science and of scientific endeavors, has helped to shift attention away from this apparent dichotomy towards other, lesser studied characteristics and processes inherent to the experience of knowing and making. The attention has also been shifted to the workflows, the organizations, and the institutional frameworks that allowed for the achievement of a final material product (Chap. 10). This means that practical activities have been recognized as also being characterized by a structure of knowledge that connects materiality and action on which such activities were based. This tendency obviously results in an association between highly abstract conceptualizations based on practical knowledge and the practical activities that made use of such organized knowledge. However, the fact that practical activities show a sort of embedded knowledge structure does not yet explain how this knowledge became relevant within the process of the emergence of new science.

To fill this gap and, therefore, to understand the epistemic value of practical knowledge, it is necessary to investigate how and through which steps the structure of knowledge used in the frame of practical activities was reflected, conceptualized and externalized, for instance, by means of written treatises—and how this allowed for the creation of more abstract structures of knowledge that linked heterogeneous intellectual and practical fields to each other, so as to build new knowledge systems."</p><figcaption class='blockquote-footer'>Valleriani M. <cite title=The Epistemology of Practical Knowledge. In: Valleriani M. (eds>, The Epistemology of Practical Knowledge. In: Valleriani M. (eds</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://scholar.harvard.edu/files/saraschechner/files/isis_v.109_review_valleriani_structures_practical_knowledge_sept2018.pdf><b>Sara J. Schechner  (2017) </b><cite>Review of Matteo Valleriani (Editor</cite></a></summary><figure><blockquote class='blockquote'><p>"According to Valleriani, the codification of practical activities in the form of texts, drawings, and models increased during the early modern period. Their circulation enabled the creation of shared conceptual structures of knowledge that linked heterogeneous theoretical and applied fields together, so as to build the new knowledge systems of modern science."</p><figcaption class='blockquote-footer'>Sara J. Schechner <cite title=Review of Matteo Valleriani (Editor>, Review of Matteo Valleriani (Editor</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://www.springer.com/gp/book/9783319456706><b>Hendriksen, M.  (2018) </b><cite>The Structures of Practical Knowledge. Ambix, 66 (1), 88–90.</cite></a></summary><figure><blockquote class='blockquote'><p>"Valleriani defines practical knowledge as knowledge that follows a defined workflow and that is needed to obtain a product or outcome, such as an artefact, a healed patient, or a mathematical result."</p><figcaption class='blockquote-footer'>Hendriksen, M. <cite title=The Structures of Practical Knowledge. Ambix, 66 (1), 88–90.>, The Structures of Practical Knowledge. Ambix, 66 (1), 88–90.</cite> </figcaption></blockquote></figure></details>

<li><a href=https://pdfs.semanticscholar.org/74ec/7e5e71d29051402dc1bee73af3c3184ecf7d.pdf><b>Gustavo Guzman  (2009) </b><cite>What is practical knowledge?</cite></a></li>

<li><a href=><b>Heidegger  (1954) </b><cite>The Question Concerning Technology</cite></a><br></li>
<li><a href=https://www.nature.com/articles/s41599-019-0331-9><b>Peter Massingham  (2019) </b><cite>An Aristotelian interpretation of practical wisdom: the case of retirees</cite></a><br></li>


<h3> Situated Knowledge</h3>

<li><a href=><b>Donna Haraway (1988) </b><cite>Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspective</cite></a><br></li>

</ul>

## Data and noise

<ul class="biblist">

<details><summary><a href=https://journals.sagepub.com/doi/full/10.1177/2053019616679854><b>Paul N Edwards (2016) </b><cite>Knowledge infrastructures for the Anthropocene</cite></a></summary><figure><blockquote class='blockquote'><p>"Analogous to the biosphere’s reuse of organic wastes, numerous online systems – Google’s search algorithms, recommender systems from Netflix and Amazon, etc. – recycle these byproducts of intelligent human activity to create more intelligent artificial behavior. <br>  The ‘mining’ of data exhaust to detect patterns, trends and individual preferences is transforming the relationship between designers, builders, marketers and consumers, as well as civil society, worldwide. It is also transforming science."</p><figcaption class='blockquote-footer'>Paul N Edwards<cite title=Knowledge infrastructures for the Anthropocene>, Knowledge infrastructures for the Anthropocene</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://journals.sagepub.com/doi/full/10.1177/2053019616679854><b>Paul N Edwards (2016) </b><cite>Knowledge infrastructures for the Anthropocene</cite></a></summary><figure><blockquote class='blockquote'><p>"Following the pattern of infrastructural globalism, massive, high-resolution sensor networks provide more, finer-grained environmental information (Hey et al., 2009). Under more recent approaches, scientists seek patterns opportunistically in large data sets created for other reasons; conduct meta-analyses of existing studies; and open up the vast existing stores of ‘dark data’ available from past research (Hampton et al., 2013). In the long run, these approaches promise new ways to monitor, analyze and potentially to optimize the technosphere’s environmental impacts. However, the data utopia envisioned by some faces a host of difficult, sometimes irresolvable issues, including data friction, data ownership, personal privacy, and metadata quality (Edwards et al., 2011)."</p><figcaption class='blockquote-footer'>Paul N Edwards<cite title=Knowledge infrastructures for the Anthropocene>, Knowledge infrastructures for the Anthropocene</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://www.bbvaopenmind.com/en/articles/reality-regained-an-inquiry-into-the-data-age/><b>Jannis Kallinikos (2019) </b><cite>Reality regained: An inquiry into the data age</cite></a></summary><figure><blockquote class='blockquote'><p>"Essential strips of reality are increasingly mediated and acted upon by means of digital marks (e.g. data) and the extraction of abstract or analytic relations between human and things that data and information enable (Borgmann 1999, 2010; Hayles 2005, 2006)."</p><figcaption class='blockquote-footer'>Jannis Kallinikos<cite title=Reality regained: An inquiry into the data age>, Reality regained: An inquiry into the data age</cite> </figcaption></blockquote></figure></details>

<li><a href=http://bigbangdata.somersethouse.org.uk/><b></b><cite>http://bigbangdata.somersethouse.org.uk/</cite></a><br></li>

<li><a href=https://link.springer.com/chapter/10.1007/978-3-319-46487-9_19><b>Jonathan Krause, Benjamin Sapp, Andrew Howard, Howard Zhou, Alexander Toshev, Tom Duerig, James PhilbinLi Fei-Fei (2016) </b><cite>The Unreasonable Effectiveness of Noisy Data for Fine-Grained Recognition</cite></a><br></li>

<li><a href=https://ec.europa.eu/info/research-and-innovation/strategy/goals-research-and-innovation-policy/open-science/open-science-monitor_en><b></b><cite>https://ec.europa.eu/info/research-and-innovation/strategy/goals-research-and-innovation-policy/open-science/open-science-monitor_en</cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>Tracking trends for open access, collaborative and transparent research across countries and disciplines.<br></li>


</ul>

## Material-computation evolution

<ul class="biblist">

<li><a href= ><b></b><cite> </cite></a>Mapping physical behaviour of materials to logic to create hardware(computers)<br></li>
<li><a href= ><b></b><cite> </cite></a>Mapping logic of hardware to create software<br></li>
<li><a href= ><b></b><cite> </cite></a>Physical behaviour of materials - repeatable states by experiment -  engineering, constructionism - kk - logic - languages - software -<br></li>


<h3> Materials as computers</h3>

<details><summary><a href=><b>Wiberg, M. (2016) </b><cite>Interaction, new materials & computing – Beyond the disappearing computer, towards material interactions. </cite></a></summary><figure><blockquote class='blockquote'><p>"Treating computers “as just another material” operating “on the same level as paper cardboard, and other materials found in design shops”, shifts the research agenda for interaction design. Instead of asking how should it work and     look?, we might now find ourselves asking, what kind of material is it? // Information has generally been regarded as that which can be abstracted from form. To shift the computer from the status of information to material, therefore, is an ontological move and we have yet to thoroughly explore the consequences of this philosophical transformation."</p><figcaption class='blockquote-footer'>Wiberg, M.<cite title=Interaction, new materials & computing – Beyond the disappearing computer, towards material interactions. >, Interaction, new materials & computing – Beyond the disappearing computer, towards material interactions. </cite> </figcaption></blockquote></figure></details>

<h4> Bistable Anisotropic Structures embodying logic</h4>

<li><a href=https://www.nature.com/articles/s41467-018-08055-3><b>Yijie Jiang, Lucia M. Korpas & Jordan R. Raney (2019) </b><cite>Bifurcation-based embodied logic and autonomous actuation</cite></a><br></li>
<li><a href=https://www.cambridge.org/core/journals/mrs-bulletin/article/advanced-memorymaterials-for-a-new-era-of-information-technology/7D6D603157A9195DB0B585C8C804ACB9><b>Hwang, C. S., & Dieny, B. (2018) </b><cite>Advanced memory—Materials for a new era of information technology</cite></a><br></li>



<h3> Computational geometry</h3>
<details><summary><a href=https://www.e-flux.com/journal/101/273221/three-thousand-years-of-algorithmic-rituals-the-emergence-of-ai-from-the-computation-of-space/><b>Matteo Pasquinelli  (2019) </b><cite>Three Thousand Years of Algorithmic Rituals: The Emergence of AI from the Computation of Space</cite></a></summary><figure><blockquote class='blockquote'><p>" Because of its spatial logic, the branch of computer science originally dedicated to neural networks was called “computational geometry.”.... All systems of machine learning, including support-vector machines, Markov chains, Hopfield networks, Boltzmann machines, and convolutional neural networks, to name just a few, started as models of computational geometry."</p><figcaption class='blockquote-footer'>Matteo Pasquinelli <cite title=Three Thousand Years of Algorithmic Rituals: The Emergence of AI from the Computation of Space>, Three Thousand Years of Algorithmic Rituals: The Emergence of AI from the Computation of Space</cite> </figcaption></blockquote></figure></details>


<h3> Aligning computational and structural geometry</h3>

<details><summary><a href=http://cba.mit.edu/docs/papers/11.12.Computing.pdf><b>Neil Gershenfeld  (2011) </b><cite>Aligning the representation and reality of computation with asynchronous logic automata</cite></a></summary><figure><blockquote class='blockquote'><p>"By abstracting physics with asynchronous logic automata I show that this alignment can not only improve scalability, portability, and performance, but also simplify programming and expand applications."</p><figcaption class='blockquote-footer'>Neil Gershenfeld <cite title=Aligning the representation and reality of computation with asynchronous logic automata>, Aligning the representation and reality of computation with asynchronous logic automata</cite> </figcaption></blockquote></figure></details>


<h3> Digital material construction kits</h3>

<details><summary><a href=https://www.researchgate.net/publication/228430160_Digital_Materials><b>George A. Popescu and Neil Gershenfeld  (2009) </b><cite>Digital Materials</cite></a></summary><figure><blockquote class='blockquote'><p>"digital materials, on the other hand, are reversibly assembled from a discrete set of components, they can bedeconstructed as well as constructed"</p><figcaption class='blockquote-footer'>George A. Popescu and Neil Gershenfeld <cite title=Digital Materials>, Digital Materials</cite> </figcaption></blockquote></figure></details>

<h3> HCI Design</h3>

<li><a href=http://www.ccm.ece.vt.edu:8444/papers/steiner_2005_RAW05_hsi.pdf><b>Neil Steiner and Peter Athanas  (2016) </b><cite>Hardware-Software Interaction: Preliminary Observations</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/331793384_Critical_Materiality_Creating_Toolkits_and_Methods_for_Engaging_Materiality_in_HCI><b>Joanna Berzowska Aisling Kelliher Daniela K. Rosner et al  (2019) </b><cite>Critical Materiality: Creating Toolkits and Methods for Engaging Materiality in HCI</cite></a><br></li>


</ul>


## Systems thinking

<ul class="biblist">

<h3> Systems interventions</h3>

<details><summary><a href=https://books.google.es/books/about/Thinking_in_Systems.html?id=CpbLAgAAQBAJ&redir_esc=y><b>Donella H. Meadows  (2008) </b><cite>Thinking in Systems: A Primer</cite></a></summary><figure><blockquote class='blockquote'><p>"  PLACES TO INTERVENE IN A SYSTEM (in increasing order of effectiveness)      
9. Constants, parameters, numbers (subsidies, taxes, standards).
8. Regulating negative feedback loops.
7. Driving positive feedback loops.
6. Material flows and nodes of material intersection.
5. Information flows.
4. The rules of the system (incentives, punishments, constraints).
3. The distribution of power over the rules of the system.
2. The goals of the system.
1. The mindset or paradigm out of which the system — its goals, power structure, rules, its culture — arises.
            - Donella Meadows ~ Leverage Points: Places to Intervene in a System"</p><figcaption class='blockquote-footer'>Donella H. Meadows <cite title=Thinking in Systems: A Primer>, Thinking in Systems: A Primer</cite> </figcaption></blockquote></figure></details>

<li><a href=https://jfsdigital.org/articles-and-essays/vol-23-no-3-march-2019/anticipating-future-system-states/?fbclid=IwAR2hO1KMXr51IqpWjuk6VnJC85mohjN9ax6mpoVKPyXg9NaIPRpeZGqZbME><b>Jamer Hunt </b><cite>Anticipating Future System States</cite></a><br></li>

<details><summary><a href=https://www.pangaro.com/published/Cybernetics_and_Counterculture-Dubberly+Pangaro-October-2015.pdf><b>Hugh Dubberly, Paul Pangaro  (2015) </b><cite>How cybernetics connects computing, counterculture, and design</cite></a></summary><figure><blockquote class='blockquote'><p>"In addition to being a utopian counterculture toolkit and a self-published manifesto for a do-it-yourself lifestyle, the Whole Earth Catalog is also an introduction to systems thinking and design.The catalog’s first section “Understanding Whole Systems” juxtaposes Buckminster Fuller and von Foerster’s review of mathematician Spencer Brown’s Laws of Form—followed quickly by biologist D’Arcy Thompson’s On Growth and Form side-by-side with architect Christopher Alexander’s Notes on the Synthesis of Form, with a sidebar on von Foerster’s Purposive Systems thrown in. And then, cheek-by-jowl, are reviews of artificial intelligence pioneer Herbert Simon’s Sciences of the Artificial and Ludwig von Bertalanffy’s General Systems Yearbook. On the next the page is a review of Wiener’s, The Human Use of Human Beings. And that’s just in the first few pages."</p><figcaption class='blockquote-footer'>Hugh Dubberly, Paul Pangaro <cite title=How cybernetics connects computing, counterculture, and design>, How cybernetics connects computing, counterculture, and design</cite> </figcaption></blockquote></figure></details>

<h3> Technology and Anthropocene</h3>

<li><a href=https://www.academia.edu/38085338/From_tools_to_technosphere><b>Bronislaw Szerszynski </b><cite>From tools to technosphere</cite></a><br></li>

<li><a href=http://pne.people.si.umich.edu/PDF/Haff%202013%20Technology%20as%20a%20Geological%20Phenomenon.pdf><b>P. K. HAFF </b><cite>Technology as a geological phenomenon: implications for human well-being</cite></a><br></li>

<li><a href=https://journals.sagepub.com/doi/pdf/10.1177/2053019616676608><b>Jonathan F Donges, Wolfgang Lucht, Finn Müller-Hansen1 and Will Steffen </b><cite>The technosphere in Earth System analysis: A coevolutionary perspective</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere><b>John Hartleya and Carsten Herrmann-Pillath </b><cite>Towards a semiotics of the technosphere</cite></a><br></li>

<li><a href=https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3034099><b>Carsten Herrmann-Pillath </b><cite>Foundational Issues of ‘Technosphere Science’ – The Case for a New Scientific Discipline</cite></a><br></li>

<li><a href=http://kemp.unu-merit.nl/Rip%20and%20Kemp.pdf><b>Arie Rip & René Kemp </b><cite>Technological change</cite></a><br></li>

<li><a href=http://technosphere-magazine.hkw.de/><b>http://technosphere-magazine.hkw.de/</b><cite>Technosphere magazine</cite></a><br></li>


<li><a href=https://www.academia.edu/21758381/The_Anthropocenic_Illusion_Sustainability_and_the_Fascination_of_Control><b>Erich Hörl in Exchange with Paul Feigelfeld and Cornelia Kastelan </b><cite>The Anthropocenic Illusion: Sustainability and the Fascination of Control</cite></a><br></li>

<li><a href=https://monoskop.org/images/e/e5/Techne_21_2-3_Anthropocene_2017.pdf><b>Guest Editors Pieter Lemmens, Vincent Blok, and Jochem Zwier </b><cite>Techné: Research in Philosophy and Technology, Volume 21, Issues 2–3, Special Issue on the Anthropocene</cite></a><br></li>

<details><summary><a href=http://pne.people.si.umich.edu/PDF/Haff%202013%20Technology%20as%20a%20Geological%20Phenomenon.pdf><b>P. K. Haff  (2014) </b><cite>Technology as a geological phenomenon: implications for human well-being</cite></a></summary><figure><blockquote class='blockquote'><p>""The technosphere, the interlinked set of communication, transportation, bureaucratic and other systems that act to metabolize fossil fuels and other energy resources, is considered to be an emerging global paradigm, with similarities to the lithosphere, atmosphere, hydrosphere and biosphere.        The technosphere is of global extent, exhibits large-scale appropriation of mass and energy resources, shows a tendency to co-opt for its own use information produced by the environment, and is autonomous. Unlike the older paradigms, the technosphere has not yet evolved the ability to recycle its own waste stream. Unless or until it does so, its status as a paradigm remains provisional.        Humans are ‘parts’ of the technosphere – subcomponents essential for system function. Viewed from the inside by its human parts, the technosphere is perceived as a derived and controlled construct. Viewed from outside as a geological phenomenon, the technosphere appears as a quasi-autonomous system whose dynamics constrains the behaviour of its human parts. A geological perspective on technology suggests why strategies to limit environmental damage that consider only the needs of people are likely to fail without parallel consideration of the requirements of technology, especially its need for an abundant supply of energy.                    - P. K. Haff (2014) Technology as a geological phenomenon"</p><figcaption class='blockquote-footer'>P. K. Haff <cite title=Technology as a geological phenomenon: implications for human well-being>, Technology as a geological phenomenon: implications for human well-being</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://journals.sagepub.com/doi/pdf/10.1177/2053019616676608><b>Jonathan F Donges, Wolfgang Lucht, Finn Müller-Hansen and Will Steffen  (2017) </b><cite>The technosphere in Earth System analysis: A coevolutionary perspective</cite></a></summary><figure><blockquote class='blockquote'><p>" Earth System analysis is the study of the joint dynamics of biogeophysical, social and technological processes on our planet.      Technologies and institutions increase societies’ robustness to external and internal disturbances but also constitute path-dependencies and lock-ins that make large-scale changes difficult."</p><figcaption class='blockquote-footer'>Jonathan F Donges, Wolfgang Lucht, Finn Müller-Hansen and Will Steffen <cite title=The technosphere in Earth System analysis: A coevolutionary perspective>, The technosphere in Earth System analysis: A coevolutionary perspective</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere><b>John Hartleya and Carsten Herrmann-Pillath  (2018) </b><cite>Towards a semiotics of the technosphere</cite></a></summary><figure><blockquote class='blockquote'><p>"Latour The Geosphere and biosphere are not systematic Latour’s (2015) critique of the ‘systems’ term in this context. In his view, ‘Gaia’ (the geosphere + biosphere) is not a system but itself a fragmented, highly dynamic and conflict-ridden entity: a system of conflicting systems, if you like. That means, we should not impose scientific standards of ‘systematicity’ on modelling the Earth system but should instead project the contingency and indeterminacy of the humanities and social sciences on modelling practice.  "</p><figcaption class='blockquote-footer'>John Hartleya and Carsten Herrmann-Pillath <cite title=Towards a semiotics of the technosphere>, Towards a semiotics of the technosphere</cite> </figcaption></blockquote></figure></details>

<h3> Quantifying the technosphere</h3>
<details><summary><a href=https://www.researchgate.net/publication/311093683_Scale_and_diversity_of_the_physical_technosphere_A_geological_perspective><b>Jan Zalasiewicz Mark Williams Colin Neil Waters, Alexander P Wolfe  (2016) </b><cite>Scale and diversity of the physical technosphere: A geological perspective</cite></a></summary><figure><blockquote class='blockquote'><p>"the physical technosphere, defined here as the summed material output of the contemporary human enterprise. It includes active urban, agricultural and marine components, used to sustain energy and material flow for current human life, and a growing residue layer, currently only in small part recycled back into the active component."</p><figcaption class='blockquote-footer'>Jan Zalasiewicz Mark Williams Colin Neil Waters, Alexander P Wolfe <cite title=Scale and diversity of the physical technosphere: A geological perspective>, Scale and diversity of the physical technosphere: A geological perspective</cite> </figcaption></blockquote></figure></details>
<li><a href=https://www.researchgate.net/publication/258838276_Metabolism_of_the_Anthroposphere_Analysis_evaluation_design_2nd_edition><b>Peter Baccini and Paul H. Brunner  (2012) </b><cite>Metabolism of the Anthroposphere, Analysis, Evaluation, Design</cite></a><br></li>
<li><a href=https://www.chelseagreen.com/product/thinking-in-systems/><b>Donella H. Meadows (2009) </b><cite> Thinking in Systems: A Primer</cite></a><br></li>

<h3> Earth systems modelling</h3>

<li><a href=https://www.earth-syst-dynam-discuss.net/esd-2017-126/><b>Donges, JF, Heitzig, J, Barfuss, W, Kassel, JA, Kittel, T, Kolb, JJ, Kolster, T, Müller-Hansen, F, Otto, IM, Wiedermann, M, Zimmerer, KB, Lucht, W (2018) </b><cite>Earth system modelling with complex dynamic human societies: the CORE World-Earth modeling framework, Earth System Dynamics Discussions</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/298070425_Geomaterials_in_construction_and_their_sustainability_Understanding_their_role_in_modern_society><b>Richard Prikryl, Ákos Török, Miguel Gomez-Heras, Karel Miskovsky, Magdalini Theodoridou (2016) </b><cite>Geomaterials in construction and their sustainability: Understanding their role in modern society</cite></a><br></li>

<li><a href=https://www.sciencedirect.com/science/article/pii/S2214629617303900><b>Frank W.Geelsa, Tim Schwanenb (2017) </b><cite>Reducing energy demand through low carbon innovation: A sociotechnical transitions perspective and thirteen research debates</cite></a><br></li>


<h3> Tracking material flows</h3>
<li><a href=https://www.plymouthart.ac.uk/documents/Liz_Corbin%2C_Eva_Gladek%2C_Chris_Monaghan_and_James_Tooze.pdf><b>Liz Corbin, Chris Monaghan, James Tooze, Eva Gladek (2019) </b><cite>Materials Democracy: An action plan for realising a redistributed materials economy</cite></a><br></li>
<li><a href=https://www.sciencedirect.com/science/article/pii/S2468203916301662><b>Pi-Cheng Chena et al (2017) </b><cite>An information system for sustainable materials management with material flow accounting and waste input–output analysis</cite></a><br></li>

<h3> Complex Systems and evolution</h3>



</ul>




## Design in the Post-technological Era

<ul class="biblist">


<li><a href=https://design.ncsu.edu/student-publication/wp-content/uploads/2016/11/Simon_H_ScienceofArtificial.pdf><b>Herbert A. Simon (1988) </b><cite>The Science of Design: Creating the Artificial</cite></a><br></li>

<h3> Interpretative action</h3>

<details><summary><a href=https://www.amazon.com/Future-Image-John-Calvelli/dp/1940813026><b>John Calvelli (2015) </b><cite>The Future Is An Image</cite></a></summary><figure><blockquote class='blockquote'><p>"It is clear that there can be no methodology for the solving of wicked problems except for one based in interpretative action: do something; evaluate its successes and consequences; do something else, based on what has been learned; repeat."</p><figcaption class='blockquote-footer'>John Calvelli<cite title=The Future Is An Image>, The Future Is An Image</cite> </figcaption></blockquote></figure></details>


<h3> Total world system</h3>


<details><summary><a href=https://www.jstor.org/stable/i270585><b>Emilio Ambasz (1971) </b><cite>I: The University of Design and Development. II: Manhattan: Capital of the Twentieth Century. III: The Designs of Freedom</cite></a></summary><figure><blockquote class='blockquote'><p>"As we are reaching a situation in which the only valid approaches will be those comprehending the totality of the world system, the urban models to be considered should behold the natural and the man-made environment - the entire planet -as a complete and totally interrelated problem."</p><figcaption class='blockquote-footer'>Emilio Ambasz<cite title=I: The University of Design and Development. II: Manhattan: Capital of the Twentieth Century. III: The Designs of Freedom>, I: The University of Design and Development. II: Manhattan: Capital of the Twentieth Century. III: The Designs of Freedom</cite> </figcaption></blockquote></figure></details>

</ul>


## Design approaches and methods in the Anthropocene

<ul class="biblist">

<li><a href=https://www.amazon.com/Design-Activism-Beautiful-Strangeness-Sustainable/dp/1844076458><b>Alastair Fuad-Luke (2017) </b><cite>Design Activism: Beautiful Strangeness for a Sustainable World</cite></a><br></li>

<h3> Embodied design</h3>

<li><a href=https://www.researchgate.net/publication/236945518_Designing_Change_by_Living_Change><b>Kakee Scott, C.A. Bakker, Jaco Quist (2012) </b><cite>Designing Change by Living Change</cite></a><br></li>

<li><a href=https://journals.sagepub.com/doi/10.1068/a43176><b>Nicky Gregson, Mike Crang (2010) </b><cite>Materiality and Waste: Inorganic Vitality in a Networked World</cite></a><br></li>

<details><summary><a href=https://www.tandfonline.com/doi/abs/10.1080/14623943.2015.1023281><b>C Hébert (?2015) </b><cite>Knowing and/or experiencing: a critical examination of the reflective models of John Dewey and Donald Schön</cite></a></summary><figure><blockquote class='blockquote'><p>"The phrase “embodied design” was first coined by van Rompay and Hekkert (2001), industrial designers who used Lakoff and Johnson's cognitive semantics theory of conceptual metaphor to predict the emotional affect that humans would attribute to architectural structures, such as bus stops. There was also a fleeting unarchived use of the phrase by Thecla Schiphorst, circa 2007. Abrahamson recycled the phrase "embodied design" into the learning sciences to describe an approach to the construction of pedagogical materials and activities that enables learners to objectify their tacit knowledge in cultural forms relevant to disciplinary content (Abrahamson, ESM 2009). (An earlier paper appeared in the proceedings of PME-NA 2007.) Later Abrahamson elaborated thus: “Embodied design is a pedagogical framework that seeks to promote grounded learning by creating situations in which students can be guided to negotiate tacit and cultural perspectives on phenomena under inquiry; tacit and cultural ways of perceiving and acting” (IDC 2013, IJCCI 2014, Cambridge Handbook of the Learning Sciences, 2014).                           - C Hébert (?2015)"</p><figcaption class='blockquote-footer'>C Hébert<cite title=Knowing and/or experiencing: a critical examination of the reflective models of John Dewey and Donald Schön>, Knowing and/or experiencing: a critical examination of the reflective models of John Dewey and Donald Schön</cite> </figcaption></blockquote></figure></details>

<li><a href=https://edrl.berkeley.edu/publications/abrahamson-d-2009-embodied-design-constructing-means-for-constructing-meaning/><b>Dor Abrahamson (2009) </b><cite>Embodied design: constructing means for constructing meaning</cite></a><br></li>

<h3> Slow media</h3>

<li><a href=https://www.anthropocene-curriculum.org/pages/root/campus-2014/slow-media/mapping-an-exercise-on-cartography/><b>Ally Bisshop and Yesenia Thibault-Picazo</b><cite>Mapping: An Exercise in Cartography </cite></a><br></li>

<h3>  Meta design</h3>

<li><a href=https://pearl.plymouth.ac.uk/bitstream/handle/10026.1/799/400424.pdf?sequence=4&isAllowed=y><b>Elisa Giaccardi (2003)</b><cite>Principles of Metadesign: Processes and Levels of Co-Creation in the New Design Space</cite></a><br></li>

<li><a href=http://l3d.cs.colorado.edu/~gerhard/papers/ead06.pdf><b>Elisa Giaccardi and Gerhard Fischer (2008) </b><cite>Creativity and Evolution: A Metadesign Perspective</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/220625827_Socio-Technical_Systems_A_Meta-Design_Perspective><b>Gerhard Fischer and Thomas Herrmann (2011) </b><cite>Socio-Technical Systems: A Meta-Design Perspective</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/226719061_Meta-design_A_Framework_for_the_Future_of_End-User_Development><b>Gerhard Fischer Elisa Giaccardi (2006) </b><cite>Meta-design: A Framework for the Future of End-User Development</cite></a><br></li>

<li><a href=https://www.researchgate.net/profile/Antonio_Piccinno/publication/316734680_Revisiting_and_Broadening_the_Meta-Design_Framework_for_End-User_Development/links/59e8b656458515c3631523ef/Revisiting-and-Broadening-the-Meta-Design-Framework-for-End-User-Development.pdf><b>Gerhard Fischer (2017) </b><cite>Revisiting and Broadening the Meta-Design Framework for End-User Development</cite></a><br></li>

<li><a href=https://sublimemagazine.com/re-inventing-inventing><b>John Wood (2018) </b><cite>RE-INVENTING INVENTING article Published in Design</cite></a><br></li>

<li><a href=https://www.academia.edu/15257979/Metadesign_Designing_the_Seeds_for_Shared_Processes_of_Change><b>Mathilda Tham and Hannah Jones (unknown) </b><cite>Metadesign tools Designing the seeds for shared processes of change</cite></a><br></li>

<details><summary><a href=https://www.oreilly.com/ideas/open-endedness-the-last-grand-challenge-youve-never-heard-of><b>Kenneth O. Stanley Joel Lehman Lisa Soros (2017) </b><cite>Open-endedness: The last grand challenge you’ve never heard of</cite></a></summary><figure><blockquote class='blockquote'><p>"Metadesign represents a cultural shift from design as “planning” to design as “seeding.” By promoting collaborative and transformational practices of design that can support new modes of human interaction and sustain an expansion of the creative process, metadesign is developing toward new ways of understanding and planning with the goal of producing more open and evolving systems of interaction. Metadesign can be seen not only as a design approach informing a speci?c design methodology for the development of interactive media and environments but also as a form of cultural strategy informing and integrating different domains. Rather than a new model  of design, metadesign represents a constructive mode  of design: an enhancement of the creative process at the convergence of “art” and “science.”           -Kenneth O. Stanley Joel Lehman Lisa Soros (2017)"</p><figcaption class='blockquote-footer'>Kenneth O. Stanley Joel Lehman Lisa Soros<cite title=Open-endedness: The last grand challenge you’ve never heard of>, Open-endedness: The last grand challenge you’ve never heard of</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.academia.edu/2113875/Metadesign_as_an_emergent_design_culture><b>Elisa Giaccardi (2006) </b><cite>Metadesign as an Emergent Design Culture</cite></a><br></li>

<li><a href=http://revistas.unisinos.br/index.php/sdrj/article/view/sdrj.2017.102.06/6085><b>Ione Maria Ghislene Bentz, Carlo Franzato (2017) </b><cite>The relationship between Strategic Design and Metadesign as defined by the levels of knowledge of design</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/305728211_Algorithms_diachronic_forms_and_metadesign><b>Remy Bourganel, Stephane Hugon Mehdi Badr (2016) </b><cite>Algorithms, diachronic forms and meta-design</cite></a><br></li>

<li><a href=http://revistas.unisinos.br/index.php/sdrj/article/viewFile/sdrj.2017.102.07/6078><b>Caio Adorno Vassão (2017) </b><cite>Design and Politics: Metadesign for social change</cite></a><br></li>

<h3> Open, distributed design systems</h3>

<li><a href=https://www.semanticscholar.org/paper/A-Framework-for-Understanding-the-Possible-of-with-Menichinelli/8bec57c5462e263046f31ff01e9368f90a9db5bf><b>Massimo Menichinelli (2016) </b><cite>A Framework for Understanding the Possible Intersections of Design with Open, P2P, Diffuse, Distributed and Decentralized Systems</cite></a><br></li>

<h3> Diagramming</h3>

<details><summary><a href=http://revistas.unisinos.br/index.php/sdrj/article/viewFile/sdrj.2017.102.07/6078><b>Caio Adorno Vassão (2017) </b><cite>Design and Politics: Metadesign for social change</cite></a></summary><figure><blockquote class='blockquote'><p>" To design making use of diagrams and topology opens up the human mind to new levels of complexity, making it reach to meta-contexts, and understanding the inherent limitations of specific social-political configurations – being that in the urban space or in the meta-space of social politics. Especially, it is very useful to utilize diagrammatics to describe and articulate social gatherings and organizations, being them short- or long-term, and small- or large-scale structures.                         - Caio Adorno Vassão (2017)"</p><figcaption class='blockquote-footer'>Caio Adorno Vassão<cite title=Design and Politics: Metadesign for social change>, Design and Politics: Metadesign for social change</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://bookstore.ams.org/chel-87/><b>Hilbert, D. and Cohn-Vossen, S. (1932) </b><cite>Preface</cite></a></summary><figure><blockquote class='blockquote'><p>" “It is possible in many cases to depict the geometric outline of the methods of investigation and proof, without necessarily entering into the details connected with the strict definitions of concepts and with the actual calculations.”"</p><figcaption class='blockquote-footer'>Hilbert, D. and Cohn-Vossen, S.<cite title=Preface>, Preface</cite> </figcaption></blockquote></figure></details>

<h3> Design for Social systems</h3>

<li><a href=https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere><b>Peter H. Jones (2014) </b><cite>Systemic Design Principles for Complex Social Systems In book: Social Systems and Design</cite></a><br></li>

<h3> Actor Network Theory and transition design</h3>

<li><a href=https://onlinelibrary.wiley.com/doi/full/10.1111/gec3.12192><b>Martin Müller (2015) </b><cite>Assemblages and Actor-networks: Rethinking Socio-material Power, Politics and Space</cite></a><br></li>

<li><a href=><b>Bruno Latour</b><cite>On actor-network theory. A few clarifications plus more than a few</cite></a><br></li>

<li><a href= ><b></b><cite>Design and anticipation: towards an organisational view of design systems</cite></a><br></li>

<li><a href=https://www.academia.edu/8058052/Design_and_anticipation_towards_an_organisational_view_of_design_systems><b>Theodore Zamenopoulos and Katerina Alexiou (2007) </b><cite>https://www.academia.edu/8058052/Design_and_anticipation_towards_an_organisational_view_of_design_systems</cite></a><br></li>

<h3> Design interventions</h3>

<details><summary><a href=https://kadk.dk/sites/default/files/1._halse_2014_design_interventions.pdf><b>Joachim Halse Laura Boffi (2014) </b><cite>Design interventions as a form of inquiry</cite></a></summary><figure><blockquote class='blockquote'><p>"design interventions as a supplement to existing research methods, one that favors and explores unsettled and imagined possibility, yet employs empiricist virtues of embodiment, empathy and documentary forms."</p><figcaption class='blockquote-footer'>Joachim Halse Laura Boffi<cite title=Design interventions as a form of inquiry>, Design interventions as a form of inquiry</cite> </figcaption></blockquote></figure></details>

<h3> Xenodesign - speculative realism</h3>

<li><a href=https://jods.mitpress.mit.edu/pub/6qb7ohpt><b>Johanna Schmeer (2019) </b><cite>Xenodesignerly Ways of Knowing</cite></a><br></li>


<h3> Inclusive design</h3>

<li><a href=https://mismatch.design/about-this-site/><b></b><cite>https://mismatch.design/about-this-site/</cite></a><br></li>

<li><a href=https://mismatch.design/resources/examples-inclusive-design/2018/06/12/inclusive-a-design-toolkit/><b></b><cite>https://mismatch.design/resources/examples-inclusive-design/2018/06/12/inclusive-a-design-toolkit/</cite></a><br></li>

<h3> Incomplete design</h3>


<details><summary><a href=https://www.researchgate.net/publication/236132597_Incomplete_by_Design_and_Designing_for_Incompleteness><b>Raghu Garud, Sanjay Jain, Philipp Tuertscher (2008) </b><cite>Incomplete by Design and Designing for Incompleteness</cite></a></summary><figure><blockquote class='blockquote'><p>"In an environment that is continually changing, designs that have been completed at a point in time are likely to become incomplete over time.   On the other hand, designs that anticipate their incompleteness are likely to remain more up to date over time."</p><figcaption class='blockquote-footer'>Raghu Garud, Sanjay Jain, Philipp Tuertscher<cite title=Incomplete by Design and Designing for Incompleteness>, Incomplete by Design and Designing for Incompleteness</cite> </figcaption></blockquote></figure></details>

<h3> Critical design</h3>


<li><a href=http://www.cd-cf.org/articles/><b></b><cite>http://www.cd-cf.org/articles/</cite></a><br></li>


<h3> Co-creation</h3>


<li><a href=https://cocreationstudio.mit.edu/wp-content/uploads/2019/06/Collective_Wisdom_Executive_Summary.pdf><b>Co-Creation Studio at MIT Open Documentary Lab (2019) </b><cite>CO-CREATING MEDIA WITHIN COMMUNITIES, ACROSS DISCIPLINES AND WITH ALGORITHMS EXECUTIVE SUMMARY</cite></a><br></li>


</ul>




## Using AI in Design

<ul class="biblist">

<li><a href=https://www.smashingmagazine.com/2017/01/algorithm-driven-design-how-artificial-intelligence-changing-design/><b></b><cite>https://www.smashingmagazine.com/2017/01/algorithm-driven-design-how-artificial-intelligence-changing-design/</cite></a><br></li>

<li><a href=http://fortune.com/2019/02/15/artificial-intelligence-ai-design/><b></b><cite>http://fortune.com/2019/02/15/artificial-intelligence-ai-design/</cite></a><br></li>


</ul>

## Maker culture

<ul class="biblist">

<details><summary><a href=http://delivery.acm.org/10.1145/3210000/3205415/p75-whelan.pdf?ip=86.140.7.178&id=3205415&acc=OPENTOC&key=4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E9F04A3A78F7D3B8D&__acm__=1565891040_8951fe5d7a32d70f3d52d6ed2a08a5c5><b>Whelan, T. (2018) </b><cite>We Are Not All Makers</cite></a></summary><figure><blockquote class='blockquote'><p>" maker practices currently form a territory that demands to be inhabited, rather than simply a set of tools to be used. For the movement to be effective in its aims, it must become as genuinely pluralistic as it purports to be, and consciously work towards a softening of identity boundaries.  The rallying cry of the maker movement is not “we all make” but rather “we are all makers”. In practice, the maker identity is easier for some to adopt than others, and despite the movement’s claims of universality, there is consistent reproduction of old paradigms of exclusion."</p><figcaption class='blockquote-footer'>Whelan, T.<cite title=We Are Not All Makers>, We Are Not All Makers</cite> </figcaption></blockquote></figure></details>

<h3> Fab lab movement</h3>

<h3> Open ended design - modification</h3>

<li><a href=https://www.tandfonline.com/doi/pdf/10.1080/14606925.2017.1352890><b>Francesca Ostuzzi, Lieven De Couvreur, Jan Detand & Jelle Saldien (2017) </b><cite>From Design for One to Open-ended Design. Experiments on understanding how to open-up contextual design solutions</cite></a><br></li>

<h3> Decolonising Making</h3>

<details><summary><a href=https://www.researchgate.net/publication/327133925_decolonizing_digital_fabrication><b>Ron Eglash (2018) </b><cite>Decolonizing digital fabrication</cite></a></summary><figure><blockquote class='blockquote'><p>"Do-it-yourself projects give researchers the equipment they need at bargain prices. But making your own technology requires commitment and time, and it is rarely easy"</p><figcaption class='blockquote-footer'>Ron Eglash<cite title=Decolonizing digital fabrication>, Decolonizing digital fabrication</cite> </figcaption></blockquote></figure></details>

<h3> Citizen / DIY science</h3>

<li><a href=https://books.google.es/books?id=7WxyDwAAQBAJ&pg=PA317&lpg=PA317&dq=%22fab+foundation%22+hackathon&source=bl&ots=UQgURJqlIO&sig=ACfU3U3HfG6wIwuyQUmgDB78hLjG5MmaRQ&hl=en&sa=X&ved=2ahUKEwjrnuLJzO7iAhVPzRoKHYs2DdEQ6AEwCHoECAkQAQ#v=onepage&q=%22fab%20foundation%22%20hackathon&f=false><b>Susanne Hecker, Muki Haklay, Anne Bowser, Zen Makuch, Johannes Vogel (edited)</b><cite>Citizen Science: Innovation in Open Science, Society and Policy </cite></a><br></li>


<li><a href=https://www.nature.com/articles/d41586-019-01590-z?utm_source=facebook&utm_medium=social&utm_content=organic&utm_campaign=NGMT_2_SJH_Nature&fbclid=IwAR2tuM1rCZVW4IoBoypwFjstNfn3ut-9SSbrrKGk7TnJC1IVgyheHhdQTfA><b>Mike May (2109) </b><cite>A DIY approach to automating your lab</cite></a><br></li>

<h3> Tech for refugees</h3>

<li><a href=- humanfablabs.org><b></b><cite>humanfablabs.org</cite></a><br></li>

<h3> AI and digital fabrication and makers</h3>

<li><a href=https://blog.hackster.io/tiny-ai-devices-invade-the-maker-movement-928c01b185df><b>Rex St John (2017) </b><cite>Tiny AI Devices Invade the Maker Movement</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/332747867_Digital_Fabrication_of_Soft_Actuated_Objects_by_Machine_Knitting><b>Lea Albaugh Scott Hudson (2019) </b><cite>Digital Fabrication of Soft Actuated Objects by Machine Knitting</cite></a><br></li>

<h3> Future of work</h3>

<li><a href=http://publications.jrc.ec.europa.eu/repository/bitstream/JRC110999/kjna29296enn.pdf><b>Paulo Rosa, Ângela Guimarães Pereira Federico Ferretti (2018) </b><cite>Futures of Work: Perspectives from the Maker Movement</cite></a><br></li>

<li><a href= ><b></b><cite> </cite></a>3d printing electronics<br></li>

<li><a href= ><b></b><cite> </cite></a>Structural electronics<br></li>

<li><a href=http://www.oejournal.org/mv_html/j00002/2018-01/180209000001_WEB.htm><b></b><cite>http://www.oejournal.org/mv_html/j00002/2018-01/180209000001_WEB.htm</cite></a><br></li>

<li><a href=http://www.edac.ethz.ch/Research/Design-of-4D-Printed-Large-Shape-Transforming-Auxetic-Structures.html><b></b><cite>http://www.edac.ethz.ch/Research/Design-of-4D-Printed-Large-Shape-Transforming-Auxetic-Structures.html</cite></a><br></li>


</ul>


## Information & Materiality

<ul class="biblist">

<li><a href=https://www.mdpi.com/2078-2489/3/1/68/htm><b>Robert K. Logan</b><cite>What Is Information?: Why Is It Relativistic and What Is Its Relationship to Materiality, Meaning and Organization</cite></a><br></li>
<li><a href=https://pdfs.semanticscholar.org/08b6/7692bc037eada8d3d7ce76cc70994e7c8116.pdf><b>E. T. JAYNES (1957) </b><cite>Information Theory and Statistical Mechanics</cite></a><br></li>
<li><a href=><b>Jonathan Minchin (2010) </b><cite>Ecological Interactions: A Genetic and Phylogeographic Framework for Growing New Innovations</cite></a><br></li>
<li><a href=https://www.sciencedirect.com/science/article/pii/S0261306915002794><b>Wiberg, M. (2016) </b><cite>Interaction, new materials & computing – Beyond the disappearing computer, towards material interactions</cite></a><br></li>
<li><a href=https://www.frontiersin.org/articles/10.3389/fevo.2019.00219/full#h3><b>Mary I. O'Connor1, Matthew W. Pennell, Florian Altermatt, Blake Matthews, Carlos J. Melián and Andrew Gonzalez (2019) </b><cite>Principles of Ecology Revisited: Integrating Information and Ecological Theories for a More Unified Science</cite></a><br></li>

</ul>

## Metamaterials /Metadevices

<ul class="biblist">

<li><a href=https://www.nature.com/articles/s42254-018-0018-y><b>Muamer Kadic, Graeme W. Milton, Martin van Hecke & Martin Wegener (2019) </b><cite>3D metamaterials</cite></a><br></li>
<li><a href=https://www.nature.com/articles/s41378-018-0042-1><b>Xiaoguang Zhao, Guangwu Duan, Aobo Li, Chunxu Chen & Xin Zhang (2019) </b><cite>Integrating microsystems with metamaterials towards metadevices </cite></a> - GREAT PAPER ABOUT ACTUATORS<br></li>
<notes></notes><li><a href= ><b></b><cite> </cite></a><br></li>
<li><a href=https://link.springer.com/chapter/10.1007/978-94-010-0738-2_25><b>Smith, D. R., Padilla, W. J., Vier, D. C., Shelby, R., Nemat-Nasser, S. C., Kroll, N., & Schultz, S. (2001) </b><cite>Left-Handed Metamaterials. Photonic Crystals and Light Localization in the 21st Century</cite></a><br></li>

(many more to add here!)

<h3> Acoustic metadevices</h3>

<h4> Overviews</h4>

<li><a href=https://link.springer.com/chapter/10.1007/978-3-319-48933-9_56><b>Fedotov V. (2017) </b><cite>Metamaterials. In: Kasap S., Capper P. (eds</cite></a><br></li>
<li><a href=https://advances.sciencemag.org/content/advances/2/2/e1501595.full.pdf><b>Guancong Ma and Ping Sheng (2016) </b><cite>Acoustic metamaterials: From local resonances to broad horizons</cite></a>  - contains coke can superlens<br></li>
<li><a href= ><b></b><cite> </cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/318494810_Electromagnetic_Inspired_Acoustic_Metamaterials_Studying_the_Applications_of_Sound-Metastructures_Interactions_Based_on_Different_Wave_Phenomena><b>Hussein Esfahlani (2017) </b><cite>Electromagnetic Inspired Acoustic Metamaterials: Studying the Applications of Sound-Metastructures Interactions Based on Different Wave Phenomena</cite></a><br></li>

<h4> Acoustic metasurfaces</h4>

<li><a href=https://www.degruyter.com/view/j/nanoph.2018.7.issue-6/nanoph-2017-0122/nanoph-2017-0122.xml><b>Bin Liang/ Jian-chun Cheng/ Cheng-Wei Qiu (2018) </b><cite>Wavefront manipulation by acoustic metasurfaces: from physics and applications</cite></a><br></li>

<h4> Acoustic imaging devices</h4>

<details><summary><a href=https://aip.scitation.org/doi/10.1063/1.4968606><b>S. Laureti, D. A. Hutchins, L. A. J. Davis, S. J. Leigh, and M. Ricci (2016) </b><cite>High-resolution acoustic imaging at low frequencies using 3D-printed metamaterials</cite></a></summary><figure><blockquote class='blockquote'><p>" Since the first experimental realizations of acoustic metamaterials, many different design strategies have been reported in the literature, although three general types can be considered:  (i)  Phononic crystals: arrays of scatterers within the material create a 3D Bragg scattering effect.The resultant negative index behaviour (?EFF< 0) allows acoustic signals to be focused at particular frequencies at a resolution beyond the usual diffraction limit.  (ii) Channeled materials: arrays of holes or channels through the thickness of the material create a set of Fabry-Peròt resonances, giving enhanced sub-wavelength imaging resolution by the coupling of evanescent waves. (iii)  Space coiling: spiral paths within a material lead to focusing effects and the possibility of negative refraction properties (?EFF< 0)."</p><figcaption class='blockquote-footer'>S. Laureti, D. A. Hutchins, L. A. J. Davis, S. J. Leigh, and M. Ricci<cite title=High-resolution acoustic imaging at low frequencies using 3D-printed metamaterials>, High-resolution acoustic imaging at low frequencies using 3D-printed metamaterials</cite> </figcaption></blockquote></figure></details>

<h4> Voxels</h4>

<li><a href=https://www.nature.com/articles/s41598-018-34581-7><b>Yangbo Xie, Yangyang Fu, Zhetao Jia, Junfei Li, Chen Shen, Yadong Xu, Huanyang Chen & Steven A. Cummer (2018) </b><cite>Acoustic Imaging with Metamaterial Luneburg Lenses</cite></a><br></li>

<h4> Array of holes</h4>

<li><a href=https://aip.scitation.org/doi/10.1063/1.4968606><b>S. Laureti1, D. A. Hutchins, L. A. J. Davis, S. J. Leigh, and M. Ricci (2016) </b><cite>High-resolution acoustic imaging at low frequencies using 3D-printed metamaterials</cite></a><br></li>

<h4> Bricks with coils</h4>

<li><a href=https://phys.org/news/2017-02-sound-shaping-metamaterial.html><b>Gianluca Memoli, Mihai Caleap, Michihiro Asakawa, Deepak R. Sahoo, Bruce W. Drinkwater & Sriram Subramanian (2017) </b><cite>Metamaterial bricks and quantization of meta-surfaces</cite></a><br></li>

<h4> Array of Coiled tunnels</h4>

<li><a href=https://iopscience.iop.org/article/10.7567/APEX.11.117301><b>Xiao Jia1, Yang Li, Yinghao Zhou, Ming Yan and Minghui Hong (2018) </b><cite>Broadband acoustic amplification via impedance-matched meta-structure resonator</cite></a><br></li>

<h4> Multiplexed surface</h4>

<li><a href=https://www.researchgate.net/publication/332449511_Systematic_design_of_multiplexed-acoustic-metasurface_hologram_with_simultaneous_amplitude_and_phase_modulations><b>Yifan Zhu, Badreddine Assouar (2019) </b><cite>Systematic design of multiplexed-acoustic-metasurface hologram with simultaneous amplitude and phase modulations</cite></a><br></li>

<h3> Energy Harvesting with metamaterials</h3>

<h5>General introduction without methodologies:</h5><br>

<li><a href=https://www.researchgate.net/publication/326008281_Metamaterials_for_Energy_Harvesting_A_Review><b>Vaishali Rawat S N Kale (2015) </b><cite>Metamaterials for Energy Harvesting A Review</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/260007855_Metamaterials-based_enhanced_energy_harvesting_A_review><b>Zhongsheng Chenn, Bin Guo, Yongmin Yang, Congcong Cheng</b><cite>Metamaterials-based enhanced energy harvesting: A review </cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>Vibroacoustic energy harvesting (VAEH)<br></li>
<li><a href= ><b></b><cite> </cite></a>Acoustic metamaterial (AM)-based vibroacousticenergy harvesting<br></li>
<li><a href= ><b></b><cite> </cite></a>Electromagnetic metamaterials (EM)-based wireless powertransmission (WPT)<br></li>

<h4> The circuit end</h4>

<h5>Antennas:</h5>

<li><a href= ><b></b><cite> </cite></a>modifying an NRF24  antenna<br></li>
<li><a href=https://www.instructables.com/id/Enhanced-NRF24L01/><b></b><cite>https://www.instructables.com/id/Enhanced-NRF24L01/</cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>Impedance matching network<br></li>
<li><a href=https://www.intechopen.com/media/chapter/49376/media/image7.png><b></b><cite>https://www.intechopen.com/media/chapter/49376/media/image7.png</cite></a><br></li>

<h5>Rectifier:</h5>

<li><a href= ><b></b><cite> </cite></a>a tunnel diode with schotki diode<br></li>
<li><a href= ><b></b><cite> </cite></a>you can get rectennas that do this bit but you still need a diode<br></li>
<li><a href= ><b></b><cite> </cite></a>Power management<br></li>
<li><a href= ><b></b><cite> </cite></a>Capacitors?<br></li>
<li><a href=http://www.emergent-rise.eu/wp-content/uploads/2018/12/4.pdf><b>Martí Boada, Antonio Lazaro</b><cite>Battery-Less Soil Moisture Measurement System Based on a NFC Device with Energy Harvesting Capability </cite></a><br></li>

<h4> Acoustic metamaterial energy harvesting</h4>
<li><a href=https://www.researchgate.net/publication/317375538_Tunable_Sub-wavelength_Acoustic_Energy_Harvesting_with_Metamaterial_Plate><b>Mourad Oudich, Yong Li (2017) </b><cite>Tunable Sub-wavelength Acoustic Energy Harvesting with Metamaterial Plate</cite></a><br></li>
<li><a href=http://iopscience-iop-org-s.vpn.whu.edu.cn:9440/article/10.1088/1361-665X/aa724e/pdf><b>Kyung Ho Sun, Jae Eun Kim, Jedo Kim and Kyungjun Song (2017) </b><cite>Sound energy harvesting using a doubly coiled-up acoustic metamaterial cavity</cite></a><br></li>
<li><a href=><b>Badreddine Assouar, Mourad Oudich, Xiaoming Zhou (2015) </b><cite>Sound insulation and energy harvesting based on acoustic metamaterial plate</cite></a><br></li>
<li><a href=https://www.sciencedirect.com/science/article/pii/S2211285518308449><b>Jaehoon Choi Inki Jungab Chong-Yun Kangab</b><cite>A brief review of sound energy harvesting</cite></a><br></li>

<h4> Infra red</h4>
<li><a href=https://substance.etsmtl.ca/en/metamaterial-capable-using-heat-loss-produce-electricity><b>XINYU LIU AND WILLIE J. PADILLA (2016) </b><cite>A Metamaterial Capable of Using Heat Loss to Produce Electricity</cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>Reconfigurable room temperature metamaterial<br></li>
<li><a href= ><b></b><cite> </cite></a>infrared emitter<br></li>


<h4> Radio energy harvesting</h4>

<h5>Overviews</h5>

<li><a href=https://doi.org/10.1186/s40486-017-0051-0><b>Tran, LG., Cha, HK. & Park, WT. (2017) </b><cite>RF power harvesting: a review on designing methodologies and applications</cite></a><br></li>
<li><a href=https://ro.uow.edu.au/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=2029&context=dubaipapers><b>Nornikman Hassan et al. (2018) </b>
<cite>Radio frequency (Rf) energy harvesting using metamaterial structure for antenna/rectenna communication network: A review </cite></a>
<br></li>

</ul>

<br>

<div style="overflow-x:auto;">
<table id="bibtable">

<tr>
<th style="width:300px">Author</th>
<th>Type of energy harvested</th>
<th>Metamaterial Configuration</th>
<th>Metamaterial Material</th>
<th style="width:200px">Experiment Equipment</th>
<th>Output result</th>
</tr>

<tr>
<td><a href=https://www.researchgate.net/publication/327905121_RADIO_FREQUENCY_RF_ENERGY_HARVESTING_USING_METAMATERIAL_STRUCTURE_FOR_ANTENNARECTENNA_COMMUNICATION_NETWORK_A_REVIEW><b>  Zhou Z., W. Liao, Q. Zhang, F. Han, Y. Chen,  (2016) </b>
<cite>A  Multi-Band  Fractal  Antenna  for  RF  Energy Harvesting, </cite></a></td>
<td>Radio </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><a href=><b> Shrestha S., S. R. Lee, and D.-Y. Choi, “A New Fractal-Based  Miniaturized  Dual  Band  Patch Antenna   for   RF   Energy   Harvesting”     (2016) </b>
<cite></cite></a></td>
<td>Radio </td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><a href=http://iopscience-iop-org-s.vpn.whu.edu.cn:9440/article/10.1088/1361-665X/aa724e/pdf><b>Kyung Ho Sun, Jae Eun Kim, Jedo Kim and Kyungjun Song  (2017) </b>
<cite>Sound energy harvesting using a doubly coiled-up acoustic metamaterial cavity</cite></a></td>
<td>Acoustic</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><a href=https://www.researchgate.net/publication/317375538_Tunable_Sub-wavelength_Acoustic_Energy_Harvesting_with_Metamaterial_Plate><b>Mourad Oudich, Yong Li (2017) </b>
<cite>Tunable Sub-wavelength Acoustic Energy Harvesting with Metamaterial Plate</cite></a></td>
<td>Acoustic</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><a href=><b>Badreddine Assouar, Mourad Oudich, Xiaoming Zhou  (2015) </b>
<cite></cite></a></td>
<td>Acoustic</td>
<td>acoustic metamaterial plate</td>
<td></td>
<td></td>
<td>Sound insulation and energy harvesting</td>
</tr>

<tr>
<td><a href=https://substance.etsmtl.ca/en/metamaterial-capable-using-heat-loss-produce-electricity><b>XINYU LIU AND WILLIE J. PADILLA (2016) </b>
<cite>A Metamaterial Capable of Using Heat Loss to Produce Electricity</cite></a></td>
<td>Infrared</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>

<tr>
<td><a href=><b>Gang Yong Song, Bei Huang, Hui Yuan Dong, Qiang Cheng & Tie Jun Cui</b>
<cite>Broadband Focusing Acoustic Lens Based on Fractal Metamaterials</cite></a></td>
<td>2 kHz and 5 kHz  </td>
<td>Fractal metamaterial lens made of 3D printed elements </td>
<td>photopolymer resin thermal plastics
</td>
<td>Linear 8-speaker array generates a plane wave, microphone scans sound pressure distributions using computer controlled 2D stepping motor.
FEM simulations using COMSOL</td>
<td>Super-resolution imaging and acoustic tunnelling.</td>
</tr>

</table>
</div>


<ul class="biblist">

<h4> SRRs</h4>

<li><a href=https://www.researchgate.net/publication/3132118_Equivalent-Circuit_Models_for_the_Design_of_Metamaterials_Based_on_Artificial_Magnetic_Inclusions><b>Filiberto Bilotti Alessandro Toscano et al. (2008) </b><cite>Equivalent-Circuit Models for the Design of Metamaterials Based on Artificial Magnetic Inclusions</cite></a><br></li>

<h5> SRR for Microfluidic Chemical Sensors</h5>

<li><a href=https://www.mdpi.com/1424-8220/16/11/1802/htm><b>Ahmed Salim and Sungjoon Lim (2016) </b><cite>Complementary Split-Ring Resonator-Loaded Microfluidic Ethanol Chemical Sensor</cite></a><br></li>

<h4> Antenna geometries (for collection of energy or other signals)</h4>

<li><a href=https://ieeexplore.ieee.org/document/7696017/><b>Zhou Z., W. Liao, Q. Zhang, F. Han, Y. Chen (2016) </b><cite>A Multi-Band Fractal Antenna for RF Energy ing, IEEE International Symposium on Antennas and Propagation</cite></a><br></li>

<li><a href=https://www.semanticscholar.org/paper/A-novel-wearable-metamaterial-Fractal-antenna-for-Ahmed-Ahmed/28376c472c5adde23ab0bb1d48374b4762b3b923><b>M. Ifjaz Ahmed, Mona F. M. Mursi Ahmed Ahmed, Abdelhamid Shaalan (2016) </b><cite>A novel wearable metamaterial Fractal antenna for wireless applications</cite></a><br></li>

<li><a href=https://www.intechopen.com/books/metamaterials-and-metasurfaces/metamaterials-in-application-to-improve-antenna-parameters><b>Wojciech Jan Krzysztofik and Thanh Nghia Cao (2018) </b><cite>Metamaterials in Application to Improve Antenna Parameters</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/317637523_Fractals_in_Antennas_and_Metamaterials_Applications><b>Wojciech Jan Krzysztofik (2017) </b><cite>Fractals in Antennas and Metamaterials Applications</cite></a><br></li>

<details><summary><a href=https://www.mdpi.com/2076-3417/8/9/1689/htm><b>Rana Sadaf Anwar 1, Lingfeng Mao 1 and Huansheng Ning (2018) </b><cite>Frequency Selective Surfaces: A Review</cite></a></summary><figure><blockquote class='blockquote'><p>"FSS is a periodic surface with identical two-dimensional arrays of elements arranged on a dielectric substrate. An incoming plane wave will either be transmitted (passband) or reflected back (stopband), completely or partially, depending on the nature of array element."</p><figcaption class='blockquote-footer'>Rana Sadaf Anwar 1, Lingfeng Mao 1 and Huansheng Ning<cite title=Frequency Selective Surfaces: A Review>, Frequency Selective Surfaces: A Review</cite> </figcaption></blockquote></figure></details>

<h3> Other applications of frequency selective surfaces</h3>

<li><a href= ><b></b><cite> </cite></a>Dynamically reconfigurable FSSs<br></li>
<li><a href= ><b></b><cite> </cite></a>Wearable sensing FSS<br></li>
<li><a href= ><b></b><cite> </cite></a>Tunable and software-defined FSSs<br></li>
<li><a href= ><b></b><cite> </cite></a>Multi-functional FSSs<br></li>
<li><a href= ><b></b><cite> </cite></a>Mechanically tunable FSS<br></li>
<li><a href= ><b></b><cite> </cite></a>Smart absorbers FSS<br></li>
<li><a href= ><b></b><cite> </cite></a>High performance tunable FSS Absorber<br></li>
<li><a href= ><b></b><cite> </cite></a>Frequency selective rasorbers<br></li>
<li><a href= ><b></b><cite> </cite></a>Doubly curved FSS surface<br></li>
<li><a href= ><b></b><cite> </cite></a>FSS for high-power radar applications<br></li>
<li><a href= ><b></b><cite> </cite></a>Integrated design of antenna-FSS system<br></li>
<li><a href= ><b></b><cite> </cite></a>Graphene based FSS<br></li>
<li><a href= ><b></b><cite> </cite></a>3D printed all-dielectric FSSs with wide BW<br></li>
<li><a href= ><b></b><cite> </cite></a>FSS for wireless power transfer applications<br></li>
<li><a href= ><b></b><cite> </cite></a>Metamaterial inspired FSS for satellite communication<br></li>
<li><a href= ><b></b><cite> </cite></a>FSS for earth observation remote sensing and satellites<br></li>
<li><a href= ><b></b><cite> </cite></a>FSS for selective shielding of frequencies in critical places such as airport and military communication.<br></li>
<li><a href= ><b></b><cite> </cite></a>FSS for isolating harmful & unwanted radiation in hospitals, schools, domestic environments at microwave low frequency bands (L, S)<br></li>
<li><a href= ><b></b><cite> </cite></a>Chipless passive structural strength monitoring FSS based wireless sensor<br></li>
<li><a href= ><b></b><cite> </cite></a>Switchable FSS based on plasma shells for aerodynamics & terrestrial EM systems protection<br></li>

<h3>  Intelligent Reconfigurable Metasurfaces</h3>

<li><a href= ><b></b><cite> </cite></a>for smart radio environments<br></li>
<details><summary><a href=https://arxiv.org/pdf/1903.08925.pdf><b>Marco Di Renzo, Merouane Debbah et al (2019) </b><cite>Smart Radio Environments Empowered by AI Reconfigurable Meta-Surfaces: An Idea Whose Time Has Come</cite></a></summary><figure><blockquote class='blockquote'><p>" The first type of meta-surfaces will be embedded into, e.g., walls, and will be directly controlled by the wireless network operators via a software controller in order to shape the radio waves for, e.g., improving the network coverage. The second type of meta-surfaces will be embedded into objects, e.g., smart t-shirts with sensors for health monitoring, and will backscatter the radio waves generated by cellular base stations in order to report their sensed data to mobile phones. These functionalities will enable wireless network operators to offer new services without the emission of additional radio waves, but by recycling those already existing for other purposes."</p><figcaption class='blockquote-footer'>Marco Di Renzo, Merouane Debbah et al<cite title=Smart Radio Environments Empowered by AI Reconfigurable Meta-Surfaces: An Idea Whose Time Has Come>, Smart Radio Environments Empowered by AI Reconfigurable Meta-Surfaces: An Idea Whose Time Has Come</cite> </figcaption></blockquote></figure></details>

<h3> Nanocomposite metamaterials (there are many)</h3>

<li><a href=https://www.sciencedirect.com/science/article/pii/S0030402617311518><b>Kim Han et al (2017) </b><cite>Fabrications of Nanocomposite Gold-Polymer Metamaterials Consisting of Periodic Microcavities with Tunable Optical Properties</cite></a><br></li>

<li><a href=https://iopscience.iop.org/article/10.1088/1361-665X/aa8832/meta><b>Mohamed Ali E Kshad1, Clement D'Hondt and Hani E Naguib (2017) </b><cite>Carbon nano fibers reinforced composites origami inspired mechanical metamaterials with passive and active properties</cite></a><br></li>

<h3> Optical metamaterials from soft matter</h3>

<li><a href=https://link.springer.com/article/10.1007/s10409-017-0657-8><b>Xiangjun Peng1, Wei He1, Yifan Liu1, Fengxian Xin1, Tian Jian Lu1, (2017) </b><cite>Optomechanical soft metamaterials</cite></a><br></li>

<li><a href=https://link.springer.com/article/10.1007/s12274-019-2437-1><b>Uri R. GabinetChinedum O. Osuji (2019) </b><cite>Optical materials and metamaterials from nanostructured soft matter</cite></a><br></li>

<li><a href=https://www.cambridge.org/core/journals/mrs-bulletin/article/sculpting-light-by-arranging-optical-components-with-dna-nanostructures/191F0D2DBE8BB6613C6229B6E13200D8><b>Mauricio Pilo-Pais , Guillermo P. Acuna , Philip Tinnefeld , and Tim Liedl (2017) </b><cite>Sculpting light by arranging optical components with DNA nanostructures</cite></a><br></li>

<h3> Seismic metastructures</h3>
<li><a href=https://arxiv.org/ftp/arxiv/papers/1712/1712.09115.pdf><b>Brûlé, Stefan Enoch1 and Sébastien Guenneau (2017) </b><cite>Emergence of Seismic Metamaterials: Current State and Future Perspective Stéphane</cite></a><br></li>

<li><a href=https://physics.aps.org/featured-article-pdf/10.1103/PhysRevLett.112.133901><b>S. Brûlé, E. H. Javelaud, S. Enoch, and S. Guenneau Ménard (2014) </b><cite>Experiments on Seismic Metamaterials: Molding Surface Waves</cite></a><br></li>

<h3> Mechanical metamaterials</h3>

<li><a href=https://journals.sagepub.com/doi/10.1177/1081286517735695><b>Emilio Barchiesi, Mario Spagnuolo, Luca Placidi (2018) </b><cite>Mechanical metamaterials: a state of the art</cite></a><br></li>

<li><a href=https://pdfs.semanticscholar.org/b7b5/c1349c39c51de6f2c3598b8d114fa0f5e7dd.pdf?_ga=2.113573831.1138369354.1565897053-1729112660.1558029616><b>Kenneth C. Cheung, Neil Gershenfeld (2013) </b><cite>Reversibly Assembled Cellular Composite Materials</cite></a><br></li>

<li><a href=https://pubs.rsc.org/en/content/articlelanding/2016/mh/c6mh00065g#!divAbstract><b>Amir A. Zadpoora (2016) </b><cite>Mechanical meta-materials Issue 5, Materials Horizons</cite></a><br></li>

<details><summary><a href=https://pubs.rsc.org/en/content/articlelanding/2017/ra/c6ra27333e#!divAbstract><b>H. M. A. Kolken and A. A. Zadpoora (2017) </b><cite>Auxetic mechanical metamaterials</cite></a></summary><figure><blockquote class='blockquote'><p>"Auxetic mechanical metamaterials"</p><figcaption class='blockquote-footer'>H. M. A. Kolken and A. A. Zadpoora<cite title=Auxetic mechanical metamaterials>, Auxetic mechanical metamaterials</cite> </figcaption></blockquote></figure></details>

<h4> Hierarchical metastructures</h4>

<details><summary><a href=https://ldrd-annual.llnl.gov/ldrd-annual-2016/materials/ultralight><b>Jianchao Ye (2016) </b><cite>Ultralight Mechanical Metamaterials with Ordered Hierarchies</cite></a></summary><figure><blockquote class='blockquote'><p>"These materials are 107 times larger than their smallest structural feature at the nanoscale, with architectures nested throughout 7 levels of length scale, creating hierarchies from tens of centimeters down to nanometers.                Jianchao Ye (2016)"</p><figcaption class='blockquote-footer'>Jianchao Ye<cite title=Ultralight Mechanical Metamaterials with Ordered Hierarchies]>, Ultralight Mechanical Metamaterials with Ordered Hierarchies</cite> </figcaption></blockquote></figure></details>

<h3> Information / Digital materials and their assembly</h3>

<li><a href= ><b></b><cite> </cite></a>Three-dimensional assembly of mass-produced two-dimensional components of digital material.<br></li>

<li><a href=http://cba.mit.edu/docs/papers/06.09.digital_materials.pdf><b>George A. Popescu, Tushar Mahale, Neil Gershenfeld (2016?) </b><cite>Digital materials for digital printing</cite></a><br></li>

<li><a href=http://cba.mit.edu/docs/papers/06.09.digital_printing.pdf><b>George A. Popescu, Patrik Kunzler, Neil Gershenfeld Digital (2009?) </b><cite>Printing of Digital Materials</cite></a> This describes the printer for the above<br></li>

<li><a href=https://doi.org/10.1515/nanoph-2019-0006><b>Lianlin Li and Tie Jun Cui (20192) </b><cite>Information metamaterials – from effective media to real-time information processing systems</cite></a><br></li>

<h3> Metamaterials in Nature</h3>

<li><a href=http://content.metamaterial.com/blog/rare-natural-metamaterials><b></b><cite>http://content.metamaterial.com/blog/rare-natural-metamaterials</cite></a><br></li>

<h4> Structural colour</h4>

</ul>


## Reconfigurable structures

<ul class="biblist">


<li><a href=https://dl.acm.org/citation.cfm?doid=1706299.1706301><b></b><cite>https://dl.acm.org/citation.cfm?doid=1706299.1706301</cite></a><br></li>

<h3> Quantum metamaterials</h3>

<li><a href=https://www.nature.com/collections/ydsxkfvwws/><b></b><cite>https://www.nature.com/collections/ydsxkfvwws/</cite></a><br></li>
<li><a href=https://www.nature.com/articles/nmat5010?proof=true><b>Nitin Samarth Quantum (2017) </b><cite>materials discovery from a synthesis perspective</cite></a><br></li>
<li><a href=https://iopscience.iop.org/chapter/978-1-64327-368-6/bk978-1-64327-368-6ch2.pdf><b>Igor I Smolyaninov (2018) </b><cite>Modeling of time with metamaterials: metamaterial models of the Big Bang, the 'end of time', and the fractal time</cite></a><br></li>

</ul>

## Inverse/ Generative design of materials

<ul class="biblist">

<h3> Heuristics and design optimization methods (not AI)</h3>

<li><a href=http://www.mit.edu/~moshref/Publications.html><b></b><cite>Mohammad Moshref-Javadi's publications</cite></a><br></li>

<h3> Finite Element Analysis and MATLAB</h3>

<li><a href=http://resolver.caltech.edu/CaltechAUTHORS:20171115-144804382><b>Matlack, Kathryn H. and Serra-Garcia, Marc and Palermo, Antonio and Huber, Sebastian D. and Daraio, Chiara (2018) </b><cite>Designing perturbative metamaterials from discrete models</cite></a><br></li>

<li><a href=https://www.osti.gov/servlets/purl/1457923><b>Walsh, Timothy; Aquino, Wilkins (2017) </b><cite>Large-Scale Inverse Methods and Design of Acoustic Metamaterials in Sierra-SD</cite></a><br>Sierra-SD is a massively parallel finite element application for structural dynamics and acoustics.</li>

<li>COMSOL</li>

<li><a href=https://www.groundai.com/project/deep-learning-reveals-underlying-physics-of-light-matter-interactions-in-nanophotonic-devices/1><b>Yashar Kiarashinejad, Sajjad Abdollahramezani, Mohammadreza Zandehshahvar , Omid Hemmatyar , Ali Adibi (2019) </b><cite>Deep Learning Reveals Underlying Physics of Light-matter Interactions in Nanophotonic Devices</cite></a><br></li>

<li><a href= ><b></b><cite>https://hal.archives-ouvertes.fr/hal-02270084/document </cite></a>https://hal.archives-ouvertes.fr/hal-02270084/document<br></li>

<h3> Key inverse design of metamaterial papers (non-AI)</h3>

<li><a href=https://pubs.acs.org/doi/abs/10.1021/acs.nanolett.8b03171><b>Zhaocheng Liu, Dayu Zhu, Sean P. Rodrigues, Kyu-Tae Lee, and Wenshan Cai (2018) </b><cite>Generative Model for the Inverse Design of Metasurfaces</cite></a>  also https://arxiv.org/ftp/arxiv/papers/1805/1805.10181.pdf<br></li>

<li><a href=https://www.researchgate.net/publication/331941989_Inverse-designed_metastructures_that_solve_equations><b>Nasim Mohammadi Estakhri, Brian Edwards, Nader Engheta (2019) </b><cite>Inverse-designed metastructures that solve equations</cite></a><br></li>

<details><summary><a href=https://www.ncbi.nlm.nih.gov/pubmed/30337522><b>Zhang, L., Chen, X. Q., Liu, S. et al. (2018) </b><cite>Space-time-coding digital metasurfaces</cite></a></summary><figure><blockquote class='blockquote'><p>"digital coding metasurfaces make it possible to control electromagnetic (EM) waves in real time, and allow the implementation of many different functionalities in a programmable way."</p><figcaption class='blockquote-footer'>Zhang, L., Chen, X. Q., Liu, S. et al.<cite title=Space-time-coding digital metasurfaces>, Space-time-coding digital metasurfaces</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.degruyter.com/downloadpdf/j/nanoph.2019.8.issue-3/nanoph-2018-0183/nanoph-2018-0183.pdf><b>Kan Yao, Rohit Unni and Yuebing Zheng (2018) </b><cite>Intelligent nanophotonics: merging photonics and artificial intelligence at the nanoscale</cite></a><br></li>

<li><a href=http://www.andreesteva.com/assets/undergrad_thesis.pdf><b>Carlos Andres Esteva</b><cite>Metamaterial Structural Design: Creating optical-frequency metamaterials with plasmonic nano-particle arrangements and generating unit cells with evolutionary algorithms</cite></a><br></li>

<h4> Coding metamaterials</h4>

<li><a href=https://www.nature.com/articles/lsa201499><b>Tie Jun Cui Mei Qing Qi Mei Qing Qi Xiang Wan Xiang Wan et al (2014) </b><cite>Coding metamaterials, digital metamaterials and programmable metamaterials</cite></a><br></li>
<li><a href=https://iopscience.iop.org/article/10.1088/2040-8986/aa7009/meta><b>Cui, T. J. (2017) </b><cite>Microwave metamaterials—from passive to digital and programmable controls of electromagnetic waves</cite></a><br></li>

<h4> Photonics</h4>

<li><a href=https://www.cambridge.org/core/journals/mrs-bulletin/article/perspectives-on-frontiers-in-electronic-and-photonic-materials/01EC402D4926F2D13313AD58F29A02FA><b>Andrea Alù, Lincoln J. Lauhon, Xiaoqin Li, Chih-Kang Shih (2018) </b><cite>Perspectives on frontiers in electronic and photonic materials</cite></a><br></li>

<h4> Egs of inverse design problems in for acoustic metamaterials</h4>

<details><summary><a href=https://www.osti.gov/servlets/purl/1457923><b>Walsh, Timothy; Aquino, Wilkins (2017) </b><cite>Large-Scale Inverse Methods and Design of Acoustic Metamaterials in Sierra-SD</cite></a></summary><figure><blockquote class='blockquote'><p>Categories of Inverse Problems:
    -  Imaging
    -  Medical ultrasound
    -  Seismic exploration
    -  Calibration of material models
    -  Structural material properties, circuits, thermal properties, etc.
    -  Force reconstruction
    -  Sub-structuring for mechanical testing of components
    -  Optimal Experimental Design
    -  Best placement of sensors, test fixture setups
    -  Shape reconstruction E.g. inverse scattering
                      - Walsh, Timothy; Aquino, Wilkins (2017)"</p><figcaption class='blockquote-footer'>Walsh, Timothy; Aquino, Wilkins<cite title=Large-Scale Inverse Methods and Design of Acoustic Metamaterials in Sierra-SD>, Large-Scale Inverse Methods and Design of Acoustic Metamaterials in Sierra-SD</cite> </figcaption></blockquote></figure></details>


<h3> Microstructures</h3>

<li><a href=https://arxiv.org/pdf/1706.03189.pdf><b>BO ZHU, MELINA SKOURAS, DESAI CHEN, WOJCIECH MATUSIK, MIT CSAIL (2017) </b><cite>Two-Scale Topology Optimization with Microstructures</cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>3D printing<br></li>

<h3> Energy absorption - Thermodynamic properties</h3>

<li><a href=https://onlinelibrary.wiley.com/doi/abs/10.1002/admt.201800419><b>Shangqin Yuan Chee Kai Chua Kun Zhou (2018) </b><cite>3D-Printed Mechanical Metamaterials with High Energy Absorption</cite></a><br></li>

<h3> Lithium Battery cells</h3>

<li><a href=https://www.it4i.cz/supercomputing-projects/?lang=en><b>Dominik Legut</b><cite>Optimization design of functional materials in a new type of lithium based battery</cite></a><br></li>

<h3> Catalysts</h3>

<li>Nørskov and Bligaard<</li>

</ul>




## Taxonomies of materials

<ul class="biblist">

<li><a href=https://www.nature.com/articles/s41524-019-0173-4><b>Juan J. de Pablo, Nicholas E. Jackson et al. (2019) </b><cite>New frontiers for the materials genome initiative</cite></a><br></li>

<li><a href=https://www.nature.com/articles/s41524-018-0107-6><b>Lauri Himanen, Patrick Rinke & Adam Stuart Foster (2018) </b><cite>Materials structure genealogy and high-throughput topological classification of surfaces and 2D materials</cite></a><br></li>

<details><summary><a href=https://www.nomad-coe.eu/uploads/images/Outreach/Publications/2018_DraxlSchefflerMRSBulletin.pdf><b>Claudia Draxl and Matthias Scheffler (2018) </b><cite>NOMAD: The FAIR concept for big data-driven materials science</cite></a></summary><figure><blockquote class='blockquote'><p>"The Novel Materials Discovery (NOMAD) Laboratory maintains the largest Repository, for input and output files of all important computational materials science codes. From its open-access data, it builds several Big-Data Services helping to advance materials science and engineering.               -  Claudia Draxl and Matthias Scheffler (2018), https://nomad-coe.eu/          "</p><figcaption class='blockquote-footer'>Claudia Draxl and Matthias Scheffler<cite title=NOMAD: The FAIR concept for big data-driven materials science>, NOMAD: The FAIR concept for big data-driven materials science</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.nomad-coe.eu/uploads/images/Outreach/Publications/Materials_struture_geneaology_Foster_2018.pdf><b>Lauri Himanen1, Patrick Rinke and Adam Stuart Foster (2018) </b><cite>Materials structure genealogy and high-throughput topological classification of surfaces and 2D materials</cite></a><br></li>

<li><a href=https://doi.org/10.1121/1.4950726><b>Noori Kim, Yong-Jin Yoon , and Jont B. Allen (2016) </b><cite>Generalized metamaterials: Definitions and taxonomy</cite></a><br></li>

<li><a href=https://www.nature.com/articles/natrevmats20154><b>Anubhav Jain, Yongwoo Shin & Kristin A. Persson (2016) </b><cite>Computational predictions of energy materials using density functional theory</cite></a><br></li>

<li><a href=https://aip.scitation.org/doi/full/10.1063/1.4812323><b>Anubhav Jain, Shyue Ping Ong et al. (2013) </b><cite>Commentary: The Materials Project: A materials genome approach to accelerating materials innovation</cite></a><br></li>



</ul>

## Machine Learning for material discovery

<ul class="biblist">


<h3> In the context of all material discovery methods</h3>

<details><summary><a href=https://dash.harvard.edu/bitstream/handle/1/35164974/233.pdf><b>Aspuru-Guzik, Alán, and Kristin Persson (2018) </b><cite>Materials Acceleration Platform: Accelerating Advanced Energy Materials Discovery by Integrating High-Throughput Methods and Artificial Intelligence.</cite></a></summary><figure><blockquote class='blockquote'><p>"We are now entering an age where integrated systems using increasing computer power, machine learning, and AI could not only enable exhaustive searching for new materials, but could also provide intermediate decision-making capabilities to accelerate the discovery and development of materials with specific, targeted properties. In principle, such AI-based algorithms can be adaptive, using feedback from experimental characterization procedures and enabling a more rational route to research objectives—either optimizing properties or even directly verifying hypotheses. If realized, such an approach would markedly increase research productivity, reduce its cost, and enable entirely new experimental paradigms that are critical for innovation                                - (Aspuru-Guzik & Persson, 2018)   "</p><figcaption class='blockquote-footer'>Aspuru-Guzik, Alán, and Kristin Persson<cite title=Materials Acceleration Platform: Accelerating Advanced Energy Materials Discovery by Integrating High-Throughput Methods and Artificial Intelligence.>, Materials Acceleration Platform: Accelerating Advanced Energy Materials Discovery by Integrating High-Throughput Methods and Artificial Intelligence. </cite>  </figcaption></blockquote></figure></details>

 <li><a href=https://doi.org/10.1038/s41578-019-0101-8><b>Oganov, A.R., Pickard, C.J., Zhu, Q. et al (2019) </b><cite>Structure prediction drives materials discovery</cite></a><br></li>


<h3> Overviews / Reviews of machine learning in material discovery</h3>

<li><a href=https://iopscience.iop.org/article/10.1088/2515-7639/ab291e/pdf><b>Steven L Brunton and J Nathan Kutz (2019) </b><cite>Methods for data-driven multiscale model discovery for materials</cite></a><br></li>
<li><a href=https://www.frontiersin.org/articles/10.3389/fmats.2019.00110/full><b>Frederic E. Bock, Roland C. Aydin, Christian J. Cyron Norbert Huber, Surya R. Kalidindi and Benjamin Klusemann (2019) </b><cite>A Review of the Application of Machine Learning and Data Mining Approaches in Continuum Materials Mechanics</cite></a><br></li>
<li><a href=https://www.sciencedirect.com/science/article/pii/S2352847817300515><b>Yue Liua, Tianlu Zhaoa, Wangwei Jua, Siqi Shibc (2017) </b><cite>Materials discovery and design using machine learning</cite></a><br></li>
<li><a href=https://iopscience.iop.org/article/10.1088/2515-7639/ab084b/pdf><b>Gabriel R Schleder et al (2019) </b><cite>From DFT to machine learning: recent approaches</cite></a><br></li>
<li><a href=https://iopscience.iop.org/article/10.1088/1757-899X/392/6/062197/pdf><b>Zhimiao Fang et al (2018) </b><cite>Research on Computer Information Processing Technology in the “BigData” Era</cite></a><br></li>
<li><a href=https://www.nature.com/articles/s41586-018-0337-2><b>Keith T. Butler, Daniel W. Davies, Hugh Cartwright, Olexandr Isayev & Aron Walsh (2018) </b><cite>Machine learning for molecular and materials science</cite></a> - lots of datasets listed<br></li>
<li><a href=https://doi.org/10.1039/C7ME00062F><b>Gadelrab, K. R., Hannon, A. F., Ross, C. A., & Alexander-Katz, A. (2017) </b><cite>Inverting the design path for self-assembled block copolymers.</cite></a><br></li>
<li><a href=https://science.sciencemag.org/content/361/6400/360><b>Benjamin Sanchez-Lengeling, Alán Aspuru-Guzik (2018) </b><cite>Inverse molecular design using machine learning: Generative models for matter engineering</cite></a><br></li>




<h4> Overviews by applications</h4>
<li><a href=https://towardsdatascience.com/the-advent-of-architectural-ai-706046960140><b>Stanislas Chaillou (2017) </b><cite>The Advent of Architectural AI - A Historical Perspective</cite></a><br></li>
<li><a href=https://pdfs.semanticscholar.org/c41e/483fee61e5040eeb49859a481352f8e3f8ac.pdf><b>HAIPENG ZENG (2016) </b><cite>TOWARDS BETTER UNDERSTANDING OF DEEP LEARNING WITH VISUALIZATION</cite></a><br></li>
<li><a href=https://www.nature.com/articles/s41524-017-0056-5><b>Rampi Ramprasad, Rohit Batra, Ghanshyam Pilania, Arun Mannodi-Kanakkithodi & Chiho Kim (2017) </b><cite>Machine learning in materials informatics: recent applications and prospects</cite></a><br></li>


<h5> For self assembly</h5>

<li><a href=https://www.advancedsciencenews.com/human-intelligence-and-experiential-learning-in-materials-discovery/><b>Geoffrey A Ozin, Chenxi Qian, Todd Siler (2018) </b><cite>Human Intelligence and Experiential Learning in Materials Discovery</cite></a><br></li>



<h5> Machine learning for sustainable materials</h5>
<li><a href=https://pubs.rsc.org/en/content/articlelanding/2019/ta/c9ta02356a#!divAbstract><b>Geun Ho Gu,a Juhwan Noh,a Inkyung Kima and Yousung Jung (2019) </b><cite>Machine learning for renewable energy materials</cite></a><br></li>




<h4> AI as scientists</h4>


<details><summary><a href=https://cloudfront.escholarship.org/dist/prd/content/qt65v9z5vp/qt65v9z5vp.pdf?t=p9uz6m><b>Daniel P. Tabor et al. (2018) </b><cite>Accelerating the discovery of materials for clean energy in the era of smart automation</cite></a></summary><figure><blockquote class='blockquote'><p>"In 2009, a hypothetico-deductive ‘robot scientist’, named Adam, was developed that could autonomously perform experiments, devise hypotheses and design experiments to validate the hypotheses in the area of functional genomics.                  "</p><figcaption class='blockquote-footer'>Daniel P. Tabor et al.<cite title=Accelerating the discovery of materials for clean energy in the era of smart automation>, Accelerating the discovery of materials for clean energy in the era of smart automation</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://www.nature.com/articles/s41524-019-0172-5><b>Mitsutaro Umehara, Helge S. Stein, Dan Guevarra, Paul F. Newhouse, David A. Boyd & John M. Gregoire (2019) </b><cite>Analyzing machine learning models to accelerate generation of fundamental materials insights</cite></a></summary><figure><blockquote class='blockquote'><p>"By building a model that predicts performance (in this case photoelectrochemical power generation of a solar fuels photoanode) from materials parameters (in this case composition and Raman signal), subsequent analysis of gradients in the trained model reveals key data relationships that are not readily identified by human inspection or traditional statistical analyses.            "</p><figcaption class='blockquote-footer'>Mitsutaro Umehara, Helge S. Stein, Dan Guevarra, Paul F. Newhouse, David A. Boyd & John M. Gregoire<cite title=Analyzing machine learning models to accelerate generation of fundamental materials insights>, Analyzing machine learning models to accelerate generation of fundamental materials insights</cite> </figcaption></blockquote></figure></details>



<h4> AutoML</h4>

<li><a href=https://www.h2o.ai/blog/custom-machine-learning-recipes-the-ingredients-for-success/><b></b><cite>https://www.h2o.ai/blog/custom-machine-learning-recipes-the-ingredients-for-success/</cite></a><br></li>


<h3> ML Methods</h3>

<h4> Genetic algorithms for material discovery</h4>

<details><summary><a href=https://www.nature.com/articles/s41524-019-0181-4><b>Paul C. Jennings, Steen Lysgaard, Jens Strabo Hummelshøj, Tejs Vegge & Thomas Bligaard (2019) </b><cite>Genetic algorithms for computational materials discovery accelerated by machine learning</cite></a></summary><figure><blockquote class='blockquote'><p>"Genetic algorithms (GAs) are metaheuristic optimization algorithms inspired by Darwinian evolution. Performing crossover, mutation, and selection operations, the algorithm progresses a population of evolving candidate solutions. Selecting well-designed operators and optimal parameters, GAs have exhibited a high degree of robustness in terms of finding ideal solutions to difficult optimization problems.        "</p><figcaption class='blockquote-footer'>Paul C. Jennings, Steen Lysgaard, Jens Strabo Hummelshøj, Tejs Vegge & Thomas Bligaard<cite title=Genetic algorithms for computational materials discovery accelerated by machine learning>, Genetic algorithms for computational materials discovery accelerated by machine learning</cite> </figcaption></blockquote></figure></details>


<h4> Geometric Deep Learning</h4>

<li><a href=http://vgl.ict.usc.edu/Research/GeometricDeepLearning/GEOMETRIC%20DEEP%20LEARNING.pdf><b>Jonathan Masci, Emanuele Rodolà, Davide Boscaini, Michael M. Bronstein, Hao Li Geometric Deep Learning (2016) </b><cite>SIGGRAPH ASIA 2016 COURSE NOTES</cite></a><br></li>



<h4> Spectral Learning</h4>

<details><summary><a href=https://nlp.stanford.edu/pubs/spectral.pdf><b>Sepandar D. Kamvar, Dan Klein, Christopher D. Manning (2003) </b><cite>Spectral Learning</cite></a></summary><figure><blockquote class='blockquote'><p>"We present here a probabilistic model and an associated spectral learning algorithm that is able to work for the unsupervised, semi-supervised, and fully-supervised learning problems. We show that this algorithm is able to cluster well, and further is able to effectively utilize prior knowledge, either given by pairwise constraints or by labeled examples.      "</p><figcaption class='blockquote-footer'>Sepandar D. Kamvar, Dan Klein, Christopher D. Manning<cite title=Spectral Learning]>, Spectral Learning</cite> </figcaption></blockquote></figure></details>


<h4> ResNets</h4>

<details><summary><a href=https://towardsdatascience.com/an-overview-of-resnet-and-its-variants-5281e2f56035><b>Vincent Fung (2017) </b><cite>An Overview of ResNet and its Variants</cite></a></summary><figure><blockquote class='blockquote'><p>"deeper network usually requires weeks for training, making it practically infeasible in real-world applications. To tackle this issue, Huang et al. [10] introduced a counter-intuitive method of randomly dropping layers during training, and using the full network in testing.          "</p><figcaption class='blockquote-footer'>Vincent Fung<cite title=An Overview of ResNet and its Variants]>, An Overview of ResNet and its Variants</cite> </figcaption></blockquote></figure></details>


<h4> Convolutional Networks</h4>

<li><a href=https://towardsdatascience.com/an-easy-guide-to-gauge-equivariant-convolutional-networks-9366fb600b70><b>Michael Kissner (2019) </b><cite>An Easy Guide to Gauge Equivariant Convolutional Networks</cite></a><br></li>

<h4> DenseNet (Densely Connected Convolutional Networks)</h4>

<li><a href=https://arxiv.org/abs/1608.06993><b>Gao Huang, Zhuang Liu, Laurens van der Maaten, Kilian Q. Weinberger (2016) </b><cite>Densely Connected Convolutional Networks</cite></a><br></li>


<h3> For Metadevices</h3>

<li><a href=https://www.osapublishing.org/ome/abstract.cfm?uri=ome-9-4-1842><b>SAWYER D. CAMPBELL, DAVID SELL,RONALD P. JENKINS, ERIC B.WHITING, JONATHAN A. FAN,AND DOUGLAS H. WERNER (2019) </b><cite>Review of numerical optimization techniques for meta-device design</cite></a><br></li>

<li><a href=https://arxiv.org/abs/1810.11709><b>Kan Yao, Rohit Unni and Yuebing Zheng (2018) </b><cite>Intelligent Nanophotonics: Merging Photonics and Artificial Intelligence at the Nanoscale</cite></a>big review of DNN in nanophotonics<br></li>

<h4> Neural networks in inverse design of metadevices</h4>

<li><a href=https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6361971/><b>Mohammad H. Tahersima, Keisuke Kojima, Toshiaki Koike-Akino1, Devesh Jha1, Bingnan Wang, Chungwei Lin, Kieran Parsons (2019) </b><cite>Deep Neural Network Inverse Design of Integrated Photonic Power Splitters</cite></a><br></li>

<li><a href=https://www.nature.com/articles/s41598-018-19796-y><b>F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin (2018) </b><cite>Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices</cite></a><br></li>

<h5>Data-driven inverse design of metamaterials</h5>

<li><a href=https://arxiv.org/ftp/arxiv/papers/1901/1901.10819.pdf><b>Wei Ma, Feng Cheng, Yihao Xu, Qinlong Wen and Yongmin Liu (2019) </b><cite>Probabilistic representation and inverse design of metamaterials based on a deep generative model with semi-supervised learning strategy</cite></a><br></li>

<li><a href=https://arxiv.org/ftp/arxiv/papers/1805/1805.05039.pdf><b>Jiaqi Jiang, David Sell, Stephan Hoyer, Jason Hickey, Jianji Yang and Jonathan A. Fan (2018) </b><cite>Data-driven metasurface discovery</cite></a> generating large datasets from small ones. GREAT DIAGRAMS of pixels to find probablities<br></li>

<li><a href=https://arxiv.org/abs/1702.07949><b>Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf & Haim Suchowski (2017) </b><cite>Plasmonic nanostructure design and characterization via Deep Learning</cite></a><br></li>
<li><a href= ><b></b><cite> </cite></a>DNN - includes data set and details of methodology of using Tensorflow and Lumerical’s FDTD simulation package<br></li>

<li><a href=https://arxiv.org/abs/1710.04724><b>Dianjing Liu, Yixuan Tan, Erfan Khoram, and Zongfu Yu (2017) </b><cite>Training deep neural networks for the inverse design of nanophotonic structures</cite></a><br></li>

<li><a href=https://ieeexplore.ieee.org/document/8806880><b>Z. Lin and S. G. Johnson (2019) </b><cite>Freeform inverse design in photonics by re-thinking the question</cite></a><br></li>
 </ul>

<br>

<div style="overflow-x:auto;">
<table id="bibtable">

<tr>
<th style="width:300px">Author</th>
<th>Application</th>
<th>Shape data set</th>
<th>Simulation software</th>
<th style="width:200px">Experiment Equipment</th>
<th>Training dataset size</th>
<th>Testing dataset size</th>
<th>Neural network and analysis used</th>
</tr>

<tr>
<td>
<a href=https://www.researchgate.net/publication/330862750_Deep_Neural_Network_Inverse_Design_of_Integrated_Photonic_Power_Splitters
<b>Tahersima et al </b>
<cite> Title </cite>
</a></td>
<td>Photonics</td>
<td>Randomly generated monochrome Pixel arrays</td>
<td>Simulation software</td>
<td>Experiment Equipment</td>
<td>20,000 </td>
<td>4000</td>
<td>Neural network and analysis used</td>
</tr>

<tr>
<td>
<a href=https://www.nature.com/articles/s41598-018-19796-y
<b>F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin </b>
<cite>Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices </cite>
</a></td>
<td></td>
<td>Shape data set</td>
<td>Simulation software</td>
<td>(f > 30 GHz) using high impact polystyrene (HIPS) and a consumer grade 3D-printer based on fused deposition modeling for the fabrication. The material is chosen for its low cost and very low attenuation in the microwave to millimeter-wave region</td>
<td>Training dataset size</td>
<td>Testing dataset size</td>
<td>Neural network and analysis used</td>
</tr>

<tr>
<td>
<a href=https://
<b>Author & year </b>
<cite> Title </cite>
</a></td>
<td>Application</td>
<td>Shape data set</td>
<td>Simulation software</td>
<td>Experiment Equipment</td>
<td>Training dataset size</td>
<td>Testing dataset size</td>
<td>Neural network and analysis used</td>
</tr>


<tr>
<td>
<a href=https://
<b>Author & year </b>
<cite> Title </cite>
</a></td>
<td>Application</td>
<td>Shape data set</td>
<td>Simulation software</td>
<td>Experiment Equipment</td>
<td>Training dataset size</td>
<td>Testing dataset size</td>
<td>Neural network and analysis used</td>
</tr>

</table>
</div>



<ul class="biblist">

<h3> Other Deep Learning examples in Material Science</h3>

<li><a href=https://www.bnl.gov/compsci/projects/deep-learning.php><b>Brookhaven National Laboratory Center</b><cite>Computation and Data-Driven Discovery (C3D) Projects</cite></a><br></li>


<li><a href=https://pubs.rsc.org/en/content/articlelanding/2013/sm/c3sm51449h#!divAbstract><b>Carolyn L. Phillips and Gregory A. Vothab</b><cite>Discovering crystals using shape matching and machine learning</cite></a><br></li>
<li><a href=https://aip.scitation.org/doi/10.1063/1.5046839><b>He Zhao, Yixing Wang, Anqi Lin, Bingyin Hu, Rui Yan, James McCusker, Wei Chen, Deborah L. McGuinness, Linda Schadler &d L. Catherine Brinson (2018) </b><cite>NanoMine schema: An extensible data representation for polymer nanocomposites</cite></a><br></li>


<h3> Compliant Mechanisms</h3>
<li><a href= ><b></b><cite> </cite></a>A compliant mechanism is a mechanism that gains at least some of its mobility from the deflection of flexible members rather than from movable joints only.  <br></li>

<li><a href=https://www.compliantmechanisms.byu.edu/maker-resources><b></b><cite>The Compliant Mechanisms Research group at Brigham Young University </cite></a>The Compliant Mechanisms Research group at Brigham Young University aim to use machine learning to build the minimum viable product sketches that enable a reduction of friction and more efficient production processes.
They have a maker resource page!<br></li>




<h3> Zeolites</h3>

<details><summary><a href=https://www.it4i.cz/supercomputing-projects/?lang=en><b>Miroslav Rubeš , Czech national supercomputing centre</b><cite>A machine learning approach for the description of zeolites, </cite></a></summary><figure><blockquote class='blockquote'><p>"zeolites, which are used as detergents, catalysts, and adsorbents. In 2017, the value of the global zeolite market was about 30 billion US dollars. The objective of Miroslav Rubeš project is to use machine learning algorithms to create a model which enables deeper understanding of the phenomena occurring in zeolitic materials."</p><figcaption class='blockquote-footer'>Miroslav Rubeš , Czech national supercomputing centre<cite title=A machine learning approach for the description of zeolites, >, A machine learning approach for the description of zeolites, </cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.chemistryworld.com/features/the-dream-of-zeolite-design/9076.article><b>Andrew Turley(2015) </b><cite>The dream of zeolite design, article in Chemistry World</cite></a><br></li>

<h3> Proteins</h3>


<details><summary><a href=https://www.sciencedaily.com/releases/2018/05/180517142529.htm><b>University of Texas at Austin, Texas Advanced Computing Center (2018) </b><cite>Supercomputing the emergence of material behavior: XSEDE Maverick simulations enable a first in biomolecular design</cite></a></summary><figure><blockquote class='blockquote'><p>"Chemists have designed the first artificial protein assembly (C98RhuA) whose conformational dynamics can be chemically and mechanically toggled. The Maverick GPU-based supercomputer at the Texas Advanced Computing Center simulated the system through an allocation on NSF-funded XSEDE, the Extreme Science and Engineering Discovery Environment. The research could help create new materials for renewable energy, medicine, water purification, and more"</p><figcaption class='blockquote-footer'>UNIVERSITY OF TEXAS AT AUSTIN, TEXAS ADVANCED COMPUTING CENTER<cite title=Supercomputing the emergence of material behavior: XSEDE Maverick simulations enable a first in biomolecular design>, Supercomputing the emergence of material behavior: XSEDE Maverick simulations enable a first in biomolecular design</cite> </figcaption></blockquote></figure></details>

<li><a href=https://deepmind.com/blog/alphafold/><b></b><cite>https://deepmind.com/blog/alphafold/</cite></a><br></li>




<h3> Tools and Dataset Sharing Projects</h3>

<li><a href=https://www.mdpi.com/2073-8994/10/7/248/htm><b>David Camilo Corrales, Agapito Ledezma and Juan Carlos Corrales (2018) </b><cite>From Theory to Practice: A Data Quality Framework for Classification Tasks</cite></a><br></li>

<details><summary><a href=https://www.sciencedirect.com/science/article/pii/S0927025619301296><b>Mareike Picklum, Michael Beetz (2019) </b><cite>MatCALO: Knowledge-enabled machine learning in materials science</cite></a></summary><figure><blockquote class='blockquote'><p>"QUANTUM ESPRESSO9, a publicly available toolbox provided by Giannozzi et al. [13] who use computations of density-functional theory and plane waves to simulate materials in terms of electronic structure. The toolbox allows the user-friendly analytical modeling of quantum simulations."</p><figcaption class='blockquote-footer'>David Camilo Corrales, Agapito Ledezma and Juan Carlos Corrales<cite title=From Theory to Practice: A Data Quality Framework for Classification Tasks>, From Theory to Practice: A Data Quality Framework for Classification Tasks</cite> </figcaption></blockquote></figure></details>  Excellent state of the art on computational material science

<li><a href=https://www.mgi.gov/><b></b><cite>The Materials Genome Initiative (MGI)</cite></a><br></li>

<li><a href=https://www.materials.zone/ai-tools/><b></b><cite>https://www.materials.zone/ai-tools/</cite></a><br></li>



<h3> Modelling / Simulation tools for tagging data</h3>
<li><a href=https://alternativeto.net/software/comsol/?license=opensource><b></b><cite>Alternativeto - Open Source COMSOL Multiphysics Alternatives</cite></a><br></li>
<li><a href=https://www.food4rhino.com/app/dodo#downloads_list><b></b><cite>Rhino Dodo machine learning tools</cite></a><br></li>
<li><a href=http://openems.de/start/index.php><b></b><cite>openEMS free and open electromagnetic field FDTD solver</cite></a><br></li>

</ul>

## HCI for AI

<ul class="biblist">

<li><a href=https://www.microsoft.com/en-us/research/blog/guidelines-for-human-ai-interaction-design/><b>Saleema Amershi, Senior Researcher; Mihaela Vorvoreanur; Eric Horvitz (2019) </b><cite>Guidelines for human-AI interaction design</cite></a><br></li>
<li><a href=http://news.mit.edu/2019/students-developing-ai-tools-all-0523><b></b><cite>MIT Quest for Intelligence's 'Quest Bridge' AI tools project</cite></a><br></li>
<li><a href=http://machine-intelligence.mit.edu/><b></b><cite>MIT Machine Intelligence Community</cite></a><br></li>

<h3> Real time open access data sources</h3>
<li><a href=http://www.k12science.org/materials/resources/realtimedata/><b></b><cite>http://www.k12science.org/materials/resources/realtimedata/</cite></a><br></li>
<li><a href=https://earthdata.nasa.gov/earth-observation-data/near-real-time><b></b><cite>https://earthdata.nasa.gov/earth-observation-data/near-real-time</cite></a><br></li>
<li><a href=https://worldview.earthdata.nasa.gov/><b></b><cite>https://worldview.earthdata.nasa.gov/</cite></a><br></li>

</ul>

## Field, sound and noise visualisation

<ul class="biblist">

<h3>Noise Functions</h3>

<li><a href=https://medium.com/@travall/procedural-2d-island-generation-noise-functions-13976bddeaf9><b></b><cite>Noise Functions - Simplex Noise for Procedural 2D Island Generation</cite></a><br></li>

<h3>Sound Field Visualization</h3>

<li><a href=https://www.hindawi.com/journals/isrn/2013/241958/><b>Daniel Comesaña , Steven Pousa, Hans-Elias de Bree and Keith R. Holland</b><cite>Scan and Paint: Theory and Practice of a Sound Field Visualization Method</cite></a><br></li>

<h4>Grasshopper</h4>

<li><a href=https://yetanotherscript.wordpress.com/2010/01/10/geometric-response-to-audio-real-time-grasshopper-version-1/><b>Dondeti</b><cite>Geometric response to audio. Real time. Grasshopper : Version 1</cite></a><br></li>

<li><a href=https://discourse.mcneel.com/t/grasshopper-and-sound-visualization/58558><b></b><cite>Grasshopper and sound visualization question on Rhino forum</cite></a><br></li>

<li><a href=https://www.grasshopper3d.com/forum/topics/music-visualizer><b></b><cite>Music Visualizer using Grasshopper</cite></a><br></li>

<h4>Processing</h4>

<li><a href=https://www.cg.tuwien.ac.at/courses/Seminar/WS2010/processing.pdf><b>Christopher Pramerdorfer</b><cite>An Introduction to Processing and Music Visualization</cite></a><br></li>

<li><a href=https://inverted-audio.com/visual/visualising-sound-with-joelle/><b></b><cite>Visualising Sound with Joelle Snaith</cite></a><br></li>


<h4>Noise Visualisation</h4>

<details><summary><a href=https://pubmed.ncbi.nlm.nih.gov/29614038/><b>Yong Thung Cho</b><cite>Noise Source Visualization Using a Digital Voice Recorder and Low-Cost Sensors</cite></a></summary><figure><blockquote class='blockquote'><p>'a low cost, compact sound visualization system was designed and introduced to visualize two actual noise sources for verification with different characteristics: an enclosed loud speaker and a small air compressor. Reasonable accuracy of noise visualization for these two sources was shown over a relatively wide frequency range. This very affordable and compact sound visualization system can be used for many actual noise visualization applications in addition to educational purposes the microphones were supplied with external power using a 9 V battery and connected to a digital recorder with unit amplification'</p><figcaption class='blockquote-footer'>Yong Thung Cho<cite title=Noise Source Visualization Using a Digital Voice Recorder and Low-Cost Sensors>, Noise Source Visualization Using a Digital Voice Recorder and Low-Cost Sensors</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.mdpi.com/1424-8220/18/4/1076><b>Yong Thung Cho</b><cite>Characterizing Sources of Small DC Motor Noise and Vibration</cite></a><br></li>

<h4>Doppler controller</h4>



<details><summary><a href=https://www.technologyreview.com/s/427849/gesture-control-system-uses-sound-alone/><b></b><cite>SoundWave Gesture Control System Uses Sound Alone and lets an ordinary laptop function like a Kinect sensor</cite></a></summary><figure><blockquote class='blockquote'><p>'The speakers on a computer equipped with SoundWave software emit a constant ultrasonic tone of between 20 and 22 kilohertz.'</p><figcaption class='blockquote-footer'><cite title=SoundWave Gesture Control System Uses Sound Alone and lets an ordinary laptop function like a Kinect sensor>, SoundWave Gesture Control System Uses Sound Alone and lets an ordinary laptop function like a Kinect sensor</cite> </figcaption></blockquote></figure></details>

<li><a href=https://danielrapp.github.io/doppler/><b>Daniel Rapp</b><cite>Soundwave - doppler controller</cite></a><br></li>



<h3>EM visualisation</h3>

<li><a href=https://www.instructables.com/id/Visualize-Bluetooth-in-a-Long-Exposure-Photo/><b></b><cite>Visualize Bluetooth in a Long Exposure Photo</cite></a><br></li>

<li><a href=http://www.architectureofradio.com/><b>Richard Vijgen</b><cite>The Architecture of Radio application - visualizes this network of networks revealing the invisible technological landscape we interact with through our devices.</cite></a><br></li>

<li><a href=http://medicaldevices.azurewebsites.net/><b></b><cite>Microsoft medical devices group</cite></a><br></li>

<h4>Scanning Wifi</h4>

<li><a href=https://www.youtube.com/watch?v=aqqEYz38ens><b>CNLohr</b><cite>High Res Wifi Signal Mapping</cite></a><br></li>

<details><summary><a href=http://cnlohr.github.io/voxeltastic/><b>CNLohr</b><cite>Voxeltastic Volume Raytracing  display of wifi</cite></a></summary><figure><blockquote class='blockquote'><p>You can now view your own datasets in-browser.  Simply record your data as a packet binary file, with one-byte intensities, or with 4-byte RGBA bytes.  Note that all edges MUST be powers of two, i.e. a 32x64x128 block solid.</p></blockquote><figcaption class='blockquote-footer'>CNLohr<cite title=></cite> </figcaption></figure></details>

<li><a href=https://hackaday.io/project/4329-wifi-power-mapping><b>CNLohr</b><cite>Wifi power mapping Using an ESP8266 to look at wifi signals in great detail.</cite></a><br></li>

</ul>

<!---
RANDOM LINKS TO SORT

<h4>Magnetic wormholes (metamaterials)</h4>
<li><a href=https://www.newscientist.com/article/dn28071-metamaterial-wormhole-teleports-magnetic-fields-across-space/><b></b><cite>Metamaterial wormhole teleports magnetic fields across space</cite></a><br></li>

## Sensors and the technosphere

https://technosphere-magazine.hkw.de/p/Sensing-Air-and-Generating-Worlds-of-Data-fL1Q5CVgGeJMDJmEdwCFr3

## Arduino stuff

https://medium.com/@islamnegm/quick-start-to-simple-daq-system-using-plx-daq-excel-arduino-d2457773384b
-->



## Finding new meanings: Ontology Symbols, resolution and scale

<ul class="biblist">



<li><a href=https://arxiv.org/ftp/arxiv/papers/1701/1701.09040.pdf><b>Gerardo L. Febres</b><cite>A Proposal about the Meaning of Scale, Scope and Resolution in the Context of the Information Interpretation Process</cite></a><br></li>

<li><a href=http://journals.isss.org/index.php/proceedings54th/article/viewFile/1466/473><b>Todd D. Bowers</b><cite>Ontological Support for Multiparadigm Multimethodologies: Isomorphic Process–Structures and The Critical Moment</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/301618553_A_communication_model_based_on_symbolic_spatial_and_semantic_information_fractions><b>GERARDO FEBRES</b><cite>A communication model based on symbolic, spatial and semantic information fractions</cite></a><br></li>

<li><a href=https://karlhammar.com/downloads/hammar2017content.pdf><b>Karl Hammar</b><cite>Content Ontology Design Patterns: Qualites, Methods, and Tools</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/327824440_Practice-based_ontological_design_for_multiplying_realities><b>Christian Nold</b><cite>Practice-based ontological design for multiplying realities</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/319653669_Integration_of_an_applied_ontology_and_wiki-resources_in_the_context_of_the_unified_knowledge_base><b>Vadim Moshkin Aleksey Filippo Nadejda Yarushkina (2017) </b><cite>Integration of an applied ontology and wiki-resources in the context of the unified knowledge base</cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/279684809_Ontology_Development_Methods><b>Pratibha Gokhale, Sangeeta Deokattey and K. Bhanumurthy</b><cite>Ontology  Development  Methods</cite></a><br></li>

<li><a href=http://www.lsi.upc.es/~bejar/aia/aia-web/4-fernandez.pdf><b>Fernández López, M</b><cite>Overview Of Methodologies For Building Ontologies</cite></a><br></li>

<li><a href=https://www.amazon.com/Information-Systems-Modern-Society-Development/dp/1466629223><b>Wang, John</b><cite>Information Systems and Modern Society: Social Change and Global Development</cite></a><br></li>

<li><a href=https://www.lesswrong.com/posts/7HMSBiEiCfLKzd2gc/quantum-mechanics-and-personal-identity><b>Eliezer Yudkowsky (2008) </b><cite>Quantum Mechanics and Personal Identity</cite></a><br></li>

<details><summary><a href=https://blogs.nicholas.duke.edu/anthropocene/2-4-breaking-the-anthropocene-illusion/><b>Breaking the Anthropocene Illusion (2018) </b><cite>Peter Haff</cite></a></summary><figure><blockquote class='blockquote'><p>'Strategies to deal with extant global-scale problems like atmospheric warming or risks from new technologies like weaponized drones can be placed on a firmer footing if we lay aside our anthropocentric lenses for the moment, stand back, and look as clearly as possible at the physical requirements and implications of living as obligate components of a purposeful technosphere.'</p><figcaption class='blockquote-footer'>Breaking the Anthropocene Illusion<cite title=Peter Haff]>, Peter Haff</cite> </figcaption></blockquote></figure></details>

<details><summary><a href=https://link.springer.com/article/10.1007/s11229-015-0972-1><b>Benjamin C. Jantzen Deborah G. Mayo Lydia Patton (2015) </b><cite>Ontology & methodology</cite></a></summary><figure><blockquote class='blockquote'><p>' The papers that deal with the question of representation suggest a novel concern with fragmentation or fracture of the scientific picture of reality. This is a contemporary phenomenon, belied by the “web of belief” or coherentist views espoused by Quine and others—which raises questions of its own, including the extent to which science can function as a reliable guide to epistemology, much less ontology, if its image of reality is broken. The development of the new, fragmented theory is driven by practice and by history, but what are the consequences of adopting such a view?'</p><figcaption class='blockquote-footer'>Benjamin C. Jantzen Deborah G. Mayo Lydia Patton<cite title=Ontology & methodology>, Ontology & methodology</cite> </figcaption></blockquote></figure></details>

<li><a href=https://philarchive.org/archive/BEROAM-2v1><b>Francesco Berto and Matteo Plebani (2015) </b><cite>Ontology and Metaontology A Contemporary Guide</cite></a><br></li>

<li><a href=http://www.tmrfindia.org/ijcsa/v9i23.pdf><b>Terje Aaberge, Rajendra Akerkar (2012) </b><cite>Ontology and Ontology Construction: Background and Practices</cite></a><br></li>



<h3>Spatial Information</h3>

<li><a href=https://internet-atlas.net/><b>Louise Drulhe</b><cite>Critical Atlas of the Internet</cite></a><br></li>
<li><a href=https://www.anthropocene-curriculum.org><b></b><cite>https://www.anthropocene-curriculum.org</cite></a><br></li>


</ul>

<!---
## Quantumness, chaos, Determinism v Chance
useful at times
- naming of things
its about whats real.
TEmes - technical memtics - susan blackmore evoluion of objects - we are replicators.

Timothy moron hyperobjects - -
ontology - uses language
is i philosophy desd

### complex systems
emergent properties tend to evolution / complexity
 - death of philosophy.
  it depends on language
 -->



## Semiotics of the Anthropocene

<ul class="biblist">

<li><a href=https://journals.sagepub.com/doi/abs/10.1177/1089268019832849?journalCode=rgpa><b>Tim Lomas (2019) </b><cite>Positive Semiotics</cite></a><br></li>
<li><a href=http://eprints.lancs.ac.uk/82351/4/Szerszynski_Gods_of_the_Anthropocene_author_final_version.pdf><b>Bronislaw Szerszynski (2016) </b><cite>Gods of the Anthropocene Geo-Spiritual Formations in the Earth’s New Epoch</cite></a><br></li>
<li><a href=https://journals.sagepub.com/doi/abs/10.1177/0263276416688946><b>Nigel Clark Kathryn Yusoff (2017) </b><cite>Geosocial Formations and the Anthropocene</cite></a><br></li>
<li><a href=https://www.academia.edu/34982354/Anthropocene_semiosis><b>Nigel Clark (2017) </b><cite>Anthropocene semiosis</cite></a><br></li>
<li><a href=https://www.tandfonline.com/doi/pdf/10.1080/14606925.2017.1352860?needAccess=true><b>Matthew Holt (2017) </b><cite>Semiotics and design: Towards an aesthetics of the artificial</cite></a><br></li>
<li><a href=http://delivery.acm.org/10.1145/3310000/3300260/paper030.pdf?ip=91.126.132.13&id=3300260&acc=OPENTOC&key=4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35%2E9F04A3A78F7D3B8D&__acm__=1560192460_10c18ebffb15a33a5364f68d9b3fd1ef><b>Adrian Hazzard, Chris Greenhalgh, Maria Kallionpää, Steve Benford,Anne Veinberg, Zubin Kanga & Andrew McPherson</b><cite>Failing with Style: Designing for Aesthetic Failure in Interactive Performance</cite></a><br></li>
<li><a href=https://www.researchgate.net/publication/305385122_Creatures_of_the_semiosphere_A_problematic_third_party_in_the_'humans_plus_technology'_cognitive_architecture_of_the_future_global_superintelligence><b>Marta Lenartowicz (2017) </b><cite>Creatures of the Semiosphere - A problematic third party in the ‘humans plus technology’ cognitive architecture of the future global superintelligence</cite></a><br></li>
<li><a href=https://www.anthropocene-curriculum.org/pages/root/campus-2016/co-evolutionary-perspectives-on-the-technosphere/><b>Daniel Niles (2016) </b><cite>Sputnik of our Time</cite></a><br></li>
<li><a href=http://openresearch.ocadu.ca/id/eprint/397/1/Logan_2014_WhatIsInformation_DEMOPublishing.pdf><b>Logan, Robert K. (2014) </b><cite>What is information?: Propagating organization in the biosphere, symbolosphere, technosphere and econosphere.</cite></a><br></li>
<li><a href=https://www.tandfonline.com/doi/abs/10.1080/09528130010029811><b>Sergei Nirenburg & Yorick Wilks (2010) </b><cite>What's in a symbol: ontology, representation and language</cite></a><br></li>
<li><a href=https://www.frontiersin.org/articles/10.3389/fpsyg.2015.01601/full><b>Chris Sinha (2015) </b><cite>Language and other artifacts: socio-cultural dynamics of niche construction</cite></a><br></li>
<details><summary><a href=http://culturemachine.net/about/><b></b><cite>culturemachine.net</cite></a></summary><figure><blockquote class='blockquote'><p>'Culture Machine is an international open-access journal of culture and theory, founded in 1999. Its aim is to be to cultural studies and cultural theory what ‘fundamental research’ is to the natural sciences: open-ended, non-goal orientated, exploratory and experimental. All contributions to the journal are peer-reviewed.'</p><figcaption class='blockquote-footer'><cite title=culturemachine.net>, culturemachine.net</cite> </figcaption></blockquote></figure></details>


<h3> Media and Culture</h3>

<details><summary><a href=https://www.mdpi.com/2409-9287/1/2/153/htm><b>Zeynep Merve Iseri and Robert K. Logan (2016) </b><cite>Laws of Media, Their Environments and Their Users: The Flip of the Artifact, Its Ground and Its Users</cite></a></summary><figure><blockquote class='blockquote'><p>'Marshall McLuhan’s Laws of Media (LOM), how artifacts or technology evolve as each artifact when pushed far enough flips into a new artifact that is its complementary form.'</p><figcaption class='blockquote-footer'>Zeynep Merve Iseri and Robert K. Logan<cite title=Laws of Media, Their Environments and Their Users: The Flip of the Artifact, Its Ground and Its Users]>, Laws of Media, Their Environments and Their Users: The Flip of the Artifact, Its Ground and Its Users</cite> </figcaption></blockquote></figure></details>

<li><a href=https://quod.lib.umich.edu/d/dcbooks/8232214.0001.001/1:2/--media-technology-and-society-theories-of-media-evolution?g=dculture;rgn=div1;view=fulltext;xc=3><b>W. Russell Neuman, Editor (2010) </b><cite>Media, Technology, and Society: Theories of Media Evolution</cite></a><br></li>

<li><a href=https://webspace.royalroads.ca/llefevre/wp-content/uploads/sites/258/2017/08/A-Schoolmans-Guide-to-Marshall-McLuhan-1.pdf><b>Culkin, J. M. (1967) </b><cite>A schoolman's guide to Marshall McLuhan</cite></a><br></li>

</ul>


## Narratives of technology and science

<ul class="biblist">


<li><a href=https://link.springer.com/article/10.1007/s10746-016-9383-7><b>Mark Coeckelbergh (2016) </b><cite>Narrative Technologies: A Philosophical Investigation of the Narrative Capacities of Technologies by Using Ricoeur’s Narrative Theory</cite></a><br></li>

<li><a href=http://www.technologystories.org/crafting-stories/><b>Cian O'Donovan, Johan Schot (2018) </b><cite>Crafting Stories of Technology and Progress: Five Considerations</cite></a><br></li>

<li><a href=http://technosphere-magazine.hkw.de/><b></b><cite>Technosphere Magazine (hkw)</cite></a><br></li>

<li><a href=https://maetl.net/talks/stories-as-systems><b>Mark Rickerby(Jan 2019)</b><cite>Stories as Systems  online presentation</cite></a><br></li>

<li><a href=https://jods.mitpress.mit.edu/pub/enlightenment-to-entanglement><b>Danny Hillis (2016) </b><cite>The Enlightenment is Dead, Long Live the Entanglement. Journal of Design and Science</cite></a><br></li>

</ul>


<ul class="biblist">

<h3> Stories about the Anthropocene</h3>

<li><a href=https://www.dukeupress.edu/staying-with-the-trouble><b>Donna Haraway (2016) </b><cite>Anthropocene, Capitalocene, Cthulucene: Staying with the trouble]</cite></a><br></li>

<li><a href=http://environmentalhumanities.org/arch/vol6/6.7.pdf><b>Donna Haraway (2015) </b><cite>Anthropocene, Capitalocene, Plantationocene, Chthulucene: Making Kin</cite></a><br></li>

<details><summary><a href=https://www.researchgate.net/publication/327747922_The_limits_of_Anthropocene_narratives><b>Zoltán Boldizsár Simon (2018) </b><cite>The Limits of Anthropocene Narratives</cite></a></summary><figure><blockquote class='blockquote'><p>'the challenge lies in coming to grips  with  how  the  stories  we  can  tell  in  the  Anthropocene  relate  to  the  radical  novelty of the Anthropocene condition about which no stories can be told. What we  need  to  find  are  meaningful  ways  to  reconcile  an inherited  commitment  to  narrativization and the collapse of storytelling as a vehicle of understanding the Anthropocene as our current predicament'</p><figcaption class='blockquote-footer'>Zoltán Boldizsár Simon<cite title=The Limits of Anthropocene Narratives>, The Limits of Anthropocene Narratives</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.academia.edu/37067767/Using_stories_narratives_and_storytelling_in_energy_and_climate_change_research><b>Mithra Moezzia, Kathryn B. Jandab, Sea Rotmannc (2017) </b><cite>Using stories, narratives, and storytelling in energy and climate change research</cite></a><br></li>

<li><a href=https://reader.elsevier.com/reader/sd/pii/S2214629617301974?token=FC0EC1267104B9A371EBA25717F2B4D121C992E9928E0E6CDFECACEC3804D92DC25A62F82FDAAAD10F3D819552C8B046><b>Joe Smitha, Robert Butlera, Rosie J. Dayb, Axel H. Goodbodyd, David H. Llewellync, Mel Rohseb,Bradon T. Smitha, Renata A. Tyszczuke, Julia Udallf, Nicola M. Whyte (2017) </b><cite>Gathering around stories: Interdisciplinary experiments in support of energy system transitions</cite></a><br></li>

<li><a href=https://www.tandfonline.com/doi/full/10.1080/21693293.2014.948324><b>Neil Scott Powell, Rasmus Kløcker Larsen & Severine van Bommel (2014) </b><cite>Meeting the ‘Anthropocene’ in the context of intractability and complexity: infusing resilience narratives with intersubjectivity</cite></a><br></li>

<li><a href=http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.103.9086><b>Mischa M Tuffield, David E Millard, Nigel R Shadbolt (2006) </b><cite>Ontological Approaches to Modelling Narrative</cite></a><br></li>
</ul>


## Identity

<ul class="biblist">

<details><summary><a href=https://jods.mitpress.mit.edu/pub/hsxloiq4><b>Michael Andrea (2019)  </b><cite>How Technology Reduces Us, and What We As Designers Can Do About It</cite></a></summary><figure><blockquote class='blockquote'><p>'Users are not only making their online personas with the reference points of a far reduced set of criteria, they are also creating their identities in this environment. ... Users buy into the authenticity of the influencer’s persona and include them in their world, consciously and unconsciously mimicking what they see. The basis of influencer marketing is that audiences aspirationally buy into an influencer’s persona, buying the product as a way of reinforcing their connection.'</p><figcaption class='blockquote-footer'>Michael Andrea<cite title=How Technology Reduces Us, and What We As Designers Can Do About It>, How Technology Reduces Us, and What We As Designers Can Do About It</cite> </figcaption></blockquote></figure></details>

<li><a href=https://www.academia.edu/10418062/Materiality_of_Connectivity_in_the_Networked_Society_A_Sociomaterial_Perspective><b>Dubravka Cecez-Kecmanovic, Sebastian K. Boell, John Campbell (2014) </b><cite>Materiality of Connectivity in the Networked Society: A Sociomaterial Perspective</cite></a><br></li>

<li><a href=><b>Zygmunt Bauman (2018) </b><cite>The trouble with being human these days</cite></a><br></li>

</ul>




<ul class="biblist">

<h3>Object Oriented Ontology</h3>

<li><a href=https://medium.com/tennis-tool-kit/life-beyond-human-centered-588172f93dcd><b>Marcello Gortana (2019) </b><cite>Life beyond Human Centered</cite></a><br></li>

<li><a href=https://www.academia.edu/37723897/Shadowing_the_Anthropocene_Eco-Realism_for_Turbulent_Times><b>Adrian Ivakhiv (2018) </b><cite>Shadowing the Anthropocene: Eco-Realism for Turbulent Times</cite></a><br></li>

<li><a href=http://www.cd-cf.org/articles/matafunctional-metafictional-objects/><b>Paolo Cardini</b><cite>Matafunctional / Metafictional Object</cite></a><br></li>

<li><a href=https://www.amazon.com/Humankind-Solidarity-Nonhuman-Timothy-Morton/dp/1786631326><b>Timothy Morton (2017) </b><cite>Humankind: solidarity with nonhuman people</cite></a><br></li>

<li><a href=https://www.academia.edu/934518/Here_Comes_Everything_The_Promise_of_Object-Oriented_Ontology><b>Timothy Morton (2011) </b><cite>Here Comes Everything: The Promise of Object-Oriented Ontology</cite></a><br></li>

<li><a href=https://www.amazon.co.uk/Object-Oriented-Ontology-Theory-Everything-Pelican/dp/0241269156><b>Graham Harman (2018) </b><cite>Object-Oriented Ontology: A New Theory of Everything Paperback</cite></a><br></li>

<li><a href=https://www.jstor.org/stable/43630853><b>Mark Foster Gage (2015) </b><cite>Killing Simplicity: Object-Oriented Philosophy In Architecture</cite></a><br></li>

<li><a href=https://www.academia.edu/1117417/Thinking_Against_Nature><b>Thinking Against Nature, Speculations I (2010) </b><cite>Edited by Ben Woodard and Paul Ennis</cite></a><br></li>

<li><a href=https://www.dukeupress.edu/vibrant-matter><b>Jane Bennett (2009) </b><cite>Vibrant Matter</cite></a><br></li>

<li><a href=https://www.amazon.com/Designers-Visionaries-Other-Stories-Sustainable/dp/1844074129><b>Jonathan Chapman (2007) </b><cite>Designers, Visionaries and Other Stories: A Collection of Sustainable Design </cite></a><br></li>

<li><a href=https://www.researchgate.net/publication/283871404_Misbehavioral_objects_Empowerment_of_users_versus_empowerment_of_objects><b>Samuel Bianchini, Remy Bourganel, Emanuele Quinz, Elisabetta Zibetti (2015) </b><cite>(Mis)behavioral objects: Empowerment of users versus empowerment of objects</cite></a><br></li>

<li><a href=http://www.seismopolite.com/future-objects-object-futures-object-oriented-ontology-at-documenta-13-and-beyond><b>Kathryn M. Floyd (2013) </b><cite>Future Objects / Object Futures: Object Oriented Ontology at dOCUMENTA (13) and Beyond</cite></a><br></li>
</ul>




<ul class="biblist">

<h4> Ontography and Diagrammatology</h4>

<li><a href=https://edinburghuniversitypress.com/book-onto-cartography.html><b>Levi R. Bryant (2014) </b><cite>Onto-Cartography: An Ontology of Machines and Media</cite></a><br></li>
</ul>



<ul class="biblist">

<h3> Artifact Intelligence</h3>

<details><summary><a href=http://www-kasm.nii.ac.jp/papers/takeda/02/roman2002.pdf><b>Hideaki Takeda, Kazunori Terada, Tatsuyuki Kawamura (2002) </b><cite>Artifact Intelligence: Yet Another Approach for Intelligent Robots</cite></a></summary><figure><blockquote class='blockquote'><p>'The basic problem for Artifact Intelligence is to establish intentional/physical relationship between humans and artifacts. Intentional relationship means that humans use artifacts in order to archive some tasks and artifacts provide some functions to satisfy such requests. Physical relationship means that such interaction between human and artifacts should be mostly done physically. Neither intentional nor physical interaction alone is not sufficient.
For example, remind how difficult it is to define “chair  physically. What artifacts can be “chair” is dependent on what is our intention on “chair”.'</p>

<figcaption class='blockquote-footer'>Hideaki Takeda, Kazunori Terada, Tatsuyuki Kawamura<cite title=Artifact Intelligence: Yet Another Approach for Intelligent Robots>, Artifact Intelligence: Yet Another Approach for Intelligent Robots</cite> </figcaption></blockquote></figure></details>

</ul>

## Cultural tools

<ul class="biblist">

<details><summary><a href=http://culturemachine.net/><b></b><cite>Culture Machine Journal</cite></a></summary><figure><blockquote class='blockquote'><p>Culture Machine, a member of the Radical Open Access Collective, is a series of experiments in culture and theory. Since 1999, it has sought out and promoted scholarly work that engages provocatively with contemporary technical objects, processes and imaginaries from the North and South. Building on its open ended, non-instrumental, and exploratory approach to media theory, cultural studies and political philosophy, Culture Machine welcomes creative proposals that contest and come up against globalizing technical narratives and the environmental logics of extraction.</p></blockquote><figcaption class='blockquote-footer'><cite title=></cite> </figcaption></figure></details>

<li><a href=https://www.nawe.co.uk/DB/magazines/culture-wars.html><b>Institute of Ideas in London. </b><cite>Culture Wars online review</cite></a><br></li>

</ul>



<ul class="biblist">

<h3> Aesthetics</h3>

<h4> Hypercubism</h4>

<details><summary><a href=https://hypercubist.com/><b> (14 May 2018 ) </b><cite>Hypercubist – Journal of Volumetric Aesthetics https://hypercubist.com</cite></a></summary><figure><blockquote class='blockquote'><p>'This emergent aesthetics is networked and decentralized, connecting seemingly divergent expressions and forms. A not exhaustive list of these forms might include: augmented reality, projection mapping, interactive art, net art, generative art, code art, video art, videomusic, audiovisual performance, street art, urban interventions, videogames, cinemagrams, animated gifs, digital photography and multitudinous as-of-yet unnamed forms.     In taking up the task of articulating the dynamics of these aesthetics we are reminded of the Cubists and their efforts to push beyond the limitations of painting and sculpture to express aspects of another dimension outside those media.'</p><figcaption class='blockquote-footer'><cite title=Hypercubist – Journal of Volumetric Aesthetics https://hypercubist.com>, Hypercubist – Journal of Volumetric Aesthetics https://hypercubist.com</cite> </figcaption></blockquote></figure></details>

<li><a href=https://cdm.link/2011/02/adding-dimension-to-cinemas-future-a-hypercubist-manifesto-and-3d-aesthetics/><b>Peter Kirn (February 28, 2011) </b><cite>Adding Dimension to Cinema's Future: A Hypercubist Manifesto, and 3D Aesthetics</cite></a><br></li>

<li><a href=http://quantumcinema.blogspot.com/><b>Gabriel Shalom (2006 - 2011) </b><cite>Quantum Cinema blog</cite></a><br></li>

</ul>



<ul class="biblist">

<h4> Glitch and noise</h4>

<h4> kitbashing</h4>

<li><a href=https://cubebrush.co/macdrab/products/flwlkq/architectural-structures-kitbash-set><b>Cuberush user Maciej</b><cite>Architectural Structures Kitbash Set</cite></a><br></li>

</ul>



<ul class="biblist">

<h3> Language and words</h3>

<h4> Hyperobject</h4>


<li><a href=https://www.upress.umn.edu/book-division/books/hyperobjects><b>Timothy Morton (2013) </b><cite>Philosophy and Ecology after the End of the World</cite></a><br></li>
</ul>

<h4> Geometry</h4>

<h5> E8 Lie group</h5>


<ul class="biblist">

<li><a href=http://theoryofeverything.org/theToE/tags/e8/><b></b><cite>J Gregory Moxness' Theory of Everything site with interactive tool</cite></a><br></li>

<li><a href=http://vixra.org/pdf/1411.0130v1.pdf><b>J Gregory Moxness (2014) </b><cite>The 3D Visualization of E8 using an H4 Folding Matrix</cite></a><br></li>

<li><a href=http://www.madore.org/~david/math/e8rotate.html><b></b><cite>The E8 root system (rotated under G2) animation</cite></a><br></li>

<details>
<summary>
<a href="https://www.thingiverse.com/tag:lie_algebra_e8"><b></b><cite>3D Printed E8 Lie Algebra Shapes on Thingiverse</cite></a></summary>
<figure>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/0P8dOWKQtLE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>       
</figure>
</details>
</ul>

## Design and society

<ul class="biblist">

<li><a href=http://www.simonerebaudengo.com/wp-content/uploads/2019/01/Design_for_Living_with_Smart_Products_cover.pdf><b>Simone Rebaudeng (2017) </b><cite>Design for Living with Smart Products The Intelligent Home </cite></a><br></li>

<details><summary><a href=https://www.researchgate.net/publication/233606873_Making_the_Social_Hold_Towards_an_Actor-Network_Theory_of_Design><b>Albena Yaneva (2009) </b><cite>Making the Social Hold: Towards an Actor-Network Theory of Design</cite></a></summary><figure><blockquote class='blockquote'><p>' It is impossible to understand how a society works without appreciating how design shapes, conditions, facilitates and makes possible everyday sociality. Viewed as a type of connector, not as a separate cold domain of material relations, design's investigation might shed light on other types of non-social ties that are brought together to make the social durable.'</p><figcaption class='blockquote-footer'>Albena Yaneva<cite title=Making the Social Hold: Towards an Actor-Network Theory of Design>, Making the Social Hold: Towards an Actor-Network Theory of Design</cite> </figcaption></blockquote></figure></details>

</ul>
