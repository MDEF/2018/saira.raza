<html>
<head>
<title></title>
<link rel="shortcut icon" href="../../img/mandala3.png">
<style>
@import url("https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600&display=swap");


.card > hr {
  margin-right: 0;
  margin-left: 0;
}

.card > .list-group {
  border-top: inherit;
  border-bottom: inherit;
}

.card > .list-group:first-child {
  border-top-width: 0;
}

.card > .list-group:last-child {
  border-bottom-width: 0;
}

.card > .card-header + .list-group,
.card > .list-group + .card-footer {
  border-top: 0;
}


.card-subtitle {
  margin-top: -0.25rem;
  margin-bottom: 0;
}

.card-text:last-child {
  margin-bottom: 0;
}

.card-link:hover {
  text-decoration: none;
}

.card-link + .card-link {
  margin-left: 1rem;
}

.card-header-tabs {
  margin-right: -0.5rem;
  margin-bottom: -0.5rem;
  margin-left: -0.5rem;
  border-bottom: 0;
}

.card-header-pills {
  margin-right: -0.5rem;
  margin-left: -0.5rem;
}

.card-img-overlay {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
}

.card-img,
.card-img-top,
.card-img-bottom {
  width: 100%;
}

.card-group > .card {
  margin-bottom: 0.75rem;
}

.card-footer {
  padding: 0.5rem 1rem;
  background-color: rgba(0, 0, 0, 0.03);
  border-top: 1px solid rgba(0, 0, 0, 0.125);
}

.bg-secondary {
  background-color: cyan !important;
}

.bg-success {
  background-color: magenta !important;
}

.bg-info {
  background-color: lime !important;
}

.bg-warning {
  background-color: magenta !important;
}

.bg-danger {
  background-color: red !important;
}

.bg-light {
  background-color: #fff !important;
}

.bg-dark {
  background-color: black !important;
}

.bg-body {
  background-color: #fff !important;
}

.bg-white {
  background-color: #fff !important;
}

.bg-transparent {
  background-color: transparent !important;
}

.bg-gradient {
  background-image: var(--bs-gradient) !important;
}

body {
  margin: 0;
  font-family: var(--bs-font-sans-serif);
  font-size: 1rem;
  font-weight: 200;
  letter-spacing: 1px;
  font-weight: 400;
  line-height: 1.5;
  color: #55595c;
  background-color: #fff;
  -webkit-text-size-adjust: 100%;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}

hr {
  margin: 1rem 0;
  color: inherit;
  background-color: currentColor;
  border: 0;
  opacity: 0.25;
}

h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6 {
  margin-top: 0;
  margin-bottom: 0.5rem;
  font-weight: 600;
  line-height: 1.2;
  color: #1a1a1a;
  font-family: "Nunito Sans";
  text-transform: uppercase;
  letter-spacing: 3px;
}

h1, .h1 {
  font-size: calc(1.325rem + 0.9vw);
}



.card {
  position: relative;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0, 0, 0, 0.125);
}


.text-white {
  color: #fff !important;
}

.bg-primary {
  background-color: blue !important;
  font-color: white;
}

.mb-3 {
margin-bottom: 1rem !important;
}


.card-header {
  padding: 0.5rem 1rem;
  margin-bottom: 0;
  background-color: rgba(0, 0, 0, 0.03);
  border-bottom: 1px solid rgba(0, 0, 0, 0.125);
}

.card-body {
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 1rem 1rem;
}

.card-title {
  margin-bottom: 0.5rem;
  font-weight: 600;
  font-family: "Nunito Sans";
  text-transform: uppercase;
  letter-spacing: 3px;
}

card-text{
}


h4.card-title h4.card-title:focus h4.card-title.focus {
  font-family: "Nunito Sans";
  text-transform: uppercase;
  letter-spacing: 3px;
  font-weight: 600;
}

div.col-md-3{
max-width: 5%;
}

div.col.md-9{
max-width: 100%;
}



</style>

**<font face color="blue" size="5"><center>[░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)</center></font>**
<center><font size="3" color="blue">

[𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)
</font></center>
<br>

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/d8f918a9-d695-401b-8501-5c64163b6a25/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/d8f918a9-d695-401b-8501-5c64163b6a25/Untitled.png)


# **MAKING**

Humans have made material things from as far back as we have been human. The things we make all contain information and making material things can be seen as a kind of coding of information into materials or inversely a materialisation of information.

Making things helps us to construct practical knowledge and enables us to put information back into our environment so that we can influence it.

We now make materials that produce, transfer and store *bits* (binary units of data) and we are living in an age when these digital technologies have blossomed and evolve with us.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e7c39ee7-36f3-418b-993b-0b21740efdc6/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e7c39ee7-36f3-418b-993b-0b21740efdc6/Untitled.png)

Schematic timeline of information and replicators in the biosphere from Michael R. Gillings, Martin Hilbert, Darrell J.Kemp (2016)

# **DATA DRIVEN MATERIAL DESIGN**

Today bits are being produced, shared and stored in quantities and forms that can seem like meaningless noise but advancing data processing techniques such as machine learning are revealing new patterns of information about the material world and what we can do with it.

These data driven methods that are able to find materials to match properties are a opening a new paradigm in material science.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/dd747032-b68f-4202-9cbb-395775d5f250/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/dd747032-b68f-4202-9cbb-395775d5f250/Untitled.png)

The Development of the paradigms (new modes of thought) of materials science and engineering’ - Claudia Draxl and Matthias Scheffler (2018)

# **METAMATERIALS AND DIGITAL FABRICATION**

Increasingly we are also making atoms from bits - using computers to design and fabricate material things in new ways.

Since the 1990s, the increasing affordability of digital fabrication tools  has enabled researchers to fabricate more easily a class of materials called  metamaterials. These materials exhibit properties rarely found in nature as a result of their repetitive fabricated structures rather than their molecular structures.

![https://media-private.canva.com/MADihZJFvrk/1/screen.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWF6QO3UH4PAAJ6Q%2F20210517%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210517T070211Z&X-Amz-Expires=82961&X-Amz-Signature=1d5a495aab471a1811847464934983a6f0ecf80365d518086c9c1d7e2635ebc3&X-Amz-SignedHeaders=host&response-expires=Tue%2C%2018%20May%202021%2006%3A04%3A52%20GMT](https://media-private.canva.com/MADihZJFvrk/1/screen.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAJWF6QO3UH4PAAJ6Q%2F20210517%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210517T070211Z&X-Amz-Expires=82961&X-Amz-Signature=1d5a495aab471a1811847464934983a6f0ecf80365d518086c9c1d7e2635ebc3&X-Amz-SignedHeaders=host&response-expires=Tue%2C%2018%20May%202021%2006%3A04%3A52%20GMT)

Analogies between electromagnetic  (nano-scale) metamaterial and (decameter scale) metacity

- Brulé, et al.(2017)

These rarely seen properties include atypical bending of waves of all scales (electromagnetic, acoustic, seismic) to achieve effects like invisibility cloaking, black holes and passive energy harvesting from sound which until now were considered science fiction .

These properties open up new possibilities to improve efficiencies of existing essential technologies and to reduce waste and pollution.

Today's desktop computers enable metamaterial structures to be drawn, modeled and fabricated relatively easily compared to natural materials. This helps to generate

datasets for machine learning and the design of metamaterials using intelligent, data-driven methods.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5f66f03c-6c12-42c5-be58-105c5daa4642/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/5f66f03c-6c12-42c5-be58-105c5daa4642/Untitled.png)

Light passing through a flat metamaterial 'meta-lens'  is focused by millions of nano structures

Khorasaninejad et al (2016)


![](assets/opportunity.png)

# THE OPPORTUNITY

**Stimulate innovation** by nurturing cultures of co-design and collaboration between makers, scientists and AI

**Represent more needs and perspectives in the development of**

**these disruptive**

**technologies** by

developing data

driven metamaterial

design tools  that are

accessible and useful

to scientists and

makers

**Improve the sustainability of our cultures surrounding materials** by using machine learning to find

metamaterials that can be used to reduce waste and pollution

and improve efficiencies of our essential technologies.

BIG DATA FOR MATERIAL SCIENCE

Sensors, the internet of things and platforms for sharing data and equipment present possibilities to distribute tasks and build knowledge about materials  collectively . **Can makers contribute data about material behavior that researchers can use to design materials ?**

MACHINE LEARNING FOR MATERIAL DISCOVERY

Material science is entering a new paradigm of finding materials using AI. Data driven deep learning is accelerating discovery of  "designer" materials. Material discoveries  can themselves change technological paradigms .  **Can we find materials that reduce pollution and waste and improve the efficiencies of renewable  energy sources and essential technologies?**

METAMATERIAL DESIGN

Digital fabrication allows us to draw, make and demonstrate with greater ease regular patterned  structures that exhibit rare and exceptional physical properties out of relatively ordinary materials. **Can these unleashed rare properties  help to improve efficiencies of technology and reduce waste and pollution?**

THE DIGITAL FABRICATION REVOLUTION

Making digital fabrication accessible to users enables customisation and innovation by a wider variety of people. **Can makers customise disruptive technologies designed by scientific researchers?  Could this change cultures of design, fabrication and consumption of materials and energy  ?**

# QUESTIONS OF FOCUS FOR THE PRELIMINARY STUDY

***HOW ACCESSIBLE TO A NOVICE ARE THESE NEW TOOLS AND METHODS OF DATA-DRIVEN MATERIAL DESIGN AND DIGITAL FABRICATION?***

***HOW CAN A NOVICE ‘MAKER’ DESIGN METAMATERIAL DEVICES FROM THE NOISE OF DATA THAT SURROUNDS US?***

# RESEARCH METHODS OF PRELIMINARY STUDY

**From an outsiders point of view**, this research project tries to:

find and organise information about metamaterials and ways to fabricate and test them.

replicate metamaterials designed by scientific researchers in a maker space.

construct a scanning sensor using low cost hardware and opensource/ free information.

find and collate documented tools and toolchains needed to design a metamaterial using data-driven methods.

In doing so it follows the journey of noisy bits generated by sensors in a maker space, through datasets and algorithms that may be designed using opensource tools, to replicable metamaterial design drawings to finally the transformation of bits to atoms using digital fabrication.

By tagging, clustering and diagramming information it structures scientific knowledge

from published research papers and first hand practical knowledge from making.

# **OUTPUTS SO FAR**

***Metamaterial taxonomy***

***Bibliography***

coming soon..

Making metamaterials in a maker space

Making a low cost scanning sensor

Metamaterial Zoo



<div class="row">
          <div class="col-lg-4">



            <div class="bs-component">
              <div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
                <div class="card-header">Metamaterial taxonomy</div>
                <div class="card-body">
                  <h4 class="card-title text-white">Metamaterial taxonomy</h4>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>

              <div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                <div class="card-header">Making metamaterials in a maker space</div>
                <div class="card-body">
                  <h4 class="card-title">Making metamaterials in a maker space</h4>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>

              <div class="card text-white bg-danger mb-3" style="max-width: 20rem;">
                <div class="card-header">Making a low cost scanning sensor</div>
                <div class="card-body">
                  <h4 class="card-title">Making a low cost scanning sensor</h4>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>

                <div class="bs-component">
                  <div class="card border-primary mb-3" style="max-width: 20rem;">
                    <div class="card-header">Header</div>
                    <div class="card-body">
                      <h4 class="card-title">Primary card title</h4>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                  </div>

                      <div class="card border-secondary mb-3" style="max-width: 20rem;">
                        <div class="card-header">Header</div>
                        <div class="card-body">
                          <h4 class="card-title">Secondary card title</h4>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                      </div>





              <div class="card bg-light mb-3" style="max-width: 20rem;">
                <div class="card-header">Header</div>
                <div class="card-body">
                  <h4 class="card-title">Light card title</h4>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>

              <div class="card text-white bg-dark mb-3" style="max-width: 20rem;">
                <div class="card-header">Header</div>
                <div class="card-body">
                  <h4 class="card-title">Dark card title</h4>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>
            <button class="source-button btn btn-primary btn-xs" role="button" tabindex="0">&lt; &gt;</button></div>
          </div>

      <div class="card text-white bg-warning mb-3" style="max-width: 20rem;">
        <div class="card-header">Header</div>
        <div class="card-body">
          <h4 class="card-title">Metamaterial Zoo</h4>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        </div>
      </div>






<div class="col-lg-4">

                <div class="card bg-secondary mb-3" style="max-width: 20rem;">
                  <div class="card-header">Bibliography</div>
                  <div class="card-body">
                    <h4 class="card-title">Bibliography</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>

                <div class="card text-white bg-info mb-3" style="max-width: 20rem;">
                  <div class="card-header">Header</div>
                  <div class="card-body">
                    <h4 class="card-title">Info card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>

                    <div class="card border-success mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Success card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>

                    <div class="card border-danger mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Danger card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>


                    <div class="card border-warning mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Warning card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>

                    <div class="card border-info mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Info card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>

                    <div class="card border-light mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Light card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>

                    <div class="card border-dark mb-3" style="max-width: 20rem;">
                      <div class="card-header">Header</div>
                      <div class="card-body">
                        <h4 class="card-title">Dark card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      </div>
                    </div>




                  <button class="source-button btn btn-primary btn-xs" role="button" tabindex="0">&lt; &gt;</button></div>
                </div>





          <div class="col-lg-4">
            <div class="bs-component">
              <div class="card mb-3">
                <h3 class="card-header">Card header</h3>
                <div class="card-body">
                  <h5 class="card-title">Special title treatment</h5>
                  <h6 class="card-subtitle text-muted">Support card subtitle</h6>
                </div>
                <svg xmlns="http://www.w3.org/2000/svg" class="d-block user-select-none" width="100%" height="200" aria-label="Placeholder: Image cap" focusable="false" role="img" preserveAspectRatio="xMidYMid slice" viewBox="0 0 318 180" style="font-size:1.125rem;text-anchor:middle">
                  <rect width="100%" height="100%" fill="#868e96"></rect>
                  <text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text>
                </svg>
                <div class="card-body">
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item">Cras justo odio</li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Vestibulum at eros</li>
                </ul>
                <div class="card-body">
                  <a href="#" class="card-link">Card link</a>
                  <a href="#" class="card-link">Another link</a>
                </div>
                <div class="card-footer text-muted">
                  2 days ago
                </div>
              </div>
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Card title</h4>
                  <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                  <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <a href="#" class="card-link">Card link</a>
                  <a href="#" class="card-link">Another link</a>
                </div>
              </div>
            <button class="source-button btn btn-primary btn-xs" role="button" tabindex="0">&lt; &gt;</button></div>
          </div>
        </div>



<div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Primary card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card bg-secondary mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Secondary card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card text-white bg-success mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Success card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card text-white bg-danger mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Danger card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card text-white bg-warning mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Warning card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card text-white bg-info mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Info card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card bg-light mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Light card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
<div class="card text-white bg-dark mb-3" style="max-width: 20rem;">
  <div class="card-header">Header</div>
  <div class="card-body">
    <h4 class="card-title">Dark card title</h4>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>






# **MAKING**

Humans have made material things from as far back as we have been human. The things we make all contain information and making material things can be seen as a kind of coding of information into materials or inversely a materialisation of information.

Making things helps us to construct practical knowledge and enables us to put information back into our environment so that we can influence it.

We now make materials that produce, transfer and store *bits* (binary units of data) and we are living in an age when these digital technologies have blossomed and evolve with us.

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e7c39ee7-36f3-418b-993b-0b21740efdc6/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/e7c39ee7-36f3-418b-993b-0b21740efdc6/Untitled.png)

Schematic timeline of information and replicators in the biosphere from Michael R. Gillings, Martin Hilbert, Darrell J.Kemp (2016)
