**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘐𝘯𝘷𝘦𝘴𝘵𝘪𝘨𝘢𝘵𝘪𝘯𝘨 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭 𝘥𝘦𝘴𝘪𝘨𝘯 𝘢𝘴 𝘢 𝘯𝘰𝘷𝘪𝘤𝘦 𝘮𝘢𝘬𝘦𝘳</font></center>
<br>



# <center>Reflection</center>
  <!--- --> <!--- --> <!--- -->


  <section id=s1>
    <div>
      <article>
        <font size="4" color="lime"><h1>Map of my community</h1>
        <p>One thing that becomes apparent very fast on this course is the total reliance on information from other makers and fairly soon you start understanding the culture of sharing problems and fixes. This is really helpful as a maker but there are rules in t his culture - you can't just ask any question and expect a helpful answer.</p>
        <p>You are hovering over the transparent area (the white tint background) which is displaying this content. This fixed child content has a colored background image with a blend mode which appears to modify the original background image you saw before hover.</p>
        <p>If you keep scrolling down you will see a line. If you hover beneath that line you will be hovering over the next section and it will make this content disappear.![](assets/BSC.jpg)</p></font>
      </article>
    </div>

    so what happens if i wrote here and put a picture in

  </section>
  <section id=s2>
    <div>
      <article>
        <h1>Changing Hover Areas</h1>
        <p>![](assets/BSC.jpg)You are now reading the second section's fixed content with a green background. This is because you are hovering over the second section (the white tint background).</p>
        <p>If you didn't move your mouse and scrolled to get here, you may have noticed that the hover event doesn't fire until scrolling has completely stopped. This is default browser behavior.</p>
        <p>This means you can quickly scroll to the bottom of the page and it will skip the sections along the way.</p>      
      </article>
    </div>
  </section>
  <section id=s3>
    <div>
    ![](assets/BSC.jpg)
      <article>
        <h1>Diagrams I Love</h1>
        <p>    ![](assets/bluewaves.gif) ![](assets/colorwave.gif)![](assets/currentcoil.png)</p>
      </article>
    </div>
  </section>
  <section id=s4>
    <div>
      <article>
        <h1>MOre diagrams please</h1>
        <p>![](assets/poles.gif)</p>
      </article>
    </div>
  </section>

  <div class=default>
    <h1>Hover if you know what's good for ya</h1>
    ![](assets/vortex.gif)
  </div>

  <div class=bg></div>





  # <center>Diary of making</center>
    <!--- --> <!--- --> <!--- --> <!--- -->

  ## Map of my community



  ### Joined the Ardunio community

  The Arduino community is really active. The fact that one piece of hardware can bring so many people together is heartening

  ## Online searches

  I've got faster answers from using search engines to learn how to do things than Fab Academy documentation. Web browsers are surely the key tool to being a maker in the "digital fabrication revolution". The internet has been such a game changer in learning about almost anything. I remember having to propose projects before the internet and there was no way I could have kept as up to date with developments as I have been this year.


  ## The Plan

  I had to describe my final project for Fab Academy for my documentation in week 12. This was a really good exercise to bring together information that I had and that I don't have. It was also quite scary to see how much I had to resolve in order to build my sound mapping kit.

  ### Sensors - things that I have found

  #### accelerometers
  https://www.analog.com/en/analog-dialogue/articles/mems-accelerometers-as-acoustic-pickups.html

  #### Laptop mics

  https://www.html5rocks.com/en/tutorials/getusermedia/intro/

  ## Ardunio sound sensor code

  https://coeleveld.com/arduino-microphone/


  I managed to get the MAX4466 sound sensor to work really well using a LOW voltage of 3.3V from the Arduino with [this code from Adafruit](https://learn.adafruit.com/adafruit-microphone-amplifier-breakout/measuring-sound-levels)

      /****************************************
      Example Sound Level Sketch for the
      Adafruit Microphone Amplifier
      ****************************************/

      const int sampleWindow = 50; // Sample window width in mS (50 mS = 20Hz)
      unsigned int sample;

      void setup()
      {
      Serial.begin(9600);
      }


      void loop()
      {
      unsigned long startMillis= millis();  // Start of sample window
      unsigned int peakToPeak = 0;   // peak-to-peak level

      unsigned int signalMax = 0;
      unsigned int signalMin = 1024;

      // collect data for 50 mS
      while (millis() - startMillis < sampleWindow)
      {
      sample = analogRead(0);
      if (sample < 1024)  // toss out spurious readings
      {
         if (sample > signalMax)
         {
            signalMax = sample;  // save just the max levels
         }
         else if (sample < signalMin)
         {
            signalMin = sample;  // save just the min levels
         }
      }
      }
      peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
      double volts = (peakToPeak * 5.0) / 1024;  // convert to volts

      Serial.println(volts);
      }



  When I tried to load this code some days later it took me over an hour to get the wiring to enable a good reading. this demonstrated to me why **a dedicated soldered board** is definitely needed.

  When things work i want to lock up my laptop and circuit and save the hardware as is so i know i have a preserved piece that works. The assembling and disassemling makes me feel really uneasy.


  ## Buzzer / Frequency generator



  ### the kinds of readings i get

  amplitude goes up from 15000 to 3000 hz but at 2000hz the pitch for some reason goes up again .. i think there must be a duration rate that i need to programme..

  anyway.. or maybe the buzzer just wont work at those frequencies

  <iframe width="560" height="315" src="https://www.youtube.com/embed/xI_qU2auVx8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  i tried to film this but it failed.





  ### how long do i want a reading for every pixel to last?


   I feellike I could attach this to a scanner to get a 2D array of readings.

   I think to start off with the code would need to move 1cm in the x direction for 20cm and then repeat this for 20 lines at 1cm steps in the y direction.

   I think I coud write G code for this movement

   Checking online I found [this tool for converting .jpg to G code](https://www.scan2cad.com/cnc/convert-jpg-to-g-code/) which could be interesting at some point


  ### can I use an nfr24 transciever to emitt a continuour signal


  >The range is 2.400 to 2.525 Ghz which is 2400 to 2525 MHz (MegaHz). The nRF24L01 channel spacing is 1 Mhz which gives 125 possible channels numbered 0 .. 124. WiFi uses most of the lower channels and we suggest using the highest 25 channels for nRF24L01 projects.

  Just remembered I have already collected lots of radio harvesting antenna shapes

  https://www.nordicsemi.com/-/media/DocLib/Other/App_Notes/nan_24-08.pdf?la=en

  ## Analogue reading to processing

  From https://www.instructables.com/id/Read-analog-data-directly-in-Processing/

  Step 1: Preparation Steps
  1. Upload the “standardFirmata” example from the Arduino IDE in your Arduino board. If you are using Arduino UNO, there is a standard firmata for UNO.

  2. Install Arduino library for processing in your libraries folder in the processing sketchbook
  For a detailed instructable for this process go to the Arduino playground example

  arduino library to processing

  uploaded  standard firmata to board through arduino ide

  configured processing to recieve serial code

  Serial
  The Serial library reads and writes data to and from external devices one byte at a time. It allows two computers to send and receive data. This library has the flexibility to communicate with custom microcontroller devices and to use them as the input or output to Processing programs. The serial port is a nine pin I/O port that exists on many PCs and can be emulated through USB.

  Issues related to the Serial library on different platforms are documented on the Processing Wiki. The source code is available on the processing GitHub repository.

  When sending data to the console, such as via print() or println(), note that the console is relatively slow. It does not support high-speed, real-time output (such as at 60 frames per second). For real-time monitoring of serial values, render those values to the Processing window during draw().



  ### adding x and y readings

  i think these would be the output pins to the motors



  -Add other variables to manipulate the X and Y axis of the accelerometer. This program tracks down the X and Y values from the accelerometer and displays them altering the position of a graphic in the screen. It needs to be calibrated for both X and Y parameters.

          import processing.serial.*;
          import cc.arduino.*;

          Arduino arduino; //creates arduino object

          color back = color(64, 218, 255); //variables for the 2 colors

          int xpin= 0;
          int ypin= 1;
          float value=0;


          void setup() {
            size(800, 600);
            arduino = new Arduino(this, Arduino.list()[0], 57600); //sets up arduino


              arduino.pinMode(xpin, Arduino.INPUT);//setup pins to be input (A0 =0?)
              arduino.pinMode(ypin, Arduino.INPUT);
              background(back);
          }

          void draw() {

            noStroke();


            ellipse(arduino.analogRead(xpin)-130, arduino.analogRead(ypin)-130,30, 30);//needs to be calibrated for X and Y separately


            }





  ## Embedded programming and the mystery surrounding programming microcontrollers

  Obviously you need attention to detail for electronics design but in Fab Academy these details are often communicated by tacit knowledge that gets found by asking people or buried in a range of links.

  Written instructions are generally not a thing on the course so you have to decipher processes by listening and writing your own instructions.

  As a class we learned how to use a programme called Eagle to begin the basics of a board known as the [hello world board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.png). What I didn't catch was that we didn't finish the board in the class so when I added the components we were asked to add obviously it didn't work and I had to go back and visit a detail I could have learned before building the board.

  This looping method of teaching was exhausting and mystifying. It didn't feel empowering or satisfying to get something working after getting it wrong 4 or 5 times - it just decreased the amount of time I could have spent applying the knowledge and impacted learning other things. I felt like a bystander in this course rather than someone who had the knowledge to actually make something that works.

  As we didn't all have dedicated making times at least half an hour was spent at the beginning of each new task looking for components and tools which is not a skill that needs learning.

  Maybe a lack of resources led to this method of teaching and maybe this will change with funding but I think a commitment to reaching people who are not already makers or embedded in the maker/ engineering culture could change these teaching methods and revisiting the constructionist roots of Fab Labs seems like an good place to start.

  The order of information communicated during the course for electronics was a huge problem for me. The Networking and Communication material held the key to understanding why components were put together and yet the terms were being used before these lessons.


  I don't see digital fabrication hardware, software and teaching being at a stage yet where most people can easily get engaged.



  ## Visualisations

  I found this GCode Analyser
  http://gcode.ws/

  An amazing voxel generated visualisation of a wifi field
  https://www.youtube.com/watch?time_continue=163&v=g3LT_b6K0Mc

  Rhino Addons


  GENERATOR is a fast and easy way to visualize and interact with generative shapes on the web, connecting Grasshopper

  [PACHYDERM ACOUSTICAL SIMULATION (by Arthur)](https://www.food4rhino.com/app/pachyderm-acoustical-simulation)
  Pachyderm Acoustic Simulation is a collection of acoustics algorithms which can be used to predict acoustiics and visualize sound propagation.
  Download

  Mateusz recommended to look at keras or caffe for machine learning

  https://skymind.ai/learn

  https://www.food4rhino.com/resource/photomodeler-photogrammetry-software
  Accurate and Affordable 3D Modeling - Measuring - Scanning

  The PhotoModeler Software extracts 3D Measurements and Models from photographs taken with an ordinary camera. A cost-effective way for accurate 2D and 3D scanning, measurement, surveying and reality capture.


  DOCUMENT DOCUMENT DOCUMENT

  what if this could be audomated into an emersive environment () that
  1. finds previous documentation, code, files, tools for you linked to the materials, tools and knowledge you have
  2. documents you making things
  3. helps you make decisions on how to make things efficiently

  ## Patchy instructions, patchy words - filling in the blanks

  to some degree all learning is filling in the blanks but can we say that people who make a facebook profile or a google accoutn know anything about coding. Most likely they dont. User interfaces ARE the digital revolution  - not the phones , not the laptops but the fact that now anyone is willing to test a n new online product or device because they are easy to learn... what if we made the user interfaces for . Why hasnt this happeneddesign and learning software and hard ware more intuitive?

   in hard
