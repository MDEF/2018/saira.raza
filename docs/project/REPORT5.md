**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦</font></center>
<br>

# Making a metamaterial devices and testing equipment in a maker space

  <!---"If you want to find the secrets of the universe, think in terms of energy, frequency and vibration" - Nikola Tesla  `` --> <!--- --> <!--- -->




<!--
Below are the weeks with activities related to this project


| Week  | Topic | Artefacts Made | Link |
| --- |-----|-----|----|
| W1 | project documentation | website for documentation | |
| W2 |computer aided design | Rhino Drawing of [Acoustic one-way open tunnel by Yi-Fan Zhu et al](https://aip.scitation.org/doi/10.1063/1.4930300)| |
| W3 |computer-controlled cutting |Johannes T. B. Overvelde et al (2016) Reconfigurable origami-inspired acoustic waveguides and [Sierpinski fractal antenna designed by Sika Shrestha et al](https://www.researchgate.net/publication/270625606_A_New_Fractal-Based_Miniaturized_Dual_Band_Patch_Antenna_for_RF_Energy_Harvesting) and laser cut [Acoustic one-way open tunnel by Yi-Fan Zhu et al](https://aip.scitation.org/doi/10.1063/1.4930300)   | |
| W4 | electronics production | | |
| W5 | 3D scanning and printing | 3D Print of acoustic metamaterial by [Dong et al (2018)](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf) | |
| W6 |electronics design| ||
| W7 |computer-controlled machining | ||
| W8 |embedded programming | ||
| W9 |molding and casting | ||
| W10 |input devices | Sound sensor and ultrasound distance sensor circuits | |
| W11 |output devices| GCODE for reprap controller of stepper motors, Piezo buzzer with Arduino ||
| W12 |applications and implications| overview of tools chain for building a sound scanner||
| W13 |networking and communication| Transmission of analogue sound signals via 2.4GHz radio transceivers, investigation of using radio tranceivers to emit one frequency, Using Wifi transcievers to send directly to a website||
| W14 |mechanical design| ||
| W15 |interface and application programming| Using processing to turn serial output of an Arduino into a graphical representation ||
| W16 |machine design | ||
| W17 |wildcard week | ||
| W18 |invention, IP, and income | Copyleft intentions of any outputs|||




| Week  | Topic | Artefacts Made | Link |
| --- |-----|-----|----|
| W1 | project documentation | website for documentation | |
| W2 |computer aided design | Rhino Drawing of [Acoustic one-way open tunnel by Yi-Fan Zhu et al](https://aip.scitation.org/doi/10.1063/1.4930300)| |
| W3 |computer-controlled cutting |Johannes T. B. Overvelde et al (2016) Reconfigurable origami-inspired acoustic waveguides and [Sierpinski fractal antenna designed by Sika Shrestha et al](https://www.researchgate.net/publication/270625606_A_New_Fractal-Based_Miniaturized_Dual_Band_Patch_Antenna_for_RF_Energy_Harvesting) and laser cut [Acoustic one-way open tunnel by Yi-Fan Zhu et al](https://aip.scitation.org/doi/10.1063/1.4930300)   | |
| W5 | 3D scanning and printing | 3D Print of acoustic metamaterial by [Dong et al (2018)](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf) | |
| W10 |input devices | Sound sensor and ultrasound distance sensor circuits | |
| W11 |output devices| GCODE for reprap controller of stepper motors, Piezo buzzer with Arduino ||
| W12 |applications and implications| overview of tools chain for building a sound scanner||
| W13 |networking and communication| Transmission of analogue sound signals via 2.4GHz radio transceivers, investigation of using radio tranceivers to emit one frequency, Using Wifi transcievers to send directly to a website||
| W15 |interface and application programming| Using processing to turn serial output of an Arduino into a graphical representation ||
| W18 |invention, IP, and income | Copyleft intentions of any outputs|||
-->

## Making Metamaterials from bits to atoms

I began exploring metamaterials by attempting to reproduce metamaterials designed by researchers in published papers.

### Transformable metamaterial using vinyl cut

A basic unit described in the paper
[Babaee, Overvelde, Chen, Tournat & Bertol (2016)](https://www.researchgate.net/publication/310766212_Reconfigurable_origami-inspired_acoustic_waveguides)
was drawn by eye in Rhino 3D CAD package and “unrolled” using one of the 3D tools in the package. Tabs were added to the external edges to enable fixing edges together.

16 units were placed together, saved as an .ai vector graphics file and imported into Cut Studio CAM package. Outlines of the shapes were cut from vinyl sticker roll using a Roland Vinyl Cutter. 18mm support squares were cut by hand and stuck to the interior of net.


![](assets/vinyl.jpg)

### Fractal Antenna using vinyl cut copper tape

A Sierpinski fractal antenna design outlined in the paper [Shrestha, Lee, & Choi (2014)](https://www.researchgate.net/publication/270625606_A_New_Fractal-Based_Miniaturized_Dual_Band_Patch_Antenna_for_RF_Energy_Harvesting) was drawn by eye using Rhino CAD software and saved as a .ai vector graphics file. The was then used to vinyl cut copper tape mounted on PVC. I was able to peel off the excess copper to leave the antenna shape.

<center>![](assets/fractalantenna.jpg)</center>

### 2D acoustic tunnel using laser cut

[Zhu, Zou, Liang, & Cheng (2015)](https://aip.scitation.org/doi/10.1063/1.4930300) fabricated an acoustic one-way open tunnel by 3D printing using ABS plastic. I attempted to replicate this using laser cut acrylic. Some of the measurements were not specified in the paper and I scaled them by measuring the diagrams in the paper.

!<center>[](assets/biglasercut.jpg)</center>

<center>
![](assets/tunnelcut.png)
</center>

### 3D printed acoustic metamaterial

[Dong et al (2018)](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf) 3D printed a labyrinthine space coiling acoustic metamaterial that they designed. I drew the cross section element as given in the paper using Rhino where I was able to replicate them easily. I extruded the cross sections and capped the extrusions and exported these as a .svg file. This was then sliced using Cura software and printed using PLA ... 3D printer.

<center>![](assets/acoustic.jpg) ![](assets/acoustic2.jpg)</center>


## Measuring equipment - extracting data from the environment

I soon found that I was unable to demonstrate the metamaterials I had made without equipment to measure the environment around the material. Below are some examples of the tool chains I attempted to construct.

### Smartphone app, laptop headphones, graph paper, manual timing and movement and Excel

The most rudimentary scanning equipment I was able to construct consisted of and earbud set plugged into a laptop which played a recording of a sinusoidal sound . One earbud was attached to a sheet of graph paper.

As per the experiments of [Dong et al (2018)](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf), I placed a 3D printed sample of their design of an acoustic metamaterial 3cm from the sound source. A recording of a sinusoidal wave of 2240Hz (corresponding to the frequency that the metamaterial was designed to) was played through the earbud.

<center>![](assets/lowfi readings.jpg)</center>

The android app Sound Meter was used on a Samsung Galaxy s6 to measure average and maximum decibels of sound. The smartphone was positioned at 2cm increments on a 18cm x 8cm  grid. At each position the app was refreshed and sound was sampled for 10 seconds. The results were recorded in a spreadsheet and conditionally formatted to produce a heatmap.
Shown below are heat maps of average and maximum sound readings of the set up with and without the metamaterial and the difference (or effect) of the metamaterial.
There is some indication of a focussing effect of sound  

<center>![](assets/heatmap.png) ![](assets/effect.png)</center>

### Sound sensor and ultrasound distance sensor circuits

<center>![](assets/sensortoolch.jpg)</center>

<center>![](assets/newsensor.jpg)</center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/v9y2teN-EmE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Custom GCODE and a Formbytes printer for scanner movement

<center>![](assets/scanner0.jpg)</center>

<center>![](assets/scanner1.jpg)</center>

<center>![](assets/plottertc3.png)</center>

<center>![](assets/scanpath.png</center>

### Piezo buzzer with Arduino

video

###  2.4GHz radio transceivers

video


###  Using Wifi transcievers to send directly to a website




### Processing to turn serial output of an Arduino into a graphical representation

<iframe width="560" height="315" src="https://www.youtube.com/embed/QTHfjnBumLk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Toolchain to make scanner

<iframe
  src="https://embed.kumu.io/6d17fc977f89ccf543fc44e127a4d7d4"
  width="940" height="600" frameborder="0"></iframe>

### Fab Academy Documentation

  <center>Full Documentation of the How to Make Almost Anything is saved in the shared MDEF Gitlab repository here:
  **[Fab Academy Documentation](https://mdef.gitlab.io/saira.raza/Fabhomepage/)**
  </center>

## Insights

### Collaborations between makers, interfaces, machines and materials  

The  digital fabrication processes felt like a collaboration between three parties. Me, the software interfaces, the machines and eventually the materials.

During the course I have had the chance to use hardware to manipulate materials all interfaced through software. The interfaces all involved setting parameters and interface design acted as an intermediary to the relationship with the tools.

More than any of the other techniques, soldering enabled me to have a connection with materials.

Access to hardware, software and materials drove the progress and direction of designs.

### An interplay of tools, knowledge and materials

Tools knowledge and materials affected the things I could make. There are many makers trying to simplify all three of these factors.

<center>![](assets/making.png)</center>
*<center>A map of my reliance on different types of resources I in the 12 week How to Build Almost Anything programme</center>*

### Many new relationships

In order to carry out one type of fabrication you depend on a number of hardware and software tools and you have a relationship with each one. Each relationship takes time to establish and if I hit a problem with one tool I found myself wanting to shift to another quite a lot.

I think that making things with someone who knows what they are doing helps build these relationships faster through a human introduction.

### Navigable interfaces can help to build understanding

I found that the Mods tool designed by Neil Gershenfeld gave a comprehensive path of the digital processes required to go from digital design files to the milling machines we used. The interface guides you prompts you with the parameter required in a set order and you have to understand how to connect them. I found this gave me a visual reminder of what we are doing in this invisible world of processing.

I found that interfaces that guide you through processes were more "friendly" to use.

#### Searching for information and finding short cuts

A lot of information is taught and found online through 3rd party resources. There is a multi-platform learning network of makers and shortcuts to ways of doing things spread easily. It can be hard to join theory to making at first but these experiences do tend to join up eventually.

Finding information for your equipment (your laptop, operating system, software and hardware) yourself by searching on the internet was a big part of starting many of the assignments.

#### A network of makers

Networks of makers evolve around hardware, software, interests, fab labs and locations. They are the best bet for looking for whether or not something can be made or fixed.

#### Wins and momentum

Being able to know there is something that you can do that works was important in feeling you have applicable knowledge. Small wins keep the momentum of learning. From my brief but intensive immersion in the Fab Lab maker network I experienced that the more I managed to make something that I knew was behaving correctly the more I wanted to use that technique.

#### Embodied design necessitates not expecting a particular result

I had underestimated how long it would take to find instructions and to troubleshoot and this resulted in much more  dependence on other people's advice and than I had hoped for. Making electronics requires attention to detail - often in reading diagrams that take time to adjust to.

#### Building Toolchains

Building toolchains in theory and in practice are key to digital fabrication because there are many parts or "collaborators" (both human and non human).

In electronics where transferring code to microcontrollers, the software (the libraries, the board configurations, the drivers and the compilers) have to be treated as "black boxes" as the information - the raw code going in and out of each process was not accessible to me. I often did not have the tools too know why things were not working.

#### Language of teaching and instruction

Digital fabrication involves a lot of technical terms and names of components. Experienced makers wanting to communicate interesting ideas may forget that a newcomer will not use these words as regularly and until I had to make something using the component it was easily lost in abstraction.  It helps to have a tool or map to categorise these components or terms to reference when making or problem-solving.

Also the time it takes to follow instructions does not give as much positive feedback as actually constructing hardware or software so took a lot of concentration.

#### Finding Actionable Instructions

As a learner, learning materials were not always what they seem. There is always room for hidden techniques.

Finding instructions and writing instructions took twice as long as making things with all the materials you need in front of you. This type of learning felt exclusive. Communities like www.instructables.com are addressing this.

#### Learning about doing things is not the same as doing things

At times I was reading and writing so much more than I was spending time with tools and materials that I felt I was doing an inverse embodied design exercise.

#### Hardware not set up for learning by doing

I was surprised that more solid hardware and software ecosystem has not developed around the Arduino considering it has been around 10 years since it was developed.

#### Building confidence with tools

The longer I sat in the fab lab the more comfortable I felt to use the tools. And after a few times of using the laser cutter it had more of a presence and it felt less scary. I think it takes me about 3 ties to use a tool before I feel comfortable to use them.

#### Key questions for making:

Before trying to solve a problem it is wise to establish
- What hardware have I got
- What software have I got
- What materials have I got

Then it is worth researching online and with your community what of these you can acquire if you dont have everything for a toolchain or if you want a different quality or faster making time etc.

#### The rate of development of tools

Part of the reason it is so hard to document various toolchains is the rapid rate of change in hardware and software as new needs arise. This is probably why there is a lack of standardisation in tools.

### Capturing and sharing data
I found that how we capture, instructional, experiential, material and numeric data is really what I ended up having to communicate.



<!---

|Week | Topic| Reference|
| --- |-----|-----|
| **Social practice element to change:**| 	 | |
| **Intended system entry point:** | | |
|**Intended affect on the social practice system:**|  | |


          ### 1. Embodied design to make and test a meta-device in a fab Lab
          **Aims:**
          * To experience energy in our surroundings through making - i.e through interacting with materials / geometric artefacts / and objects of digital fabrication.
          * Expressing who we are though the things we make.
          #### Subjective documentation shared with the fab lab community to contribute to a co-design culture
          **Aims:**
          * Expressing and sharing experiences of making and interacting with objects
          ### 2. A user interface for people to co-design with AIs
          **Aim:**
          * To explore a speculative, non-anthropocentric ways of relating to AI, materials, environment, energy and shapes that effect them.
          #### Presentation or demonstration of how AI can help us to optimise the design of a metadevice through machine learning
          **Aim:**
          * To demonstrate how AI can help us reduce waste by finding optimum shapes using the banks of data we provide it.

          #### Design for inclusion


          Making the user interface accessible to as many age groups, literacies, and improving access for people with disabilities.

-->








<!---**[Fab Academy Non-Technical Diary](PROJECTDOCS/DIARY.md)**

<!---
## A video montage of information from experience -
memories
loops
words
hardware blowing up
-->

<!--
###  Speculating Future interface / experience

Is there a way these would ever combine? what would that look like? an interactive video memory montage with a data panel and files whenever a solution happens - like a running record of your interaction with the material world.

and is ai doing the same?

Would it be helpful to have an automated data collecting making area that does your documentation for you? it could teach you while you make?


### Output from insights

#### Map of toolchain options for process of making a 2D scanner

#### Visualisation of reliance on resources available

#### Map toolchain in electronics

### Can we improve any aspects of this through computation

## A user interface for people to co-design with AIs

-->
<!--
## Presentation or demonstration of how AI can help us to optimise the design of a metadevice through machine learning

### State of the art map of toolchain for inverse design of materials using AI


### Demonstrating Collecting, visualising and storing numeric data

So far
Sensor data from a string
Excel
Processing
scanner works
connecting sensor to wifi or arduino
processing sketch

-->
