 <center><font size="7" color="blue">Being the technosphere`</font></center>

<center><font size="4" color="blue">A metadesign approach to explore our relationship with technology, making and eachother</font></center>
<br>


# <center>Mid term status</center>
  <!--- --> <!--- --> <!--- --> <!--- -->


What I want to say

AI is really good at matching shapes


1. the virtues of embodied experiment
a. the experience of it, 1-1 experiences with no internet builds a relationship with sensors


## Getting Data to test devices

### Acoustic apparatus

#### Sensors

https://www.researchgate.net/publication/277611546_Direct_acoustic_vector_field_mapping_new_scanning_tools_for_measuring_3D_sound_intensity_in_3D_space




#### Collecting data from a sensor via an arduino





<iframe width="560" height="315" src="https://www.youtube.com/embed/KFgGLmLvLw8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Radio Apparatus



### Acoustic metamaterials




## Making shapes




## Machine learning to optimise the shapes of devices


## Feedback and ideas discussed

### Oscar Tomico

Include:

 - a representation of the evolution of materials with computation
 - a representation  of finding shapes from noise
 - what do we consider as noise ? why does is it have value ?
 - the noise of today looks like non-replicable data - is this what artists look at? is it subjective? does the AI play and intersubjective role?
 - a critical look at current maker practices and their limitations in the context of the future of materials
  - a mutation of roles from makers to scientists and machines as curators. Should it be data / noise gathering and co-working with AI to find efficient "things that must be" - made from hard data and noiseforms ?


### Tomas Diez

  - Why is the protraction of computation just digital? What about the implications of quantum
  - Maybe this project is not so much about making material things.
  - There is a big jump between the experience of makers today and the protraction to a future with lots of data and storage space. they almost seem unrelated.

  
