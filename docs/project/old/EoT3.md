# <center>Metamaterials</center>

## <center>Transformable metamaterials</center>

Transformable metamaterials enable structures to change shape.

* Hasso Plattner Institut are looking into transforming the surfaces of 3D structures:

    <iframe width="560" height="315" src="https://www.youtube.com/embed/t12kHV001hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Origami strategies
A number of researchers are materialising strategies from origami using digital fabrication methods.

**Johannes T.B. Overvelde** has made some notable recent breakthroughs in this field


* Extruded cubes put together to utilise hinges and stiffness to make a range of shapes. 'A three-dimensional actuated origami-inspired transformable metamaterial with multiple degrees of freedom.' Johannes T.B. Overvelde, Twan A. de Jong, Yanina Shevchenko, Sergio A. Becerra, George M. Whitesides, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2016) Nature Communications

    <iframe width="560" height="315" src="https://www.youtube.com/embed/AbCb1TtYTbg?start=14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* 'Rational design of reconfigurable prismatic architected materials' Johannes T. B. Overvelde, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2017)  Nature

    <iframe width="560" height="315" src="https://www.youtube.com/embed/7A_jPky3jRY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
'

* 'Topological kinematics of origami metamaterials' Bin Liu, Jesse L. Silverberg, Arthur A. Evans, Christian D. Santangelo, Robert J. Lang, Thomas C. Hull & Itai Cohen (2018)  Nature Physics

* 'Geometry of Transformable Metamaterials Inspired by Modular Origami' Yunfang Yang and Zhong You (2017) Journal of Mechanisms and Robotics
