<!---  <center><font size="7" color="blue">Reclaiming mass, energy and agency from the technosphere</font></center>

<center><font size="4" color="blue">A metadesign approach</font></center>
<br>
 <center>![e8 lie](assets/e9liemox.png)</center> -->


## End of term 2 presentation
<br>

<iframe src="https://sraza.kumu.io/reclaiming-mass-energy-and-agency-from-the-technosphere" width="1024" height="576" frameborder="0"></iframe>




### Schematic of approach
<br>
<iframe
  src="https://embed.kumu.io/ed39d1ce8f4a1156dded3898a0bbc0da"
  width="940" height="600" frameborder="0"></iframe>

<br>


## The broader picture: weak signals to interventions
<br>

<iframe   src="https://embed.kumu.io/ba0d3d9e2d0e25aed0fbe4fc5cfff96b"   width="940" height="600" frameborder="0"></iframe>


<br>
## Feedback
<br>
#### Jordi Montaner
 - More focus is needed on how to present the ideas
 - Consider focussing on something that people will invest money into
 - Some references on the [hologram universe theory](https://www.youtube.com/watch?v=HnETCBOlzJs) could link to the narrative about the e8 Lie group
 - There may be people who can protoype in shenzhen anything you cant make in a fab lab

#### Oscar Tomico
  - There will not be enough time to do all of the deliverables well so maybe trim the intervention - suggested to leave out the speculative deliverables
  - Collaborate on the sensor design and work closer with the fab lab on this to see if they have a need for a specific type of sensor

#### Ramon Sanguesa and Lucas Pena
 - Massimo Menichinelli's [Open P2P Design methodology](http://www.openp2pdesign.org/open-p2p-design/) looks at systems interactions to find ways to codesign
 - You can ask researchers to share algorthms, set up conditions, use tensorflow a dataset and simulator with guidance. It would be valuable to at least see how far you get.
 - Map people for a bottom up collaboration (as per the metadesign principles)
 - Document the steps towards the speculative product if needs be

#### Xavier Domínguez Aparicio
- Recommended speaking to Oscar from Fab Lab as he has experience of using tensorflow
- look at [Illac Diaz](https://climateheroes.org/heroes/illac-diaz-light-by-the-liter/) and [Arvind Gupta](http://www.arvindguptatoys.com/toys-from-trash.php) for low cost tech projects

#### Liz Corbin
- Narrow down to one device for one purpose  and it can be speculative
- [Compliant mechanisms - why machines that bend are better](https://www.youtube.com/watch?v=97t7Xj_iBv0) this project aims to build the minimum viable product sketch

    <iframe width="560" height="315" src="https://www.youtube.com/embed/97t7Xj_iBv0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- Make a prioritisation matrix for an AI
- [Materiom](https://materiom.org/visualise) are using AI to find optimise combinations of biological molecules / particles  that can make a cubic lattice structure.
- Materiom are also working to make data accessible to non experts and Liz actively encouraged this

#### Jonathan Minchin
- Jonathan has explored building a semiotic framework for new ways to codesign. At the moment the narrative is not the main deliverable but I will keep talking to Jonathan.

#### Guillem Camprodon
- Guillem has a lot of experience working with sensors and kindly advised me on some shortcuts I could look for in setting upon a test rig for metamaterials. He advised not reinventing the wheel and after the review also felt that I need to concentrate n one sensor not try to find the easiest experiment to set up.

<br>
<br>
## Reflections and points of improvement
<br>

* <font size="4"> **Make a solid start on one device** you care about or a method you know you can replicate or a need that exists in partners. This needs to be done fast.</font>
<br>

* <font size="4">**Building or replicating a real device and / or optimisation method would contribute more actively to real metadesign cultures** such as fablab than speculative interventions would. I think although this is a risky path I feel it might be more in line with my existing skills</font>
<br>

* <font size="4"> Once the particular experiment is set, **map out collaborators and actively reach out to collaborate for expertise** in sections of the project (fabrication, programming, storytelling, data). This will need to be done asap so that the exhibited work can be built in time and may speed up progress.</font>


<br>


## Activity plan for term 3

<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQVIwrnMmv2k8ULg5mDB2WJi0b6wdAf9amqMnCi8injfFP4qkovUmshT3kUUEtUjkTMeZ50vpxuAKPF/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

https://docs.google.com/spreadsheets/d/1c9yrwDh2KT2Gx3h_C14IN2ihzSaKTIXLXO7pKuTyYpQ/edit?usp=sharing


## Messages to communicate



<center><font size="4"> Everyone is a designer</font></center>
<br>


<center><font size="4"> Take back the Technosphere</font></center>
><center>“We shape our tools and, thereafter, our tools shape us.” 
  — John Culkin (1967)</center>

  </center>


<center><font size="4"> Embrace incompleteness and complexity today to make room for future needs and design.</font></center>

<!---
# TERM 3

## Context - Technology and evolution
### Anthropocene and the technosphere
### Artefacts - symbolism -identity

## Area of Interest - Design approaches for the anthopocene and the future
### Metadesign
### Inclusive Design
###


## Area of Interest - Building new narratives
### Modern n identities


## State of the Art

Who does what?


## Description of the Intervention - Deliverables

### A sensor
### Documentation of manking a sensor
### An AI optimising interface
###  A new narrative


 Results of the Design Action

 ### A sensor
 ### Documentation of manking a sensor
 ### An AI optimising interface
 ###  A new narrative
 #### 


 Projection of the intervention in the future


 Sustainability model



 Final reflection / Learnings

 -->






## Bibliography / Source material





### Technology and innovation evolution theory

[From tools to technosphere Bronislaw Szerszynski](https://www.academia.edu/38085338/From_tools_to_technosphere)

[Carsten Herrmann-Pillath (2017) Foundational Issues of ‘Technosphere Science’ – The Case for a New Scientific Discipline](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3034099)

[Technological change - Arie Rip & René Kemp (1997)](http://kemp.unu-merit.nl/Rip%20and%20Kemp.pdf)

[SPATIAL PARADIGM SHIFTS — FROM EUCLIDEAN GEOMETRY TO AR/MR/VR](https://medium.com/@amber_r_murray/spatial-paradigm-shifts-from-euclidean-geometry-to-ar-mr-vr-54252aa82d3a)

[SAHAL, D. (1981) , Patterns of Technological Innovation]

Sahal (1985, p.64) argues that technological innovations can be: ‚structural innovations that arise from a process of differential growth; whereby the parts  and  the  whole  of  a  system  do  not  grow  at  the  same rate.  Second,  we  have what may  be  called  the material innovations that are necessitated  in an  attempt to meet  the  requisite  changes  in  the  criteria  of  technological  construction  as  a consequence  of  changes  in  the  scale  of  the  object.  Finally,  we  have  what  may  be called the systems innovations that arise from integration of two or more symbiotic technologies in an attempt to simplify the outline of the overall structure‛. This trilogy  can  generate the  emergence  of  various  techniques  including  revolutionary innovations  in  a  variety  of technological  and scientific fields(cf., Sahal,  1981; Coccia, 2016, 2016a)

[from Classification of innovation considering technological interaction](Available from: https://www.researchgate.net/publication/326518857_Classification_of_innovation_considering_technological_interaction)

[Is Technological Evolution Infinite or Finite?- From Quarksto Quasars for Futury 2013](https://futurism.com/is-technological-evolution-infinite-or-finite)

[Technological Innovation as an Evolutionary Process - Edited by John Ziman 92000)](https://pdfs.semanticscholar.org/1bb3/17a5487af7fbe69d274b0d0bead519af735a.pdf)

[Complexity and technological evolution: What everybody knows? Krist Vaesencorresponding and Wybo Houkes](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5842246/)

[Driving forces of technological change: The relation between population growth and technological innovation : Analysis of the optimal interaction across countries](https://www.researchgate.net/publication/260029656_Driving_forces_of_technological_change_The_relation_between_population_growth_and_technological_innovation_Analysis_of_the_optimal_interaction_across_countries)

[The technosphere in Earth System analysis: A coevolutionary perspective (2017) -Jonathan F Donges, Wolfgang Lucht,Finn Müller-Hansen1 and Will Steffen](https://journals.sagepub.com/doi/pdf/10.1177/2053019616676608)

[John Hartleya and Carsten Herrmann-Pillath (2018)  Towards a semiotics of the technosphere](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)

[Natalia Popkova - Controllability of Technosphere and Paradigm of Global Evolutionism](https://www.researchgate.net/publication/327782016_Controllability_of_Technosphere_and_Paradigm_of_Global_Evolutionism)

[Jiri Bila (2014) The syntheses of technological materials as emergences in complex systems](https://www.researchgate.net/publication/282716418_The_syntheses_of_technological_materials_as_emergences_in_complex_systems)


http://technosphere-magazine.hkw.de/

[DANIEL NILES and SANDER VAN DER LEEUW (2018) The Material Order -Do cultural ideas order our material worlds and technologies, or is it the other way around?](https://technosphere-magazine.hkw.de/p/The-Material-Order-4gK5EMpZ3SzB79aTePfJo7)


[Dubravka Cecez-Kecmanovic, Sebastian K. Boell, John Campbell (2014) Materiality of Connectivity in the Networked Society: A Sociomaterial Perspective](https://www.academia.edu/10418062/Materiality_of_Connectivity_in_the_Networked_Society_A_Sociomaterial_Perspective)


[Mark Richardson (2012) Designer/Maker: The Rise of Additive Manufacturing, Domestic-Scale Production and the Possible Implications for the Automotive Industry](https://www.researchgate.net/publication/272730549_DesignerMaker_The_Rise_of_Additive_Manufacturing_Domestic-Scale_Production_and_the_Possible_Implications_for_the_Automotive_Industry)



### Design approaches and methods in the Anthropocene

[Nicky Gregson, Mike Crang (2010) Materiality and Waste: Inorganic Vitality in a Networked World](https://journals.sagepub.com/doi/10.1068/a43176)

https://www.anthropocene-curriculum.org/pages/root/campus-2014/slow-media/mapping-an-exercise-on-cartography/

[Donges, JF, Heitzig, J, Barfuss, W, Kassel, JA, Kittel, T, Kolb, JJ, Kolster, T, Müller-Hansen, F, Otto, IM, Wiedermann, M, Zimmerer, KB, Lucht, W (2018) Earth system modelling with complex dynamic human societies: the copan:CORE World-Earth modeling framework, Earth System Dynamics Discussions, in review](https://www.earth-syst-dynam-discuss.net/esd-2017-126/)

[Richard Přikryl, Ákos Török, Miguel Gomez-Heras, Karel Miskovsky, Magdalini Theodoridou (2016) Geomaterials in construction and their sustainability: Understanding their role in modern society](https://www.researchgate.net/publication/298070425_Geomaterials_in_construction_and_their_sustainability_Understanding_their_role_in_modern_society)

[Frank W.Geelsa Tim Schwanenb  (2017) Reducing energy demand through low carbon innovation: A sociotechnical transitions perspective and thirteen research debates](https://www.sciencedirect.com/science/article/pii/S2214629617303900)

[ELISA GIACCARDI (2003)- PRINCIPLES OF METADESIGN Processes and Levels of Co-Creation in the New Design Space](https://pearl.plymouth.ac.uk/bitstream/handle/10026.1/799/400424.pdf?sequence=4&isAllowed=y)

[Elisa Giaccardi and Gerhard Fischer (2008) Creativity and Evolution: A Metadesign Perspective](http://l3d.cs.colorado.edu/~gerhard/papers/ead06.pdf)

[Gerhard Fischer and Thomas Herrmann (2011) Socio-Technical Systems: A Meta-Design Perspective](https://www.researchgate.net/publication/220625827_Socio-Technical_Systems_A_Meta-Design_Perspective)

[Gerhard Fischer Elisa Giaccardi (2006)](Meta-design: A Framework for the Future of End-User Development)https://www.researchgate.net/publication/226719061_Meta-design_A_Framework_for_the_Future_of_End-User_Development

[Gerhard Fischer (2017) Revisiting and Broadening the Meta-Design Framework for End-User Development](https://www.researchgate.net/profile/Antonio_Piccinno/publication/316734680_Revisiting_and_Broadening_the_Meta-Design_Framework_for_End-User_Development/links/59e8b656458515c3631523ef/Revisiting-and-Broadening-the-Meta-Design-Framework-for-End-User-Development.pdf)

[John Wood (2018)RE-INVENTING INVENTING article Published in Design](https://sublimemagazine.com/re-inventing-inventing)

[Mathilda Tham  and Hannah Jones - Metadesign tools Designing the seeds for shared processes of change](https://www.academia.edu/15257979/Metadesign_Designing_the_Seeds_for_Shared_Processes_of_Change)

[Peter H. Jones(2014) Systemic Design Principles for Complex Social Systems In book: Social Systems and Design](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)

Alastair Fuad-Luke (2017) Design Activism: Beautiful Strangeness for a Sustainable World

Jonathan Chapman - Designers, Visionaries and Other Stories: A Collection of Sustainable Design ...

[Kenneth O. Stanley Joel Lehman Lisa Soros (2017) Open-endedness: The last grand challenge you’ve never heard of](https://www.oreilly.com/ideas/open-endedness-the-last-grand-challenge-youve-never-heard-of)

Jonathan Minchin (2010) Ecological Interactions: A Genetic and Phylogeographic Framework for Growing New Innovations

[Critical Design Critial Futures](http://www.cd-cf.org/articles/)

[Paolo Cardini MATAFUNCTIONAL / METAFICTIONAL OBJECTS](http://www.cd-cf.org/articles/matafunctional-metafictional-objects/)


https://mismatch.design/resources/examples-inclusive-design/2018/06/12/inclusive-a-design-toolkit/

Inclusive deign https://mismatch.design/about-this-site/


### Incomplete design

Incomplete by Design and Designing for Incompleteness
January 2009Lecture Notes in Business Information Processing
In book: Design Requirements Engineering: A Ten-Year PerspectivePublisher: Springer Editors: Kalle J. Lyytinen, P. Loucopoulos, J. Mylopoulos, W. Robinson


### Tracking material flows

[Liz Corbin, Chris Monaghan, James Tooze, Eva Gladek (2019) Materials Democracy: An action plan for realising a redistributed materials economy](https://www.plymouthart.ac.uk/documents/Liz_Corbin%2C_Eva_Gladek%2C_Chris_Monaghan_and_James_Tooze.pdf)

[Pi-Cheng Chena et al (2017) An information system for sustainable materials management with material flow accounting and waste input–output analysis](https://www.sciencedirect.com/science/article/pii/S2468203916301662)

Heuristics Design and Optimization
http://www.mit.edu/~moshref/index.html

### Semiotics

http://culturemachine.net/about/
Culture Machine is an international open-access journal of culture and theory, founded in 1999. Its aim is to be to cultural studies and cultural theory what ‘fundamental research’ is to the natural sciences: open-ended, non-goal orientated, exploratory and experimental. All contributions to the journal are peer-reviewed.

[Logan, Robert K. (2014) What is information?: Propagating organization in the biosphere, symbolosphere, technosphere and econosphere.](http://openresearch.ocadu.ca/id/eprint/397/1/Logan_2014_WhatIsInformation_DEMOPublishing.pdf)

[Marta Lenartowicz CREATURES OF THE SEMIOSPHERE A problematic third party in the ‘humans plus technology’ cognitive architecture of the future global superintelligence](https://www.researchgate.net/publication/305385122_Creatures_of_the_semiosphere_A_problematic_third_party_in_the_'humans_plus_technology'_cognitive_architecture_of_the_future_global_superintelligence)

Tim Lomas - Positive Semiotics

Sergei Nirenburg  & Yorick Wilks What's in a symbol: ontology, representation and language Pages 9-23 (2010)

[Bronislaw Szerszynski 2016 Gods of the Anthropocene Geo-Spiritual Formations in the Earth’s New Epoch]()

Nigel Clark Kathryn Yusoff (2017) Geosocial Formations and the Anthropocene

Anthropocene semiosis - Nigel Clark (not found)

[Matthew Holt (2017)]Semiotics and design: Towards an aesthetics of the artificial](https://www.tandfonline.com/doi/pdf/10.1080/14606925.2017.1352860?needAccess=true)

[Language and other artifacts: socio-cultural dynamics of niche construction Chris Sinha](https://www.frontiersin.org/articles/10.3389/fpsyg.2015.01601/full)

### Narratives of technology

[Mark Coeckelbergh Narrative Technologies: A Philosophical Investigation of the Narrative Capacities of Technologies by Using Ricoeur’s Narrative Theory](https://link.springer.com/article/10.1007/s10746-016-9383-7)

http://www.technologystories.org/crafting-stories/

Donna Haraway, Anthropocene, Capitalocene, Cthulucene: Staying with the trouble

http://technosphere-magazine.hkw.de/

https://maetl.net/talks/stories-as-systems.pdf


### Cultural tools

http://culturemachine.net/

### Alligning software with hardware geometries and logic

To improve scalability, portability, and performance, but also simplify programming and expand applications.

[Aligning the representation and reality of computation with asynchronous logic automata Neil Gershenfeld](http://cba.mit.edu/docs/papers/11.12.Computing.pdf)


### Inverse/ Generative design

SAWYER D. CAMPBELL, DAVID SELL,RONALD P. JENKINS, ERIC B.
WHITING, JONATHAN A. FAN,AND DOUGLAS H. WERNER Review of numerical optimization techniques for meta-device design
https://www.osapublishing.org/DirectPDFAccess/985F7A76-9A95-7221-4C87D41AEE2BB3BB_407576/ome-9-4-1842.pdf?da=1&id=407576&seq=0&mobile=no

[Generative Model for the Inverse Design of Metasurfaces Zhaocheng Liu, Dayu Zhu, Sean P. Rodrigues, Kyu-Tae Lee, and Wenshan Cai](https://pubs.acs.org/doi/abs/10.1021/acs.nanolett.8b03171)
https://arxiv.org/ftp/arxiv/papers/1805/1805.10181.pdf

[Kan Yao, Rohit Unni and Yuebing Zheng (2018) Intelligent nanophotonics: merging photonics and artificial intelligence at the nanoscale](https://www.degruyter.com/downloadpdf/j/nanoph.2019.8.issue-3/nanoph-2018-0183/nanoph-2018-0183.pdf)

[Inverse-designed metastructures that solve equations Nasim Mohammadi Estakhri, Brian Edwards, Nader Engheta]()

[Probabilistic representation and inverse design of metamaterials based on a deep generative model with semi-supervised learning strategy Wei Ma1, Feng Cheng2, Yihao Xu1, Qinlong Wen1 and Yongmin Liu](https://arxiv.org/ftp/arxiv/papers/1901/1901.10819.pdf)

[Deep Neural Network Inverse Design of Integrated Photonic Power Splitters - Mohammad H. Tahersima, Keisuke Kojima, Toshiaki Koike-Akino1, Devesh Jha1, Bingnan Wang, Chungwei Lin, Kieran Parsons1](https://www.researchgate.net/publication/330862750_Deep_Neural_Network_Inverse_Design_of_Integrated_Photonic_Power_Splitters)

[supplementary materials](file:///C:/Users/saira/Downloads/41598_2018_37952_MOESM1_ESM%20(1).pdf)

1. We use numerical simulation (Methods section) to generate labeled data for training the network. Lumerical’s FDTD simulation package to generate the training data. The data contains more than 20,000 numerical simulations, where each experiment is a 3D FDTD simulation com-posed of passive SOI waveguides and the beam splitter device. Initial random hole position matrix of the beam splitter was generated, exported, and manipulated using the MATLAB automation. Hole position generating script uses different algorithms (such as Direct Binary Search), initial conditions, and optimization metrics to sequentially create sufficiently large data set required for a robust neural network representation of the device structure. It took a desktop computer with a core-i7 CPU with 3.7 GHz clock speed and 64 GB RAM about two weeks to complete collecting the 20,000 simulation data.Dispersive refractive indices of silicon and silica from literature41 were used for all simulations for a broadband simulation in the range of 1.45–1.65 μm. The fundamental TE mode at 1550 nm was used at input source and TE mode output power was recorded for transmission and reflection. We note that TM mode output is below 10−5

2. We then feed the DNN with numerical optical experiments  and train a neural network able to represent the relationship between hole vectors and spectral response at each port. Each pixel can have a binary state of 1 for etched (n = nsilicon) and 0 for not etched (n = nsilica)
We use the open source machine learning framework of Tensorflow in python language to build and test our deep neural networks. The runtime to train the neural network model depends on network and training parameters including data size, hidden layer depth and width, batch size, and epoch numbers. The runtime to train the neural network model depends on network and training parameters including data size, hidden layer depth and width, batch size, and epoch numbers. For the representative network parameters, with hidden layer width of 100, hidden layer depth of 8, batch size of 100, epoch number of 10,000, and trained on 20,000 data, shown in Fig. 2d, it take 1337 seconds (~22 minutes) to train the model

Plasmonic nanostructure design and characterization via Deep Learning Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf & Haim Suchowski()

Zeolites
[Czech national supercomputing centre ](https://www.it4i.cz/supercomputing-projects/?lang=en)
A machine learning approach for the description of zeolites
Researcher: Miroslav Rubeš
zeolites, which are used as detergents, catalysts, and adsorbents. In 2017, the value of the global zeolite market was about 30 billion US dollars. The objective of Miroslav Rubeš project is to use machine learning algorithms to create a model which enables deeper understanding of the phenomena occurring in zeolitic materials.

Lithium Battery cells
Optimization design of functional materials in a new type of lithium based battery Call: 12th Open Access Grant Competition
Researcher: Dominik Legut

[F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin (2018) Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices](https://www.nature.com/articles/s41598-018-19796-y)

#### Neural Networks for designing metamaterials

| Author     | Application | Shape data set | Simulation software | Training dataset size | Testing dataset size | Neural network and analysis used | Output format  | How do you converge the result?  |
| ------------- |-------------| -----|-----|-----|
|  [Tahersima et al](https://www.researchgate.net/publication/330862750_Deep_Neural_Network_Inverse_Design_of_Integrated_Photonic_Power_Splitters)     | Photonics | Randomly generated monochrome Pixel arrays |  | 20,000  | 4000 ||  |  |
| [Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin](https://www.nature.com/articles/s41598-018-19796-y)    |   (f > 30 GHz) using high impact polystyrene (HIPS) and a consumer grade 3D-printer based on fused deposition modeling for the fabrication. The material is chosen for its low cost and very low attenuation in the microwave to millimeter-wave region, with a loss-tangent measured to be tanδ<0.003 over the 26–38 GHz band. In this band, the real part of the dielectric constant of HIPS ε′≈2.3(n≈1.52)   |    |  |  |  | |  |  |
|  |      |     |  |  |  | |  |  ||



### Modelling tools

https://alternativeto.net/software/comsol/?license=opensource

https://www.food4rhino.com/app/dodo#downloads_list

[openEMS is a free and open electromagnetic field solver using the FDTD method. Matlab or Octave are used as an easy and flexible scripting interface](http://openems.de/start/index.php)


### Metadevices

[Integrating microsystems with metamaterials towards metadevices Xiaoguang Zhao, Guangwu Duan, Aobo Li, Chunxu Chen & Xin Zhang(2019)](https://www.nature.com/articles/s41378-018-0042-1) - GREAT PAPER ABOUT ACTUATORS

### Antenna geometries

https://www.semanticscholar.org/paper/A-novel-wearable-metamaterial-Fractal-antenna-for-Ahmed-Ahmed/28376c472c5adde23ab0bb1d48374b4762b3b923

https://www.intechopen.com/books/metamaterials-and-metasurfaces/metamaterials-in-application-to-improve-antenna-parameters

[Wojciech Jan Krzysztofik (2017)Fractals in Antennas and Metamaterials Applications](https://www.researchgate.net/publication/317637523_Fractals_in_Antennas_and_Metamaterials_Applications)

[Rana Sadaf Anwar, Lingfeng Mao and Huansheng Ning (2018) Frequency Selective Surfaces: A Review](https://www.mdpi.com/2076-3417/8/9/1689/htm)

#### Applications of Frequency selective surfaces

Applications	Applications
Dynamically reconfigurable FSSs
Wearable sensing FSS
Tunable and software-defined FSSs
Multi-functional FSSs
Mechanically tunable FSS
Smart absorbers FSS
High performance tunable FSS Absorber
Frequency selective rasorbers
Doubly curved FSS surface
FSS for high-power radar applications
Integrated design of antenna-FSS system
Graphene based FSS
3D printed all-dielectric FSSs with wide BW
FSS for wireless power transfer applications
Metamaterial inspired FSS for satellite communication
FSS for earth observation remote sensing and satellites
FSS for selective shielding of frequencies in critical places such as airport and military communication.
FSS for isolating harmful & unwanted radiation in hospitals, schools, domestic environments at microwave low frequency bands (L, S)
Chipless passive structural strength monitoring FSS based wireless sensor
Switchable FSS based on plasma shells for aerodynamics & terrestrial EM systems protection



### Energy Harvesting with metamaterials

General introduction without methodologies:
[Vaishali Rawat  S N Kale (2015) Metamaterials for Energy Harvesting A Review](https://www.researchgate.net/publication/326008281_Metamaterials_for_Energy_Harvesting_A_Review)

[Metamaterials-based enhanced energy harvesting: A review Zhongsheng Chenn, Bin Guo, Yongmin Yang, Congcong Cheng](https://www.researchgate.net/publication/260007855_Metamaterials-based_enhanced_energy_harvesting_A_review)


Vibroacoustic energy harvesting (VAEH)
Acoustic metamaterial (AM)-based vibroacousticenergy harvesting
Electromagnetic metamaterials (EM)-based wireless powertransmission (WPT)

Acoustic metamaterial energy harvesting

[Mourad Oudich, Yong Li(2017 Tunable Sub-wavelength Acoustic Energy Harvesting with Metamaterial Plate](https://www.researchgate.net/publication/317375538_Tunable_Sub-wavelength_Acoustic_Energy_Harvesting_with_Metamaterial_Plate)


[Kyung Ho Sun, Jae Eun Kim, Jedo Kim and Kyungjun Song (2017) Sound energy harvesting using a doubly coiled-up acoustic metamaterial cavity](http://iopscience-iop-org-s.vpn.whu.edu.cn:9440/article/10.1088/1361-665X/aa724e/pdf)


[Badreddine Assouar, Mourad Oudich, Xiaoming Zhou (2015)](Sound insulation and energy harvesting based on acoustic metamaterial plate)



#### Infra red

[XINYU LIU AND WILLIE J. PADILLA (2016) A Metamaterial Capable of Using Heat Loss to Produce Electricity](https://substance.etsmtl.ca/en/metamaterial-capable-using-heat-loss-produce-electricity)
Reconfigurable room temperature metamaterial
infrared emitter


#### Radio


| Author | Type of energy harvested | Configuration of metamaterial | material used | type of output result |
| ------------- |-------------| -----|-----|-----|
|   [Zhou Z., W. Liao, Q. Zhang, F. Han, Y. Chen, A  Multi-Band  Fractal  Antenna  for  RF  Energy Harvesting, 2016](https://www.researchgate.net/publication/327905121_RADIO_FREQUENCY_RF_ENERGY_HARVESTING_USING_METAMATERIAL_STRUCTURE_FOR_ANTENNARECTENNA_COMMUNICATION_NETWORK_A_REVIEW)   | Radio |  | | |
| Shrestha S., S. R. Lee, and D.-Y. Choi, “A New Fractal-Based  Miniaturized  Dual  Band  Patch Antenna   for   RF   Energy   Harvesting”    |   Radio   |   | | |
| [Kyung Ho Sun, Jae Eun Kim, Jedo Kim and Kyungjun Song (2017) Sound energy harvesting using a doubly coiled-up acoustic metamaterial cavity](http://iopscience-iop-org-s.vpn.whu.edu.cn:9440/article/10.1088/1361-665X/aa724e/pdf) |   Acoustic |    | | |
| [Mourad Oudich, Yong Li(2017) Tunable Sub-wavelength Acoustic Energy Harvesting with Metamaterial Plate](https://www.researchgate.net/publication/317375538_Tunable_Sub-wavelength_Acoustic_Energy_Harvesting_with_Metamaterial_Plate) |  Acoustic | | | |
| [Badreddine Assouar, Mourad Oudich, Xiaoming Zhou (2015)](Sound insulation and energy harvesting based on acoustic metamaterial plate) |  Acoustic  |    | | |
| [XINYU LIU AND WILLIE J. PADILLA (2016) A Metamaterial Capable of Using Heat Loss to Produce Electricity](https://substance.etsmtl.ca/en/metamaterial-capable-using-heat-loss-produce-electricity) | Infrared   |    | | |
|  |    |    | | |
|  |    |    | | | |



### Acoustic metamaterials

ultra sound brick - sussex uni
https://phys.org/news/2017-02-sound-shaping-metamaterial.html



### Chemical Sensors

[Ahmed Salim and Sungjoon Lim (2016) Complementary Split-Ring Resonator-Loaded Microfluidic Ethanol Chemical Sensor](https://www.mdpi.com/1424-8220/16/11/1802/htm)


### SRRs

[Filiberto BilottiFiliberto BilottiAlessandro ToscanoAlessandro ToscanoLucio VegniShow all 6 authorsEkmel OzbayEkmel Ozbay Equivalent-Circuit Models for the Design of Metamaterials Based on Artificial Magnetic Inclusions (2008)



### Tech for refugees
jeanmichel.moleanaar@tdh.org

humanfablabs.org



### E8 Lie group narrative building

http://theoryofeverything.org/theToE/tags/e8/

<iframe width="560" height="315" src="https://www.youtube.com/embed/0P8dOWKQtLE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

https://www.thingiverse.com/thing:2313909

[J Gregory Moxness (2014) The 3D Visualization of E8 using an H4 Folding Matrix](http://vixra.org/pdf/1411.0130v1.pdf)


http://www.madore.org/~david/math/e8rotate.html

#### The technosphere constrains us

"The technosphere, the interlinked set of communication, transportation, bureaucratic and other systems that act to metabolize fossil fuels and other energy resources, is considered to be an emerging global paradigm, with similarities to the lithosphere, atmosphere, hydrosphere and biosphere.

The technosphere is of global extent, exhibits large-scale appropriation of mass and energy resources, shows a tendency to co-opt for its own use information produced by the environment, and is autonomous. Unlike the older paradigms, the technosphere has not yet evolved the ability to recycle its own waste stream. Unless or until it does so, its status as a paradigm remains provisional.

Humans are ‘parts’ of the technosphere – subcomponents essential for system function. Viewed from the inside by its human parts, the technosphere is perceived as a derived and controlled construct. Viewed from outside as a geological phenomenon, the technosphere appears as a quasi-autonomous system whose dynamics constrains the behaviour of its human parts.

A geological perspective on technology suggests why strategies to limit environmental damage that consider only the needs of people are likely to fail without parallel consideration of the requirements of technology, especially its need for an abundant supply of energy.

[P. K. Haff Technology as a geological phenomenon: implications for human well-being](http://pne.people.si.umich.edu/PDF/Haff%202013%20Technology%20as%20a%20Geological%20Phenomenon.pdf)

Technologies and institutions increase societies’ robustness to external and internal disturbances but also constitute path-dependencies and lock-ins that make large-scale changes difficult.

[Jonathan F Donges, Wolfgang Lucht, Finn Müller-Hansen (2017)The technosphere in Earth System analysis: A coevolutionary perspective](https://journals.sagepub.com/doi/full/10.1177/2053019616676608)

Latour The Geosphere and biosphere are not systematic
Latour’s (2015) critique of the ‘systems’ term in this context. In his view, ‘Gaia’ (the geosphere + biosphere) is not a system but itself a fragmented, highly dynamic and conflict-ridden entity: a system of conflicting systems, if you like. That means, we should not impose scientific standards of ‘systematicity’ on modelling the Earth system but should instead project the contingency and indeterminacy of the humanities and social sciences on modelling practice.  

[John Hartleya and Carsten Herrmann-Pillath (2018)  Towards a semiotics of the technosphere](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)



#### Fab lab movement

#### Sensors

https://technosphere-magazine.hkw.de/p/Sensing-Air-and-Generating-Worlds-of-Data-fL1Q5CVgGeJMDJmEdwCFr3
