<center><font size="7" color="blue">Geometry as a transdisciplinary tool</font></center>
<center><font size="7" color="blue">in the 21st Century</font></center>
<center><font size="5" color="blue">Its role in sensing, ideating and materialising today and in the future</font></center>
<center>![higgsblue](wa/assets/w12/higgsbluebright.png)</center>



<center><font size="6" color="blue">Why?</font></center>

<center>We sense  much perceivable information from our environment through geometric forms. </center>
<center>Technology is enabling us to perceive new geometric representations and is also enabling us to construct geometric forms with greater ease than ever before. </center>

<center>What is the effect on us?</center>
<center>What should we make?</center>


<center>The bits to atoms revolution is on the horizon and the time to influence the paths of these technologies is now</center>

> <center> “The best time to shape the destiny of transformative, accelerating technologies is early, before changes have become both widespread and entrenched…. The benefits and risks of accelerating technologies are very real, with deep impacts on many lives, but we have the agency, individually and collectively, to shape these impacts now “</center>

*- Neil Gershenfeld, Alan Gershenfeld and Joel Cutcher-Gershenfeld, Designing Reality*

<center>How can we..</center>

<center>
* Use digital fabrication to work contemporary geometric information between sensing, imagining and making today?</center>

<center>
* Make things in a Fab Lab to express our needs and identities in the 21st century? </center>

<center>
* Inquire into our future relationships with bits and atoms given that they are both bound by geometry? </center>




 <center><font size="6" color="blue">How?</font></center>

**Mapping** of research, technologies and tools of inquiry

**Joining the dots** between research into information processing using Geometry, tools of inquiry from the MDEF course and futures of interest.

**Design activism** by exploring what open systems, accessibility, inclusion and representation could look like in emergent futures.

**Prototyping** ways to:
  - Digitally fabricate metamaterials
  - Mimic biological wave bending shapes (diatom frustules and structural colour).
  - Make intuitive user interfaces to switch easily between seeing (or sensing), drawing (or designing) and fabricating.

**Story telling** about experiencing and using information through geometry using video, audio, immersive experiences and diegetic devices.






<center><font size="5" color="blue">Approach</font></center>

The theory behind interventions in the MDEF course is that our environment influences and motivates us to do things and that these interventions can have a wider scales of influence over time.


![influence graph](assets/graph.png)

## Term 1 - Understanding tools

![Term one strategy](assets/term1graph.png)
### Resonating content from Term 1

![journey](wa/assets/w12/journey2.jpg)

<center><font size="5" color="lime">Desk Research</font></center>

<iframe
  src="https://embed.kumu.io/e16cad7dfe00f63ee706c8524a417fc7"
  width="940" height="600" frameborder="0"></iframe>


  <center><font size="5" color="blue">Motivations</font></center>

 The motivation for this project is the growing availability of imaging and fabrication technologies which are bound together by geometry - as our senses are bound to the material world.

<font size="5" color="lime">Narratives from Term 1</font>

**Multiplicity**
Quantum concept of many different states existing at the same time.
Transdisciplinary approaches to projects.
Multiple identities and modes of working.
Simultaneous actions.
Inclusive and open systems of governance and design.
Subjectivity.

**Multi-scalar patterns**
Hierarchical structures.
Similarity within fractals

**Chaos**
Systemic thinking. Determinism and agency to influence chaotic, dynamic systems. The myth of equilibrium and defining sustainability.

**Finding coherence in noise**
Neural Networks, iterations through parametrics, computer processing, Machine Learning and material driven design.


<font size="5" color="lime">Possible material interventions through Fab Academy in Term 2</font>


<p><font size="3" color="lime">Geometric Interfaces / Controllers to enable feedback, iterations and "working" information like a material</font></p>

- 3d voxel design software to design metamaterials
- sound interfaces that help you sense sound through touch
- physical controller for grasshopper to modify electronic and maybe acoustic sounds
- using representations of fields and fluids as information interfaces

<font size="3" color="lime">Experiences</font>

* Acoustic
* Immersive narratives

<font size="3" color="lime">Transformable materials</font>

* Soft robotics https://softroboticstoolkit.com/
* Metasurfaces with purpose - **energy transfer surfaces**

<font size="3" color="lime">Emergency ready tools</font>

* For water purification, extraction or management
* For communication - wifi, radio
* For innovation - apps and mobile tools

<font size="3" color="lime">Diatom frustules for energy transfer and capture</font>

Working with partners to experiment with diatoms in

* Photovoltaics
* Batteries
* Water purification
* Sensors
* Carbon capture strategies

<center><font size="5" color="cyan">Imagined futures of interventions</font></center>



![Voros section](wa/assets/w10/futurescone-voros.png)

 | Future  |   Scenario  |
 | ------------- |:-------------|
 | Possible |  War, famine, water and fuel shortages |
 | Plausible | Political instability, greater inequality |  
 | Probable |  Climate change, population growth, climate change refugees  |
 | Preferable |  Less reliance on fossil fuels, more rights-based decision making. Buy in from major world leaders in paradigm change regarding wealth distribution. More inclusive education and governance systems. More representation. |
 | Premeditated | Costs of electronic equipment being driven down, greater processing power |
 | Preposterous | Alien landing, posthuman worlds,  |
 | Wildcard |  Asteroid, postcolonial futures |   |




### End of Term 1 reflections on feedback

<font size="3" color="blue">Developing an opensource resource</font>

Tomas was interested in how the findings of my research is currently hosted on opensource platforms and that this could be an interesting output of the project.
I will continue to keep my research mapping public to potentially turn into a comprehensive informal repository of information.

<font size="3" color="blue"> Interfaces</font>

I was advised by Merce Rua that the investigation should include interfaces as well as fabrication methods. This is an important observation in how technology has changed our relationships with shapes. User interfaces are exercises in using geometry as tools as well a gateways to experiences in many facets of life these days. I was reminded of how the visual language of the internet has changed.

Veronica mentioned the use of "metasurfaces" as user interfaces and there seems to be a tie in to this with transformable metamaterials. There may be an opportunity to collaborate on fabricating a metasurface during the next term.

<font size="3" color="blue"> Narrative</font>

I was advised by Guillem to look at a talk by Neil Gershenfeld because he has a well informed narrative on the topic of fabrication. This has been really helpful in framing the significance of geometry in the transfer of information to materiality.

<font size="3" color="blue"> DNA Origami</font>

Nuria gave me a tip of a field called DNA Origami. This is an excellent example how geometry's coded rules can be exploited.

<font size="3" color="blue"> Parametric software</font>

Ollie suggested that I might find exploring this topic using Grasshoper useful as it enables easy experimentation with parameters. When I tried this addon I found an excellent interface that allowed the manipulation of geometry using the analogy of a machine with inputs and outputs.




### Field Trip to Shenzhen

The most obvious observation from Shenzhen was the different in scale of the city and it's amenities compared to any other city I have visited.

I was able to see many examples of digital fabrication at SZOIL and SEED studio and was encouraged at how quickly ideas could be prototyped.

Also looking at the marketplaces gave encouragement that ideas could be easily modified very fast.


## Term 2 Designing Interventions

<center><font size="5" color="magenta">Experimenting with fabricating geometries in the self effecting stage</font></center>
<center>![Graph](assets/firstperson.png)</center>


In term 2 I started personal interventions in familiarising myself with digital fabrication tools to explore my own relationship to the technology and materials and resulting geometries and the way they interact with the environment.

<center><font size="5" color="blue">Term 2 Stimulus</font></center>

I took a multi-start approach to incorporate my research interest in geometry, the futures of my political interests and the tools I will use on the MDEF course this term.

There are many possible intersections of these stimuli so I am "seeding" interventions in parallel depending on what the MDEF course is focussing on each week.

I hope that some interventions will grow as more  opportunities, interest and skills arise and that interventions will crystallise with direction from our practice in the Atlas of Weak Signals modules.

I have been taking an embodied "first person" design approach towards an intervention, using new fabrication skills as starting points rather than starting from a future scenario. This has led to a better understanding of the workflows, language, materials and time needed to fabricate simple things.

<center>![Interventions](assets/updatedactivities.jpg)</center>


<!--
I have been documenting my progress between ideas and completed experiments below. I am looking for a way to make a layered representation of these project paths.
<iframe
  src="https://embed.kumu.io/75f2505a4871b929126a619d20251b7f"
  width="940" height="600" frameborder="0"></iframe>   -->

<p><font size="4" color="magenta">Cutting transformable metamaterials</font></p>
I have been looking at the work of Bas Overvelde who has defined mathematically how to design modules for transformable metamaterials.
I tried vinyl cutting one of the module he has designed and learned that these shapes require a lot of material as they have high surface areas.

<p><font size="4" color="magenta">3D printing acoustic metamaterials metamaterials</font></p>

<p><font size="3" color="magenta">Labyrinthine configuration</font></p>

I attempted to replicate to scale [a labyrinthine shaped 'double-negative' acoustic metamaterial unit designed by Dong et al](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf).
This configuration was easy to draw and 3D print however I found with acoustic metamaterials consideration always needs to made to how it should be installed in relation to the sound source and listener. An application is necessary to see how well it works.

<p><font size="3" color="magenta">Metasurface acoustic tunnel</font></p>

I replicated this [acoustic one-way open tunnel by using metasurface by Zhu et al](https://www.researchgate.net/publication/283005608_Acoustic_one-way_open_tunnel_by_using_metasurface). This turned out to be an inefficient use of materials and time as the dimensions were so big. I modelled a 3d version of this tunnel to see if They could be used as eaf muffs. They turned out to be very large and would take a lot of materials and time to 3D print this. I am thinking about alternative construction methods.


<p><font size="4" color="magenta">Trying to cultivate live diatoms</font></p>

In my Material Driven Design project I tried to cultivate live diatoms from the sea and from an aquarium. It was difficult to do this without a powerful microscope or a photobioreactor so I realised that this was not a "material" I could work with in the timeframe of the project.

<p><font size="4" color="magenta">Working with fossilised diatoms</font></center></p>

I tried to work with fossilised diatoms to see if this was a material I could do anything with using the equipment available to us. Diatomaceous earth is often used as a filter or powder to contain other substances. I was able to incorporate it into some bioplastics which achieved some water resistance.

I also tried to see if I could combine titanium oxide and / or aloe vera with diatomaceous earth to see if I could use it as the semiconductor layer in a dye sensitised solar cell.


### Term 2 Mid-term Reflections

#### Sensual mathematics
Experimenting through timed tasks is testing my limits of making and sensing (and to a lesser extent reconfiguring information) through geometry. I have been thinking about how **through geometry, mathematics transforms information into dimensions we can sense - allowing us to experience information that may not be otherwise  experienced**. User interfaces can use geometry to make information "workable" through feedback loops to our senses. This leads me to want to explore ways to connect our senses to immaterial information (thoughts, emotions) or to unusual experiences of information (across scales and movements).

>"..it is certain that mathematics generally, and particularly geometry, owes its existence to the need which was felt of learning something about the relations of real things to one another. "

  *Albert Einstein, from [a lecture on 'Geometry and Experience' (1921) at the Prussian Academy of Sciences in Berlin](http://www-history.mcs.st-andrews.ac.uk/Extras/Einstein_geometry.html).*

#### A framework for first person interventions - What geometry lets us do
I have been reflecting further on the qualities of geometry that we leverage to help us work with information:

1. **Sensing our environment**  geometry is sensual mathematics. Vision sound and touch are geometric effects. These three senses let us physically sense information and perceive things in 3 dimensions and can help us conceptualise more. Diagrams, Imaging and mapping tools and acoustic devices like speakers and musical instruments are extensions of our senses. But even before we apply any meaning or use to Geometric effects, these give us raw subjective qualitative experiences.

2. **Controlling and shaping information** - geometry is something we can control through mathematics and through our bodies (dancing, drawing, speaking, designing). We have tools that helps us to draw and manipulate shapes. This helps us to be expressive, playful and to solve problems through our senses.

3. **Materialising and replicating material things** - Maths is translatable across systems and repeatable with the same results - allowing us to make things out of matter, scale them up, multiply them and use the information held by shapes to do different things.

    > "With a lego brick you don't need a ruler to place it, the block has geometry, it corrects errors and you don't put Lego in the trash, you take the bricks apart. The reason you can do this is the bricks contain information.”

    *Neil Gershenfeld*


<center>![Project tools](assets/experimenttools.jpg)</center>

### Reflection on feedback from mid-term review

#### Defining the intervention into one intervention

I had hoped that I could work with geometry in a material driven approach - using the information contained in it to let it guide an intervention - this also fit well with the narrative of the digital fabrication revolution - where bits are becoming more and more workable into atoms as bits are workable today into other bits.
The embodied design approach enabled me to test the limitations of things I could make in the time available with the tools. I was able to learn how to use my laptop to draw, reconfigure, scale and format 3 dimensional drawings that the fabricating tools could make.

The review made me revisit the different dimensions of what might constitute an intervention I realised that many things needed addressing that we have touched upon including:

* Political focus
* Scales of influence of interventions over time
* Futures of interest
* Weak signals of trends (new perspectives and tools coming this term)

##### **Political focus: Inclusion**

My political focus in a broad sense is to include more people into the development of technology to secure their basic needs in a sustainable way.

##### **Physical Focus: Energy and capture devices **
My desk research suggests to me that geometry mediates (transforms, resonates )not only matter (atoms) but also waves (material or not) all of which have energy. I believe that matter and geometry is most useful to us in its ability to capture, transfer and transform energy.

I thought what was interesting about transformable metasurfaces is that they enable tuning of energy transfer processes and transformable 3D metamaterials could help to tune filtering processes as well as wave interaction.

I have learned that metamaterials can have:

- transformable surface areas which have implications on energy transfer processes.
- Voids - which have applications for their effects on fluids and waves carried by fluids. Voids can also act as filters or barriers at various scales  to separate molecules and particles (as membranes and filters)

These qualities of exchange and capture in the material world is what I think I should leverage.


##### **Scales of influence**
Geometry is already a part of anything material at several scales - one of the reasons I chose this topic was its scalability. With metamaterials especially, it would be possible to scale them up or down for different uses. It is time now to focus on a use with a purpose. I feel that working with energetic and water problems are also easily scalable in terms of easily manufactured small devices and larger devices for larger operations.

**Embodied design for the first person**
As someone who is learning these new tools myself, I feel I should start by designing for myself, then design for someone else using data from them and then share the result for people to adapt as they need. We learned about such an approach in term 1.

**Design for another known user**
Possible users could include - people not used to digital fabrication.

**Design for 3rd person based on theory to be field tested**
for people who need personal water, energy, heat or communication devices.

##### **Consider waste implications in fabrication**
This was an excellent point as my approach to the project is wasteful. Addressing waste during the experimentation could form part of the question - working with waste product could form a line of inquiry.

##### **Exploring future contexts**
Futures that interest me include preposterous futures that are a complete departure from the culture and landscape we have today.  
These present an opportunity to reframe current narratives about underrepresented points of view such as women, children, refugees, people with disabilities, future generations and non-humans - although these must be authentic and based on field study.

### What is the intervention

Using emerging geometries in materials, surfaces and devices to prototyping ways to capture carbon dioxide, water, pollutants and renewable energy towards influencing future contexts of the Anthropocene.



### What next in term 2

#### Tools
Focus on making geometries transform.

* Use fab academy techniques to find ways to control and measure the effect on metamaterials - more emphasis on sensors, control and programming.

Inverse-design of material shapes - metamaterial antennas

  * Exploring simulation software for waves, fields and fluids acoustics:
    * shttps://www.code-aster.org/spip.php?rubrique2
    *openEMS FDTD solver written in C++.

  * Deep Neural networks
    * Tensorflow in python languag

#### Futures
* Research futures of machine learning and supercomputing in inverse design
* Cultural impacts and interventions

#### Fields and fluids - probability, movement and densities
* If time permits look at applications of field (energy, probability) and fluid (movements of matter) geometries to understand information

  > Contemporary society seems to be losing its solidity:
its organizations are becoming ductile and the forms of
life within it are becoming fluid, every project tends to be
flexible and every choice reversible. Or at least that’s what
we like to think.
It follows that the best metaphors to describe it come
from the physics of fluids rather than of solids, more from
living systems than from mineral ones.

  *- Ezio Manzini*




<center><font size="5" color="magenta">Coming soon..</font></center>
<center><font size="5" color="magenta">Home effecting stage</font></center>

<center><font size="5" color="magenta">Neighbourhood effecting stage</font></center>

<center><font size="5" color="magenta">City effecting stage</font></center>

<center><font size="5" color="magenta">Country effecting stage</font></center>

<center><font size="5" color="magenta">Planet effecting stage</font></center>

## References

[Prototyping Interactive Nonlinear Nano-to-Micro Scaled Material Properties and Effects at the Human Scale - Jenny E. Sabin, Andrew Lucia1, Giffen Ott1, Simin Wang]

[Fabricating structural Colour](https://www.nature.com/articles/s41598-017-17914-w)

[Activities and findings of the MIT Center for Bits and Atoms, 2008](http://cba.mit.edu/docs/reports/08.06.NSF/index.html)

[New materiality: Digital fabrication and open form. Notes on the arbitrariness of architectural form and parametric design. Carlos L. Marcos (2011)](http://www.improve2011.it/Full_Paper/41.pdf)

[Aligning the representation and reality of computation
with asynchronous logic automata - Neil Gershenfeld
](http://cba.mit.edu/docs/papers/11.12.Computing.pdf)

[A Geometric Theory of Everything - Garrett Lisi James Owen Weatherall 2010](https://www.researchgate.net/publication/49667548_A_Geometric_Theory_of_Everything)

### Reading list

<style type="text/css" media="screen">
       .gr_grid_container {
         /* customize grid container div here. eg: width: 500px; */
       }

       .gr_grid_book_container {
         /* customize book cover container div here */
         float: left;
         width: 98px;
         height: 160px;
         padding: 0px 0px;
         overflow: hidden;
       }
     </style>
<div id="gr_grid_widget_1553007354">
<!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->

 <div class="gr_grid_container">
   <div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://images.gr-assets.com/books/1499718864m/34272565.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Forbidden Science: From Ancient Technologies to Free Energy" rel="nofollow" href="https://www.goodreads.com/book/show/2762643-forbidden-science"><img alt="Forbidden Science: From Ancient Technologies to Free Energy" border="0" src="https://images.gr-assets.com/books/1378705883m/2762643.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" rel="nofollow" href="https://www.goodreads.com/book/show/41257165-creating-a-universe-from-nothing"><img alt="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" border="0" src="https://images.gr-assets.com/books/1534744765m/41257165.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Wholeness and the Implicate Order" rel="nofollow" href="https://www.goodreads.com/book/show/204523.Wholeness_and_the_Implicate_Order"><img alt="Wholeness and the Implicate Order" border="0" src="https://images.gr-assets.com/books/1385055758m/204523.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Geometry of Meaning: Semantics Based on Conceptual Spaces" rel="nofollow" href="https://www.goodreads.com/book/show/18699199-geometry-of-meaning"><img alt="Geometry of Meaning: Semantics Based on Conceptual Spaces" border="0" src="https://images.gr-assets.com/books/1392025960m/18699199.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" rel="nofollow" href="https://www.goodreads.com/book/show/2374714.The_Geometry_of_Multiple_Images"><img alt="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" border="0" src="https://images.gr-assets.com/books/1347661899m/2374714.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Quantum Language and the Migration of Scientific Concepts" rel="nofollow" href="https://www.goodreads.com/book/show/36722595-quantum-language-and-the-migration-of-scientific-concepts"><img alt="Quantum Language and the Migration of Scientific Concepts" border="0" src="https://images.gr-assets.com/books/1541090340m/36722595.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="How Change Happens" rel="nofollow" href="https://www.goodreads.com/book/show/42068857-how-change-happens"><img alt="How Change Happens" border="0" src="https://images.gr-assets.com/books/1541664529m/42068857.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" rel="nofollow" href="https://www.goodreads.com/book/show/34523271-designing-reality"><img alt="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" border="0" src="https://images.gr-assets.com/books/1493825166m/34523271.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Media, Materiality and Memory: Grounding the Groove" rel="nofollow" href="https://www.goodreads.com/book/show/29513395-media-materiality-and-memory"><img alt="Media, Materiality and Memory: Grounding the Groove" border="0" src="https://images.gr-assets.com/books/1495698688m/29513395.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" rel="nofollow" href="https://www.goodreads.com/book/show/17239972-how-matter-matters"><img alt="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" border="0" src="https://images.gr-assets.com/books/1363584198m/17239972.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" rel="nofollow" href="https://www.goodreads.com/book/show/12923329-the-future-of-post-human-geometry"><img alt="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" border="0" src="https://images.gr-assets.com/books/1348319287m/12923329.jpg" /></a></div>
   <br style="clear: both"/><br/><a class="gr_grid_branding" style="font-size: .9em; color: #382110; text-decoration: none; float: right; clear: both" rel="nofollow" href="https://www.goodreads.com/user/show/89166601-saira-rrza"></a>
 </div>
</div>
