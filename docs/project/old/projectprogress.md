
<!-- - Documentation of the 1st term work on your project (your journey + reflections about final presentation feedback)
 --->

<center><font size="6" color="blue">Sacred Geometry in the 21st century</font></center>
<center>An exploration of our contemporary and future scientific and experiential relationships with geometry</center>
<center>![higgsblue](assets/w12/higgsbluebright.png)</center>


<center><font size="6" color="blue">Why?</font></center>

<center>Because technology is enabling us to see new geometric occurrences and  construct geometric forms with greater ease than ever before.</center>
<center>This is an opportune moment to explore how we can interpret, use and share these breakthroughs,</center>
<center>make them opensource / inclusive from the offset,</center>
<center>and produce artefacts representative of what it is and what it feels like to be a human in the 21st century</center>


<center><font size="6" color="blue">How?</font></center>

<center>![Proposed methods and sources of inquiry](assets/w12/invertedview.png)</center>


<!--  **<span style="background-color: lime"><font color="blue">[Researching the state of the art](#tsota)</font></span>** in:

  * scientific findings of geometry (mathematics, modelling of phenomena and processes)
  * use of geometry in art especially using new media
  * fabrication methods and technologies employing geometry

**<span style="background-color: cyan"><font color="blue">[Speculation](#speculation)</font></span>** of:

  * Future objects and scenarios that could exist using the axes of equality and rationality.
  * Ridiculous futures
  * Postcolonial futures
  * Fictional and 'extended present' materials

**<span style="background-color: yellow"><font color="blue">Experimentation</font></span>** with:

  * 3D modelling interfaces and techniques
  * 3D printing using standard materials and equipment to make repeatable things
  * Unusual materials and adapting printers to use these
  * Audio, video and animation storytelling methods
  * Living with a speculative material / artefact / situation (design by experience) --->


## <center>The journey so far</font></center>


![Term 1 journey](assets/w12/journey.jpg)

## **<center><font size="6" color="lime">Research</font></center>**

<center><font size="5" color="blue">The State of the Art</font></center>

<iframe
  src="https://embed.kumu.io/e49633851de30f585f3ab1aeabb0999f"
  width="940" height="600" frameborder="0"></iframe>

Research findings that were presented at discussion can be found **<span style="background-color: lime"><font color="blue">[HERE](week12research.md)</font></span>**




## **<center><font size="6" color="cyan">Speculation</font></center>**

<center><font size="5" color="blue">Future scenarios and possible intervention in this area</font></center>


  ![Voros section](assets/w10/futurescone-voros.png)


| Future  |   Scenario  | Interventions using preferred paths of inquiry  |
| ------------- |:-------------| -----|
| Possible |  War, famine, water and fuel shortages | 1. Making information more accessible: Integrating GIS with open access user interfaces to gain access to water supplies and governance systems. 2. Mutualistic strategies working with biological processes to find sustainable research into undersea greenhouses using metamaterial structures to tune light sources for optimum photosynthesis |
| Plausible | Political instability, greater inequality | Speculative devices that use geometry to increase representation in the arts,  science and design. This could include design interfaces |
| Probable |  Climate change, population growth, climate change refugees  | Using geometry to get qualities out of materials and interfaces that can be easily made by refugees |
| Preferable |  Less reliance on fossil fuels, more rights-based decision making. Buy in from major world leaders in paradigm change regarding wealth distribution. More inclusive education and governance systems. More representation. | Cheaper or more engaging / accessible educational tools |
| Premeditated | Costs of electronic equipment being driven down | using geometry to enhance opensource tools that can be used on current smartphones to learn about, design and fabricate items cheaply |
| Preposterous | Alien landing | Speculative investigation into how geometry can be used to communicate with aliens |
| Wildcard |  Asteroid | Speculative disaster risk readiness plan ||

## **<center><font size="6" color="fuchsia">Experimentation</font></center>**

<center><font size="5" color="blue">Possible Areas of interventions</font></center>

Below is a map of interests that have developed since starting the course.  You can filter the elements by what weeks inquiries influenced them using the buttons at the bottom of the map.

<iframe
  src="https://embed.kumu.io/c7423b6d371fc686fe6001e75401e1c7"
  width="940" height="600" frameborder="0"></iframe>


### <center>Possible outputs / interventions</center>





<center>![Experimentations](assets/w12/pinkexperiments.png)</center>
**3D printed acoustic metamaterials** for

* "sound field"/ soundscape installations/ environments
    * References: [Sound Field Synthesis (2014) Rudolf Rabenstein, Sascha Spors, Jens Ahrens](https://acousticstoday.org/wp-content/uploads/2015/05/Sound-Field-Synthesis-for-Audio-Presentation-%E2%80%93-Jens-Ahrens-Rudolf-Rabenstein-and-Sacha-Spors-.pdf) and [Univrsity of Sussex - Interact Lab](http://interact-lab.com/our-research)

* Acoustic earmuffs / Earphones / filters
    * References [Duke University talking filters 'Single-sensor multispeaker listening with acoustic metamaterials' (2015) Yangbo Xie](https://phys.org/news/2015-08-metamaterial-device-cocktail-party-problem.html)

* Biomimicry of diatoms on the acoustic scale  

**Sacred Geometrical Artefacts**
      leveraging the experiential qualities of geometry to convey ideas about who we are now and in the future

**Accessible design interfaces**

  - for fabrication and development of opensource tools
  - for people with little access to technology
  - to increase engagement with current observations of geometry and emerging technologies that use these

**Design fiction methods**

  * **Speculative diegetic devices**

      - investigating our relationship with electromagnetic waves from radio to light to microwaves and what we could do if we could control these better.
      - invisibility cloaking
      - lighter more efficient motors and turbines
      - Foldable / transformable / textile lenses, antennae, magnets, conductors, computers (every electronic thing that exists)

      * References:
          * ['Design Fiction Product Design Work Kit' - Near Future Laboratory](http://shop.nearfuturelaboratory.com/collections/frontpage/products/design-fiction-product-design-work-kit)
          * ['Patently untrue: fleshy defibrillators and synchronised baseball are changing the future' - Bruce Sterling for Wired, 2013](https://www.wired.co.uk/article/patently-untrue)

* **Experiential futures** (using the above objects) inviting inclusive experiential inquiry into the impact of new observations of geometry. Possible topics include:

    - Tensor networks and the nature of spacetime
    - Leveraging biomimicry of diatoms
    - Networks of information and learning (neural networks, network analysis methods)

    - References:         
      * ['Design futures in action: Documenting experiential futures for participatory audiences' (2014) Aisling Kelliher Daragh Byrne ](https://www.sciencedirect.com/science/article/pii/S0016328714001980)


## Presentation

Ideally I would have liked to present my desk research and experimentation findings in a way that utilised the symbolism and visual logic that geometry helps us to learn through.
Oscar advised that my findings needed to be interactive and I do agree.
I was disappointed that I did not manage to use the time I had to create an environment that was more visual and interactive in a non conventional way.

What I ended up presenting on was:

1. 1 A2 poster containing the introduction and journey so far
2. 1 tablet containing a mind map hosted on Kumu.io on the interests developed during the term (shown above)
3. 3 large LCD screens hosting 6 web browsers which invited the viewer to explore the research topics **<span style="background-color: lime"><font color="blue">[HERE](week12research.md)</font></span>** including an existing opensource mechanical metamaterial design interface.
4. 3D printed models of:
      - Opensource metamaterial pliers design 3D adapted and printed in nylon
      - Opensource 3D printed diatom frustule
      - 3D printed metamaterial surface borrowed from Fab lab
5. A greenscreen app and green material to experiment with the experience of invisibility cloaking (a hot topic in metamaterials that could use some speculative investigation) although the lighting did not permit this to work in the end.

![set up](assets/w12/setup.jpg)![3dprinted](assets/w12/3dprinted2.jpg) ![greenscreen](assets/w12/app2.jpg)  <iframe src="https://giphy.com/embed/loyjfqCGHDWZeigo1y" width="270" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/greenscreen-loyjfqCGHDWZeigo1y"></a></p>

<font size="4" color="blue">Actions for future presentation</font>

* As the stage of the project is very early and more about pulling out some interesting findings and joining the dots I hope in later stages - when there is more of a distilled concept to grasp - that the presentation method can be more engaging and imaginative.

* I think there is scope to employ narrative methods once findings are more narrowed down.

* I am keen to improve my ability to use 2D software to help me improve the graphical methods I am going to need to use.

* I am also keen to learn Python and Javascript to do more interesting original presentations of data




## Feedback

<font size="4" color="blue">Developing an opensource resource</font>

Tomas was interested in how the findings of my research is currently hosted on opensource platforms and that this could be an interesting output of the project.
I will continue to make my findings public as works in progress.

<font size="4" color="blue"> Interfaces</font>

I was advised by  that the investigation should include interfaces as well as fabrication methods. This is a rich area of research and an important observation in how technology has changed our relationships with shapes. User interfaces are exercises in using geometry as tools as well a gateways to experiences in many facets of life these days.
I was reminded of how the visual language of the internet has changed.

Veronica mentioned the use of "metasurfaces" as user interfaces and there seems to be a tie in to this with transformable metamaterials. There may be an opportunity to collaborate on fabricating a metasurface during the next term.

<font size="4" color="blue"> Narrative</font>

I was advised by Guillem to look at a talk by Neil Gershenfeld because he has a well informed narrative on the topic of fabrication.

<font size="4" color="blue"> DNA Origami</font>

Nuria gave me a tip of a field called DNA Origami.

<font size="4" color="blue"> Parametric software</font>

Ollie suggested that I might find exploring this topic using Grasshoper useful as it enables easy experimentation with parameters.

## Action points
* Continue to map actors in research in the fields
* Research opensource interface tools
* Research state of the art and future interface methods
* Refine the narrative of the project by referencing Neil Gershenfelds findings on the topic of the future of fabrication
* Research DNA Origami as a field - its applications and tools available.
* Learn Grasshopper and Python to use as tools to explore fabrication and possibly in developing interfaces.

<style type="text/css" media="screen">
  .gr_grid_container {
    /* customize grid container div here. eg: width: 500px; */
width: 800px;
  }

  .gr_grid_book_container {
    /* customize book cover container div here */
float: left;
width: 98px;
height: 160px;
padding: 0px 0px;
overflow: hidden;
  }
</style>

<div id="gr_grid_widget_1546431891">
  <!--- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->
<h2>Reading list</h2>
<div class="gr_grid_container">
<div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://images.gr-assets.com/books/1499718864m/34272565.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Forbidden Science: From Ancient Technologies to Free Energy" rel="nofollow" href="https://www.goodreads.com/book/show/2762643-forbidden-science"><img alt="Forbidden Science: From Ancient Technologies to Free Energy" border="0" src="https://images.gr-assets.com/books/1378705883m/2762643.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" rel="nofollow" href="https://www.goodreads.com/book/show/41257165-creating-a-universe-from-nothing"><img alt="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" border="0" src="https://images.gr-assets.com/books/1534744765m/41257165.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Wholeness and the Implicate Order" rel="nofollow" href="https://www.goodreads.com/book/show/204523.Wholeness_and_the_Implicate_Order"><img alt="Wholeness and the Implicate Order" border="0" src="https://images.gr-assets.com/books/1385055758m/204523.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Geometry of Meaning: Semantics Based on Conceptual Spaces" rel="nofollow" href="https://www.goodreads.com/book/show/18699199-geometry-of-meaning"><img alt="Geometry of Meaning: Semantics Based on Conceptual Spaces" border="0" src="https://images.gr-assets.com/books/1392025960m/18699199.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" rel="nofollow" href="https://www.goodreads.com/book/show/2374714.The_Geometry_of_Multiple_Images"><img alt="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" border="0" src="https://images.gr-assets.com/books/1347661899m/2374714.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Quantum Language and the Migration of Scientific Concepts: Quantum Physics, Nuclear Discourse, and the Cultural Migration of Scientific Concepts" rel="nofollow" href="https://www.goodreads.com/book/show/36722595-quantum-language-and-the-migration-of-scientific-concepts"><img alt="Quantum Language and the Migration of Scientific Concepts: Quantum Physics, Nuclear Discourse, and the Cultural Migration of Scientific Concepts" border="0" src="https://images.gr-assets.com/books/1541090340m/36722595.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="How Change Happens" rel="nofollow" href="https://www.goodreads.com/book/show/42068857-how-change-happens"><img alt="How Change Happens" border="0" src="https://images.gr-assets.com/books/1541664529m/42068857.jpg" /></a></div>
<br style="clear: both"/><br/>
</div>
</div>
<script =date_added&widget_id=1546431891" type="text/javascript" charset="utf-8"></script>


<!--   Symbolism and Geometry
Logos and Branding
In the age of mass production and consumerism being the driving force of major economies, ever more sophisticated marketing and branding use symbols to signify identity. Brands produce tribes of consumers.
Nike
Adidas
Louis Vuitton   -->


<!-- <font size="4" color="blue">Papers not yet accessed</font>


* 'A New Look at Gaia's Relationship with Water' - Peter Champoux (2017) Anthropology of consciousness. An exploration of the interconnectedness of the geometries of a water molecule, the geologic and geographic regions of our planet, and the universe as a whole. Water is shown as a crucial “bridge” passing from the microscopic to the stellar and interstellar.

* 'Cosmological and phenomenological transitions into how humans conceptualize and experience time' Nathalie Gontier (2018)  This paper focuses on delineating the phenomenologies associated with worldview turnovers. By identifying group-level dynamics, it demonstrates that time phenomenology extends individuals, and it demonstrates that paradigmatic shifts are driven by new observations, new jargon, and the invention of new quantitative methodologies and modelling techniques.
 -->

<!--
- Reflections after the research trip
- 2 posts per term (at midterm and at the end of the term), for term 2 and 3  -->

## Term 2 Opportunities and progress

<iframe
  src="https://embed.kumu.io/97088317aec82a6ca863bf45ad818981"
  width="940" height="600" frameborder="0"></iframe>
