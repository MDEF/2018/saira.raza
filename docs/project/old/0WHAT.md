<center><font size="7" color="blue">Reclaiming mass, energy and agency from the technosphere</font></center>

<center><font size="4" color="blue">A metadesign approach</font></center>
<center>![higgsblue](assets/e9liemox.png)</center>


## End of term presentation


### Video
<iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/QZDUkGVqxjw?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe src="https://sraza.kumu.io/reclaiming-mass-energy-and-agency-from-the-technosphere" width="1024" height="576" frameborder="0"></iframe>




### The overall approach

<iframe
  src="https://embed.kumu.io/d59fbd4a6111c939b6b6b3213c8b2b25"
  width="940" height="600" frameborder="0"></iframe>

<iframe   src="https://embed.kumu.io/ba0d3d9e2d0e25aed0fbe4fc5cfff96b"   width="940" height="600" frameborder="0"></iframe>

## Reflections and points of improvement of your project, based on the feedback

## Feedback
- Jordi Montaner
 - More focus is needed on how to present the ideas



- Xavi


- Ramon and Lucas
- Liz Corbin
- Oscar



## Intervention proposal for the next trimester (detailed planning of the actions that you will take in a 6 weeks period).







### The technosphere constrains us

"The technosphere, the interlinked set of communication, transportation, bureaucratic and other systems that act to metabolize fossil fuels and other energy resources, is considered to be an emerging global paradigm, with similarities to the lithosphere, atmosphere, hydrosphere and biosphere.

The technosphere is of global extent, exhibits large-scale appropriation of mass and energy resources, shows a tendency to co-opt for its own use information produced by the environment, and is autonomous. Unlike the older paradigms, the technosphere has not yet evolved the ability to recycle its own waste stream. Unless or until it does so, its status as a paradigm remains provisional.

 Humans are ‘parts’ of the technosphere – subcomponents essential for system function. Viewed from the inside by its human parts, the technosphere is perceived as a derived and controlled construct. Viewed from outside as a geological phenomenon, the technosphere appears as a quasi-autonomous system whose dynamics constrains the behaviour of its human parts.

 A geological perspective on technology suggests why strategies to limit environmental damage that consider only the needs of people are likely to fail without parallel consideration of the requirements of technology, especially its need for an abundant supply of energy.
A geological perspective on technology suggests why strategies to limit environmental damage that consider only the needs of people are likely to fail without parallel consideration of the requirements of technology, especially its need for an abundant supply of energy.

[P. K. Haff Technology as a geological phenomenon: implications for human well-being](http://pne.people.si.umich.edu/PDF/Haff%202013%20Technology%20as%20a%20Geological%20Phenomenon.pdf)


Technologies and institutions increase societies’ robustness to external and internal disturbances but also constitute path-dependencies and lock-ins that make large-scale changes difficult.

[Jonathan F Donges, Wolfgang Lucht, Finn Müller-Hansen (2017)The technosphere in Earth System analysis: A coevolutionary perspective](https://journals.sagepub.com/doi/full/10.1177/2053019616676608)

Latour The Geosphere and biosphere are not systematic
Latour’s (2015) critique of the ‘systems’ term in this context. In his view, ‘Gaia’ (the geosphere + biosphere) is not a system but itself a fragmented, highly dynamic and conflict-ridden entity: a system of conflicting systems, if you like. That means, we should not impose scientific standards of ‘systematicity’ on modelling the Earth system but should instead project the contingency and indeterminacy of the humanities and social sciences on modelling practice.  

 [John Hartleya and Carsten Herrmann-Pillath (2018)  Towards a semiotics of the technosphere](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)


 ### The technosphere needs to respect the material requirements of earth systems and social systems in order to be sustainable


 ### How are we addressing it (weak signals)


### A connected world changes the materiality of information and the types of information available to materialise.


### Symbolic effects of technology for the post-modern connected world


## Metadesign


### Embodied design

### Fab lab movement

### Sensors

https://technosphere-magazine.hkw.de/p/Sensing-Air-and-Generating-Worlds-of-Data-fL1Q5CVgGeJMDJmEdwCFr3



### Co creating with AI

NN for designing metamaterials

| Author     | Application | Shape data set | Simulation software | Training dataset size | Testing dataset size | Neural network and analysis used | Output format  | How do you converge the result?  |
| ------------- |-------------| -----|-----|-----|
|  [Tahersima et al](https://www.researchgate.net/publication/330862750_Deep_Neural_Network_Inverse_Design_of_Integrated_Photonic_Power_Splitters)     | Photonics | Randomly generated monochrome Pixel arrays |  | 20,000  | 4000 ||  |  |
| [Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin](https://www.nature.com/articles/s41598-018-19796-y)    |   (f > 30 GHz) using high impact polystyrene (HIPS) and a consumer grade 3D-printer based on fused deposition modeling for the fabrication. The material is chosen for its low cost and very low attenuation in the microwave to millimeter-wave region, with a loss-tangent measured to be tanδ<0.003 over the 26–38 GHz band. In this band, the real part of the dielectric constant of HIPS ε′≈2.3(n≈1.52)   |    |  |  |  | |  |  |
|  |      |     |  |  |  | |  |  |
|  |      |     |  |  |  | |  |  |
|  |      |     |  |  |  | |  |  |
|  |      |     |  |  |  | | |  ||


## Everyone as a designer


## Building new narratives


## Future

<iframe width="900" height="700" src="http://www.madore.org/~david/math/e8rotate.html" allowfullscreen></iframe>

### Bibliography

#### Technology and innovation evolution theory

[From tools to technosphere Bronislaw Szerszynski](https://www.academia.edu/38085338/From_tools_to_technosphere)

[Carsten Herrmann-Pillath (2017) Foundational Issues of ‘Technosphere Science’ – The Case for a New Scientific Discipline](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3034099)

[Technological change - Arie Rip & René Kemp (1997)](http://kemp.unu-merit.nl/Rip%20and%20Kemp.pdf)

[SPATIAL PARADIGM SHIFTS — FROM EUCLIDEAN GEOMETRY TO AR/MR/VR](https://medium.com/@amber_r_murray/spatial-paradigm-shifts-from-euclidean-geometry-to-ar-mr-vr-54252aa82d3a)

[SAHAL, D. (1981) , Patterns of Technological Innovation]

>Sahal (1985, p.64) argues that technological innovations can be: ‚structural innovations that arise from a process of differential growth; whereby the parts  and  the  whole  of  a  system  do  not  grow  at  the  same rate.  Second,  we  have what may  be  called  the material innovations that are necessitated  in an  attempt to meet  the  requisite  changes  in  the  criteria  of  technological  construction  as  a consequence  of  changes  in  the  scale  of  the  object.  Finally,  we  have  what  may  be called the systems innovations that arise from integration of two or more symbiotic technologies in an attempt to simplify the outline of the overall structure‛. This trilogy  can  generate the  emergence  of  various  techniques  including  revolutionary innovations  in  a  variety  of technological  and scientific fields(cf., Sahal,  1981; Coccia, 2016, 2016a)

[from Classification of innovation considering technological interaction](Available from: https://www.researchgate.net/publication/326518857_Classification_of_innovation_considering_technological_interaction)

[Is Technological Evolution Infinite or Finite?- From Quarksto Quasars for Futury 2013](https://futurism.com/is-technological-evolution-infinite-or-finite)

[Technological Innovation as an Evolutionary Process - Edited by John Ziman 92000)](https://pdfs.semanticscholar.org/1bb3/17a5487af7fbe69d274b0d0bead519af735a.pdf)

[Complexity and technological evolution: What everybody knows? Krist Vaesencorresponding and Wybo Houkes](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5842246/)

[Driving forces of technological change: The relation between population growth and technological innovation : Analysis of the optimal interaction across countries](https://www.researchgate.net/publication/260029656_Driving_forces_of_technological_change_The_relation_between_population_growth_and_technological_innovation_Analysis_of_the_optimal_interaction_across_countries)

[The technosphere in Earth System analysis: A coevolutionary perspective (2017) -Jonathan F Donges, Wolfgang Lucht,Finn Müller-Hansen1 and Will Steffen](https://journals.sagepub.com/doi/pdf/10.1177/2053019616676608)

[John Hartleya and Carsten Herrmann-Pillath (2018)  Towards a semiotics of the technosphere](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)

[Natalia Popkova - Controllability of Technosphere and Paradigm of Global Evolutionism](https://www.researchgate.net/publication/327782016_Controllability_of_Technosphere_and_Paradigm_of_Global_Evolutionism)

[Jiri Bila (2014) The syntheses of technological materials as emergences in complex systems](https://www.researchgate.net/publication/282716418_The_syntheses_of_technological_materials_as_emergences_in_complex_systems)


http://technosphere-magazine.hkw.de/

[DANIEL NILES and SANDER VAN DER LEEUW (2018) The Material Order -Do cultural ideas order our material worlds and technologies, or is it the other way around?](https://technosphere-magazine.hkw.de/p/The-Material-Order-4gK5EMpZ3SzB79aTePfJo7)


[Dubravka Cecez-Kecmanovic, Sebastian K. Boell, John Campbell (2014) Materiality of Connectivity in the Networked Society: A Sociomaterial Perspective](https://www.academia.edu/10418062/Materiality_of_Connectivity_in_the_Networked_Society_A_Sociomaterial_Perspective)



#### Design approaches and methods in the Anthropocene

[Nicky Gregson, Mike Crang (2010) Materiality and Waste: Inorganic Vitality in a Networked World](https://journals.sagepub.com/doi/10.1068/a43176)

https://www.anthropocene-curriculum.org/pages/root/campus-2014/slow-media/mapping-an-exercise-on-cartography/

[Donges, JF, Heitzig, J, Barfuss, W, Kassel, JA, Kittel, T, Kolb, JJ, Kolster, T, Müller-Hansen, F, Otto, IM, Wiedermann, M, Zimmerer, KB, Lucht, W (2018) Earth system modelling with complex dynamic human societies: the copan:CORE World-Earth modeling framework, Earth System Dynamics Discussions, in review](https://www.earth-syst-dynam-discuss.net/esd-2017-126/)

[Richard Přikryl, Ákos Török, Miguel Gomez-Heras, Karel Miskovsky, Magdalini Theodoridou (2016) Geomaterials in construction and their sustainability: Understanding their role in modern society](https://www.researchgate.net/publication/298070425_Geomaterials_in_construction_and_their_sustainability_Understanding_their_role_in_modern_society)

[Frank W.Geelsa Tim Schwanenb  (2017) Reducing energy demand through low carbon innovation: A sociotechnical transitions perspective and thirteen research debates](https://www.sciencedirect.com/science/article/pii/S2214629617303900)

[ELISA GIACCARDI (2003)- PRINCIPLES OF METADESIGN Processes and Levels of Co-Creation in the New Design Space](https://pearl.plymouth.ac.uk/bitstream/handle/10026.1/799/400424.pdf?sequence=4&isAllowed=y)

[Elisa Giaccardi and Gerhard Fischer (2008) Creativity and Evolution: A Metadesign Perspective](http://l3d.cs.colorado.edu/~gerhard/papers/ead06.pdf)

[Gerhard Fischer and Thomas Herrmann (2011) Socio-Technical Systems: A Meta-Design Perspective](https://www.researchgate.net/publication/220625827_Socio-Technical_Systems_A_Meta-Design_Perspective)

[Gerhard Fischer Elisa Giaccardi (2006)](Meta-design: A Framework for the Future of End-User Development)https://www.researchgate.net/publication/226719061_Meta-design_A_Framework_for_the_Future_of_End-User_Development

[Gerhard Fischer (2017) Revisiting and Broadening the Meta-Design Framework for End-User Development](https://www.researchgate.net/profile/Antonio_Piccinno/publication/316734680_Revisiting_and_Broadening_the_Meta-Design_Framework_for_End-User_Development/links/59e8b656458515c3631523ef/Revisiting-and-Broadening-the-Meta-Design-Framework-for-End-User-Development.pdf)

[John Wood (2018)RE-INVENTING INVENTING article Published in Design](https://sublimemagazine.com/re-inventing-inventing)

[Mathilda Tham  and Hannah Jones - Metadesign tools Designing the seeds for shared processes of change](https://www.academia.edu/15257979/Metadesign_Designing_the_Seeds_for_Shared_Processes_of_Change)

[Peter H. Jones(2014) Systemic Design Principles for Complex Social Systems In book: Social Systems and Design](https://www.researchgate.net/publication/329962720_Towards_a_semiotics_of_the_technosphere)

Alastair Fuad-Luke (2017) Design Activism: Beautiful Strangeness for a Sustainable World

Jonathan Chapman - Designers, Visionaries and Other Stories: A Collection of Sustainable Design ...

[Kenneth O. Stanley Joel Lehman Lisa Soros (2017) Open-endedness: The last grand challenge you’ve never heard of](https://www.oreilly.com/ideas/open-endedness-the-last-grand-challenge-youve-never-heard-of)

#### Tracking material flows

[Liz Corbin, Chris Monaghan, James Tooze, Eva Gladek (2019) Materials Democracy: An action plan for realising a redistributed materials economy](https://www.plymouthart.ac.uk/documents/Liz_Corbin%2C_Eva_Gladek%2C_Chris_Monaghan_and_James_Tooze.pdf)

[Pi-Cheng Chena et al (2017) An information system for sustainable materials management with material flow accounting and waste input–output analysis](https://www.sciencedirect.com/science/article/pii/S2468203916301662)

Heuristics Design and Optimization
http://www.mit.edu/~moshref/index.html

#### Semiotics

http://culturemachine.net/about/
Culture Machine is an international open-access journal of culture and theory, founded in 1999. Its aim is to be to cultural studies and cultural theory what ‘fundamental research’ is to the natural sciences: open-ended, non-goal orientated, exploratory and experimental. All contributions to the journal are peer-reviewed.



[Logan, Robert K. (2014) What is information?: Propagating organization in the biosphere, symbolosphere, technosphere and econosphere.](http://openresearch.ocadu.ca/id/eprint/397/1/Logan_2014_WhatIsInformation_DEMOPublishing.pdf)

[Marta Lenartowicz CREATURES OF THE SEMIOSPHERE A problematic third party in the ‘humans plus technology’ cognitive architecture of the future global superintelligence](https://www.researchgate.net/publication/305385122_Creatures_of_the_semiosphere_A_problematic_third_party_in_the_'humans_plus_technology'_cognitive_architecture_of_the_future_global_superintelligence)

Tim Lomas Positive Semiotics

Sergei Nirenburg  & Yorick Wilks What's in a symbol: ontology, representation and language Pages 9-23 (2010)

[Bronislaw Szerszynski 2016 Gods of the Anthropocene Geo-Spiritual Formations in the Earth’s New Epoch]()
 July

Nigel Clark Kathryn Yusoff (2017) Geosocial Formations and the Anthropocene

Anthropocene semiosis
Nigel Clark (not found)

[Matthew Holt (2017)]Semiotics and design: Towards an aesthetics of the artificial](https://www.tandfonline.com/doi/pdf/10.1080/14606925.2017.1352860?needAccess=true)


[Language and other artifacts: socio-cultural dynamics of niche construction Chris Sinha](https://www.frontiersin.org/articles/10.3389/fpsyg.2015.01601/full)



#### Narratives of technology

[Mark Coeckelbergh Narrative Technologies: A Philosophical Investigation of the Narrative Capacities of Technologies by Using Ricoeur’s Narrative Theory](https://link.springer.com/article/10.1007/s10746-016-9383-7)

http://www.technologystories.org/crafting-stories/

Donna Haraway, Anthropocene, Capitalocene, Cthulucene: Staying with the trouble

http://technosphere-magazine.hkw.de/

https://maetl.net/talks/stories-as-systems.pdf


#### Cultural tools

http://culturemachine.net/

#### Alligning software with hardware geometries and logic

To improve scalability, portability, and performance, but also simplify programming and expand applications.

[Aligning the representation and reality of computation with asynchronous logic automata Neil Gershenfeld](http://cba.mit.edu/docs/papers/11.12.Computing.pdf)



#### Inverse/ Generative design

SAWYER D. CAMPBELL, DAVID SELL,RONALD P. JENKINS, ERIC B.
WHITING, JONATHAN A. FAN,AND DOUGLAS H. WERNERReview of numerical optimization techniques
for meta-device design [Invited]
https://www.osapublishing.org/DirectPDFAccess/985F7A76-9A95-7221-4C87D41AEE2BB3BB_407576/ome-9-4-1842.pdf?da=1&id=407576&seq=0&mobile=no

[Generative Model for the Inverse Design of Metasurfaces Zhaocheng Liu, Dayu Zhu, Sean P. Rodrigues, Kyu-Tae Lee, and Wenshan Cai](https://pubs.acs.org/doi/abs/10.1021/acs.nanolett.8b03171)
https://arxiv.org/ftp/arxiv/papers/1805/1805.10181.pdf

[Kan Yao, Rohit Unni and Yuebing Zheng (2018) Intelligent nanophotonics: merging photonics and artificial intelligence at the nanoscale](https://www.degruyter.com/downloadpdf/j/nanoph.2019.8.issue-3/nanoph-2018-0183/nanoph-2018-0183.pdf)

[Inverse-designed metastructures that solve equations Nasim Mohammadi Estakhri, Brian Edwards, Nader Engheta]()

[Probabilistic representation and inverse design of metamaterials based on a deep generative model with semi-supervised learning strategy Wei Ma1, Feng Cheng2, Yihao Xu1, Qinlong Wen1 and Yongmin Liu](https://arxiv.org/ftp/arxiv/papers/1901/1901.10819.pdf)

[Deep Neural Network Inverse Design of Integrated Photonic Power Splitters - Mohammad H. Tahersima, Keisuke Kojima, Toshiaki Koike-Akino1, Devesh Jha1, Bingnan Wang, Chungwei Lin, Kieran Parsons1](https://www.researchgate.net/publication/330862750_Deep_Neural_Network_Inverse_Design_of_Integrated_Photonic_Power_Splitters)

[supplementary materials](file:///C:/Users/saira/Downloads/41598_2018_37952_MOESM1_ESM%20(1).pdf)

1. We use numerical simulation (Methods section) to generate labeled data for training the network. Lumerical’s FDTD simulation package to generate the training data. The data contains more than 20,000 numerical simulations, where each experiment is a 3D FDTD simulation com-posed of passive SOI waveguides and the beam splitter device. Initial random hole position matrix of the beam splitter was generated, exported, and manipulated using the MATLAB automation. Hole position generating script uses different algorithms (such as Direct Binary Search), initial conditions, and optimization metrics to sequentially create sufficiently large data set required for a robust neural network representation of the device structure. It took a desktop computer with a core-i7 CPU with 3.7 GHz clock speed and 64 GB RAM about two weeks to complete collecting the 20,000 simulation data.Dispersive refractive indices of silicon and silica from literature41 were used for all simulations for a broadband simulation in the range of 1.45–1.65 μm. The fundamental TE mode at 1550 nm was used at input source and TE mode output power was recorded for transmission and reflection. We note that TM mode output is below 10−5

2. We then feed the DNN with numerical optical experiments  and train a neural network able to represent the relationship between hole vectors and spectral response at each port. Each pixel can have a binary state of 1 for etched (n = nsilicon) and 0 for not etched (n = nsilica)
We use the open source machine learning framework of Tensorflow in python language to build and test our deep neural networks. The runtime to train the neural network model depends on network and training parameters including data size, hidden layer depth and width, batch size, and epoch numbers. The runtime to train the neural network model depends on network and training parameters including data size, hidden layer depth and width, batch size, and epoch numbers. For the representative network parameters, with hidden layer width of 100, hidden layer depth of 8, batch size of 100, epoch number of 10,000, and trained on 20,000 data, shown in Fig. 2d, it take 1337 seconds (~22 minutes) to train the model

Plasmonic nanostructure design and characterization via Deep Learning Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf & Haim Suchowski()



[Czech national supercomputing centre ](https://www.it4i.cz/supercomputing-projects/?lang=en)
A machine learning approach for the description of zeolites
Call: 14th Open Access Grant Competition
Researcher: Miroslav Rubeš
zeolites, which are used as detergents, catalysts, and adsorbents. In 2017, the value of the global zeolite market was about 30 billion US dollars. The objective of Miroslav Rubeš project is to use machine learning algorithms to create a model which enables deeper understanding of the phenomena occurring in zeolitic materials.

Optimization design of functional materials in a new type of lithium based battery Call: 12th Open Access Grant Competition
Researcher: Dominik Legut

[F. Callewaert, V. Velev, P. Kumar, A. V. Sahakian & K. Aydin (2018) Inverse-Designed Broadband All-Dielectric Electromagnetic Metadevices](https://www.nature.com/articles/s41598-018-19796-y)


### Modelling tools

https://alternativeto.net/software/comsol/?license=opensource

https://www.food4rhino.com/app/dodo#downloads_list

[openEMS is a free and open electromagnetic field solver using the FDTD method. Matlab or Octave are used as an easy and flexible scripting interface](http://openems.de/start/index.php)



### Metadevices

[Integrating microsystems with metamaterials towards metadevices Xiaoguang Zhao, Guangwu Duan, Aobo Li, Chunxu Chen & Xin Zhang(2019)](https://www.nature.com/articles/s41378-018-0042-1) - GREAT PAPER ABOUT ACTUATORS

### Antenna geometries

https://www.semanticscholar.org/paper/A-novel-wearable-metamaterial-Fractal-antenna-for-Ahmed-Ahmed/28376c472c5adde23ab0bb1d48374b4762b3b923


https://www.intechopen.com/books/metamaterials-and-metasurfaces/metamaterials-in-application-to-improve-antenna-parameters

[Wojciech Jan Krzysztofik (2017)Fractals in Antennas and Metamaterials Applications](https://www.researchgate.net/publication/317637523_Fractals_in_Antennas_and_Metamaterials_Applications)



[Rana Sadaf Anwar, Lingfeng Mao and Huansheng Ning (2018) Frequency Selective Surfaces: A Review](https://www.mdpi.com/2076-3417/8/9/1689/htm)

#### Applications of Frequency selective surfaces

Applications	Applications
Dynamically reconfigurable FSSs
Wearable sensing FSS
Tunable and software-defined FSSs
Multi-functional FSSs
Mechanically tunable FSS
Smart absorbers FSS
High performance tunable FSS Absorber
Frequency selective rasorbers
Doubly curved FSS surface
FSS for high-power radar applications
Integrated design of antenna-FSS system
Graphene based FSS
3D printed all-dielectric FSSs with wide BW
FSS for wireless power transfer applications
Metamaterial inspired FSS for satellite communication
FSS for earth observation remote sensing and satellites
FSS for selective shielding of frequencies in critical places such as airport and military communication.
FSS for isolating harmful & unwanted radiation in hospitals, schools, domestic environments at microwave low frequency bands (L, S)
Chipless passive structural strength monitoring FSS based wireless sensor
Switchable FSS based on plasma shells for aerodynamics & terrestrial EM systems protection



### Energy Harvesting with metamaterials

Acoustic metamaterial energy harvesting

[Mourad Oudich, Yong Li(2017 Tunable Sub-wavelength Acoustic Energy Harvesting with Metamaterial Plate](https://www.researchgate.net/publication/317375538_Tunable_Sub-wavelength_Acoustic_Energy_Harvesting_with_Metamaterial_Plate)


[Kyung Ho Sun, Jae Eun Kim, Jedo Kim and Kyungjun Song (2017) Sound energy harvesting using a doubly coiled-up acoustic metamaterial cavity](http://iopscience-iop-org-s.vpn.whu.edu.cn:9440/article/10.1088/1361-665X/aa724e/pdf)


[Badreddine Assouar, Mourad Oudich, Xiaoming Zhou (2015)](Sound insulation and energy harvesting based on acoustic metamaterial plate)


https://www.researchgate.net/publication/327905121_RADIO_FREQUENCY_RF_ENERGY_HARVESTING_USING_METAMATERIAL_STRUCTURE_FOR_ANTENNARECTENNA_COMMUNICATION_NETWORK_A_REVIEW



Infra red

[A Metamaterial Capable of Using Heat Loss to Produce Electricity](https://substance.etsmtl.ca/en/metamaterial-capable-using-heat-loss-produce-electricity)
Reconfigurable room temperature metamaterial
infrared emitter
XINYU LIU AND WILLIE J. PADILLA*
Department of Electrical and Computer Engineering, Duke University, Durham, North Carolina 27708, USA
*Corresponding author: willie.padilla@duke.edu
Received 2 December 2016;


Radio

[NORNIKMAN HASSAN et al (2018) RADIO FREQUENCY (RF) ENERGY HARVESTING USING METAMATERIAL STRUCTURE FOR ANTENNA/RECTENNA COMMUNICATION NETWORK: A REVIEW](https://www.researchgate.net/publication/327905121_RADIO_FREQUENCY_RF_ENERGY_HARVESTING_USING_METAMATERIAL_STRUCTURE_FOR_ANTENNARECTENNA_COMMUNICATION_NETWORK_A_REVIEW)



| Author | Type of energy harvested | Configuration of metamaterial | material used | type of output result |
| ------------- |-------------| -----|-----|-----|
|   [Zhou Z., W. Liao, Q. Zhang, F. Han, Y. Chen, A  Multi-Band  Fractal  Antenna  for  RF  Energy Harvesting, 2016](https://www.researchgate.net/publication/327905121_RADIO_FREQUENCY_RF_ENERGY_HARVESTING_USING_METAMATERIAL_STRUCTURE_FOR_ANTENNARECTENNA_COMMUNICATION_NETWORK_A_REVIEW).    | Radio |  | | |
| Shrestha S., S. R. Lee, and D.-Y. Choi, “A New Fractal-Based  Miniaturized  Dual  Band  Patch Antenna   for   RF   Energy   Harvesting”    |   Radio   |   | | |
|  |    |    | | |
|       |  |  | | |
|       |  |  | | |
|       |  |  | | |
|       |  |  | | |
|       |  |  | | |
|       |  |  | | |


### Acoustic metamaterials

ultra sound brick - sussex uni
https://phys.org/news/2017-02-sound-shaping-metamaterial.html



Chemical Sensors

[Ahmed Salim and Sungjoon Lim (2016) Complementary Split-Ring Resonator-Loaded Microfluidic Ethanol Chemical Sensor](https://www.mdpi.com/1424-8220/16/11/1802/htm)


SRRs
[Filiberto BilottiFiliberto BilottiAlessandro ToscanoAlessandro ToscanoLucio VegniShow all 6 authorsEkmel OzbayEkmel Ozbay Equivalent-Circuit Models for the Design of Metamaterials Based on Artificial Magnetic Inclusions (2008)




### Tech for refugees
jeanmichel.moleanaar@tdh.org

humanfablabs.org





### E8 Lie group narrative building

http://theoryofeverything.org/theToE/tags/e8/

<iframe width="560" height="315" src="https://www.youtube.com/embed/0P8dOWKQtLE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

https://www.thingiverse.com/thing:2313909

[J Gregory Moxness (2014) The 3D Visualization of E8 using an H4 Folding Matrix](http://vixra.org/pdf/1411.0130v1.pdf)

























---------------- old garbage


Define your area of intervention (position your project in a specific area of intervention, or in the intersection of different areas). For this, you will have to apply the work developed in the Atlas of Weak Signals seminar.

### Spheres of influence on humans
Part of a global ecosystem of living things
Part of a Political system that we feed into and that controlls us
Part of a material world that we sense
Part of a technological system that shows us things and lets us make things

Inputs
looking for signs
Sensing out environment
Finding - symbols, flags

Outputs

Doing
meta - unnatural
bio - unnatural

## Area of intervention
*Define your area of intervention (position your project in a specific area of intervention, or in the intersection of different areas).*

**Area of intervention:**

Making, DIY, low tech engineering, biohacking

Energy

capturing

### Mapping the context
 For this, you will have to apply the work developed in the Atlas of Weak Signals seminar.







## Design actions
Describe your design actions.

Growing diatoms
Energy Metadevices
machine learning to find shapes
periodic tables of shapes




Making metadevices as a way of leveraging digital fabrication
Inverse-design design methods [A Generative Model for Inverse Design of
Metamaterials - Liu etal](https://arxiv.org/ftp/arxiv/papers/1805/1805.10181.pdf)






## Speculate about the impact of your intervention in the future, taking 2050 as a reference year.




<center><font size="6" color="blue">Why?</font></center>

<center>We sense  much perceivable information from our environment through geometric forms. </center>
<center>Technology is enabling us to perceive new geometric representations and is also enabling us to construct geometric forms with greater ease than ever before. </center>

<center>What is the effect on us?</center>
<center>What should we make?</center>


<center>The bits to atoms revolution is on the horizon and the time to influence the paths of these technologies is now</center>

> <center> “The best time to shape the destiny of transformative, accelerating technologies is early, before changes have become both widespread and entrenched…. The benefits and risks of accelerating technologies are very real, with deep impacts on many lives, but we have the agency, individually and collectively, to shape these impacts now “</center>

  *- Neil Gershenfeld, Alan Gershenfeld and Joel Cutcher-Gershenfeld, Designing Reality*

<center>How can we..</center>

<center>
* Use digital fabrication to work contemporary geometric information between sensing, imagining and making today?</center>

<center>
* Make things in a Fab Lab to express our needs and identities in the 21st century? </center>

<center>
* Inquire into our future relationships with bits and atoms given that they are both bound by geometry? </center>




 <center><font size="6" color="blue">How?</font></center>

**Mapping** of research, technologies and tools of inquiry

**Joining the dots** between research into information processing using Geometry, tools of inquiry from the MDEF course and futures of interest.

**Design activism** by exploring what open systems, accessibility, inclusion and representation could look like in emergent futures.

**Prototyping** ways to:
  - Digitally fabricate metamaterials
  - Mimic biological wave bending shapes (diatom frustules and structural colour).
  - Make intuitive user interfaces to switch easily between seeing (or sensing), drawing (or designing) and fabricating.

**Story telling** about experiencing and using information through geometry using video, audio, immersive experiences and diegetic devices.


 <center><font size="5" color="blue">Approach</font></center>

The theory behind interventions in the MDEF course is that our environment influences and motivates us to do things and that these interventions can have a wider scales of influence over time.

![influence graph](assets/graph.png)

 <center><font size="5" color="blue">Motivations</font></center>

The motivation for this project is the growing availability of imaging and fabrication technologies which are bound together by geometry - as our senses are bound to the material world.

## Term 1

### The journey to the project

![journey](wa/assets/w12/journey2.jpg)

<center><font size="5" color="lime">Desk Research</font></center>

<iframe
  src="https://embed.kumu.io/e16cad7dfe00f63ee706c8524a417fc7"
  width="940" height="600" frameborder="0"></iframe>



<font size="5" color="lime">Geometric narratives from Term 1</font>

**Multiplicity**
Quantum concept of many different states existing at the same time.
Transdisciplinary approaches to projects.
Multiple identities and modes of working.
Simultaneous actions.
Inclusive and open systems of governance and design.
Subjectivity.

**Multi-scalar patterns**
Hierarchical structures.
Similarity within fractals

**Chaos**
Systemic thinking. Determinism and agency to influence chaotic, dynamic systems. The myth of equilibrium and defining sustainability.

**Finding coherence in noise**
Neural Networks, iterations through parametrics, computer processing, Machine Learning and material driven design.


<font size="5" color="lime">Possible material interventions through Fab Academy in Term 2</font>


<p><font size="3" color="lime">Geometric Interfaces / Controllers to enable feedback, iterations and "working" information like a material</font></p>

- 3d voxel design software to design metamaterials
- sound interfaces that help you sense sound through touch
- physical controller for grasshopper to modify electronic and maybe acoustic sounds
- using representations of fields and fluids as information interfaces

<font size="3" color="lime">Experiences</font>

* Acoustic
* Immersive narratives

<font size="3" color="lime">Transformable materials</font>

* Soft robotics https://softroboticstoolkit.com/
* Metasurfaces with purpose - **energy transfer surfaces**

<font size="3" color="lime">Emergency ready tools</font>

* For water purification, extraction or management
* For communication - wifi, radio
* For innovation - apps and mobile tools

<font size="3" color="lime">Diatom frustules for energy transfer and capture</font>

Working with partners to experiment with diatoms in

* Photovoltaics
* Batteries
* Water purification
* Sensors
* Carbon capture strategies

<center><font size="5" color="cyan">Imagined futures of interventions</font></center>



![Voros section](wa/assets/w10/futurescone-voros.png)

 | Future  |   Scenario  |
 | ------------- |:-------------|
 | Possible |  War, famine, water and fuel shortages |
 | Plausible | Political instability, greater inequality |  
 | Probable |  Climate change, population growth, climate change refugees  |
 | Preferable |  Less reliance on fossil fuels, more rights-based decision making. Buy in from major world leaders in paradigm change regarding wealth distribution. More inclusive education and governance systems. More representation. |
 | Premeditated | Costs of electronic equipment being driven down, greater processing power |
 | Preposterous | Alien landing, posthuman worlds,  |
 | Wildcard |  Asteroid, postcolonial futures |   |




### End of Term 1 reflections on feedback

<font size="3" color="blue">Developing an opensource resource</font>

Tomas was interested in how the findings of my research is currently hosted on opensource platforms and that this could be an interesting output of the project.
I will continue to make my findings public as works in progress as this is a central part of exploring opensource and fab labs.

<font size="3" color="blue"> Interfaces</font>

I was advised by  that the investigation should include interfaces as well as fabrication methods. This is a rich area of research and an important observation in how technology has changed our relationships with shapes. User interfaces are exercises in using geometry as tools as well a gateways to experiences in many facets of life these days.
I was reminded of how the visual language of the internet has changed.

Veronica mentioned the use of "metasurfaces" as user interfaces and there seems to be a tie in to this with transformable metamaterials. There may be an opportunity to collaborate on fabricating a metasurface during the next term.

<font size="3" color="blue"> Narrative</font>

I was advised by Guillem to look at a talk by Neil Gershenfeld because he has a well informed narrative on the topic of fabrication. This has been really helpful in framing the significance of geometry in the transfer of information to materiality.

<font size="3" color="blue"> DNA Origami</font>

Nuria gave me a tip of a field called DNA Origami. This is an excellent example how geometry's coded rules can be exploited.

<font size="3" color="blue"> Parametric software</font>

Ollie suggested that I might find exploring this topic using Grasshoper useful as it enables easy experimentation with parameters. When I tried this addon I found an excellent interface that allowed the manipulation of geometry using the analogy of a machine with inputs and outputs.


### Reading list

<style type="text/css" media="screen">
       .gr_grid_container {
         /* customize grid container div here. eg: width: 500px; */
       }

       .gr_grid_book_container {
         /* customize book cover container div here */
         float: left;
         width: 98px;
         height: 160px;
         padding: 0px 0px;
         overflow: hidden;
       }
     </style>
<div id="gr_grid_widget_1553007354">
<!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->

 <div class="gr_grid_container">
   <div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://images.gr-assets.com/books/1499718864m/34272565.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Forbidden Science: From Ancient Technologies to Free Energy" rel="nofollow" href="https://www.goodreads.com/book/show/2762643-forbidden-science"><img alt="Forbidden Science: From Ancient Technologies to Free Energy" border="0" src="https://images.gr-assets.com/books/1378705883m/2762643.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" rel="nofollow" href="https://www.goodreads.com/book/show/41257165-creating-a-universe-from-nothing"><img alt="Creating a Universe From Nothing: Integrating Sacred Geometry & Modern Science" border="0" src="https://images.gr-assets.com/books/1534744765m/41257165.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Wholeness and the Implicate Order" rel="nofollow" href="https://www.goodreads.com/book/show/204523.Wholeness_and_the_Implicate_Order"><img alt="Wholeness and the Implicate Order" border="0" src="https://images.gr-assets.com/books/1385055758m/204523.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Geometry of Meaning: Semantics Based on Conceptual Spaces" rel="nofollow" href="https://www.goodreads.com/book/show/18699199-geometry-of-meaning"><img alt="Geometry of Meaning: Semantics Based on Conceptual Spaces" border="0" src="https://images.gr-assets.com/books/1392025960m/18699199.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" rel="nofollow" href="https://www.goodreads.com/book/show/2374714.The_Geometry_of_Multiple_Images"><img alt="The Geometry of Multiple Images: The Laws That Govern the Formation of Multiple Images of a Scene and Some of Their Applications" border="0" src="https://images.gr-assets.com/books/1347661899m/2374714.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Quantum Language and the Migration of Scientific Concepts" rel="nofollow" href="https://www.goodreads.com/book/show/36722595-quantum-language-and-the-migration-of-scientific-concepts"><img alt="Quantum Language and the Migration of Scientific Concepts" border="0" src="https://images.gr-assets.com/books/1541090340m/36722595.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="How Change Happens" rel="nofollow" href="https://www.goodreads.com/book/show/42068857-how-change-happens"><img alt="How Change Happens" border="0" src="https://images.gr-assets.com/books/1541664529m/42068857.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" rel="nofollow" href="https://www.goodreads.com/book/show/34523271-designing-reality"><img alt="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" border="0" src="https://images.gr-assets.com/books/1493825166m/34523271.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="Media, Materiality and Memory: Grounding the Groove" rel="nofollow" href="https://www.goodreads.com/book/show/29513395-media-materiality-and-memory"><img alt="Media, Materiality and Memory: Grounding the Groove" border="0" src="https://images.gr-assets.com/books/1495698688m/29513395.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" rel="nofollow" href="https://www.goodreads.com/book/show/17239972-how-matter-matters"><img alt="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" border="0" src="https://images.gr-assets.com/books/1363584198m/17239972.jpg" /></a></div>
   <div class="gr_grid_book_container"><a title="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" rel="nofollow" href="https://www.goodreads.com/book/show/12923329-the-future-of-post-human-geometry"><img alt="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" border="0" src="https://images.gr-assets.com/books/1348319287m/12923329.jpg" /></a></div>
   <br style="clear: both"/><br/><a class="gr_grid_branding" style="font-size: .9em; color: #382110; text-decoration: none; float: right; clear: both" rel="nofollow" href="https://www.goodreads.com/user/show/89166601-saira-rrza"></a>
 </div>
</div>





## Term 2

<center><font size="5" color="magenta">Experimenting with fabricating geometries in the self effecting stage</font></center>
<center>![Graph](assets/firstperson.png)</center>


In term 2 I started personal interventions in familiarising myself with digital fabrication tools to explore my own relationship to the technology and materials and resulting geometries and the way they interact with the environment.

<center><font size="5" color="blue">Term 2 Stimulus</font></center>

I took a multi-start approach to incorporate my research interest in geometry, the futures of my political interests and the tools I will use on the MDEF course this term.

There are many possible intersections of these stimuli so I am "seeding" interventions in parallel depending on what the MDEF course is focussing on each week.

I hope that some interventions will grow as more  opportunities, interest and skills arise and that interventions will crystallise with direction from our practice in the Atlas of Weak Signals modules.

<center>![Project Stimulus](assets/projectplan.jpg)</center>

I have been taking an embodied "first person" design approach towards an intervention, using new fabrication skills as starting points rather than starting from a future scenario. This has led to a better understanding of the workflows, language, materials and time needed to fabricate simple things.

<center>![Interventions](assets/updatedactivities.jpg)</center>


<!-- Write your comments here –and the comment closes with  
I have been documenting my progress between ideas and completed experiments below. I am looking for a way to make a layered representation of these project paths.
<iframe
  src="https://embed.kumu.io/75f2505a4871b929126a619d20251b7f"
  width="940" height="600" frameborder="0"></iframe>   -->

<p><font size="4" color="magenta">Cutting transformable metamaterials</font></p>
I have been looking at the work of Bas Overvelde who has defined mathematically how to design modules for transformable metamaterials.
I tried vinyl cutting one of the module he has designed and learned that these shapes require a lot of material as they have high surface areas.

<p><font size="4" color="magenta">3D printing acoustic metamaterials metamaterials</font></p>

<p><font size="3" color="magenta">Labyrinthine configuration</font></p>

I attempted to replicate to scale [a labyrinthine shaped 'double-negative' acoustic metamaterial unit designed by Dong et al](https://arxiv.org/ftp/arxiv/papers/1811/1811.03228.pdf).
This configuration was easy to draw and 3D print however I found with acoustic metamaterials consideration always needs to made to how it should be installed in relation to the sound source and listener. An application is necessary to see how well it works.

<p><font size="3" color="magenta">Metasurface acoustic tunnel</font></p>

I replicated this [acoustic one-way open tunnel by using metasurface by Zhu et al](https://www.researchgate.net/publication/283005608_Acoustic_one-way_open_tunnel_by_using_metasurface). This turned out to be an inefficient use of materials and time as the dimensions were so big. I modelled a 3d version of this tunnel to see if They could be used as eaf muffs. They turned out to be very large and would take a lot of materials and time to 3D print this. I am thinking about alternative construction methods.


<p><font size="4" color="magenta">Trying to cultivate live diatoms</font></p>

In my Material Driven Design project I tried to cultivate live diatoms from the sea and from an aquarium. It was difficult to do this without a powerful microscope or a photobioreactor so I realised that this was not a "material" I could work with in the timeframe of the project.

<p><font size="4" color="magenta">Working with fossilised diatoms</font></center></p>

I tried to work with fossilised diatoms to see if this was a material I could do anything with using the equipment available to us. Diatomaceous earth is often used as a filter or powder to contain other substances. I was able to incorporate it into some bioplastics which achieved some water resistance.

I also tried to see if I could combine titanium oxide and / or aloe vera with diatomaceous earth to see if I could use it as the semiconductor layer in a dye sensitised solar cell.


### Term 2 Mid-term Reflections

#### Sensual mathematics
Experimenting through timed tasks is testing my limits of making and sensing (and to a lesser extent reconfiguring information) through geometry. I have been thinking about how **through geometry, mathematics transforms information into dimensions we can sense - allowing us to experience information that may not be otherwise  experienced**. User interfaces can use geometry to make information "workable" through feedback loops to our senses. This leads me to want to explore ways to connect our senses to immaterial information (thoughts, emotions) or to unusual experiences of information (across scales and movements).

>"..it is certain that mathematics generally, and particularly geometry, owes its existence to the need which was felt of learning something about the relations of real things to one another. "

*Albert Einstein, from [a lecture on 'Geometry and Experience' (1921) at the Prussian Academy of Sciences in Berlin](http://www-history.mcs.st-andrews.ac.uk/Extras/Einstein_geometry.html).*

#### A framework for interventions - What geometry lets us do
I have been reflecting further on the qualities of geometry that we leverage to help us work with information:

1. **Sensual** - as I mentioned before - geometry is sensual mathematics. Vision sound and touch are geometric effects. These three senses let us physically sense information and perceive things in 3 dimensions and can help us conceptualise more. Diagrams, Imaging and mapping tools and acoustic devices like speakers and musical instruments are extensions of our senses. But even before we apply any meaning or use to Geometric effects, these give us raw subjective qualitative experiences.

2. **Malleable** - geometry is something we can control through mathematics and through our bodies (dancing, drawing, speaking, designing). We have tools that helps us to draw and manipulate shapes. This helps us to be expressive, playful and to solve problems through our senses.

3. **Translatable and Repeatable** - Maths is translatable across systems and repeatable with the same results - allowing us to make things out of matter, scale them up, multiply them and use the information held by shapes to do different things.

  > "With a lego brick you don't need a ruler to place it, the block has geometry, it corrects errors and you don't put Lego in the trash, you take the bricks apart. The reason you can do this is the bricks contain information.”

*Neil Gershenfeld*

I used this reflection on how geometry lets us use information as a framework or at least inspiration for my experiments throughout this project.

<center>![Project Stimulus](assets/experimenttools.jpg)</center>


### Presentation





### Reflection on feedback from mid-term review


#### Defining the intervention into one intervention

I had hoped that I could work with geometry in a material driven approach - using the information contained in it to let it guide an intervention - this also fit well with the narrative of the digital fabrication revolution - where bits are becoming more and more workable into atoms as bits are workable today into other bits.
The embodied design approach enabled me to test the limitations of things I could make in the time available with the tools. I was able to learn how to use my laptop to draw, reconfigure, scale and format 3 dimensional drawings that the fabricating tools could make.

The review made me revisit the different dimensions of what might constitute an intervention I realised that many things needed addressing that we have touched upon including:

* Political focus
* Scales of influence of interventions over time
* Futures of interest
* Weak signals of trends (new perspectives and tools coming this term)

**Political focus**
My political focus in a broad sense is to include more people into the development of technology to secure their basic needs in a sustainable way.

As someone who is learning these new tools myself, I feel I should start by designing for myself, then design for someone else using data from them and then share the result for people to adapt as they need. We learned about such an approach in term 1.

My desk research suggests to me that geometry mediates not only matter (atoms) but also waves (material or not) all of which have energy. I believe that matter and geometry is most useful to us in its ability to capture, transfer and transform energy.

I thought what was interesting about transformable metasurfaces is that they enable tuning of energy transfer processes and transformable 3D metamaterials could help to tune filtering processes as well as wave interaction.

**Scales of influence**
Geometry is already a part of anything material at several scales - one of the reasons I chose this topic was its scalability. With metamaterials especially, it would be possible to scale them up or down for different uses. It is time now to focus on a use with a purpose.

**Consider waste implications in fabrication**
This was an excellent point about my approach to the project. It is wasteful. Perhaps addressing waste during the experimentation could form part of the question.

**Exploring future contexts**
Futures that interest me include preposterous futures that are a complete departure from the culture and landscape we have today.  
These present an opportunity to reframe current narratives about underrepresented points of view such as women, children, refugees, people with disabilities, future generations and non-humans - although these must be authentic.

### So what IS it?

I have learned that metamaterials can:

- Have transformable surface areas which have implications on energy transfer processes.
 - Voids - which have applications for their effects on fluids and waves carried by fluids. Voids can also act as filters or barriers at various scales  to separate molecules and particles (as membranes and filters)

These qualities of exchange and capture in the material world is what I think I should leverage.

My intervention:

* seeks to use geometry to capture, transfer or transform matter and energy.
* working towards a future context of the Anthropocene
* will focus on renewable energy and water capture and treament.

### What next in term 2

Tools
Focus on making geometries transform.

* Develop narrative of geometries of transformation and capture - open and closed surfaces. See if acoustics plays any useful part in this narrative.

* Use fab academy techniques to find ways to control transformable metamaterials - more emphasis on electronics, control and programming.

Simulation
* Exploring simulation software for fields and fluids acoustics:
    * shttps://www.code-aster.org/spip.php?rubrique2

Futures
* Research futures of machine learning to find capture molecules.
* Cultural impacts


* If time permits look at applications field (energy, probability) and fluid (movements of matter) geometries to understand information

* cultural interventions


> Contemporary society seems to be losing its solidity:
its organizations are becoming ductile and the forms of
life within it are becoming fluid, every project tends to be
flexible and every choice reversible. Or at least that’s what
we like to think.
It follows that the best metaphors to describe it come
from the physics of fluids rather than of solids, more from
living systems than from mineral ones.

*- Ezio Manzini*




<center><font size="5" color="magenta">Home effecting stage</font></center>



<center><font size="5" color="magenta">Neighbourhood effecting stage</font></center>

<center><font size="5" color="magenta">City effecting stage</font></center>

<center><font size="5" color="magenta">Country effecting stage</font></center>

<center><font size="5" color="magenta">Planet effecting stage</font></center>
## References

[Prototyping Interactive Nonlinear Nano-to-Micro Scaled Material Properties and Effects at the Human Scale - Jenny E. Sabin, Andrew Lucia1, Giffen Ott1, Simin Wang]

[Fabricating structural Colour](https://www.nature.com/articles/s41598-017-17914-w)

[Activities and findings of the MIT Center for Bits and Atoms, 2008](http://cba.mit.edu/docs/reports/08.06.NSF/index.html)

[New materiality: Digital fabrication and open form. Notes on the arbitrariness of architectural form and parametric design. Carlos L. Marcos (2011)](http://www.improve2011.it/Full_Paper/41.pdf)
