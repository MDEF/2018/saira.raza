# <center>Metamaterials</center>

## <center>Wave Interaction</center>

In the 1960s 'negative refractive index' was proposed - could we get light to bend the opposite way to how it does in nature?

The structure of materials interact with waves that travel through materials (like air and water) and electromagnetic wave radiation (that doesn't a need material).
Perfect lenses were proposed in the late 1990s and Sir John Pendry demonstrated this in early 2000 using a metamaterial - structures much smaller than the wavelengths of light - in the world of **nanotech**.


### <center>Acoustic metamaterials</center>

**University of Sussex's Interact Lab** looked into:

* a [3D printable modular acoustic metamaterial building system in 2017](http://www.sussex.ac.uk/broadcast/read/39354)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/BcVxRyvipcU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* They also looked into dynamic acoustic control using metamaterials and transducers.

  <iframe width="560" height="315" src="https://www.youtube.com/embed/zixzyeF25SY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


[**Disney Research Labs**  and **Columbia University** have been trying to incorporate Modular Acoustic Filters using Acoustic Voxels into everyday objects](http://www.cs.columbia.edu/cg/lego/)  (https://www.disneyresearch.com/publication/acoustic-voxels/)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/7JbN9vXxGYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**Steve Cummer's team at Duke University** looked into Acoustic Holograms in 2016:
 ["it should be possible to manufacture entire 3D printed fields of sound that don’t rely on a transducer array controlled by electronics, but rather a grouping of cells that work together to control the acoustic waves."](https://3dprint.com/154311/duke-3dp-acoustic-holograms/)

* ['Acoustic Holographic Rendering with Two-dimensional Metamaterial-based Passive Phased Array' - Yangbo Xie, Chen Shen, Wenqi Wang, Junfei Li, Dingjie Suo, Bogdan-Ioan Popa, Yun Jing, and Steven Cummer (2016)](https://arxiv.org/ftp/arxiv/papers/1607/1607.06014.pdf)

* The printed material is acrylonitrile butadiene styrene plastic with density of 1180 kg/m3  ['Systematic design and experimental demonstration of bianisotropic metasurfaces for scattering-free manipulation of acoustic wavefronts' Junfei Li, Chen Shen, Ana Díaz-Rubio, Sergei A. Tretyakov & Steven A. Cummer (2018) Nature Communications](https://www.nature.com/articles/s41467-018-03778-9)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/pNsnvRHNfho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This year a team at the **University of Strathclyde** tried using 'A Helmholtz resonator' configuration in their acoustic metamaterial. A Helmholtz oscillator is a container of gas (usually air) with an open hole (or neck or port). A volume of air in and near the open hole vibrates because of the 'springiness' of the air inside - like a glass bottle when you blow across it. The team concluded that this type of acoustic metamaterials for sound control could be integrated in electroacoustic devices such as headphones and hearing aids. **"In-ear headphones** could be 3D printed with a shape corresponding to the listener external ear".

* ['Enhancing the Sound Absorption of Small-Scale 3D Printed Acoustic Metamaterials Based on Helmholtz Resonators' Cecilia Casarini,  Ben Tiller, Carmelo Mineo, Charles N. MacLeod, James F.C. Windmill and Joseph C. Jackson (2018)](https://pureportal.strath.ac.uk/files-asset/82476858/Casarini_etal_IEEE_Sensors_2018_Enhancing_the_sound_absorption_of_small_scale_3D_printed_acoustic_metamaterials.pdf)




### <center>Magnetically controlled metamaterial shapes</center>

MIT 2018
<iframe width="560" height="315" src="https://www.youtube.com/embed/MUt1YKtn6kM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This year, a team led by **USC Viterbi** developed 3-D printed acoustic metamaterials that can be switched on and off remotely using a magnetic field](https://viterbischool.usc.edu/news/2018/04/3-d-printed-active-metamaterials-for-sound-and-vibration-control/) https://www.3dnatives.com/en/metamaterials-3d-printed180420184/

* ['Magnetoactive Acoustic Metamaterials' Kunhao Yu  Nicholas X. Fang  Guoliang Huang  Qiming Wang (2018) ](https://onlinelibrary.wiley.com/doi/abs/10.1002/adma.201706348)


<iframe width="560" height="315" src="https://www.youtube.com/embed/aV07hCF7-AQ?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Also this year, University of California looked into ReTunable Field responsive mechanical metamaterials (FRMMs) made of polymeric tubes filled with 'magnetorheological fluid'

* ['Field responsive mechanical metamaterials'- Julie A. Jackson et al (2018)](http://advances.sciencemag.org/content/4/12/eaau6419)

<iframe width="560" height="315" src="https://www.youtube.com/embed/iNaMxIad7Io" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
