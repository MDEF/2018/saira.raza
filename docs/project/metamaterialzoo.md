
<head>

<style>
<link href="https://fonts.googleapis.com/css?family=Archivo+Black&display=swap" rel="stylesheet">

@import url('https://fonts.googleapis.com/css?family=Archivo+Black&display=swap');

body {
font-family: monospace;
font-size: 9px;
background-color: #eee;
margin: 10px;
}

.box {
width: 400px;
float: left;
padding: 0px 40px 80px 0px;
}

.screenshot {
width: 400px;
overflow: hidden;
}

</style>
</head>

<body>
**<font face color="blue" size="5"><center>[░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)</center></font>**
<center><font size="3" color="blue">

[𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦](https://www.canva.com/design/DADigm4jHRo/bLpM2zB1z_YlkhEUQsGbOQ/view?website#4)
</font></center>
<br>

# Metamaterial Zoo

<div class="box">
<font size="4" color="blue">This project was inspired by intriguing illustrations shared by researcher on metamaterials and the exceptional way they interact with waves or with forces. This page assembles a collection of images and animations demonstrating some of the structures, media and effects of Metamaterials.</font>

</div>

<div class="box">
<p>
# REPOSITORY OF DIAGRAMS
<br/>
–<br/>
In its ruggedness and lack of concern to look comfortable or easy, Brutalism can be seen as a reaction by a younger generation to the lightness, optimism, and frivolity of today's web design.<br/>
–<br/>
<a href="">Submit</a></p>
</div>

<font face="Archivo Black" color="blue" size="5">ACOUSTIC</font>


<div class="box"><div class="screenshot"><img src="https://media.nature.com/lw926/nature-assets/ncomms/2017/170227/ncomms14608/images/ncomms14608-f3.jpg" style="width:400px;"/></div>
<p>


<p> **Fig 1: Metamaterial bricks** Building blocks for making quantal meta-surfaces</p>
<p>
Metamaterial bricks and quantization of meta-surfaces
<a href="https://www.nature.com/articles/ncomms14608">Gianluca Memoli, Mihai Caleap, Michihiro Asakawa, Deepak R. Sahoo, Bruce W. Drinkwater & Sriram Subramanian (2017)</a></p>
</div>

<br>

<div class="box"><div class="screenshot">
<img src="https://www.researchgate.net/profile/Spyros_Polychronopoulos/publication/328328868/figure/fig3/AS:682545658216449@1539742974873/Metamaterial-3D-Printing-Model-a-2D-drawing-of-encoded-metamaterial-bricks-b-3D_W640.jpg" style="width:400px;"/></div>

<p>
<br/>
**Fig 2:  Metamaterial 3D Printed Sound bender device designed using metamaterial bricks**
<br/>
(a) cross section; (b) 3D Sliced view of metamaterial model after revolving cross section; (c) 3D printed metamaterial.<br/>
<br/>
</p>
</div>

<br>
<div class="box"><div class="screenshot"><img src="https://www.seas.harvard.edu/sites/default/files/FocusingLensGifforWeb_0.gif" style="width:400px;"/></div><p>**Fig 4: ** Light passing through the meta-lens is focused by millions of nano structures (Capasso Lab)</p><p><a href="https://www.seas.harvard.edu/news/2016/06/metalens-works-in-visible-spectrum-sees-smaller-than-wavelength-of-light">Mohammadreza Khorasaninejad,Wei Ting Chen, Robert C. Devlin1, Jaewon Oh, Alexander Y. Zhu, Federico Capasso (2016)</a>Metalenses at visible wavelengths: Diffraction-limited focusing and subwavelength resolution imaging</p></p></div>
<div class="box"><div class="screenshot"><img src="https://www.researchgate.net/profile/Spyros_Polychronopoulos/publication/328328868/figure/fig2/AS:682545658220544@1539742974836/Summary-of-method-a-We-identify-a-few-points-around-the-prop-and-compute-a_W640.jpg" style="width:400px;"/></div><p>**Fig 3:  Designing method of sound bender device**<br>Summary of method (a) We identify a few points around the prop and compute a curve/spline;(b) we compute the phases producing a self-bending beam from this spline; (c) we discretize phases using a reduced set; (d) final metamaterial and setup.</p><p>SoundBender: Dynamic Acoustic Control Behind Obstacles<br><a href="https://www.researchgate.net/publication/328328868_SoundBender_Dynamic_Acoustic_Control_Behind_Obstacles/figures?lo=1&utm_source=google&utm_medium=organic">Mohd Adili, Diego Martínez, Spyros Polychronopoulos, Sriram Subramanian et al (2018)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://media.nature.com/m685/nature-assets/lsa/journal/v3/n10/images/lsa201499f1.jpg" style="width:400px;"/></div><p>**Fig 4:  Digital metamaterials that you can code**<br>The 1-bit digital metasurface and coding metasurface. (a) The 1-bit digital metasurface is composed of only two types of elements: ‘0’ and ‘1’. (b) A square metallic patch unit structure (inset) to realize the ‘0’ and ‘1’ elements and the corresponding phase responses in a range of frequencies. (c, d) Two 1-bit periodic coding metasurfaces to control the scattering of beams by designing the coding sequences of ‘0’ and ‘1’ elements: (c) the 010101…/010101... code and (d) 010101…/101010... code.</p><p>Coding metamaterials, digital metamaterials and programmable metamaterials <br><a href="https://www.nature.com/articles/lsa201499">Tie Jun Cui, Mei Qing Qi, Xiang Wan, Jie Zhao & Qiang Cheng (2014)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://media.nature.com/m685/nature-assets/lsa/journal/v5/n11/images/lsa2016172f4.jpg" style="width:400px;"/></div><p>**Fig 5:  Coding metasurfaces can enhance the transmission of information, and the information can be controlled by the coding patterns. **<br>The far-field patterns are demonstrated in Figure 4a–4c(ii–iii). As the number of iterations increases, from Figure 4a–4c, we note that the coding pattern becomes more random, the 2D polar far-field pattern becomes more diffuse and the 3D far-field pattern has increasing amounts of radiation or scattering beams. </p><p>Information entropy of coding metasurface<br><a href="https://www.nature.com/articles/lsa2016172">Tie-Jun Cui, Shuo Liu & Lian-Lin Li (2016)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://media.springernature.com/full/nature-static/assets/v1/image-assets/srep41000-f1.jpg" style="width:400px;"/></div><p>**Fig 6:  Tunable Thermal Metamaterials**<br>Scheme of tunable multifunctional thermal metamaterials.</p><p>Manipulation of Local Heat Flux via Assembly of Unit-Cell Thermal Shifters<br><a href="https://www.nature.com/articles/srep41000">Gwanwoo Park, Sunggu Kang, Howon Lee & Wonjoon Choi (2017)</a></p></div><br>
<div class="box"><div class="screenshot"><img src="https://media.springernature.com/full/nature-static/assets/v1/image-assets/ncomms14325-f1.jpg" style="width:400px;"/></div><p>**Fig 7:  Liquid-interface metamaterial. **<br>By dynamically shaping a fluid interface using rotating waves a 2D material with prescribed transport properties can be produced(a,b) Schematics of the experimental set-up for the controlled superposition of two orthogonal standing waves in a fluid tank. Waves are created using two computer controlled electrodynamic shakers. (c) A photo of the laboratory set-up showing the time-averaged streaks of drifting imaging particles. (d) Zoom into spatially resolved, small-scale particle drifting orbits.</p><p>Wave-based liquid-interface metamaterials<br><a href="https://www.nature.com/articles/ncomms14325">N Francois, H Xia, H Punzmann, P W Fontana & M Shats</a></p></div><br>




</body>
