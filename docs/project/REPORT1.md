<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Coda+Caption:800|Francois+One|Goblin+One|Ramabhadra|Secular+One&display=swap" rel="stylesheet">

<style>
p.headline4 {
color: #333; /* this is the fallback */
-webkit-text-stroke: 1px #333;
-webkit-text-fill-color: transparent;
font-family: 'Poppins', sans-serif;
font-weight: 900;
font-size: 60px;
text-align: center;
}
</style>
</head>

**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘍𝘳𝘰𝘮 𝘥𝘢𝘵𝘢 𝘵𝘰 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭𝘴 𝘪𝘯 𝘢 𝘮𝘢𝘬𝘦𝘳 𝘴𝘱𝘢𝘤𝘦</font></center>
<br>



  <!--- <center>![e8 lie](assets/e9liemox.png)</center>-->
   <!---  background: linear-gradient(120deg, lime 0%, blue 100%);
     -webkit-background-clip: text;
     -webkit-text-fill-color: transparent; --> <!--- --> <!--- --> <!--- -->
   <!--- <font face="Montserrat" color="black" size="6"><center>Context</center></font> -->

# Context
  <!--- <font face="Montserrat" color="black" size="5" background color="transparent"><center>   </center></font>  -->

<!---
Humans have been making material things from [as far back as we have been human](https://www.nature.com/news/oldest-stone-tools-raise-questions-about-their-creators-1.17369).
The material things we make (drawings, artefacts, tools) all contain information and making material things can be seen as a kind of coding of materials or inversely a materialisation of information.

[Making things helps us to construct meaningful knowledge](http://namodemello.com.br/pdf/tendencias/situatingconstrutivism.pdf).
The things we make can make or suggest people to do certain actions or challenge present practices.
So making helps us both extract information from our environment, change it and put it back.

Humans have been making material things (computers, semiconductors) that produce, transfer and store bits (basic units of logic) for an age where these technologies have blossomed. Today the resulting bits are being produced, shared and stored in amounts and formats that can seem like meaningless noise but that are revealing new patterns of information. Increasingly we are also making atoms from bits - using computers to design and fabricate material things that we were not able to make before.
What are the new things that all this data and increasingly accessible digital fabrication tools allow us to make?

One of the things that scientists are able to fabricate more easily with the increasing affordability of digital fabrication tools since the 1990s are **metamaterials** - materials that exhibit properties rarely found in nature - including how structures transform the wave properties of its surroundings. These properties (such as energy cloaking, harvesting or dissipating) are so rarely experienced that they are the stuff of science fiction. Meta materials are now being designed using data-driven, machine learning methods.

How accessible to a novice are these new tools and methods of data-driven material design and digital fabrication?

Can a novice participate in making these new materials and artefacts from the noise of data that surrounds us?

This research project tries to organise from an outsiders point of view information on metamaterials and ways to fabricate them and test them.

It tries to replicate metamaterials designed by scientists in a maker space and construct a test bed using easily available hardware and information.

It finds and gathers documented toolchains and tools available to design a metamaterial using data-driven methods.

-->


## The fruits of the information age

The information age has changed paradigms in analysis, materialisation of and access to information. Advances in computation produce new ways of capturing information to describe the physical universe, new experiences and the design and fabrication of new kinds material things in new ways.

### Memory, connections and time

Innovations in computation hardware and software have enabled  [more storage capacity](https://www.cambridge.org/core/journals/mrs-bulletin/article/advanced-memorymaterials-for-a-new-era-of-information-technology/7D6D603157A9195DB0B585C8C804ACB9) , [more networked devices](https://iot-analytics.com/state-of-the-iot-update-q1-q2-2018-number-of-iot-devices-now-7b/) and smaller and faster computation devices. Communication devices are now held in the hands of around half of the world’s adult population – removing much of the material and time invested into connecting with people and devices and accessing the goods and services we need.

<center>![](assets/computation.png)</center>


### Noisy data

More storage capacity enables us to store large volumes of "Big Data", metadata and data which doesn't seem to have meaning. With an increasingly networked world, the number of people producing data has boomed and with it the variety of data on subjective experiences and behaviours. What will we do with [2 billion addresses for every human?](https://alandix.com/academic/papers/JVLC-2016-HCI-FNP/JVLC-special-2016-draft.pdf)

>After many years, IP v5 is being fully deployed, with 64 9 bit addresses allowing 2 billion addresses for every human being on the earth, or enough for more than ten computers on every square centimetre of the earth's land mass.

*<right>[- Dix, A. (2017)](https://dl.acm.org/citation.cfm?id=3165326.3165623)</right>*


### Tuning noisy data to find patterns and probabilities

Faster iterations and increasing data storage enable highly iterative tasks. These include machine learning to check large sets of data against levels of set criteria (decision trees) to find links between sets of data and their labels (results). This artificial intelligence can then iterate through smaller targeted data sets to find the most probable solutions to problems.

Machine learning is being used in everyday accessible applications such as music recommendations, predictive text and speech recognition but the strategies, tools and datasets that build these algorithms can end up contributing to scientific research in other fields.

>Analogous to the biosphere’s reuse of organic wastes, numerous online systems – Google’s search algorithms, recommender systems from Netflix and Amazon, etc. – recycle these by products of intelligent human activity to create more intelligent artificial behavior.
The ‘mining’ of data exhaust to detect patterns, trends and individual preferences is transforming the relationship between designers, builders, marketers and consumers, as well as civil society, worldwide. It is also transforming science.

<right>*[Paul N Edwards(2016)](https://journals.sagepub.com/doi/10.1177/2053019616679854)*</right>

Machine learning is developing for a widening range of users with increasing access to processing power (such as cloud based applications), datasets, algorithms, interfaces and languages. This is fuelling a growth in innovations in problem solving and their applications as testing new methods become more feasible  across a widening range of disciplines.


### Pushing limits of measuring the physical

More data storage, processing power and networking enable scientists to demonstrate and measure phenomena in the physical world that were not previously feasible. Computing hardware and software today help scientists make measurements at precise instances and positions such as the CubeSat “SpooQy-1” nanosatellite which generated correlated photons in space of the first time in 2016.
<center>![](assets/satellite.jpg)</center>

<center>*CubeSat “SpooQy-1” nanosatellite at Quàntica exhibition, CCCB, Barcelona 2019*</center>

Quantum theory challenges the limits of measurement itself and quantum computing has the potential to solve problems that classical computers cannot. Scientists are pushing current instrumentation to meet the demands of quantum computing which will in turn open up new opportunities for measuring and modelling the universe.

<center>![](assets/quantumchip.jpg)</center>

<center>*3D Transmon Quibit Superconductor from Quantum Group, Barcelona Supercomputing Center / Catalan Institute of Nanoscience and nanotechnology (ICN2) Pol Forn Diaz (BSC-CSN), Jordi Aribol (ICN2) exhibited at Quantica, CCCB, Barcelona 2019*</center>


<!--his is old Libby Hearny-->
<!--this is old ## The material world
The medium is the message - Material things reorganise information - and can make us reorganise around them
 computers are hypermaterials
### This is oldThe boundaries of Hardware and Software, Material and Virtual
>The argument is that by broadening our foundation, we may find new ways and perhaps better ways to do computation. Since reconfigurable computing is so deeply rooted in both hardware and software, if there are indeed unforeseen properties at the hardware-software interface, then reconfigurable computing, more than any other discipline, stands to benefit from them. The resulting systems might prove to be more fluid and more efficient, giving way to more responsive and life-like behavior.
*[ - Neil Steiner and Peter Athanas (2016) Hardware-Software Interaction: Preliminary Observations](http://www.ccm.ece.vt.edu:8444/papers/steiner_2005_RAW05_hsi.pdf)*
-->

### Simulation

Better measurements can be used to produce better models of our environment. Problems regarding the physical world are being modelled in ways that are getting closer to the limits of our perceptions of the physical world. Simulations can run from laptops to supercomputers enabling a wider and deeper understanding of how things (matter, systems, human behaviour) might work and how we can change them.

We can also simulate environments that depart the from the known physical world to produce virtual, augmented, hyperreal, surreal and experiences.


### New experiences

> Life imitates art. We shape our tools and thereafter they shape us. These
extensions of our senses begin to interact with our senses

<right> *[John M. Culkin (1967)](https://webspace.royalroads.ca/llefevre/wp-content/uploads/sites/258/2017/08/A-Schoolmans-Guide-to-Marshall-McLuhan-1.pdf)* </right>

The changes in time and materiality that automation and networking enable, allow us to experience relationships with other people, devices, materials and our environment in new ways.

> Social forms and institutions no longer have enough time to solidify and cannot serve as frames of reference for human actions and long-term life plans, so individuals have to find other ways to organize their lives

<right>*[- http://cyborganthropology.com](http://cyborganthropology.com)*</right>

> Users are not only making their online personas with the reference points of a far reduced set of criteria, they are also creating their identities in this environment. ... Users buy into the authenticity of the influencer’s persona and include them in their world, consciously and unconsciously mimicking what they see. The basis of influencer marketing is that audiences aspirationally buy into an influencer’s persona, buying the product as a way of reinforcing their connection.

<right>*[- Michael Andrea (2019) 'How Technology Reduces Us, and What We As Designers Can Do About It'Journal of Design and Science (JoDS)](https://jods.mitpress.mit.edu/pub/hsxloiq4)*</right>


## From atoms to bits - Extracting data

Information is stored, processed and networked through material things.

<!--This is oldIn 2011 Jean-François Blanchette warned in his Material History of Bits
> without modes of analysis grounded in the stuff of computing, we shall find ourselves in the awkward situation of resorting to theories that account for embodied subjects situated and interacting in environments curiously lacking
specific material constraints.
*[Blanchette, J.-F. (2011)](https://onlinelibrary.wiley.com/doi/abs/10.1002/asi.21542)* --->

### Living hardware

The material human body and mind has computed and fabricated material computers that we have material interactions with to generate bits. We are information machines ourselves - making other types of information machines.

>Since matter  can be  considered  as code  and code  as  matter,  the  latter  also  has  become programmable  (Ratto,  2010),  (Sangüesa,  2009,  2010). The  same  algorithms  that   guide  3D  laser  printers  in FabLabs  (Gershenfeld,  2005)  are  used  to "print"  organic  matter  and  build  new  human  organs  (Ringeisen,  2010).  In  a  symmetrical  move, living  matter becomes the  substratum  of computing  processes:  living  cells are interconnected to act as  computers  (Regot,  2011),  (Bray,  2011).

<right>*[Ramon Sangüesa (2010)](https://www.researchgate.net/publication/266887969_Technoculture_and_its_democratization_noise_limits_and_opportunities_of_the_labs)*</right>

<center>![](assets/Hilbertevolution.jpg)</center>

*<center> Schematic timeline of information and replicators in the biosphere from [Michael R. Gillings, Martin Hilbert, Darrell J.Kemp (2016)](https://www.researchgate.net/publication/289707073_Information_in_the_Biosphere_Biological_and_Digital_Worlds) </center>*

### Material interactions

Today, billions of people have material interactions with computers. [Close to half of the global potential market use mobile internet devices](https://www.gsmaintelligence.com/research/?file=b9a6e6202ee1d5f787cfebb95d3639c5&download). Access to computers or internet are indicators for [3 UN Sustainable Development Goals.](https://unstats.un.org/sdgs/indicators/Global%20Indicator%20Framework%20after%202019%20refinement_Eng.pdf)

<!-- This is old The metaphysics of computers matter far less than the “symbiosis” that we are striving to achieve
*[Mikael Wiberg (2015) Interaction, new materials & computing – Beyond the disappearing computer, towards material interactions](https://www.sciencedirect.com/science/article/pii/S0261306915002794)* -->

<center>![](assets/i7core.jpg)</center>
*<center>An intel i7core processor</center>*

Making computation devices is pushing the limits of science and engineering.

<center>![](assets/MEMSgyroscope.jpeg)</center>

*<center>An electron microscope image of a MEMS gyroscope</center>*




 <!--This is old
 Beyer, D., Gurevich, S., Mueller, S., Chen, H.T., Baudisch, P.,
 Platener: Low-Fidelity Fabrication of 3D Objects of 3D Objects by Substituting 3D Print with Laser-Cut Plates.
- -->

#### Metamateriality - Finding forms that show rare properties

Physicist Victor Veselago theorised in the 1960s that properties of materials interactions with waves that are not often found in nature (negative permittivity, ε, and permeability, μ) could exist for specific shapes. These structures were theorised to enable applications such as invisibility cloaking. Only in the 1990s were the computing power and tools accessible to fabricate these materials now known as metamaterials. Today we can draw metamaterials easily using CAD programmes, simulate their interactions with waves using simulation software and use machine learning to inverse design metadevices for specifics purposes.

>The prefix “meta” means “beyond” in Greek, so the term metamaterials refers to structures that go “beyond” the ordinary materials. For example, can we make a structured material that when it is put around an object, it can cause the incoming wave to go around the object (instead of being “splashed” away from the object), and thus the object can become essentially “invisible”..

<right>*[- Nader Engheta, Interview in Huffington post 'Nader Engheta: The Pinnacle of Interdisciplinary Research'](https://www.huffpost.com/entry/nader-engheta-the-pinnacl_b_11556358)*</right>


##### Shapes that bend waves

Perhaps the most remarkable property detected from metamaterials is their ability to bend material and electromagnetic wave forms in ways not found in materials in other types of shapes.

> Many exotic optical properties have been demonstrated
experimentally based on various metamaterials, including negative refractive index, broadband chirality, strong nonlinearity, which lead to important applications, such as electromagnetic cloaking , perfect absorption , super lensing  and wave front manipulation.

<right>*[- Wei Ma, Feng Cheng, Yihao Xu, Qinlong Wen and Yongmin Liu Probabilistic representation and inverse design of metamaterials based on a deep generative model with semi-supervised learning strategy](https://arxiv.org/ftp/arxiv/papers/1901/1901.10819.pdf)*</right>

Metamaterials can affect wave energy in different forms and therefore allows applications across many fields and scales. The regularity of metamaterials have to be in the sub-wavelength range of a wave in order to affect it in a counter-natural way. Despite this, metamaterials are being explored at the nano scale in photonics to accomplish extraordinary optical effects and applications. For example this year Duke University's David R Smith group developed ["Out-of-plane computer-generated multicolor waveguide holography"](https://www.researchgate.net/publication/330614197_Out-of-plane_computer-generated_multicolor_waveguide_holography) enabling "lens-free, ultraminiature augmented and virtual reality displays".

And on the other end of the scale urbanists are testing metamateriality with seismic waves.

<center>![](assets/sei.png)</center>

<center>Analogies between electromagnetic (nano-scale) metamaterial and (decameter scale) metacity  - Brulé, et al.(2017)</center>

Metamaterials have been made that can make their wearer appear "invisible" to sound and radio frequencies and using principles for designing antennas, conductive metamaterials can resonate at certain frequencies to harvest electricity from infrared, wifi, radio and, via piezos, even sound.

Scientists are finding different geometric strategies to get the same effects across materials and wave types and new taxonomies of metamaterials are underway.


##### New shape shifting forms

Meta materials can also enable localised, movements, strengths and porosities enabling flexible, transformable materials and mechanical devices made from one printed material that ordinarily would require several.

With more conductive and composite materials becoming affordable, active transformable devices are becoming more feasible such as [The kinetiX project by MIT Media Lab's Tangible Media Group](https://www.media.mit.edu/projects/kinetix/overview/) is looking at transformable auxetic tunable metamaterial structures that together can be made to move in complex ways.

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/XP5Fk-lHvK0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

Bas Overvelde also works with transformable metamaterial configurations and has devised [a toolkit for the design of these](http://www.studioovervelde.com/)

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/7A_jPky3jRY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

<br>
<br>


##### Shapes that compute from information in waves

<center>![](assets/swisscheese.jpg)</center>

*<center>A material that solves equations on waves - [Nasim Mohammadi Estakhri, Brian Edwards, Nader Engheta (2019) Inverse-designed metastructures that solve equations](https://science.sciencemag.org/content/363/6433/1333.abstract)</center>*

Using theoretical principles, Nasim Mohammadi Estakhri, Brian Edwards and Nader Engheta of UPenn this year demonstrated [an informatic metastructure - an analogue computer that operates on microwaves](https://penntoday.upenn.edu/news/penn-engineers-demonstrate-metamaterials-can-solve-equations). It can solve one set integral equation for an electromagnetic wave of a certain frequency but like all metamaterials - as it is depends on the shape of the solid it can be scaled up or down. This adds a layer on top of the work of Neil Gershenfeld who has [demonstrated ways that physicality and computation can be bound by geometry](http://cba.mit.edu/docs/papers/11.12.Computing.pdf)



## (De)coding physics through shape matching

Processing power is enabling new materials to be found not only through theoretical mathematics but also through teaching AIs to match materials to effects.  This enables a new order of intentionality to be employed n the design of material things.

> data-driven science is becoming the forth paradigm of materials research

*[Claudia Draxl and Matthias Scheffler (2018) NOMAD: The FAIR Concept for Big-Data-Driven Materials Science](https://arxiv.org/ftp/arxiv/papers/1805/1805.05039.pdf)*

Machine learning can be used to inverse design material things by using a dataset of a variety of material configurations which are then labelled with how they behave according to the physical processes of focus to give training samples. Samples are then used to train a network of logic gates to produce the kinds of results that are acceptable and this forms the algorithm. The algorithm is then inverted and a model of a desired physical state is put in to give a number of structures that should give that state. These can be verified for their energetic stability using density-functional theory.

<center>![](assets/discovery.jpg)</center>

*<center>The general process of machine learning in the discovery of new materials - Liua, Zhaoa, Jua, & Shi (2017) Materials discovery and design using machine learning</center>*

The original data can be computer generated from theory, by randomisation or using second hand data from experiments.  The labelled samples can be found using simulation software. The training can be done according to theory or supervised by a trainer “tuning” the different gates.

Machine learning is also being used to search and extract data on the properties of materials from existing academic literature.

<center>![](assets/BSC.jpg)</center>

*<center>MareNostrum supercomputer at Barcelona Supercomputing Center</center>*


### Finding new orders of materials

Supercomputers today are paving the way in finding "periodic tables" for molecules and structures several orders higher both biological and inorganic.

<center>![](assets/DraxlScheffler.jpg)</center>

*<center>'The Development of the paradigms (new modes of thought) of materials science and engineering' by [Claudia Draxl and Matthias Scheffler (2018) NOMAD: The FAIR Concept for Big-Data-Driven Materials Science](https://arxiv.org/ftp/arxiv/papers/1805/1805.05039.pdf)</center>*


[The Materials Genome Initiative (MGI)](https://www.nature.com/articles/s41524-019-0173-4) seeks to collect banks of data to accelerate this process whilst NOMAD (Novel Materials Discovery Center of Excellence (CoE) advocates for making data FAIR - Findable, Accessible; Interoperable and Re-purposable.

<center>![](assets/MimanenRinckeFoster.png)</center>


*<center>2D Material taxonomy from [Lauri Himanen, Patrick Rinke and Adam Foster (2018). Materials structure genealogy and high-throughput topological classification of surfaces and 2D materials](https://www.nature.com/articles/s41524-019-0181-4)</center>*


### Designing devices using machine learning

The discovery or design of materials using AI can extend to the design of [metamaterials](https://arxiv.org/abs/1805.10181), metadevices, [compliant mechanisms](https://www.youtube.com/watch?v=97t7Xj_iBv0)) and [microstructure optimisation](http://people.csail.mit.edu/desaic/pdf/tto.pdf). These methods unlock new ways of using materials and provoke new ways of fabricating things, accelerating the evolution of the things we make.


<center><iframe width="560" height="315" src="https://www.youtube.com/embed/97t7Xj_iBv0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>



<!--This is old hidden
## The emerging sensor economy - networking a unified hardware and software
>Rapid development and innovations in artificial intelligence, mobile network technology, quantum computing, nanotechnology and blockchain technology are finding their way into the devices and digital technology we use. However, not until these technologies are conceived of as components of a larger, integrated technology model, will the true disruptive potential of these simultaneously emerging technologies become evident. The idea of the sensor-based economy captures this, and shows how this emerging techno-economic paradigm will reshape our daily lives and economy.
*[- FreedomLab website, accessed June 2019](http://freedomlab.org/the-rise-of-the-sensor-based-economy/)* -->

## From bits to atoms - the things we make in The Digital Fabrication Revolution

The ability for more and more people to fabricate material things from code and the potential to code functionality into the materials we fabricate beyond computer chips is signalling a digital fabrication revolution. All this is driven by our increasing ability physically and financially to perform complex computations and access fabrication tools with the number of fab labs approximately doubling every year. Industries are building around the tools that makers use.

> “The best time to shape the destiny of transformative, accelerating technologies is early, before changes have become both widespread and entrenched…. The benefits and risks of accelerating technologies are very real, with deep impacts on many lives, but we have the agency, individually and collectively, to shape these impacts now “

*- Neil Gershenfeld, Alan Gershenfeld and Joel Cutcher-Gershenfeld, (2017) Designing Reality*

### Coding procedures into materials - functional materials

Assembly or self-assembly of forms is a recurring theme in fabrication research labs. From DNA origami to self-assembling robots, [self-replicating spacecraft](http://cba.mit.edu/docs/papers/17.04.11.SelfAssemSpacecraft.pdf) to [3D printed venus fly traps](https://www.nature.com/articles/s41467-018-08055-3y) to [fab labs that make fab labs](http://mtm.cba.mit.edu/) - computation is being used to code self-organisation into the things we design and we are trying to design things that have self-organisation coded into them. Making is like programming materials.

### Logic in the geometry of material things

> "With a lego brick you don't need a ruler to place it, the block has geometry, it corrects errors and you don't put Lego in the trash, you take the bricks apart. The reason you can do this is the bricks contain information.”

<right>*- Neil Gershenfeld -2019 Fab Academy lecture*</right>

Improving information processing power and processing methods are enabling us to observe (measure and visualise) material structures with greater resolution - down to the scale of atoms.

CAD interfaces enable 3D structures to be drawn and specified.

We can simulate physical processes with increasing complexity to predict behaviours of material structures with greater accuracy at different scales and [fast prototyping](http://mtm.cba.mit.edu/) can help to physically make and test structures for certain responses and scales.

Being able to predict behaviours of geometries of structures (of molecules, tissues, organs, devices) under a range of conditions begins to give us the data for a toolkit to design these structures with increasing specificity. We can inverse design structures with increasingly specific responses to particular stimuli. [Functional materials](http://cba.mit.edu/docs/papers/11.12.Computing.pdf) are a name given to materials based on our ability to control their physical responses.


<center>![](assets/GIK.png)</center>

*<center>[Grace Gershenfeld's Invention Kit, subsequently the Great Invention Kit](http://cba.mit.edu/docs/papers/06.09.digital_materials.pdf),is an example of digital material built by press-fitting together elementary components.</center>*


## The interdependence of life, the things we do, the things we use and the things we make

Geologist, civil engineer, and physicist Peter Haff coined the term technosphere to describe a system of physical processes of which humans are a part. This provides a tool to consider the Anthropocene (another geological term) separately from anthropological, social and cultural analysis and bias. In the technosphere humans serve to sustain technologies as much as technologies sustain humans.

>.. the technosphere includes “the world’s large-scale energy and resource extraction systems, power generation and transmission systems, communication, transportation, financial and other networks, governments and bureaucracies, cities, factories, farms and myriad other ‘built’ systems, as well as all the parts of these systems, including computers, windows, tractors, office memos and humans”.

*[- Peter Haff (2014),“Humans and technology in the Anthropocene: Six rules](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.832.4056&rep=rep1&type=pdf)*
-->
<!---This is old hidden dump of info
### New experiences
Materials can now be meta materials, relationships and communities no longer grow in allotted places and times, and you can represent your self in a much reduced, edited way through only words, sounds and images mediated by objects.
#### Liquid Modernity
> Social forms and institutions no longer have enough time to solidify and cannot serve as frames of reference for human actions and long-term life plans, so individuals have to find other ways to organize their lives
*[- http://cyborganthropology.com](http://cyborganthropology.com)*
#### Reduced identities
> Users are not only making their online personas with the reference points of a far reduced set of criteria, they are also creating their identities in this environment. ... Users buy into the authenticity of the influencer’s persona and include them in their world, consciously and unconsciously mimicking what they see. The basis of influencer marketing is that audiences aspirationally buy into an influencer’s persona, buying the product as a way of reinforcing their connection.
*[- Michael Andrea (2019) 'How Technology Reduces Us, and What We As Designers Can Do About It'Journal of Design and Science (JoDS)](https://jods.mitpress.mit.edu/pub/hsxloiq4)*

### Technology evolves
![](assets/lawsofmedia.PNG)
*[Izabella Pruska-Oldenhof, Robert Logan (2017) The Spiral Structure of Marshall McLuhan’s Thinking](https://www.researchgate.net/publication/315965165_The_Spiral_Structure_of_Marshall_McLuhan's_Thinking)*
Technology are driven by symbols
> In the case of technology it is the symbolic concepts and organization that goes into the creation of the physical tools that propagates not the actual physical tools themselves.
*[Robert K Logan (2014) What is information?: Propagating organization in the biosphere, symbolosphere, technosphere and econosphere]
(http://openresearch.ocadu.ca/id/eprint/397/1Logan_2014_WhatIsInformation_DEMOPublishing.pdf)*
## The symbolosphere
> The symbolosphere is defined as the human mind and all the products of the human mind including symbolic abstract thought, language and culture.
*Robert K Logan (2014) What is information?: Propagating organization in the biosphere, symbolosphere, technosphere and econosphere.*
### A multiplicity of subjective experiences - Quantumness
### Quantumness and identity
### New information pathways - a multiplicity of roles
Artists need to know about science, and scientists need to practice arts.
-->
-->
