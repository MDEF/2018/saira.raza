**<font face color="blue" size="5"><center>░M░a░k░i░n░g░ ░f░r░o░m░ ░n░o░i░s░e░̚</center></font>**
<center><font size="3" color="blue">𝘐𝘯𝘷𝘦𝘴𝘵𝘪𝘨𝘢𝘵𝘪𝘯𝘨 𝘮𝘦𝘵𝘢𝘮𝘢𝘵𝘦𝘳𝘪𝘢𝘭 𝘥𝘦𝘴𝘪𝘨𝘯 𝘢𝘴 𝘢 𝘯𝘰𝘷𝘪𝘤𝘦 𝘮𝘢𝘬𝘦𝘳</font></center>
<br>
# <center>Reflection</center>
  <!--- --> <!--- --> <!--- -->


## Collect > Structure > Imagine > Make

Throughout the process of building this project and in to trying to make things I experienced 4 distinct stages of achieving anything: Collecting information, decoding or structuring information, imagining or strategising and finally making a thing.

  ![](assets/CSIM.png)

I was introduced to and found some tools to help with each step

  ![](assets/tools.png)

I imagined that I would find tools that I could learn to use by following instructions but I struggled to find instructions and understanding the language of making. As a result most exercises became about structuring information not about making. This may be a reflection of where the Maker movement at large (and many other communities) is at the moment - trying to tie together information in a fast changing environment.

  ![](assets/time.png)

This presents an opportunity to innovate in the structuring, sharing and presenting of information on digital fabrication and computational material science - to make knowledge and skills in these fields more accessible so that more agents (human and non-human) can be involved in the making of the material world.

## Sensing, saving and sharing data

### Internet of things and the sensor network as sources of data

In trying to make my metamaterial test bed I encountered the internet of things subculture of makers. This culture is trying to develop suites of software and hardware that help to ease the transition between material and immaterial information flows. Data from these tools could be used to enable design solutions that use machine learning.

### Standardising and sharing material datasets

Looking at the toolchain for designing metamaterials, an effective inverse-design machine learning algorithm needs a labelled geometric dataset combined with a simulation algorithm and sensors and emitter to test the structures in real life. In the time given I was able to find one labelled dataset in the .lua language and one opensource simulation package but bringing the elements together required a lot of learning steps.

Labelled datasets for machine learning are valuable for testing and speeding up the building of algorithms. It is promising to find that within computational material science, steps are being proposed to standardise materials data.

If datasets, simulation packages and machine learning frameworks were easier to join in a modular way it might encourage more non-scientists to look into how different inputs might react with materials.

The material science community are not joined up with maker communities enough yet in this field to make this sort of commitment a reality but it might become useful to accelerate experiments if makers were brought on board to test metamaterials. For everyone else these partnerships could encourage more accessible tools so that more people can learn about, make and contribute to the development of material things.


## Language and interfaces for making and learning

To some degree all learning is filling in the blanks but people who make a spreadsheet or an email account don’t necessarily know anything about coding and people who use smart phones do not have to know about communications protocols and the names of chips in their phones. User interfaces are the digital revolution - not the phones or the laptops or even the internet but the fact that now anyone is willing to use things because they are easy to learn and trusted. What would happen we made the user interfaces for learning and making software and hardware as intuitive as social media?
If making is technology then making is STEM education. If we want to teach technology at a rate that can keep up with demands of coming economies then hardware needs to be easier to use, languages need to be as easy as possible to learn and interfaces need to be as intuitive as possible.
Research in the field of Human Computer Interaction is investigating how knowledge in HCI is translated from researchers to design practitioners then onto users, funders and policymakers.

>applicability might be the most significant barrier keeping applied research from influencing design practice. ..A practitioner noted designers will not — and cannot spend the time to read papers


[Lucas Colusso Ridley Jones Sean A. Munson Gary Hsieh (2019) A Translational Science Model for HCI](http://www.smunson.com/portfolio/projects/translationalresearch/colusso-HCI_TS_Model-CHI2019.pdf)

Insights like this signal an opportunity to bridge knowledge gaps between these fields in HCI but also potentially between researchers and practitioners of making material things.

## Material and abstract knowledge building – between theory and action

In the forward to his editorial for the collection of Essays on The Structures of Practical Knowledge Matteo Valleriani puts forward the idea that historically documentation and codification of practices, crafts and actions linked to knowledge structures on more organisational levels and that if we are now finding new kinds of practices and experiences, the documentation of these new experiences should inform our abstract ideas about technology and process.

My experience of trying to learn many new skills within 6 weeks brought unexpected experiences and changed what I thought learning to make things today was. It was not necessarily modular learning. It was highly meandering. Not having all the necessary knowledge structure nor materials to complete an assignment was an uneasy middle ground between thinking about making and actual making.

## I/O - Cause and effect - Building tools and reaching goals

The machine analogy of inputs and output emerged in my exploration of making in everything from coding to finding toolchains to routing circuit boards. Problem solving of any kind can have a machine representation. Modularity helps to build a machine and produces instructions. The machine layout helps us to simplify tool chains and superimpose problems from different fields.
The tool chain and the machine analogy are also a geometries.

## Geometry and diagramming

I think there is a certain transparency to diagrams – an invitation to augment. Because of this I think diagrams make good tools for documenting and allowing embodied knowledge to evolve. The structure of knowledge can be embedded into shapes just information can be programmed or fabricated into materials.
Caio Adorno Vassão writes eloquently that

>Of course, diagrams can represent realities – i.e. to make the absent present. But its importance in Metadesign lies in its ability to create, in a pre-linguistic manner, very complex, intricate and operative entities – a diagram makes present a reality, in the same way that an electronic circuit does not represent but is the flux of electrons; also a fluvial system does not represent but is the flux of water

Caio Adorno Vassão (2017) Design and Politics: Metadesign for social change

Geometry is for me a complex language of abstraction, strategy and attractors for meaning. All written language is geometry and everything you see, hear or touch has geometry. If sound and vision is geometry then words too have geometry, then do thoughts have geometry also?

Geometry makes it easy to start picturing things in your head because we infer so much about reality through our geometric sensors.

I was interested to come across a geometric theory of everything by Antony Garrett Lisi earlier this year who theorised that the E8 lie group of mathematical equations could unify field particle physics with Einstein's theory of gravitation. It was easy to picture a geometric structure underlying all matter and energy, with dimensions that we cannot perceive but that can project onto 3 dimensions or even 2 dimensions as this app he helped to produce demonstrates.

<center>*[The Elementary Particle Explorer (EPE)](http://deferentialgeometry.org/epe), designed and written by Garrett Lisi, Troy Gardner, and Greg Little geometrically illustrates the charges of all known particles. *</center>

I was also lucky enough to find Louise Drulhe’s Critical Atlas of Internet - a graphical meditation on the socio-politics of the internet. She has described herself as an internet cartographer by using diagrams. I find her work compelling in that it begins to give a shape to the shapeless and reminded me of a class project we worked on in term 2 called the Atlas of weak signals.

## Resolution and meaning from shapes

There seems to be a hidden embodied knowledge in shapes that begins to creep into the metaphysical. Sacred geometry is a thing and I felt a certain compulsion towards understanding what the shapes of metamaterials meant, what they were trying to do – and in doing so I found out that the tools we make are unlocking our ability to make forms that can harness another level of physics.

The idea that certain shapes of certain homogenous materials can flip the effects of physics that we are used to seeing is like a key to a new material environment.

<center>![](assets/infomat.png)</center>


## Inclusion in the co-evolution of technology and culture

We have seen how material things accelerate what is knowable through making, analysing and experiencing and in doing so we connect to the discourse of making i.e technology.

We have also learned how making things connects us to systems of social practices and it has been observed how material things can force social systems to reorganise.

Who we think we are, what we think our roles are and what we think our capabilities are affect our ability to make things to contribute to the discourse of making and therefore the rules of social behaviour (culture) and organisation (politics) that material things command.

Metadesign has foreseen the challenges that our ideas about agency in design might pose in the future towards sustainability by proposing open cultures of design where users are considered as agents of design.  

Organisations like Fab Foundation is committed to making education on fabrication tools more accessible for all and Fab Labs have done much to bring hardware and teaching to more people using physical spaces and online platforms.
However, (Whelan, 2018) found that underlying identity boundaries do exist in maker spaces.

“maker practices currently form a territory that demands to be inhabited, rather than simply a set of tools to be used. For the movement to be effective in its aims, it must become as genuinely pluralistic as it purports to be, and consciously work towards a softening of identity boundaries.”

Being mindful of not encouraging stereotypical maker identities could help to avoid creating boundaries. However, I wondered when speculating about the future practice of making, if it could become a social norm for users of material things – what we call consumers today – to consider themselves as makers of materials and guardians of these resources. This would require a narrative shift in the culture of consuming as well as making.





## Towards transdisciplinary making

Metadesign has foreseen the challenges that our ideas about agency in design might pose in the future towards sustainability bu proposing open cultures of design where users are considered as agents of design.

The scientific community has done much to open up and collaborate as well. Science favours transparency, peer review and technological connections including the internet which was built initially for scientific collaboration. The Maker movement is increasingly connected, with many online communities as well as maker space networks like Fab labs. The sharing of information across disciplines is less obvious.

In this intervention I have gained some practical knowledge, experience and knowledge structures of engaging with computational material science and digital fabrication in order to make material things as a novice outsider. I was not able to demonstrate a functioning link between the two fields but I have highlighted some physical interventions that could accelerate participation from novices two both fields that might help to build links to connect their practices and wider participation from people from external fields.

I have prototyped interfaces and relationships between humans and artificial intelligence in a desirable future open transdisciplinary making culture. Lacking the expertise in interface design, these prototypes were not engaging enough to build a compelling vision of this culture. Further consideration of how future practices of transdisciplinary making might feel would be valuable to explore to unearth our ideas about making, materials and technology. These in turn might engage us in the everydayness of a future where materials and technology are valued for their agency within a sustainable system. Prototyping these thought experiments of cocreation between humans and non-humans could engage us to participate in the building our future material environment and the social systems that will develop around them.



## Vision and agency

Thinking about the future from purely a material perspective is exciting as metamaterials, quantum computers and quantum metamaterials will reconfigure the shapes and things we associate with our everyday lives. Social practices will change around these objects. This is what I wanted to demonstrate through making.

Instead I found that the social practice of digital fabrication which enables us to make such materials is not in a state to enable everyone to learn to make and make these materials easily on their own.

Shifting the image of makers as being a special type of person with existing technical knowledge would help to engage more types of people into learning about and contributing to the evolution of technology and with it how we use materials in the future and everyday lives and cultures in the future.




<!---
## Constructionism and time
IN 1999 Seymour Papert's vision for the future of education was collective yet individual
>The "Vision" Vision of School: School is a place where students learn largely by working on projects that come from their own interests -- their own visions of a place where they want to be, a thing they want to make or a subject they want to explore. The contribution of technology is that it makes possible projects that are both very difficult and very engaging.
>It is a place where teachers do not provide information. The teacher helps the student find information and learn skills -- including some that neither knew before. They are always learning together. The teacher brings wisdom, perspective and maturity to the learning. The student brings freshness and enthusiasm. All the time they are all meeting new ideas and building new skills that they need for their projects. Some of what they learn belongs to the disciplines school has always recognized: reading, writing, mathematics, science and history. Some belongs to new disciplines or cut across disciplines. Most importantly, students and teachers are learning the art and skill and discipline of pursuing a vision through the frustrating and hard times of struggle and the rewarding times of getting closer to the goal.
[Seymour Papert and Gaston Caperton (1999) Vision for Education: The Caperton-Papert Platform](http://www.papert.org/articles/Vision_for_education.html)
This statement from Seymour does not account time, different abilities and disabilities and differing interests, acquiring skills quickly helps people pursue interests. All people have interests and try to learn more about them but what are the barriers stopping most people from persuiing science? why have so many people persued coding? why do so few people use fab labs as compared to cinemas and football pitches?
In a time where technology is evolving very quickly it is hard to learn the basics without short practical ways to learn how to do things.



## Reconfigurable data -reconfigurable materials



### Simplification of language

### Simplification of choices

### Simplification of instructions

### Simplification of toolchain

### technology is problem solving: Inputs, tools, outputs

a map of everything bringing everything together
an evolving genome of technology


### materials genome


## Open cultures

### Lowering costs

### Targeting the under-represented

### Removing assumptions coded into tools

Using and seeking data. Finding networks.

## Opportunities for Co-Design
-->
