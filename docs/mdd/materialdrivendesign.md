# <center>Material Driven Design</center>

<center>![](assets/)</center>

<center>![Diatoms](assets/Diatoms-blue.png)</center>

Since week 2 of the MDEF course I have been interested in a group of algae called Diatoms.
Having first been attracted to these organisms by their varied and fascinating appearance, I came to find out that these abundant algae play a surprising significant part in some of the 'wickedest' problems we face today but also have incredible properties and behaviours that could be used to address these same problems.

The more I learned about diatoms, the more I wanted to explore how to work with these organisms in a hope to gain some first hand experience on how structures and materials they produce can manipulate light, absorb other materials, cut softer materials and absorb other substances.

<iframe
  src="https://embed.kumu.io/dae73548e61fbe6dd90c7c197d2541d4"
  width="940" height="600" frameborder="0"></iframe>

## Influence

Asides from their mesmerising appearance, diatoms significantly influence our most vital systems including

* **Oxygen levels in the atmosphere**
Diatoms are photosynthesising floating plants and so convert carbon dioxide into oxygen using light. [Somewhere between a fifth and a quarter of all photosynthesis on our planet is carried out by diatoms. That means that as much as a quarter of Earth's oxygen comes from diatoms.]() Given their size and the abundances of species of other photosynthesizing plants it is surprising to learn that 20% of

* **Carbon capture from the atmosphere** to feed most of the ocean. Diatoms play a key role in the transferring carbon from the ocean surface to the deep ocean. Diatoms contribute around 40% of the world ocean particulate organic carbon export of the world ocean and play an essential role in "The biological carbon pump" - the flow of carbon through living things.

* **Crude Oil**
 [Geologists and chemists have pieced together lots of compelling evidence to suggest that crude oil is formed from phytoplankton](https://www.nytimes.com/2010/08/03/science/03oil.html)


* **Water purification**
Diatoms are found in biofilms in wastewater treatment i slow sand filtration when alive AND their shells found in diatomaceous earth are used in water filtration as well as filtration in many food products.
The presence of Diatoms both living and fossilised are indicators of the chemical quality of water systems both past and present.

* **Nutrient Currents and ecosystems**
Diatoms are eaten by small animals called zooplankton. Diatoms grow in many places even in polar icecaps

## Sources

### Living diatoms
Diatoms are phytoplankton - single-celled, free-floating, non-swimming plants. There are over 20,000 species of diatoms.
Diatoms live in water and need silica, iron and zinc as well as light to grow and survive (for their short life of less than a week). Although diatoms only live for a few days, they can reproduce very fast in suitable conditions with vary between species - . Places you can find them include nutrient rich spots in the ocean surface water and freshwater, sponges, and in the active bio layer in slow sand water filters and septic tanks. They have a brownish colour owing to the oil they contain to store carbon however they still contain chloroplasts for photosynthesis. They also produce Polysacharides and chitin while alive.

![harvesting](assets/diatomcollection.jpg)

*Harvesting diatoms from a living sample*

[In the ocean, ratios of nitrogen, phosphorus and silicate determine competitive dominance of either diatoms or other groups of phytoplankton. **When silicate to phosphorus to nitrogen ratios are high diatoms grow more easily**.](https://www.researchgate.net/publication/229355927_Nutrient_and_phytoplankton_trends_on_the_western_Black_Sea_shelf_in_response_to_cultural_eutrophication_and_climate_changes)

#### Marine diatoms

I initially looked for a live sample on Bogatell beach near IAAC on some seaside wet rocks.

I also read in an article that diatoms can also live in sea foam. I was able to collect some sample from light sea foam found on Bogatell beach. The sample seemed to live but did not flourish in a glass jar.

> "The foam is made up of the smashed up cells of phytoplankton – single-celled algae – and the nutrients they release when pounded by waves on surf beaches...High energy surf beaches have their own special ecosystem dominated by diatoms, some of which live only in this environment."

*Janet Bradford-Grieve, a marine ecologist and plankton specialist. [National Institute of Water and Atmospheric Research (NIWA) Summer Series 8: The forces behind beach ‘foam’](https://www.niwa.co.nz/news/summer-series-8-the-forces-behind-beach-foam)*

![rock](assets/diatomrock300.jpg) ![foam](assets/diatomfoam300.jpg)


#### Freshwater aquarium
My local aquarium were familiar with diatoms are they are notorious for 'fouling' aquariums. I was able to scrape a small sample from the filter of one of their warm freshwater tanks and tried to grow them in a test tube.
The aquarium cleans the tanks once a week and I was able to see that they grow quite fast in a warm freshwater with fish.  

![Aquarium](assets/diatomaquarium.jpg)

The microscope we had could not make out individual diatoms so I was not able to identify them to find out what nutrients they needed.

As well as this after trying to grow diatoms for over a week, I did not get much growth from the initial sample. I did not know what to feed them.

I found later that
> It is well known that diatoms get smaller as they subdivide asexually (http://ag.arizona.edu/azaqua/algaeclass/lecturenotes/Diatomnotes), and new seed cultures are needed to maintain vigorous growth. Partial but continuous replacement of the indigenous diatom species under cultivation with the most competitive diatoms from the wild ensures continuous, efficient production

*[Prospects for commercial production of diatoms- Jaw-Kai Wang1 and Michael Seibert](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5241979/)*

#### Biofilms

> A biofilm comprises any syntrophic consortium of microorganisms in which cells stick to each other and often also to a surface. These adherent cells become embedded within a slimy extracellular matrix that is composed of extracellular polymeric substances (EPS)... Because they have three-dimensional structure and represent a community lifestyle for microorganisms, they have been metaphorically described as "cities for microbes"

Biofilms grow on slow filters that are used for purifying waste water and they remove much organic matter. I contacted the water treatment works in Forum and one of their engineers informed me that diatoms don't tend to grow in high volume treatment works.

I also visited the Besos river around the water exit of a gas plant. Here I found some underwater pebbles that looked like they were growing diatoms however when I tried to collect them I found worms underneath them and decided that I did not have the equipment needed to keep these samples hygienically.



### Fossilised Diatoms
River and sea beds or what were river and sea beds can contain layers of soft powdery fossilised diatoms - known as Diatomaceous Earth or Diatomite.  Diatomaceous earth has some exceptional properties due to the hierarchical porous structure of diatom frustules. Their porosity and nano charged structure enables the frustules to absorb and contain other materials which makes diatomaceous earth excellent for absorbing heavy metals, oils and even viruses.


My sources of fossilised diatoms were bought gardening grade and food grade diatomaceous earth:

![Diatical](assets/diatical.jpg)
![Food grade diatomaceous earth](assets/diafoodgrade.jpg)


###  Outer space!

[Analysis of interstellar dust in the 1980s suggested the existence of cosmic microbiological systems containing diatoms.](https://www.researchgate.net/publication/328682910_DIATOMS_ON_EARTH_COMETS_EUROPA_AND_IN_INTERSTELLAR_SPACE)

 One fossilised diatom frustule has been found in a meteorite on earth!

 ![meteorite](assets/diatommeteorite.PNG)


## Properties

### Light bending shells

In life, these beautiful unicellular organisms build an amorphous silica (glass) shells. These multi-layered hierarchical structures called frustules have incredible 'metamaterial' light bending properties. This enables them to distribute light of optimum (blue) frequencies for photosynthesis. They come in several intriguing shapes.

A number of diatoms exhibit iridescence properties and are often referred to as ‘sea opals’




### Multi-stage pores for filtration
Multi-stage pores make diatoms excellent filters.

![Layers](assets/diatom layers.PNG)
![Hierarchical Structure](assets/diatom microstructure.jpg)

*Wavelength and orientation dependent capture of light by diatom frustule nanostructures Julien Romann et al (2015) Scientific Reports*

### Strength

[Diatoms frustules were recently found to be the highest strength-to-weight ratio of any biological material owing to their honeycomb silica structure and distribution of pores](https://www.independent.co.uk/news/science/diatom-caltech-strongest-natural-biological-material-strength-toughness-a6863176.html)

![Honeycomb Structure](assets/diatomhoneycomb300.jpg)

*[Microstructure provides insights into evolutionary design and resilience of Coscinodiscus sp. frustule - Aitken, Zachary H. and Luo, Shi and Reynolds, Stephanie N. and Thaulow, Christian and Greer, Julia R. (2016)](https://authors.library.caltech.edu/64304/3/PNAS-2016-Aitken-2017-22.pdf)*

[Diatom frustules have also been shown to have increased mechanical strength when grown in limited iron concentrations by Susanne Wilken and all](https://aslopubs.onlinelibrary.wiley.com/doi/pdf/10.4319/lo.2011.56.4.1399)

When fossilised, diatom frustules can become more crystalline and abrasive quartz structures.

### Able to process into semi conductors including silicon

Treatment with TiO2 and MGO improve the semiconducting and capacitive properties of the diatom frustule and silica frustules can be processed into silicon by various high energy methods.


## Cultural and experiential factors

Diatomaceous earth was discovered in Germany in the 1800s in mines.
With improving microscopy there was increasing awareness and study into their intriguing structures.

### Sci Art

Arranging microscopic algae into patterns is a way for scientists to marvel at new discoveries of these organism. Prominent examples are these meticulous drawings by Ernst Haeckel in his book 'Kunstformen der Natur' in 1904 and the slides of Johann Diedrich Möller and R. I. Firth.



Scientist Klaus Kemp has rebooted this art using modern microscopy methods.

![Drawing](assets/haeckel-diatom.jpg)   <iframe src="https://player.vimeo.com/video/90160649" width="528" height="297" frameborder="0" allowfullscreen></iframe>

Diatoms are inspiring contemporary media arts as well. [Diatomic Number](http://leegj.com/diatomic-number/) is a game under development by GJ Lee about collecting diatoms and seeing impossible colors. "It uses the Oculus Rift to display “impossible colors,” which exist outside of the commonly known color space"


### 3D Printing Frustule Shapes

#### At IAAC

Intricate highly structured diatom structures challenge us to mimic them. Last term I was able to try 3D printing an opensource CAD drawing of a diatom frustule. There are a number of people attempting to do this.

[Past students at IAAC have also tried to 3D print diatoms.](https://www.flickr.com/photos/fraguada/albums/72157624022963834)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/fraguada/4591210505/in/album-72157624022963834/" title="IaaC at Santa Monica Arts"><img src="https://farm5.staticflickr.com/4014/4591210505_db2ab12521_n.jpg" width="240" height="320" alt="IaaC at Santa Monica Arts"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/fraguada/4591839170/in/album-72157624022963834/" title="IaaC at Santa Monica Arts"><img src="https://farm5.staticflickr.com/4059/4591839170_7d7fb89280_n.jpg" width="240" height="320" alt="IaaC at Santa Monica Arts"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/fraguada/4591222847/in/album-72157624022963834/" title="IaaC at Santa Monica Arts"><img src="https://farm5.staticflickr.com/4020/4591222847_577b463852_n.jpg" width="240" height="320" alt="IaaC at Santa Monica Arts"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

*In 2010 IaaC students worked with Marta Malé-Alemany (Digital Fabriation) and Luis Fraguada (Digital Tools) and Cesar Cruz Cazares to produce a study on Diatoms and Radiolaria (the animal version of diatoms) as part of the Mireya Masó exhibition on Antartica at Santa Monica Arts*


The website [Fractal Teapot documents 3D printing of diatom frustules and has linked this to the carbon cycle as well as to the craft of documentation.](https://www.fractalteapot.com/portfolio/3d-printing-carbon-cycle/)

## Possible Applications

Below is a map of processes, materials and devices that I have found related to diatoms.

<iframe
  src="https://embed.kumu.io/8714c4086a7272763c21558ef455194a"
  width="940" height="600" frameborder="0"></iframe>


As diatoms play such an important ecological role I think they should be used in something that exploits their influence on ecology.

### Growing living algae on surfaces

Diatoms love to grow on high surface area substrates like filters. [They grow better on rough, hydrophobic surfaces](https://core.ac.uk/download/pdf/81148965.pdf). [Microalgae in general tend to grow well on materials with low 'surface free energy' e.g nylon, cotton](https://core.ac.uk/download/pdf/81148965.pdf). Stainless steel and Titanium have also shown to enable microalgal growth.

Could diatom covered surfaces be harvested to harness the photonic crystal properties and high surface area of their frustules?

[Living diatoms in a photobioreactor can be used to capture carbon and produce biofuels.](https://scinapse.io/papers/2086086576)

## Cold applications of diatomaceous earth

### Filler for paints
Diatomaceous earth can be used as an additive to paints to give light flattening  properties. .

### Filtration applications

Diatomaceous earth is used as a filter for many applications including within the food industry.

Diatomaceous earth is a widely used to separate of solids and microorganisms from raw water to produce drinking water. It also removes chlorine resistant pathogens.
Using a water bottle I made a rudimentary water filter from a coffee filter filled with food grade diatomaceous earth with a layer of activated carbon pellets on top. After one filtration, the diatomaceous earth had formed a compacted surface and had costed the carbon. it easily turned back into a powder.

![water filter](assets/)

Spent cakes of diatomaceous earth are a waste product from the food and beverage industry which may be useful to salvage as fertiliser or synthesise into new products. Some examples include [from the brewing industry](http://www.web.uwa.edu.au/__data/assets/pdf_file/0008/1637270/Johnson_1997.pdf), [in oil purification](http://www.globalearthregeneration.com/technology/soy-oil-spent-filter-cake-disposal) and from [winemaking](http://blog.epminerals.com/blog/california-vineyards-recycle-diatomaceous-earth-spent-cake-into-compost).

Diatomaceous earth is also used in swimming pool filters as calcined (crystalised) silica.


### Exploring the shape and surface treatment of filters

### Extracting fresh and fossilised silicate frustules chemically with acid.

[Acid can be used to extract the silicate frustules from diatomaceous earth.](https://www.researchgate.net/publication/288931510_Engineering_of_three_dimensional_3-D_diatomTiO2MnO2_composites_with_enhanced_supercapacitor_performance)

As hydrochloric acid was not available in the materials lab, I tried using rice vinegar with gardening grade diatomaceous earth and left it for 2 weeks - it became much whiter and particles were much smaller.


![Vinegar washed gardening grade DE](assets/acidwashed.jpg)



## Low heat to diatomaceous earth

### Waterglass
Diatomaceous earth reacts with alkali to become soluble and form a metasilicate polymer called water glass or liquid glass. This is used in ceramics as a glaze that gives ornamental cracks.

I used a DIY method of mixing Caustic Soda slowly to Water and then adding Diatomaceous earth in the proportions 10 ml water 4g caustoc soda to 6g diatomaceous earth.
Using food grade diatomaceous earth gave a smoother toffee like substance which when dried was soluble.

![waterglass](assets/roughwglass.jpg)
![waterglass](assets/waterglass.jpg)
![waterglass glaze](assets/glaze.jpg)

[I later found that water glass can be used as an ingredient in geopolymers:](https://www.researchgate.net/publication/316442024_Utilization_of_sodium_waterglass_from_sugar_cane_bagasse_ash_as_a_new_alternative_hardener_for_producing_metakaolin-based_geopolymer_cement)


### Additive to bioplastics

I tried adding gardening grade diatomaceous earth to cornstarch, vinegar, glycerine and activated carbon. I thought the carbon might produce some electrically conductive properties. It did not and was insulating.

![Bioplastic with carbon](assets/carbonbioplastic.jpg)
![Dried bioplastic with carbon](assets/carbondried.jpg)

I added food grade diatomaceous earth to gelatine water and glycerine and this produced a translucent insulating material that shrank over 3 days to the point where it could resist water for several hours.

![Gelatine plastic](assets/gelatine.jpg)

After 2 weeks the gelatine plastic had held its shape and was difficult to tear

![Gelatine plastic](assets/gelatinedry.jpg)



Using tapioca starch, glycerine and vinegar I was able to form a hydrophobic white slime. It took 2 weeks to harden and softened immediately on contact with water.

![Starch plastic](assets/starchplastic.jpg)
![Starch plastic](assets/starchplastic2.jpg)

I added a little Titanium Oxide to the tapioca starch / Diatomaceous earth / vinegar bioplastic to see if it might make a semi conductive plastic. It did not dry in 2 days but in two weeks this did shrink to a harder state.

I out it into a pipette to see if it cloud be easily extruded or might even form a bubble. it stayed as a slime and was not easy to work into other shapes.
On drying it became easy to tear and eventually brittle.


![TiO2](assets/ti02plastic.jpg)



## High heat to diatomaceous earth

### Calcinating
Diatomaceous earth can be heat treated at High heat (calcination) with sodium carbonate (soda ash) as a fluxing agent, to turn amorphous silicon dioxide into a crystalline form. Calcined diatomaceous earth is heat-treated and activated for filters. This makes the material more inert.



I tried to calcine food grade diatomaceous earth with 20% sodium carbonate by weight at 850 degrees for 4 hours in a kiln. It produced a highly porous material with a greenish stain (presumable where-the Sodium oxide melted). The material was fairly brittle except for one sample containing a lower amount of Soda which was quite hard to break.

![Calcined diatomaceous earth with Sodium carbonate](assets/calcinedtest.jpg)

A larger sample of 16% Sodium carbonate managed to hold its shape slightly more.

![Calcinated 16% Soda Ash](assets/calcinated.jpg)

I found out later that this material is dangerous if inhaled so I kept it under water and decided not to pursue further experiments.

### Soda lime glass using calcined eggshells

The oldest known forms of manufactured glass are made of crystalline silica sand. Diatomaceous earth is also made of silica but the structure is amorphous.
Ordinary glazing container glass is formed from soda-lime glass, composed of approximately 75% silicon dioxide (SiO2), sodium oxide (Na2O) from sodium carbonate (Na2CO3 or soda), calcium oxide (CaO or lime). Pure silica melts at around 1700 degrees Celsius but sodium oxide melts at a much lower temperature of 851 degrees which helps the reaction to happen faster but makes the silica water soluble, the lime makes the silica more chemically stable.

![Equilibrium chart](assets/glassmeltingpoints.png)

I thought I could try to modify waste glass objects such as household jars and bottles.

For my first test I used 70% food grade diatomaceous earth, 18% Sodium Carbonate and 12% calcium Carbonate. I could not find a source of lime so tried calcined eggshells that my classmate Maite had produced. I added some water glass glaze with eggshells to one side of the glass as a side experiment

This mixture did not work at all and resulted in a crumbly course powder of calcinated diatomaceous earth. The glass pot completely melted except for where the eggshell glaze was applied to it suggesting that alkaline waterglass and lime make glass more resitant to temperature.

I later found out that [water glass used to be used to preserve eggs during the war.](https://www.1900s.org.uk/1940s50s-preserving-eggs.htm)

![glass pot containing soda glass mixture](assets/glassbefore.jpg)
![glass pot after high heat](assets/glassafter.jpg)
![powder remaining](assets/glasspowder.jpg)

The recommended mixture for soda glass had not worked so I assumed that the calcium carbonate was maybe not pure enough.

I tried a second batch of the successful calcinating mixture of 16% Sodium Carbonate 84% Diatomaceous earth but this time with a little waterglass on top to see if this might create a glazed calcinated cake. I made 3 small samples of these directly onto wells i had cut into heatproof brick. I put a larger sample into a ceramic unglazed bowl and for one last sample I put the glaze onto the heatproof brick first and added the calcinating mix on top of the glaze as shown below

![16% Sodium carbonate mix with waterglass](assets/glazed.jpg)

These samples were heated for 900 degree Celcius for 4 hours.

![Glazed calcinated samples](assets/glazed1.jpg)

I cleaned the samples from the brick and bowl and soaked them in water. They were not soluble.

![Glazed calcinated samples](assets/glasscrystals.jpg)

The glass in the bowl was bonded to the bowl I could not remove it

![Glazed bowl bottom](assets/sodaglass.jpg)


At first I thought the green colour came from the Sodium carbonate as it as you can see it in some of the calcined samples. I have seen some examples of green/ grey calcined diatomaceous earth but it is not certain if this is because of the calcination process.
The other possibility is that the diatomaceous earth contains impurities that react to turn green. The container for the diatomaceous earth said that it was 94.2% silicon dioxide. On further research I found that this brand of Peruvian diatomaceous earth did indeed contain impurities including Iron Oxide which is known to colour glass a slightly green colour and Manganese Oxide and Sulphur Oxide which give some brown colours.

![DE composition](assets/composition.png)


### Geopolymers

When I tried to make soda glass using eggshells some sodium hydroxide in the test patch of water glass glazing on the outside of the glass container may have resulted in a geopolymer-like substance .

Geopolymers are a new approach to making concrete with reduced Co2 emissions. They are made by polymerizing silica and alumina containing minerals using alkali solvents. This involves:
Silica (e.g DE, metakaolin, fly ash) + alkali solvent (NAOH caustic soda) + Calcium  Oxide (e.g eggshell) baked in a kiln.

[Fadhil et al researched](https://www.researchgate.net/publication/321158849_Methods_of_curing_geopolymer_concrete_A_review) researched using fly ash and metakaolin (a mineral containing Silicon and Aluminium) to make geopolymers. They found that class C fly ash can replace the metakaolin and lime (Calcium Oxide). They recommended mixing all dry ingredients together then stir in just enough alkaline solution to make a stiff mix, keeping the liquid content as low as possible then curing the mixture like concrete, keeping it warm and moist. Using Fly ash or ash waste from power plants could be a way to capture potentially wasted carbon.



Later my classmate Nico and I mixed some of my diatomaceous earth sample with a few crystals of caustic soda and then added pure calcium which he had derived from bones. This never solidified.




### Sintering using a laser cutter on diatomaceous earth with sodium carbonate flux
[Sand with sodium carbonate can produce glass using a 400 W CO2 laser cutter at high power and low speed](https://hackaday.com/2016/12/07/sintering-sand-with-a-laser-cutter/).

Mikel, the Fab lab  expert on the laser cutter helped me to experiment briefly with a laser cutter. We used a ceramic container to avoid reflections occurring on the surface.

2 runs of sintering a 1cm diameter ring gave us beads of solid and a shallow edge on the surface of the container. At maximum power and slowest speed and a smaller diameter circle, a glass ring of 2 mm formed on the base of the ceramic container. The glossy finish of the high power sample suggests that it may have kept its amorphous structure. We were unable to experiment any more with the laser so as not to damage the laser.

![laser2](assets/laser2.jpg)
![laser1](assets/laser1.jpg)
![laser3](assets/laser3.jpg)


### Making Silicon

I found a number of high heat / high energy ways of turning silica into silicon. We did not have the apparatus nor chemicals to perform any of these but I continue to look for feasible ways. Methods include:

- Thermite of aluminium and sulphur with sand - required a sulphur to combust aluminium with silica.
- electrolysis in molten calcium chloride (this material was obtained but the equipment was too complex)
- magnesothermionic reactions - required inert gas conditions for the reaction and hard to obtain chemicals - this method is very useful to maintain he nano structure of diatom frustules and is used in nanophotonics.

## Nano scale applications of diatomaceous earth


[Diatoms have been explored as a potential source of Nanomaterials by Meerambika et al](https://www.researchgate.net/publication/318230017_All_New_Faces_of_Diatoms_Potential_Source_of_Nanomaterials_and_Beyond)

[Diatom frustules have been mimicked using silica deposition onto shapes formed using DNA Origami](https://www.sciencedaily.com/releases/2018/07/180716151626.htm)


### As photonic crystals in dye-sensitized solar cells

[The photonic scattering qualities of diatom frustules have been exploited by embedding them whole into layers of organic dye-sensitized photovoltaic cells.](https://www.researchgate.net/publication/254560152_The_potential_of_diatom_nanobiotechnology_for_applications_in_solar_cells_batteries_and_electroluminescent_devices
)

![dye-sensitized solar cells](assets/dyecell.png)

*Diagram from https://www.mansolar.nl/technology*





[Lyndsey McMillon-Brown et al demonstrated using diatomaceous earth as light traps in regular poly(3-hexylthiophene) (P3HT) and fullerene derivative [6,6]-phenyl-C60-butyric acid methyl ester (PCBM) solar cells.](https://phys.org/news/2017-10-solar-power-diatoms.html#jCp)

[High temperature  methods of mixing diatomaceous earth with TiO2 to make electrodes in DSSCs have been implemented successfully by Der-Ray et al](https://www.researchgate.net/publication/276096996_Enhancing_the_Efficiency_of_Dye-Sensitized_Solar_Cells_by_Adding_Diatom_Frustules_into_TiO2_Working_Electrodes) Diatom frustules were mixed with TiO2 paste at a weight ratio of 1:50 and spin-coated into working electrodes and sintered at 500 °C. [Jeremy Campbell and Greg Rorrer have also attempted this but method is not made public](https://www.researchgate.net/publication/260967102_Integration_of_biological_photonic_crystals_in_dye-sensitized_solar_cells_for_enhanced_photocurrent_generation)

[DIY DSSCs can be made using annealed titanium oxide paste soaked with raspberry juice applied to a conductive glass.](https://www.instructables.com/id/How-to-Build-Use-A-Dye-Sensitized-Solar-Cell-DS/). It requires acid to separate the particles if TiO2. I could not find a cheap and readily available source for the acid conductive glass but there are some [DIY ways of producing conductive glass using tin chloride](http://www.teralab.co.uk/Experiments/Conductive_Glass/Conductive_Glass_Page1.htm)



Aloe vera, like titanium oxide is a dielectric material which means it can be useful in manipulating current flow. It would be interesting to test what happens when we add diatomaceous earth to aloe vera and test diatomaceous earth enhances its dielectric properties as it does with titanium oxide. [The dielectric properties of Aloe Vera has been researched by Li Qian Khor K.Y. Cheong of Universiti Sains Malaysia](https://www.researchgate.net/publication/257583141_Aloe_vera_gel_as_natural_organic_dielectric_in_electronic_application)

I added food grade diatomaceous earth to aloe vera and it produced a paste which never hardened.

![Aloe](assets/aloedepaste.png)

I tried to create a paste of food grade diatomaceous earth, titanium oxide and Aloe Vera which might be able to form the semi conductive layer of a DSSC with added light scattering properties of diatom frustules.
This dried fairly quickly into a flakey powder.

![Aloe](assets/aloe.jpg)


### Doping with TiO2 for enhanced semiconducting properties

Doping frustules with titanium oxide can make an excellent semi-conductor. However this only happens with very thin layers deposited chemically. Currently this involves chemicals that are not readily available such as [TiF4 Titanium Fluoride and Potassium Permanganate.](https://www.researchgate.net/publication/288931510_Engineering_of_three_dimensional_3-D_diatomTiO2MnO2_composites_with_enhanced_supercapacitor_performance)

### Doping Diatomaceous earth into nano silicon with graphite to create anodes of batteries

The high surface area of diatom frustules can be used to make efficient battery anodes. [The silica diatom frustules in diatomaceous earth (diatomite, DE) can be converted to nanosilicon (nanoSi) by a process called magnesiothermic reduction and Polyacrylic acid (PAA) can be used as a binder to made an anode.](https://www.nature.com/articles/srep33050). the resulting anode is nearly twice the specific capacity of graphite’s theoretical value.

### Coating frustules with silver nanoparticles

Silver nanoparticles have bactericidal effects and antimicrobial activity, and have been used as a preservative, mixed into plastics and in the medical field to treat burns and infections.

 Silver nanoparticles of importance in medical applications as they have antibacterial and antimicrobrial properties and have been mixed into plastics to treat burns. They can also have localised surface plasmon resonance (LSPR) responses which could be used in quantitative real time bio sensing platforms.

[Toster et al cleaned frustules by plasma treatment and immersed them in silver nitrate solution and reduced this by adding Sodium hydroxide. Silver oxide particles bound to the surface then were readily reduced to silver by glucose solution.](https://www.researchgate.net/publication/239549088_In_situ_coating_of_diatom_frustules_with_silver_nanoparticles)




## Learnings

* Growing diatoms requires a bioreactor, nutrients and preferably a microscope to optimise conditions for the species.

* Diatom frustules in diatomaceous earth are very thermodynamically stable and difficult to bond to other materials without a strong alkali. The geometries of silicon and amorphous silica made by diatoms are very stable at room temperature.

* Diatomaceous earth can stiffen bioplastics and make them transparent bioplastics translucent.

* 






## Bibliography

#### Astrobiology

* [2038 – When Microbes Rule the Earth Terry J. McGenity](https://www.researchgate.net/publication/328336220_2038_-_When_Microbes_Rule_the_Earth)

#### Collecting Diatom Samples

* [A Methods Manual for the Collection, Preparation and Analysis of Diatom Samples Version 1.0 (2007) JC Taylor, WR Harding and CGM Archibald](http://docs.niwa.co.nz/library/public/1770054839.pdf)

#### Growing diatoms

* [A unique method for culturing diatoms on agar plates- KEI KIMURA1,2 & YUJI TOMARU1](http://www.plankton.jp/PBR/issue/vol08_1/pbr0801_46.pdf)
* [Non-enclosure methods for non-suspended microalgae cultivation: literature review and research needs - Ledwoch Katarzyna, Gu Sai,  Oinam Avijeet Singh](https://www.researchgate.net/publication/268527683_Non-enclosure_methods_for_non-suspended_microalgae_cultivation_Literature_review_and_research_needs)
* [The Effect of Substrate Properties on the Attachment and
Reproduction of Diatoms - Kristen Bishop 2011](http://physics.wm.edu/Seniorthesis/SeniorThesis2011/bishopthesis.pdf)

#### Fertiliser

* [Behaviour of Different Levels and Grades of Diatomite as Silicon Source in Acidic and Alkaline Soils - N. B. PrakashEmail M. S. Anitha K Sandhya](https://link.springer.com/article/10.1007/s12633-015-9373-9)

#### Calcinating diatomaceous earth at high heat

* [Producing a micro-porous diatomite by a simple classification-calcination process January 2011 Suzan S. IbrahimSuzan S. IbrahimAli SelimAli Selim](https://www.researchgate.net/publication/257985867_Producing_a_micro-porous_diatomite_by_a_simple_classification-calcination_process)

#### Biofuels

* [Milking Diatoms for Sustainable Energy: Biochemical Engineering versus Gasoline-Secreting Diatom Solar Panels T. V. Ramachandra et al, 2009](https://scinapse.io/papers/2086086576)
* [Prospects for commercial production of diatoms
 Jaw-Kai Wang1 and Michael Seibert](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5241979/)
* The Science of Algal Fuels - Phycology, Geology, Biophotonics, Genomics and Nanotechnology - Edited by Richard Gordon and Joseph Seckbach
* [Doping of magnetite nanoparticles facilitates clean harvesting of diatom oil as biofuel for sustainable energy - Vikas Kumar, Ramesh Singh, Shipra Thakur, Khashti Ballabh Joshi1 and Vandana Vinayak](https://iopscience.iop.org/article/10.1088/2053-1591/aab86a/meta)
* [Potential of diatom consortium developed by nutrient enrichment for biodiesel production and simultaneous nutrient removal from waste water - Thomas Kiran et al](https://www.sciencedirect.com/science/article/pii/S1319562X17301493)

#### Photonic properties

* [The  photonic crystal behaviour of diatom frustules is explored further by Sophie Cassaignon et al](https://cyberleninka.org/article/n/3469.pdf)

#### Photovoltaics and batteries

* [The potential of diatom nanobiotechnology for applications in solar cells, batteries, and electroluminescent devices - Clayton Jeffryes, Jeremy Campbell, Haiyan Li, Jun Jiao and Gregory Rorrer](https://www.researchgate.net/publication/254560152_The_potential_of_diatom_nanobiotechnology_for_applications_in_solar_cells_batteries_and_electroluminescent_devices)
* [Integration of biological photonic crystals in dye-sensitized solar cells for enhanced photocurrent generation
October 2013 - Jeremy Campbell Greg Rorrer](https://www.researchgate.net/publication/260967102_Integration_of_biological_photonic_crystals_in_dye-sensitized_solar_cells_for_enhanced_photocurrent_generation)
* [Light-trapping in polymer solar cells by processing with nanostructured diatomaceous earth, Organic Electronics - Lyndsey McMillon-Brown et al. (2017)](https://phys.org/news/2017-10-solar-power-diatoms.html#jCp)
* Green Materials for Electronics edited by Mihai Irimia-Vladu, Eric D. Glowacki, Niyazi S. Sariciftci, Siegfried Bauer
* [Enhancing the Efficiency of Dye-Sensitized Solar Cells by Adding Diatom Frustules into TiO2 Working Electrodes
April 2015 Der-Ray Huang Yan-Jang Jiang Run-Lin Liou Show Chih-Hung Tsai](https://www.researchgate.net/publication/276096996_Enhancing_the_Efficiency_of_Dye-Sensitized_Solar_Cells_by_Adding_Diatom_Frustules_into_TiO2_Working_Electrodes)
* [Aloe_vera_gel_as_natural_organic_dielectric_in_electronic_application Li Qian Khor K.Y. Cheong of Universiti Sains Malaysia](https://www.researchgate.net/publication/257583141_Aloe_vera_gel_as_natural_organic_dielectric_in_electronic_application)
* [Dye-sensitized solar cells using Aloe Vera and Cladode of Cactus extracts as natural sensitizers - Deepak Ganta J. Jara  Rolando Villanueva](https://www.researchgate.net/publication/316589408_Dye-sensitized_solar_cells_using_Aloe_Vera_and_Cladode_of_Cactus_extracts_as_natural_sensitizers).
* [DIY DSSCs on Instructibles.com](https://www.instructables.com/id/How-to-Build-Use-A-Dye-Sensitized-Solar-Cell-DS/)
* [DIY ways of producing conductive glass using tin chloride](http://www.teralab.co.uk/Experiments/Conductive_Glass/Conductive_Glass_Page1.htm)

#### Coating with silver for biomedical nano devices

* [In situ coating of diatom frustules with silver nanoparticles
2013 Green Chemistry Jeremiah Toster  Qin Lin Zhou Nicole Smith Colin L Raston](https://www.researchgate.net/publication/239549088_In_situ_coating_of_diatom_frustules_with_silver_nanoparticles)

#### Geopolymers

* [Methods of curing geopolymer concrete: A review 2018 - Muhd Fadhil Nurruddin Sani Haruna Sani Haruna Bashar S Mohammed Ibrahim G. Shaaban Ibrahim G. Shaaban](https://www.researchgate.net/publication/321158849_Methods_of_curing_geopolymer_concrete_A_review)
* [Use of residual diatomaceous earth as a silica source in geopolymer production April 2018 - Alba Font Jordi Payá J. Monzo](https://www.researchgate.net/publication/324204675_Use_of_residual_diatomaceous_earth_as_a_silica_source_in_geopolymer_production)
* [Fly ash-based geopolymers containing added silicate waste. A review - Nicoletta Toniolo Aldo R.Boccaccini](https://www.sciencedirect.com/science/article/pii/S0272884217316681)*
* Effect of Concentration of Sodium Hydroxide and Degree of Heat Curing on Fly Ash-Based Geopolymer Mortar - Subhash V. Patankar,1 Yuwaraj M. Ghugal,2 and Sanjay S. Jamkar1
* [Rice husk ash and spent diatomaceous earth as a source of silica to fabricate a geopolymeric binary binder January 2016](https://www.researchgate.net/publication/292342580_Rice_husk_ash_and_spent_diatomaceous_earth_as_a_source_of_silica_to_fabricate_a_geopolymeric_binary_binder)

#### Biomimicry

* [Direct Ink Writing of 3D Functional Materials By Jennifer A. Lewis](https://lewisgroup.seas.harvard.edu/files/lewisgroup/files/lewis_afm_2006.pdf)

#### Synthetic Biology, CRISPR and Diatoms

* [Transgene-free genome editing in marine algae by bacterial conjugation – comparison with biolistic CRISPR/Cas9 transformation -
A. K. Sharma, M. Nymark, T. Sparstad, A. M. Bones & P. Winge
(2018)](https://www.nature.com/articles/s41598-018-32342-0)
* [Genetic and metabolic engineering in diatoms - Weichao Huang and Fayza Daboussi (2017)](https://royalsocietypublishing.org/doi/pdf/10.1098/rstb.2016.0411)
