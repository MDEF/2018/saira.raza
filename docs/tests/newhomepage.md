

# Fictions

<p>I think that stories are humans' most powerful communication technology.</p>
<p>Like material technologies, they are embedded with decisions about information and therefore with political decisions, impositions and questions.</p>
<p>The substance of stories are roles that perform functions in relation to one another and actions that have consequences. The actions of the roles and how their environment responds gives the role a character, an essence that becomes a symbol (sometimes whose name even spawns a word) that stands for a combination of behaviours and experiences and defining moment.</p>
They contain a lot of dimensions of information that humans easily visualise, remember and love transmitting to each other and from generation to generation, imprinting more of their nature into them with each telling - like DNA evolving.</p>
<p>Might story structures even tell us something of the architecture of our neurones and how we process all information? </p>

<p> I will use this space to inspect fictional narratives that speculate about the nature of the universe and the future for the dimensions of information they contain, what political messages and questions they raise and to see whether any symbols contained in them echo across genres and time.</p>

<p> I would like to ultimately compile the findings into a short history of science fiction told through an animated video.
<br>
<br>


<div class="container">

  <div class="right">
  </div>

  <div class='spacer'>

    <a href="https://mdef.gitlab.io/saira.raza/fictions/aliencovenant" class='wide blue'(text);>
      <h2> Alien Covenant </h2>
    </a>



    <a href="javascript://" class='wide orange'; https://www.google.es/>
      <h2>Arrival</h2>
    </a>

    <a href="https://www.google.es/" class='box redgay'>
      <h2>Tron</h2>
    </a>

    <a href="javascript://" class='box lime'>
      <h2>The White King</h2>
    </a>

    <a href="javascript://" class='box bluefish'>
      <h2>Sunshine</h2>
    </a>

    <a href="javascript://" class='box yellow'>
      <h2>Annihilation</h2>
    </a>

    <a href="javascript://" class='wide gallery'>
      <h2>Gallery</h2>
    </a>

    <a href="javascript://" class='box redgay'>
      <h2>Browser</h2>
    </a>

    <a href="javascript://" class='box orange'>
      <h2>The Thing</h2>
    </a>

    <a href="javascript://" class='wide blue cal_e'>
      <h2 class="top cal_i">Total Recall</h2>
    </a>

    <a href="javascript://" class='box lime'>
      <h2>Pi</h2>
    </a>

    <a href="javascript://" class='box blue'>
      <h2>Black Mirror "Metalhead"</h2>
    </a>

    <a href="javascript://" class='box bluefish'>
      <h2>Solaris</h2>
    </a>

    <a href="javascript://" class='box redgay'>
      <h2>Task</h2>
    </a>

    <a href="javascript://" class='box magenta'>
      <h2>Something Else</h2>

    </a>
    <a href="javascript://" class='wide yellow'>
      <h2>Media</h2>
    </a>

  </div>

</div>


<div class="square"></div>



----------

### Hang the DJ (Black Mirror) 2017
Extreme Reinforcement Learning to find love

###
