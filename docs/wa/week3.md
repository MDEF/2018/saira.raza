# 03. Design for the Real Digital World

## Brief
Use found materials to customise room 201 in IAAC building 102 Carrer de Pujades.



![201 Floor Plan](assets/w3/floorplan.png)



## Design Process



![flow diagram](assets/w3/week3flowdiag.png)



The class was divided into 4 groups each with at least one person who is good at 3D modelling and one person who is good at documentation. My group consisted of Ollie, Barbara, Veronica, Laura, Alexandre and myself.
Each group presented floor plans and features for the room.

## Floor Plans

**Group 1**

![Group 1 plan](assets/w3/w3group1plan.png)

**Group 2**

![Group 2 plan](assets/w3/w3group2plan.png)

**Group 3**


![Group 3 plan](assets/w3/w3group3plan.png)


**Group 4 (my group)**

![Group 4 plan](assets/w3/w3group4plan.png)

### How we arrived at our floor plan

We worked individually and then shared our ideas about floor plans. We all reached different levels of detail. Some of us were concerned about fitting in fixed workspaces for each student while others were more interested in finding ways to create interesting shared spaces.

**Fixed Individual spaces**

Ollie was able to produce the layout below with to-scale tables allowing each student to have half a table as a fixed personal workspace. This attracted us because as the course progresses we will need to spend more time working on our individual projects and lockers may not be large enough to store our materials.


![Desk plan](assets/w3/w3group4plandesk.png)

**Dynamic shared space between entrances**

The layout was also conducive to encouraging flow through the room without disturbing fixed workspaces and opened up the design to include the possibility of mobile furniture which is both easy to rehouse to other locations at the end of the year or even take home! Also we thought we could use this flow to encourage a participatory feature-piece that passers by could contribute to. We agreed this was an interesting layout to explore.


![Flow plan](assets/w3/w3group4planflow.png)



## Functions and features proposed by the groups

| Space Use | Group 1 | Group 2  | Group 3 | Group 4 |
| --------  |:--------| :-----   | :-----  |:-----   |
| Coffee Area |  | Coffee counter with mushroom grower and plants |Coffee Machine | Coffee area on balcony |
| Relaxation Area | Part of multifunctional area |     | Reflection area near coffee station | On balcony |
| Presentation Area | Projection screen moved to back half of room | Projection screen moved to back half of room  | | Stays where it is |
| Making Area | Tool storage, Dynamic table, 2 Soldering table, 3D printer stable table |  |  |  |
| Multifunctional Area with modular furniture |  Space for reflection, meeting, reading chilling |   | movable multifunctional screens, large table| Movable multifunctional screens and furniture |
| Fixed work stations | Shared fixed workstations  |  | Near the windows, includes shared tools | Each person has a fixed half desk|
| General Storage |  | Fixed Shelves across columns | Fixed Storage unit made from drawers | movable modular furniture |
| Other features |  |  | Recycling unit |  |


### Results of the vote
<p align="center">
![Vote](assets/w3/vote.png)</p>

## Finding materials


![junk collection days](assets/w3/poster.jpg)
![Found items](assets/w3/junk.jpeg)
![Map for junk collection](assets/w3/w3mapofjunkdays.png)

## Final brief for group 4


**1. Design an object that can be use to mark desk space and be used in combination with other units to form something bigger.**

**2.	Make the columns more useful.**


### 1. Multiuse Modular Unit
#### Concept
We looked at the idea of the unit being a puzzle that could be put together to make something larger. We found some reference images of simple parts making complex twisting tables and using Rhino I was able to produce component parts.
<p align="center">
![Twisting Puzzle Table design](assets/w3/w3twisttable3d.png)</p>


Alexandre produced these initial sketches for modular seating/ tables in our presentation which were a really strong starting point for us.
<p align="center">
![Multifunctional Modular segments](assets/w3/w3alexpresdrawings.png)</p>
He had looked at tessellating geometries we could utilise.
<p align="center">
 ![tessellating  segments](assets/w3/w3alextessellation.png)</p>
I enjoyed playing with tessellations to see what other possibilities we had that might make construction easier, more resourceful or more useful.
<p align="center">
 ![tessellating  segments](assets/w3/w3tessellatingsketch.jpg)</p>


 We looked at geometries to increase the number of uses for each module.
Uses we tried to incorporate included:
* Full height (74cm) side table
* Laptop stand for a desk
* Desk tidy for pens and small objects
* Pin board
* Tessellating tables that could make a larger meeting table

I found 3D Modelling was especially good at helping to visualise how to do this.
<p align="center">
 ![t section joined pieces](assets/w3/w3tsectionoption.png)</p>
<p align="center">
 ![square pieces](assets/w3/w3othertesselatingdesign.png)</p>


We discussed the benefits of strength and number of component parts needed against the number of uses we could get out of the finished construction.

#### Laser Cutting a Scale Model
We decided to make scale models from laser cut 3mm plywood.
We used a 5-piece construction and trialled 3 different cross sections

![cross section sketches](assets/w3/w3facesketches.jpg)
</p>

![cross sections tested](assets/w3/w3faces.png)</p>

![laser cutting](assets/w3/w3lasercutting.png)   ![model](assets/w3/w3models.png)

#### Final design
We used the models to refine the final shapes of components:
* The design was modified to a 6 piece structure which was more stable for supporting weight in the horizontal position and allowed more accessories to be added.
* Joints were pocketed (not slotted) so they were easier to machine with a CNC machine.
* Spaces for attachments were included and these attachments were designed before machining.


![finished design](assets/w3/w3finishedcaddrawing.png)


![accessory for 3D printing](assets/w3/w3accessory.png)  ![fabric accessory](assets/w3/w3fabricaccessory.jpg)

#### CNC Machining
Ollie spent several hours preparing 3D Modelling files of the final design and overseeing the CNC machining.

<p align="center">
![cnc machining](assets/w3/w3olliecnc.jpg)</img></p>

<p align="center"><img style="float:right">
![cnc machined plywood](assets/w3/w3CNCpieces.jpg)</img></p>

We all sanded the parts and used wood glue to secure the pieces

![gluing tables](assets/w3/w3gluingtables.jpg)
![tesselating tables](assets/w3/w3tessellatingtables.jpg)

#### 3D Printing
  Ollie was able to use the Rhino model he prepared for the CNC machine to prepare a file to 3D print an accessory pot overnight. He took this photograph which shows the white 3D printed pot.
<p align="center">
  ![3d printed pot](assets/w3/w3finalwaccessories.png)</p>

#### Use of the finished product

<img style="float:right" src="assets/w3/w3juliaonstool.jpg"/>
So far the modular unit has been taken up well for its use as small individual table and stool.
It's use as a desk tidy or laptop stand has not been tested fully yet but looks promising.
The animation below is by Veronica and last photo was taken by Barbara
</img>
![Unit as a stool](assets/w3/w3juliaonstool.jpg)  
![Table Animation by Veronica](assets/w3/tableanimation.gif)  
![Unit as a tessellating table](assets/w3/w3meattable.jpg)


### 2. Use of Columns


  Alexandre, Veronica and Laura experimented with how to build around the columns without drilling and ended up creating a un-invasive freestanding shelf network.

  ![Building shelf](assets/w3/w3laurashelf.jpg)   ![Building shelf](assets/w3/w3shelfphones.jpg)


  Alexandre later found that our drill was not powerful enough to drill into the columns ...

  <p align="center"> ![Drilling Column](assets/w3/w3alexdrill.jpg)</p>

  Alexandre had already suggested using specialised paints to enable the columns to be used as whiteboards or pin boards


![Column Painting sketch](assets/w3/w3alexcolumnsketch.png)

..so for the second column we opted for the less invasive interventions of painting and sticking useful surfaces to the column.

  I sourced some magnetic paint for one side of the pillar.
  <p align="center">
  ![Column Painting](assets/w3/w3alexpaint.jpg)</p>

  Barbara found a light mirrorlike finished MDF from the back of a cabinet and Alexandre found some cork.






### Furniture on Balcony
  Laura built these table / seating units for the relaxation area on the balcony which are very useful and were received well.
  <p align="center">
  ![Balcony Furniture](assets/w3/w3balcony.jpg)</p>


## Results from other groups

### Maker space – Group 1
<p align="center">
![Maker Space](assets/w3/minifablab.jpg)</p>


### Coffee area with mushroom grower – Group 2
<p align="center">
![Coffee station](assets/w3/w3coffestation.jpg)</p>


### Separator Screen – Group 3
<p align="center">
![Screen](assets/w3/w3screen.jpg)
</p>

  ### Storage unit – Group 3
<p align="center">
  ![Storage](assets/w3/w3storage.jpg)
</p>

## Reflection
  <p align="center">
  ![Features in place](assets/w3/w3fullworkspace.jpg)</p>

### What I learned this week
It was empowering to learn that the CNC machines, laser cutters and 3D printers in the FabLab can all work from Rhino files. This encouraged me to use Rhino as an exploratory tool to produce drawings for components for constructions. I learned how 3D modelling tools can be used to calculate dimensions of precise shapes easily.

It was useful to learn how much preparation was required before machining using the laser cutter and CNC machine as well as the time required to cut.

In using found furniture this week I learned that design can increase the re-usability (and therefore the lifespan) of components beyond their initial intended use - which increases the energy efficiency of the component before it is scrapped. It would be interesting to see if this approach could be a norm in certain industries (perhaps in fashion and textiles).

I found this week that by sharing designs and expertise on how to build things, we can collaboratively start to understand how best to use and repurpose things to fit into an “ecosystem” of things and of construction.

### How I learned this week
* I learned from members of my work group who are designers that research and reference images are very important to avoid reinventing the wheel and also to save time when working to short deadlines.
* I learned from my classmates on different ways to approach a design problem – some designed more through making and others through an existing understanding of materials and construction (the latter being more typically used in engineering).
* I learned how things might be constructed by using 3D modelling tools as exploratory tools.
* I learned from demonstrations by fab lab staff on how to prepare laser cutters and CNC machines for use with interface software.
* I learned from exploring the shops in the neighbourhood what materials are readily available in Barcelona.
* I learned by trying to build prototypes using found materials how designs might be improved.




### How this week’s activities relate to my work

I try to use as much reused or repurposed equipment as possible in my work - from my computer to my furniture and keep a hoard of raw materials and components. This weeks activities have helped me see how I can use a Fab Lab to utilise my materials in ways I was not able to before. I also feel more tuned in to the material resources available in the neighbourhood.



Digital Fabrication necessitates 3D modelling and this tool has helped me to understand shapes in 3D and how best to design and construct objects.

I look forward to using new fabrication techniques to improve my ability to use found resources.
