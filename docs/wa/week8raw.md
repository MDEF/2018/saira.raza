# 08. Living with Ideas

> Material speculation emphasizes the material or mediating experience of specially designed artefacts in our everyday world in order to speculatively and critically inquire through design.
A counterfactual in material speculation is a virtual or tangible artefact or system rather than a statement or text.
The counterfactual artefact is also an embodied proposition that, when encountered, generates possible world accounts to explain its existence.

This week we tried using some experiential and speculative design methods.

## Using experiences of counterfactual artefacts to inform design

**The brief:**
We were all asked to bring in an ordinary household object to modify. We each picked an object that was not ours and trying to modify it to fit a fictional scenario with a "What if.." question as a starting point.

** My objects: **
I was drawn to a sleeping mask and a Rubik cube puzzle.
The sleeping mask made me wonder about the possibilities of using or optimising what happens to our bodies during sleep.
I liked the idea of the Rubik cube puzzle functioning like a security feature that contains something very precious - this is mentioned in fictional stories such as Hellraiser, Chronos, Theseus and the Minotaur, Stargate and many others. In these stories solving the puzzle often unleashes something extremely powerful..good or bad. The stories reflect humans innate curiosity and desire to solve problems but are often cautionary tales.

** The Question: **
What if we could optimise the functions of our sleep o become psychologically and physically healthier ?
Our bodies naturally use sleep to maintain the function of our bodies but in the quest for being more content and productive will we try to optimise our productivity in the time we spend unconscious sleeping.

**My Response:**
**The Sleep Keeper:** An Eyemask with an activator that stimulates the brain and a controller/data storage device

![Sleep Keeper](assets/w8/sleepkeeper.jpg)

**Experiencing the object:**
* I tried to set the sleep cycle using the programmer / data storage. and then have a nap wearing the eyemask.
* The data storage was not something i could keep easily on my person and I have to put it to one side. This made me worried that my precious data was not safe because I could not see it and someone might take it.
* I felt that I wanted a data storage / controller attached to my body preferably on my eye mask so I would notice if it was not on me.

**Reflection:**
* The thinking behind the function of the object was quite rushed and the 'experiencing' stage would have been easier if the object had a more important purpose.
* Going through the ritual of going to sleep was important because it is integral to the uptake of the object.
* The size, feel and location of the object was easier to do by designing around the experience.

In researching my interests this week I came across a project called [Pillow talk](https://www.edwinaportocarrero.com/PILLOW-TALK) by Edwina Portocarro and David Cranor
>"It consists of two devices, one of which is a pillow embedded with a voice recorder that is activated upon squeezing together several conductive patches at the corner of the pillow. This interaction minimizes the steps necessary to record a fresh memory of a dream immediately upon awakening.
After the dream is recorded into the pillow, the audio file is transmitted wirelessly to a jar containing shimmering LEDs to display the "capture" of a new memory, and electronics in the jar can play back the recordings through a small speaker under its lid when it is opened. "

Although this device was about recording dreams rather than controlling dreams the physical exploration of the form felt really familiar to my experience. A paper documenting their design process can be found [here](https://dam-prod.media.mit.edu/x/files/wp-content/uploads/sites/10/2013/07/4p375-portocarrero.pdf).

![Pillow Talk jar](assets/w8/pillowtalkjar.jpg)

*"After the dream is recorded into the pillow, the audio file is transmitted wirelessly to a jar containing shimmering LEDs to display the "capture" of a new memory, and electronics in the jar can play back the recordings through a small speaker under its lid when it is opened." - www.edwinaportocarrero.com*

https://www.media.mit.edu/videos/edwina-portocarrero-defense-2017-08-01/

## Building an artefact, system and situation for a speculative material

We used a greenscreen app to experiment with different ways we could use a speculative material (one that could display digital images and video).

**Brief:** Think of an item of clothing and a situation where you could use a speculative material that can display digital images and video.

**My Response: Protest Balaclava**

I thought very broadly that the technology could be used to either blend in or stand out and make a statement.
A place where you might want to make a statement is at a protest. You might also want to show unity with others at a protest which you could arrange with this technology (like audience light shows at sporting events).

![Audience Display](assets/w8/olympics.jpg)

*Olympic games 2012 light display using audience members as pixels*

You might also want to remain anonymous if you think you might get arrested.

I thought an item of clothing you might wear at a protest to make a statement or to remain anonymous might be a balaclava although these are frightening because you can't see facial expressions and balaclavas have connotations of violence - so the object might be counter productive.



**Experiencing the object:**
* I spent a lot of time learning to use the technology. It was interesting to try new ways of using the technology.
* I also experienced how it felt having someone else use me as a canvas for their experiments which could be something that could happen with this material if it existed. It felt slightly intrusive. THis might be especially dangerous if  they can project images onto where your face should be.




## Magic Machines - Materialising an abstract idea

**Brief:**
Today we chose as a starting point a value from a list of values that represented abstract needs - we designed speculative objects that addressed these abstract ideas of needs - trying to meet them or to tackle them - usually both.

**Need:** Acceptance
**Implied action needed:** the need to be appreciated
How I feel about this definition: I don't think acceptance equated to the need for appreciation - to me it is the need for inclusion and validation.

**Name of Machine:** Role Model Projector

![Pojector](assets/w8/rolemodel1.jpg)

**Function:** The Role Model Protector is an embodied interactive AI that is able to:

*  converse with you using sound and use a human form of your choosing to use body language.
* It can project videos and images onto a surface to show you examples of real people and fictional characters who look like you or are from similar backgrounds who face similar challenges to you - reflecting parts of your experience back to validate them.
* It can project several unspecific scenarios of positive feedback to use reinforcement learning to learn that you are accepted by many different types of complex characters
* It can access and reinforce real memories of positive feedback.

**What is the 'magic' in the machine?:**
The magic is the ability to hack human ideas of the self.

**Reflection on the design process:**
* I found that I had to spend a lot of time thinking about the need and what it meant to me. Then I had to think about the causes of the need. I felt that research would have been beneficial to the process if more time was available.
* I felt I was lacking some skills in fast prototyping.

## How do we value work?
An inquiry into how people value work with a view to question current wealth/ salary distribution.

<iframe width="560" height="315" src="https://www.youtube.com/embed/D7bRgE8le-o?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Above: 360 film about the future of work Merger by Keichi Matsuda. "An international city in 2030. Automation and big corporations are taking over.
A top accountancy graduate from an elite university has made it her mission to help small business in their brutal, winner-takes-all world.
She works night and day in her control centre, dedicating herself to the efficient workflows that will help her clients survive. But her obsession with productivity and drive for total control has come at a cost.
As the potential of the Artificial Intelligence networks around her grow, she finds herself at a crossroads. Should she remain in the physical world? Or upload herself to the network and become the ultimate worker?"*


How can we reward work?
 - by exchanging material things that people need for their work (barter)
 - by giving them money "a joker card" that they can exchange for skills and things
 - by emotional, intellectual and physical returns from the job
 - by exchanging our skills for it?


 what is work from the point of view of the worker?
 - an investment for the things we need and value.
 - an investment into a system of money

 what do we invest into doing work / earning money?
 - physical, emotional, intellectual investments
 - time
 - choice

What do people want in exchange for work?
 - money (access to a virtual value system)
 - leisure
 - survival
 - self expression
 - things based on guiding values - world value survey sets out a map of values are survival, self expression, rational and traditional
 - things based on their personality and psyche: five broad dimensions commonly used to describe the human personality and psyche: openness to experience, conscientiousness, extraversion, agreeableness, and neuroticism,
- physical, emotional, intellectual returns
- dimensions of wellness

What returns can we get from a job?
 - physical, emotional, intellectual returns
 - autonomy
 - meaningfulness
 - criticality
 - skill variety
 - task variety
 - money

http://www.openlearningworld.com/books/Job%20Design%20and%20Enrichment/Job%20Design%20and%20Enrichment/Approaches%20to%20Job%20Design.html


### 1. Data Gathering - Why do you do things and what do you get in return - Actions as investments
Gathering data on the links between the things we want/value and the things we do for them.
This project did this in the USA http://wearethe99percent.us/main/page_graphics_info_graphics.html

#### 1a. Data Gathering by experience: A Diary of everything I did in 20 hours, why and what it cost me.

I kept a diary of every activity I did for 20 hours and then attributed costs and rewards for these activities - analysing each action as an investment.
I recorded the time spend, costs and purposes of each action and arranged the data in the mind map below.

<iframe
  src="https://embed.kumu.io/a19046f2044b8164ab387479e4cc2a7e"
  width="940" height="600" frameborder="0"></iframe>

**Observations:**
* Watching your own behaviour is a direct way to start mapping variables.
* In keeping very specific I was able to see the extent to which longer term choices were governing many of my decision - why I eat certain foods and don't go out much, why I am studying . I was able to unpicking my decision process which led to values governing those decisions.
* Qualitative data over time is easier to analyse if recorded well - video recording can help you see things you might not have seen otherwise.
making data devices -  dont forget about qualitative data -
* It was interesting to take note of emotional costs as these actions as real things
* a lot of my costs were money costs

#### 1b. My body as the system - a system without money
In noticing how many costs of my actions were monetary I wondered if we could use the human body as the system and only look at the costs and payoffs of different jobs. If we want to find an alternative to the current money system we need to look at the flow of action and needs without it.

I modelled myself doing my last role as a researcher as a system.

 !diagram of incoming and outgoings

**Observations:**
* values
returns from the job and outside of the jobs

#### 1c. Another body and another system
I realised that the data I had was only about me and that in order to look at work in general I need to look across several jobs - this was the initial purpose of the question.
I interviewed my flat mate about her job
**Observations:**
* she mentioned a new thing - sacrifices which could be a subset of investments - because they are things you have to let go forever




#### 1d. Data Strings to see flows of  'investments'/costs to wants/needs across different jobs

In order to capture experiences of a wide number of jobs i looked up if there were some theoretical or empirical generalised parameters or indexes of jobs investments and return.
As starting points I found:
* this document about job creation which I used for a category of "Returns from your job" as well as "Investments into your job"
* Lazlo's hierarchy of needs which I thought could represent a category of "Returns from the wider world outside of your job"
* and this world values survey generalising 4 broad categories of "Values which guide your work"

From the interview I did with my flat mate I wanted to add
* a category of irredeemable investments (except time - because everyone loses that) called "sacrifices".

This led to the following data string board

![Datastrings](assets/w8/datastrings.jpg)

**Observations:**
* I was able to add items to sacrifices that I might not have thought of.
* I realised that this method was really a way of collecting more ideas on the parameters that describe the investment of work.
* visualising more than one job together made imagine ways of showing how investments and returns of different jobs are interrelated.
* By doing a data string for myself I realised that I have changed careers several times to try and 'tweak' the investments and returns of work - initially working for safety by sacrificing emotional fulfilment or in some cases any fulfilment from my job. Later I tried jobs which gave me high emotional fulfilment but high emotional investment  and very low security and esteem and a mismatch with my values. I find myself studying now to align my intellectual investment and sacrificing some choices in order to align with my values, gain intellectual and emotional fulfilment in studying and hopefully ultimately fulfilling all of Lazlos hierarchy of needs from the wider world.
* I realised that maybe my search for finding a way of understanding how different jobs feel and are interrelated is in one way a search for validation of all the different roles I have worked in.

### 2. Materialising the concept of work
I wanted to look for an analogy that might materialise the concept of work as something you invest in with a hope in getting a return. I would want to use that analogy to help people understand how different jobs feel to different people and that your returns require someone else's investment into a job not just money. I wanted to remove the sense of entitlement that money can give you.

#### 2a. Analogies of currency

**What if work was rewarded in time**

<iframe width="560" height="315" src="https://www.youtube.com/embed/5K0s6eveTsI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



The space opera Consider Phlebas by Iain M. Banks features coins convertible for chemical elements, land, or computers


a money-free economy may still need a unit of exchange: in The Great Explosion by Eric Frank Russell, the Gands use favor-exchange based on "obs" (obligations)

* A material analogy of the relationship between the free market and work


#### 2b. Work and machines
* A machine where you have to take time and sacrifice something (choice) in order to get something else you want or need mediated by economics. The definition of a machine is something that does work.


#### 2c. Work as a strategic game
* A very obvious and overused analogy is a game where you have to stay on track to do something to ultimately get what you want (like many platform video games)


-




## Reflection on the week

### Finding bad practices
I found myself asking bad questions which led to some overengineered responses.

I made a some bad unresearched analogies about the function of the brain being something that functions like a computer.

The protest balaclava went from an idea to a terrible idea very fast.

### Questioning objectivity
These methods oppose the objectivist view of reason being the source of all information and support the view that human experiences are unique.

### Using time as an agent
Phenomena and Understanding and take time - time helps build context and improves chances of insight.

#### Unpacking behaviour and motivations


#### Experimenting intuitively
reinforced ideas of outputs being made objects


#### Dynamic and generative processes
We were advised to try jumping between levels of examination - from personal to cultural to global or from material, to the body to the system. This method should be dynamic and generative.

### Tools to work across scales of influence
Tomas explained the thinking behind the path of inquiry within the MDEF course.

alligning systems rather than creating new systems to insight changes

### The importance of leveraging your interests

[Repository of interests](https://mdef.gitlab.io/saira.raza/interests/)


## References

### Websites

* http://wearethe99percent.us/main/page_home.html

### Research Papers

* HOME ARCHIVE MARCH + APRIL 2016 A SHORT GUIDE TO MATERIAL SPECULATION: ACTUAL ARTIFACTS FOR CRITICAL INQUIRY Ron Wakkary, William Odom, Sabrina Hauser, Garnet Hertz, Henry Lin

### Books

* Bullshit Jobs - A Theory By David Graeber
