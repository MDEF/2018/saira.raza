
# 01. MDEF BOOTCAMP

<center>![Week 1](assets/mars.gif)</center>



## Motivations and Expertise

It has been a new experience to discuss ideas about the future with people from a variety of backgrounds who are committed to future-focussed design.
We found we have a wide variety of expertise ranging from human geography, politics, entertainment and business to architecture, industrial design, interior design and communication design.
Our motivations crossed over a lot and it was also a new experience to form a group with people based on their vision for a project. Our group wants to work on ways of making systems (governance / design) highly participatory. We have been talking about the subject of housing.
//
## Shenzhen
It was proposed that we may go on a field trip to Shenzen, China - the maker capital of the world.
This documentary gave me a sense of its unique role in  China and the world.

<iframe width="560" height="315" src="https://www.youtube.com/embed/SGJ5cZnoodY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Sustainability
We thought about sustainability with Tomas Diez. He highlighted our current reliance on fossil fuels and asked if we can continue consuming the way we are today. He posed the question is there is a way to make the consumption sustainable whilst supporting the culture of consuming out of desire.
His work with Fab Cities made me think about how sustainable cities cross with ecological systems.
We also thought about how data can be opened up to enable citizens to see what is happening in the world to help them make better decisions and improve accountability. This led us to think about whether or not people want data to make their decisions.

## Information
Mariana Quintero spoke to us about documenting and expressing information in different ways. I found it interesting how she encouraged developing our own taxonomies or personal geographies of meaning.

## Designing through Experience
We learned from Oscar Tomico about how important the experience of the wearer is in wearable tech projects - context and body and material can all be used to shape the outcome of an intervention. What struck me is that it’s ok to not know before you start what you are making.

## Poble Nou
We visited a number of studios and workshops including a more formal architects studio, a upcycling makerspace / materials library and a bicycle cooperative.
There were regular events such as the open night which is on 23rd November when these organisations and the local community can see what everyone is doing. IAAC will also be open this evening which I think could be a good opportunity for us to showcase our skills.

 ![Picture of BiciClot](assets/20181004_185339.jpg)

<iframe src="https://player.vimeo.com/video/293929912" width="640" height="1138" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<a href="https://vimeo.com/293929912">Video of brain installation</a>

It was interesting to visit the superblock in Poble Nou as it was quieter than I had imagined and we were able to see that there was some visible opposition to it which i did not expect.


## Proposed process of learning throughout course

## Learning process this week

* **Exploring what motivations and expertise we have within our class - building work groups based on our motivations.** In my work as a researcher I need to find people with expertise and motivations relevant to my projects.

* **Exploring the organisations, networks and sources of materials and expertise that exist around us in Poble Nou.** Exploring local networks and organisations have helped me get information directly linked to my research and also have helped me with accessing equipment.

* **Documenting ideas as notes, photographs, videos and animations.** Documentation has been key in all my work as themes do not appear in a linear way and only by documentation can I remember all points that are interesting.

* **Finding ways to arrange ideas ( mapping, making a website) so patterns or themes start to emerge.** Finding ways to arrange ideas is the interesting part of research. A tool or a model might emerge or gaps may become evident. This step enables constructive next steps to happen.

* **Learning how to use new tools such as GitLab, Rhino, Aftereffects and Arduino to enable better presentation and potentially new ways of exploring ideas. Learning relevant skills from teachers and classmates.** I think learning to use GitLab and Rhino can help me to express ideas in new ways. I’m already enjoying Rhino  and hope to be more fluent in it so I can use it as a tool to fabricate prototypes.
