<meta name="keywords" content="Symbols, Philosophy, Politics, Inclusion">
# 05. Navigating Uncertainty

## Reflection

This week's discussions felt like groundwork for the MDEF course - asking where we are in time, how we feel, what tools we have and how can we (philosophically) use them. By the end of the week I felt more enabled through a new vocabulary to use my intuition to place and guide my work.

## José Luis de Vicente
José Luis de Vicente is curator and researcher working on digital culture, innovation and new media art.

### Placing ourselves in time
The session placed us in time by using scales, milestones and narrative to link data and events and actors concerning climate change and the Anthropocene.

### Describing feelings towards a wicked problem
The oil industry, water management, vulnerable island nations, fragile ecosystems with endangered species, rights-based decision making, behaviour change economics and some specific locations mentioned are all contexts that I have had direct experiences with and the narrative José Luis told felt somewhat autobiographical. The indifference of the oil industry, the desperation of trying to bring clean water to people in emergencies, the dark arts of communication programmes - are hard enough to tackle on their own so the realisation that change needs to happen across the board is frightening and hard to envisage.

José Luis likened our collective reaction to this data and events to trauma experienced after receiving bad news from a doctor - where the scales of the change are outside of our understanding.

In the exhibition "After the end of the world" that José Luis curated he used speculative devices to help the audience come to terms with how they feel and the nature of their position in this situation. They serve as a tools to unpick the reaction to trauma that they may be experiencing.

###  Long term politics
We heard about how long term politics had no benefit to politicians in current political systems and this compounds our inaction. This was particularly poignant to hear in the light of the USA recently pulling out of the Paris Climate Agreement as well as Brexit which is looming on the horizon.

### Speculating ahead to post-Paris futures
José Luis shared with us political camps of thought on development following the Paris Agreement:
- **De-growth Creationists** - slow down growth and get people to produce less Co2 by changing our culture and politics around consumption
- **Capitalists** - produce products that reduce co2, let consumption culture continue. Let systems adjust around markets and products.
- **Extinctionists** - Accept that humans' time may well be over on this planet - our behaviour is inherently unsustainable and we cannot change it in time. Explore colonising or using resources from other planets or selectively save parts of our species for future regeneration. Save our data (consciousness) into other storage systems.
- **Accelerators** - Devise one-shot, irreversible interventions to alter Co2 levels
- **Mutulatists** - better understanding of the environment and symbioses may enable humans to become more sustainable

These speculations about human development in the future have been explored in science fiction.
I hope to elaborate further on the historical and political dimensions of these narratives throughout the progress of this course as part of **<font color="blue">a side project on fiction</font>** **[here](fictions.md)**.
<!---   
<script src="//www.powr.io/powr.js?external-type=html"></script>
 <div class="powr-tumblr-feed" id="9dec42a2_1542116390"></div>
 -->

## Pau Alsina

Pau Alsina has a background in Engineering & Philosophy and advises designers and artists on how they can incorporate their practice into PhD projects.
https://paualsina.wordpress.com/
Below are my understandings of some of the ideas he presented.

### Design contains a history of decisions
All design is in some sense borrowed and all objects contain a history of design, politics, manufacture, crafts, economics, demand. This reminded me of hearing that "life is about copying" in week 2 and also in the utilising of reference designs by my classmates who are professional designers.

### Design is always political
Information cannot be known without the material.
Designers are interested in what information does (not in it's inherent nature) and make decisions about both information and materials.
Designers also exist in social systems.
If Society is an information exchange (discourse) between people and if information is actioned by material things, social discourse can also be seen as systems of material things carrying and therefore influencing information with all the decisions they contain.

Politics is the social process of making decisions with information but Design implements political decisions in the material world.
Design and Politics provoke and question each other but they are also connected to economics, psychology and every other way of processing information. Design  embeds all this information into materials.

Design can reinforce or challenge existing power structures.

### Material things carry information which can answer or ask questions
Design poses questions as well as answering them and the information system may not receive the change you intend.
Design can create **"black boxes"** out information accumulated from a history of material and political decisions by reducing them to seemingly physical ones.

**Speculative design** is the opposite of the "black box" approach and allows the user to examine the information contained in the material object, its origin, its layers of decision-making and how the user might place themselves in relation to it.
Speculative design can utilise fictional scenarios to do this.

> "political value resides on its ability to unfold a fictional scenario that operates as a polemical playfield in which sustainability does not emerge as a technical problem requiring a technical solution, but as a new system of cohabitation, a new cosmopolitical regime, which not only requires the production of new technologies, but also the production of new bodies, a new set of cultural practices, as well as a new set of connections and attachments between all these elements"

  *'Unfolding the political capacities of design' - Fernando Domínguez Rubio & Uriel Fogué*


## Mariana Quintero

We can look at everything in terms of information and every material thing as a potential technology (or changer of states to some end).

### Read-Write Technologies
All Physical systems are based on information interchanges. Read-Write technologies occur at the limits of information systems - they link systems of information.

These points of information exchange determine how the attached systems organise them selves.

These limits are not necessarily membranes.

All human culture is based on Read-Write Technologies.

They can be languages, made up of symbols, alphabets and ways of recording languages

![language tree](assets/w5/tree_1.png)

*Minna Sundberg's Tree of Languages*

**Maps**

Spatial distributions of data help us to visualise relationships.

![African Village](assets/w5/fractalvillage.jpg)

*Fractal model for Ba-ila village by Ron Eglash*

**Machines**

Read write technologies transform information from one form to another

![record stylus](assets/w5/stylus.gif)

*Gif by Applied Science*

DNA is read and written to transform information about our environment into the structure of living things.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xSQybiea2AA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
*Animation by Drew Berry*

**Sensors**

Read-Write technologies are extensions to our senses, stretching the limits of our physical senses.


![higgs](assets/god-particle-higgs-transpcopy.png)


*A interpretation of an image by CERN showing CMS proton-proton collision events characteristic of Higgs boson decay (Four high energy electrons shown in green lines and red towers.)*

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FATLASexperiment%2Fvideos%2F609292082821487%2F&show_text=0&width=476" width="476" height="476" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>



#### Questions about Consciousness
Observation creates a system.

Is there a consciousness behind every observation and system? Can a system create a consciousness? Has the internet created a new kind of collective consciousness?
is consciousness fractalling?
What interfaces occur between consciousnesses?


#### Symbols
> All nature is a symbol exchange

Symbols are a technology that enable reading and writing of information.
Symbols can interface with quantitative and qualitative information.
Symbols can be talismans to other dimensions - of faith and mystical experience, embodied with stories and character.



### Interface technologies drive ways of organising societies, roles and identities

Different ways of organising data (linear , cyclical, hierarchical, using similarity, multiplicity, spectra, continua, modular,  wavelike) are reflected in technology, politics and art.

Harnessing of new materials leads to new types of interfaces (technologies) being developed which provoke new ways of organising people (political systems and roles and identities).

Materials + ways of organising information → Interfaces (technologies) → Political systems (ways of employing technologies to organise people) → Roles (relationships to technologies and society) → Agency (ability to affect technology and society)

An interesting example Mariana gave was of the  concept of quantum information - being able to be many states at once which is being used to develop quantum computers. This seems to be occurring in parallel with development of gender politics and identities - where a spectrum of gender is being explored.

This made me think of something mentioned to me about the development of agriculture running in parallel with the development of patriarchy. It suggested that perhaps the development of agricultural tools → idea of hierarchies of agricultural tasks →  hierarchy of rights of living things → hierarchy in the rights of genders → roles and identities gender roles (especially in relation to the technologies)


## Guiding Insights
* Every material thing contains information therefore every material thing has a political dimension (a possibility to affect politics or exert politics - to provoke or question how humans organise themselves)
* Design binds us to each other with both material and political decisions.
* The information in material things can ask questions as well as answer problems. Whatever you put out into the world - it influence is never finished.
* All material things are in some respect interfaces between information systems. They can be transformative, reflective and generative. What connections do we want to make or question?

### The case for greater inclusion to instigate change
1. We exist in a system of information exchange mediated through systems of decision-making. For paradigm shifts to happen, systems of decision-making need to *conspire for change*.
2. In Physics, nature is modelled to tend towards the distribution of energy or the distribution of 'potential to do work' or what we might call "agency". Biophysicist **[Jeremy England's theory on dissipation-driven adaptation ](https://www.quantamagazine.org/first-support-for-a-physics-theory-of-life-20170726/)** extends this thermodynamic theory to biological forms - explaining different levels of complexity and emergence of order in living forms (life and evolution)to be the result of thermodynamics.
3. We can increase the probability of affecting decision-making systems to conspire if we have more interventions working in parallel towards similar goals. To do this we need to equip more actors with awareness of the information system and with skills to make interventions.
Changing the entire system to solve wicked problems will require involving as many actors as possible. Debate about rights and hierarchies of these need to be established in order to solidify the case for inclusion.



### Finding my path to instigate change
Everyone makes scientific evaluations, has mystical experiences and makes material and political (design) decisions every day. Everyone has multiple roles (and agency) in a system of information, decisions and power.

We can let this system drive us into us playing certain roles - we can feel like the system controls us but can interventions by each agent change the system?

We are part of the system of information but as observers we also create the system.

Doing activities we feel compelled to do towards our visions is the best approach to a solving wicked problems.

**<font color="blue">I will be navigating my interests in a side project</font> [here](https://mdef.gitlab.io/saira.raza/interests/)** which will start with mapping interests and political values and using the framework of information explored this week I will attempt to find flows of information that could trigger ideas for interventions.

### How this affects my practice
* Thinking about the inherent political power of material things has reinforced my **intention to produce material objects**.
* The idea of material objects being often presented black boxes to "consumers" reminded me of feeling left out of understanding of how things worked as a teenager and feeling beholden to them. This led me to study engineering - but even this left me feeling left out of many levels of understanding. I found that I learn best be experimenting, making mistakes and making things work but often education systems and education tools used were not designed for this learning style. There were also social norms around who should learn how machines work (mechanical machines in particular - as they are messy, loud and sometimes heavy). In this context breaking open physical "black boxes" questions consumerism and opposes power structures surrounding education. This analysis has strengthened my **desire to produce objects to learn about and reflect on their mechanics but also to reflect on the political information they contain and the sometimes "mystical experiences" they present to us.** **I would like to explore the inclusive and exclusive experiences we have with machinery / technology.**
* Educational tools are information interface technologies (obvious ones include books and computers). They can promote inclusion. Education gives people more tools to innovate. **I was inspired by the variety of forms that information interfaces take and am interested in exploring new forms of educational tools**.

I am keeping **<font color="blue"> a working repository of ideas for projects inspired by the content of this course</font>** ** [here](https://mdef.gitlab.io/saira.raza/proposals/)**

## Selected Reading List

<style type="text/css" media="screen">
    .gr_grid_container {
      /* customize grid container div here. eg: width: 500px; */
    }

    .gr_grid_book_container {
      /* customize book cover container div here */
      float: left;
      width: 98px;
      height: 160px;
      padding: 0px 0px;
      overflow: hidden;
    }
  </style>
  <div id="gr_grid_widget_1542080966">
    <!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->
      <div class="gr_grid_container">
<div class="gr_grid_book_container"><a title="Elements of a Philosophy of Technology: On the Evolutionary History of Culture" rel="nofollow" href="https://www.goodreads.com/book/show/39732746-elements-of-a-philosophy-of-technology"><img alt="Elements of a Philosophy of Technology: On the Evolutionary History of Culture" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="The Tone of Our Times: Sound, Sense, Economy, and Ecology" rel="nofollow" href="https://www.goodreads.com/book/show/22104589-the-tone-of-our-times"><img alt="The Tone of Our Times: Sound, Sense, Economy, and Ecology" border="0" src="https://images.gr-assets.com/books/1411333903m/22104589.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Ecstatic Worlds: Media, Utopias, Ecologies" rel="nofollow" href="https://www.goodreads.com/book/show/34540050-ecstatic-worlds"><img alt="Ecstatic Worlds: Media, Utopias, Ecologies" border="0" src="https://images.gr-assets.com/books/1502124144m/34540050.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Tactical Biopolitics: Art, Activism, and Technoscience" rel="nofollow" href="https://www.goodreads.com/book/show/2662539-tactical-biopolitics"><img alt="Tactical Biopolitics: Art, Activism, and Technoscience" border="0" src="https://images.gr-assets.com/books/1367785443m/2662539.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Information Arts: Intersections of Art, Science, and Technology  " rel="nofollow" href="https://www.goodreads.com/book/show/745862.Information_Arts"><img alt="Information Arts: Intersections of Art, Science, and Technology" border="0" src="https://images.gr-assets.com/books/1347580631m/745862.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Aesthetics Equals Politics: New Discourses Across Art, Architecture, and Philosophy" rel="nofollow" href="https://www.goodreads.com/book/show/41385470-aesthetics-equals-politics"><img alt="Aesthetics Equals Politics: New Discourses Across Art, Architecture, and Philosophy" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="Design Unbound: Designing for Emergence in a White Water World: Ecologies of Change" rel="nofollow" href="https://www.goodreads.com/book/show/39644238-design-unbound"><img alt="Design Unbound: Designing for Emergence in a White Water World: Ecologies of Change" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="Waste Is Information: Infrastructure Legibility and Governance" rel="nofollow" href="https://www.goodreads.com/book/show/34540074-waste-is-information"><img alt="Waste Is Information: Infrastructure Legibility and Governance" border="0" src="https://images.gr-assets.com/books/1502112021m/34540074.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Linguistic Bodies: The Continuity Between Life and Language" rel="nofollow" href="https://www.goodreads.com/book/show/39644139-linguistic-bodies"><img alt="Linguistic Bodies: The Continuity Between Life and Language" border="0" src="https://images.gr-assets.com/books/1541139665m/39644139.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Speculative Everything: Design, Fiction, and Social Dreaming" rel="nofollow" href="https://www.goodreads.com/book/show/17756296-speculative-everything"><img alt="Speculative Everything: Design, Fiction, and Social Dreaming" border="0" src="https://images.gr-assets.com/books/1381287521m/17756296.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="The Meaning of Language" rel="nofollow" href="https://www.goodreads.com/book/show/1978082.The_Meaning_of_Language"><img alt="The Meaning of Language" border="0" src="https://images.gr-assets.com/books/1347472401m/1978082.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="A Billion Little Pieces: Rfid and the Infrastructures of Identification" rel="nofollow" href="https://www.goodreads.com/book/show/42068875-a-billion-little-pieces"><img alt="A Billion Little Pieces: Rfid and the Infrastructures of Identification" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="Shaping the Future of Work: A Handbook for Action and a New Social Contract" rel="nofollow" href="https://www.goodreads.com/book/show/35858961-shaping-the-future-of-work"><img alt="Shaping the Future of Work: A Handbook for Action and a New Social Contract" border="0" src="https://images.gr-assets.com/books/1502103161m/35858961.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Philosophy, Technology, and the Environment (MIT Press Book 2)" rel="nofollow" href="https://www.goodreads.com/book/show/35516778-philosophy-technology-and-the-environment"><img alt="Philosophy, Technology, and the Environment" border="0" src="https://images.gr-assets.com/books/1498443362m/35516778.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Missed Information: Better Information for Building a Wealthier, More Sustainable Future" rel="nofollow" href="https://www.goodreads.com/book/show/29889452-missed-information"><img alt="Missed Information: Better Information for Building a Wealthier, More Sustainable Future" border="0" src="https://images.gr-assets.com/books/1465582983m/29889452.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://images.gr-assets.com/books/1499718864m/34272565.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Control: Digitality as Cultural Logic" rel="nofollow" href="https://www.goodreads.com/book/show/26263175-control"><img alt="Control: Digitality as Cultural Logic" border="0" src="https://images.gr-assets.com/books/1442815654m/26263175.jpg" /></a></div>
<noscript><br/>Share <a rel="nofollow" href="/">book reviews</a> and ratings with Saira, and even join a <a rel="nofollow" href="/group">book club</a> on Goodreads.</noscript>
</div>

  </div>
  <script src="https://www.goodreads.com/review/grid_widget/89166601.Saira's%20navigating-uncertainty-mdef%20book%20montage?cover_size=medium&hide_link=true&hide_title=true&num_books=40&order=a&shelf=navigating-uncertainty-mdef&sort=date_added&widget_id=1542080966" type="text/javascript" charset="utf-8"></script>













https://www.quantamagazine.org/how-cells-pack-tangled-dna-into-neat-chromosomes-20180222/

https://www.dezeen.com/2018/02/20/movie-good-design-bad-world-politics/



Wolfe, Lauren. (2018). Elements of a Philosophy of Technology: On the Evolutionary History of Culture, translation of Ernst Kapp's Grundlinien einer Philosophie der Technik.

On The Evolutionary Origin of Symbolic Communication Paul Grouchy, Gabriele M. T. D’Eleuterio, Morten H. Christiansen & Hod Lipson

Energy, Agriculture, Patriarchy and Ecocide - Thomas S. Lough 1999

The Smart Enough City - Ben Green  2019

Layers of Silence, Arenas of Voice: The Ecology of Visible and Invisible Work


## Other practitioners
