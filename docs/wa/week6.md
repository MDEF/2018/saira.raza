# 06. Designing with Extended Intelligence
<head>
   <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2096725/audio_classification.js"></script>
   <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2096725/onset.js"></script>
   <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/2096725/nt-utils.js"></script>
</head>





This week I gained some fascinating insights into intelligence and artificial intelligence with Ramon Sangüesa and Lucas Lorenzo Peña. I found it interesting how well the teachers laid out the topics we were going to discuss before starting each day - it reminded me of how code is written and of the ideas from last week about how ways of organising information echo across different applications (in this case from coding to teaching)

## What is intelligence?

We began this week considering the nature of intelligence.

Is intelligence a top-down flow of symbol assignment or bottom up consensus of experiences?

Is it an imitation of a human behaviour or a goal orientated learning system?


## Symbols and the Symbol grounding problem

Taxonomy, classification and symbols are key to processing information. If you can objectify information and attribute a symbol to it you can process that information artificially. However the uniqueness of human readers makes it impossible to know if we all mean the experiences with the same symbols. How do we objectively define “the content of how something is” when this is subjectively created. Do we really know what other people mean with symbols and words? For example when we name children people will understand some reasoning behind them but can not fully understand the story behind the name just by seeing it written somewhere. The symbols we attribute to information cause us to lose data.

One symbol system that seems to transcend this problem is mathematics.


## Machine Learning
Machine learning uses matching to examples and clustering of information.
Machine learning can be:

- **supervised** (using an adjustable algorithm to categorise data)
- **unsupervised** (using well labelled data to discover emergent properties - includes deep learning)
- **reinforcement learning** (optimisation of strategies to reach a goal by trying lots of different ways of doing things).

It was interesting trying to imagine experiencing these information processes.
Supervised learning reminded me of learning a language - constantly being tweaked by feedback from your environment.

Reinforcement learning felt more profound in considering what it would feel like playing out many possibilities (maybe infinite possibilities) of the same scenario  and reminded me of the idea of a multiverse where every possible event in every possible order happens. I was reminded an episode of the TV series Black Mirror called "Hang the DJ" where a copy of the consciousness of humans are uploaded to an app which runs through every possible series of encounters in a dating system in order to ascertain who you would keep returning to and would ultimately leave the system to be with.

### Infinite paths and iterations to make sense from noise
The idea of an infinite possible ways to do a task or to learn about something also reminded me of Jorge Luis Borges' "The Library of Babel" - a short story which likens the universe to a library of every book that has ever been written and shall be written. It describes by analogy: symbols, trying to find symbols and order out of chaos, how infinite the search could be and the layers of comprehension we can have about them.

PhD student Jonathan Basile has created an online version of the library using C++ [here](https://libraryofbabel.info/).
<iframe width="600" height="300" src="https://libraryofbabel.info/" allowfullscreen></iframe>


<iframe src="https://player.vimeo.com/video/218573298?color=ffffff&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/218573298">Archive Dreaming</a> from <a href="https://vimeo.com/refo">Refik Anadol</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

*Inspired by "The Library of Babel" and dveloped during an artist residency at Google's Artists and Machine Intelligence program, "Archive Dreaming" by Refik Anadol and Mike Tyka uses AI and machine learning algorithms to search and discover interrelations between 1,7 million digitized items from SALT Istanbul institution's library and turn these into an immersive room.*




## Neural networks
It was really interesting to go through the logic of neural networks and see the kind of results they can produce. A neural network is a probability machine - an exhaustive run of possibilities to calculate probabilities of each outcome. They can be supervised or unsupervised. How the probabilities turn out depend on the trigger values of the gates.
Here is an online neural network that can be tuned:
https://playground.tensorflow.org

### Bias
The process of fragmenting information, running these pieces through a probability machine and then tuning the machine to produce desired results reminded me of the process of bias in humans.


### Using Neural Networks to find sounds

The schematic of the neural network reminded me of electronic music hardware set ups with knobs tuning sound waves with effects.

![Modular Synth Rack](assets/w6/modsynth.jpg)


Neural networks can been trained to try and pick out certain parts of sounds from noise.

Magenta's opensource N-Synth project has been used to develop some instruments that use neural networks to create mixtures of sounds.

<iframe width="800" height="400" src="https://experiments.withgoogle.com/ai/sound-maker/view/" allowfullscreen></iframe>

A physical N-Synth Super instrument has also been developed
<iframe width="560" height="315" src="https://www.youtube.com/embed/iTXU9Z0NYoU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Neural Networks and mechanical systems
The operation of the 'nodes' tuning information flow sounded like the operation of valves in a hydraulic system. I wondered if there were any mechanical analogies to the neural network. I found several applications of neural networks in modelling mechanical systems including the [WW2 Enigma encryption machine](https://hackaday.com/2017/08/11/decoding-enigma-using-a-neural-network/).


## Can AI remove prejudice?

In groups we devised hypothetical AI systems. We were advised that AI tasks tend fall somewhere on a spectrum between Analysis and Design tasks.

Our group talked briefly about an embodied AI nanobot that could be put inside the human body to perform medical tasks.
We then explored a concept of an **AI jury** that would be resistant to human prejudices.
Here is a summary of our thinking:

|   Elements of the AI   | Thoughts | Critique |
| ------------- |----------------------------------| -----|
| Goals | To reduce prejudices in the gathering of evidence, presenting of evidence and in decision making by juries and due to social pressure, social psychology, emotions and risk of bribery - by replacing juries and lawyers and questioning police | |
| Tasks | 1. To ask and record information from defendants and witnesses in a methodical manner | Language recording requires a level of machine learning (and therefore data and processes) |
| | 2. To ascertain inconsistencies in evidence using a set of questions that form a logical framework | Language interpretation requires a level of machine learning (and therefore data and processes) |
|  |  3. Critical analysis to find the probability of guilt through logic and machine learning of previous  rulings based on similar evidence | Highly complex tasks as language will need to be interpreted, labelled then processed in two ways - in the framework of evidence in the current case and then through machine learning using data from past cases|
| Inputs  | 1. Responses (verbal and physical in the case of lie detector tests) on questioning of the accused and witnesses | As with the current legal system, the defendant's vocabulary and emotions in the situation  can their affect their ability to be clear and avoid misinterpretation. To offset this there could be an AI that could interpret the defendants case in the most positive light possible (an AI lawyer) and an AI prosecuting lawyer that does the opposite. I think these would be something like adversarial neural networks|
|  | 2. A repository of data on: Laws, evidence from previous cases and outcomes of those cases. Data on questioning of the accused and witnesses | The preposition that the law is currently executed with conscious and unconscious prejudice will pass on the prejudices of the past in calculating the probability of guilt. It was proposed that we could run a large number of cases without the machine learning from past cases before going live and this would offset the "amount" of decisions that were likely prejudiced |
| Outputs | neutral presentation of evidence, a probability of guilt and a reasoning behind the probability for the judge | The lack trust of such technological systems will need to be developed for humans to want to use it as explanations given by machine learning especially can be full of misinterpretation |
| Context | Implementing laws to remove unfair prejudices | 1. Someone asked to consider if all prejudices and bias are bad - bias exists because of a reason  2. It is hard to spot bias in language with an AI, so is it likely that it can spot mere inconsistencies based on language?  |
| Other thoughts| 1. The system would need to be as secure from hacking as possible so would not be online. It would also need to be written in a unique or heavily encrypted language | |
| | 2. We wondered if the inverse of this system would be a blockchain jury | ||


We assessed the ethics of our system which was designed to respond to an ethical problem. This process was interesting to go through to understand some of the ethical mechanics of AI.

We decided that using a system that has no awareness of its own logic and computes probability is what bias is. So using bias to counter bias seems a futile exercise.

If the system were to operate on 100% machine learning it would require huge amounts of data to compute sentences and compare them to laws based in human languages. It would require a large bank of comparable interviews with a wide variety of people which in of itself would take a lot of time.

And in the end decisions made by computers are not easily accepted culturally nor politically.

Bias is a learning tool in humans (a short cut) to judge probabilities of behaviours. It would take a lot less resources and would be much more sustainable to try and make people conscious of their own biases. Creating effective communication interventions to help people do this might help.

Other groups in our class explored:
- networked ideas involving distributed sensors and touch points.
- use of profiling to generate suggestions
- trying to measure immeasurable quantities such as the happiness of a pig contained in a meat product or loneliness.

It was a very thought provoking discussion which gave a good grounding on the ethical limits of using AI.

### Addressing power dynamics in technology

One topic that came up in our discussions was that power dynamics can be reinforced rather than challenged by technologies and if we intend on remove power imbalances, we must ensure technologies are designed against them. This reminded me of some research by Nancy Hafkin into the gender gap in Information and Communication Technologies and how these technologies were intended to be "gender-neutral" but ended up being differently by men and women. She proposed that social norms around gender roles and technology should be challenged from the design stage by representing of a wider range of user and designer experiences into designs in an attempt to change social norms surrounding these issues.

## Using AI as an artistic tool

### Exploring the nature of Data ###
[Kyle McDonald](http://www.kylemcdonald.net) is a media artist who is also a coder who builds tools for artists to use. He works with immersive installations as well as data, and the tools he makes by coding. In his talk at IAAC at IAAC as part of the MIRA Audio Visual Festival, he showed some of his work that used data to tell stories and to transform spaces using sensors and light and sound actuators.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FIAACbcn%2Fvideos%2F498934090609166%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>


### Exploring the experience of AI
By using AI as a medium we learn something about the experience of the processes involved. Kyle McDonald showed us examples of when an AI had been shown a representation of a photograph and how the AI was able to reverse engineer the representation to form a guess of what the original photograph looked like. The effects were grotesque and funny.

The BigGAN image synthesis algorithm (which uses generative adversarial networks)has recently produced some eerie results shared [here](
https://drive.google.com/drive/folders/1lJEZw3KLmSXvh95Aem8t-aAgRybOIeYl). These feel like the answer to the question "Do androids dream of electric sheep?" - maybe  while they are being trained in an adversarial neural network system.

![BigGan generated sheep](assets/w6/electricsheep.jpg)

*BigGAN generated images of sheep from an internship project by Andrew Brock from Heriot-Watt University in collaboration with Jeff Donahue and Karen Simonyan from DeepMind. Their paper 'Large Scale GAN Training for High Fidelity Natural Image Synthesis'is under review*

<iframe width="560" height="315" src="https://www.youtube.com/embed/YY6LrQSxIbc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Generative Music - Complexity from Simplicity

Thinking about automation of tasks to produce complex learning functions made me think of composers who have tried to automate certain parts of composition to 'generate' music.

** Steve Reich ** designed a generative mechanical system using 2 out of phase tape recorders to generate music for his 1965 piece "It's Gonna Rain" which inspired ** Brian Eno ** 's ongoing investigation into how complexity arises out of simplicity in the fields of Generative and Ambient Music. His intention with Ambient music was to make endless non repeating music. Eno worked with Dentsu Lab Tokyo to produce [a music video for his album “The Ship” based on neural network learning](http://dentsulab.tokyo/rad/?p=255).
<iframe width="560" height="315" src="https://www.youtube.com/embed/TQVkW1f4QOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Kyle McDonald has written [an excellent piece on the use of Neural Nets to produce music  ]( https://medium.com/artists-and-machine-intelligence/neural-nets-for-generating-music-f46dffac21c0) up to the state of the art application which he himself developed and [this beautiful interactive presentation](https://teropa.info/loop/#/title) by Tero Parviainen lets you experience the effect of some of these generative processes - from Steve Reich to [Markov Chains](https://teropa.info/loop/#/markovplayer).
Parviainen discusses among other things the concept of **randomness** being something a computer can use to generate music as well as **data sonification** - turning different types of data (from sensors in real time or recorded) into sounds. He also looks into the possibilities of connecting the listener with the music (supervised AI) through **interactive sound installations** another area being explored by Brian Eno to connect composers with the audience. For his album 'Reflections' released last year, Eno worked with a developer to create an app so that the work can be experienced as a new work with infinite possibilities on each hearing.

Other artists looking at Neural Networks to compose music include **Daniel Johnson** who is [using Recurrent Neural Networks](http://www.hexahedria.com/2015/08/03/composing-music-with-recurrent-neural-networks/)

**Google's [Magenta](https://magenta.tensorflow.org/demos/web/)** is an open source research project exploring the role of machine learning as a tool in the creative process. They have a number of web app demos

Google DeepMind have produced
[WaveNet](https://deepmind.com/blog/wavenet-generative-model-raw-audio/) a deep generative model of raw audio waveforms used mainly to mimic human voices. As well as sounds, the algorithm is fed text in a sequence of linguistic and phonetic features (which contain information about the current phoneme, syllable, word, etc.)
Wavenet produces interesting outputs when trained it on datasets of music.

I also found this amazing interactive **[Neural Network Beatbox by Nao Tokui](https://codepen.io/naotokui/pen/NBzJMW)** which is really effective.

<iframe width="900" height="700" src="https://codepen.io/naotokui/full/NBzJMW/" allowfullscreen></iframe>






## Do machines make art?

It was interesting to hear how the engineers and scientists of the Darmouth Conference felt that randomness was essential for a system to be creative:
> ".. the difference between creative thinking and unimaginative competent thinking lies in the injection of a some randomness. The randomness must be guided by intuition to be efficient."

*-from 'A proposal for the Dartmouth Summer Research Project on Artifical Intelligence8' J. McCarthy , M. L. Minsky , N. Rochester, C.E. Shannon (1955)'*

I feel that art is far from random but grounded in subjective experiences which have context and are part of a long line of communication - full of cultural and personal influences and spawning new cultures.

Attempts have been made to simulate creativity in AI. For example Sony have been looking into song writing machines with their [Flowmachines system]( https://www.flow-machines.com/).
The song below was generated by their AI system trained on Beatles songs.
<iframe width="560" height="315" src="https://www.youtube.com/embed/LSHZ_b05W7o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

AI is a result of inquiries by humans but it's outputs often feel alien from the human experience and sometimes incomprehensible or too data driven and predictable - revealing our biases and oversimplifications of human experiences - and a perceivable lack of  intention to communicate.




## Using AI to compose spaces in 3D
Nicolay Boyadjiev in his talk at IAAC presented a project using AI as a creative tools that expand our logic to design spaces. He described using AI as moving away from Hal in 2001 to the sentient planet-system in Solaris.

<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FIAACbcn%2Fvideos%2F2051695368186166%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>

He mentioned the [Atoll Urban design software suite]( https://thenewnormal.strelka.com/research/project/atoll) that uses deep learning to generate spatial ideas.

He also mentioned how in the smart city system bias and uncertainty becomes a limiting condition.

Nao Tokui  is also exploring Interactive 3D Modeling with a Generative Adversarial Network
http://createwith.ai/paper/20170625/831



## Machine-like learning applied to human systems


### Comparing human intelligence with artificial intelligence
> “The best material model for a cat is another, or preferably the same cat”

*Arturo Rosenblueth and Norbert Wiener (founders of cybernetics, 1945)*

It has been tempting to look for analogies between human and cultural intelligence systems and the AI processes we learned about however it would be foolish to ignore the glaring differences between computers and humans:
* material - the stuff we use to process information (neurones) living, reproducing and dying in a human body are very different to silicone crystals and metal sat in computers.
* time - the amount of time it takes for a neuron to send a signal is much less than a silicon and the time it has taken for a neurone to evolve and optimise for its environment from the first living cell is incomparably longer than the time humans have been optimising computer processors.
* system complexity - there are far more connections and path possibilities in a brain than in a computer.


### The belief in computer networks enabling a collective consciousness of humans

In his 3 part documentary "All Watched over by Machines of Loving Grace" Adam Curtis tells how cyberutopians of silicon valley believed that with computers and networks human's could create a new kind of order where **feedback from connected nodes in a network would create a stable, self-organising  governance system** without central control through a sort of collective consciousness inherent in the network. This belief which was also behind the stock market and the "New Economy" had its roots in values of Objectivism (rational self interest and a rejection of altruism) developed by Ayn Rand.

Ayn Rand's close circle of followers included Alan Greenspan who was Chairman of the Federal Reserve of the United States from 1987 to 2006 - injecting Randian values into the heart of American policy in the run up to the financial crash of 2008. ** Instead of encouraging stability, computer systems enabled short term speculative capital flows which formed bubbles and led to financial crashes.** The effect was a feeling of helplessness in a system we are powerless to change. Adam Curtis explores this further in his later film Hypernormalisation.

With the concept of the information feedback loop, Cybernetics enabled equating natural intelligence to computer intelligence. ** The complexity and variability of data from natural systems were forced to fit machine systems to create "a machine like fantasy of stability".**
People were encouraged to see themselves as equal members of a natural global system aiming to reach a steady state when in reality nature evolves with time.







<iframe frameborder="0" width="480" height="270" src="https://www.dailymotion.com/embed/video/x5u8bgk" allowfullscreen allow="autoplay"></iframe>

<iframe frameborder="0" width="480" height="270" src="https://www.dailymotion.com/embed/video/x2eagvn" allowfullscreen allow="autoplay"></iframe>





## AI in Fiction

### What it feels like to be an AI

#### Pinocchios
The topic of how it feels like to be a conscious AI and the ethics surrounding this has been well explored in fiction. Many versions of this theme feel like the story of Pinocchio - with some amount of yearning by an AI to be real, to have agency and to understand their identity. Perhaps the most direct recent permutation is the film AI about a boy robot AI who ends up sinking to the bottom of the ocean and thinks he sees a blue fairy (a character from Pinocchio). Other popular films that explore this topic include Blade Runner and more recently Alien Covenant and the TV series Westworld, all covering the elements of the importance of memory in forming identities.

### How humans feel about AIs
#### Ghosts
The film (and book) Solaris was mentioned by Ramon this week as an example of sentience in a system. This film feels quite different to the other examples I've mentioned so far - more like a haunting interaction with an environment than one with an embodied AI. This reminded me of the phrase "ghost in the machine" which inspired the film Ghost in the Shell about an AI that becomes conscious and can travel between bodies and also the actual ghost story told in Event Horizon.

#### (Mis-)Trust
The employment of new technology involves changing of social norms and order which is always met with speculation and often opposition.
It is interesting to me that science fiction seemed to take off in the Victorian era as a offshoot of horror literature - perhaps a reflection of the fears of the impending technological advancements of  the industrial revolution.

Many classic science fictional stories explore the unpredictable nature of AI (with varying degrees of consciousness) - often turning on their master (Frankenstein, Fritz Langs Metropolis, 2001: A Space Odyssey, Blade Runner, Terminator, The Matrix, Ex-Machina, Alien: Covenant).
This idea feels reminiscent of ancient stories warning us that knowledge always comes with punishment - such as banishment and pain for eating the apple from the tree of knowledge and banishment for stealing fire from the gods and perhaps more relevant - a proliferation of languages for trying to use a unified language to give yourself a name - confusion and chaos for trying to create a symbol.


## References

### Websites
* https://playground.tensorflow.orgh
* http://www.generativemusic.com/
* https://teropa.info/loop/#/title
* http://www.flow-machines.com/
* http://createwith.ai/
* http://moralmachine.mit.edu/
* http://aiweirdness.com/

### Articles
* https://medium.com/artists-and-machine-intelligence/neural-nets-for-generating-music-f46dffac21c0
* http://artssantamonica.gencat.cat/en/detall/Lightforms---Soundforms
* https://www.soundonsound.com/people/brian-eno
* https://www.scientificamerican.com/article/how-a-machine-learns-prejudice/?WT.mc_id=SA_TW_TECH_NEWS
* https://www.quantamagazine.org/machine-learnings-amazing-ability-to-predict-chaos-20180418/

### Research Papers:
* A proposal for the Dartmouth Summer Research Project on Artifical Intelligence J. McCarthy , M. L. Minsky , N. Rochester, C.E. Shannon 1955
* Computer Machinery and Intelligence - Turing (1950)
* Minds Brains and Programs - Searle (1980)
* How Anticipatory Design Will Challenge Our Relationship with Technology Joël van Bodegraven
* Hafkin, N. (2002, July). “Is ICT gender neutral? A gender analysis of six case studies of multi-donor ICT projects.”
* Sophia Huyer and Nancy Hafkin (2007) Engendering the Knowledge Society: Measuring Women’s Participation
* Computational Intelligence in architectural and interior design: a state-of-the-art and outlook on the field Emil RACECa, Stefania BUDULAN, Alfredo VELLIDO


### Films

* All watched over by Machines of loving Grace (2011) - Adam Curtis
* Black Mirror - Hang the DJ  (2017) - Charlie Brooker
* Solaris
* Planet of the Apes


### Books

<style type="text/css" media="screen">
.gr_grid_container {
          /* customize grid container div here. eg: width: 500px; */
width: 1000px;
}

.gr_grid_book_container {
          /* customize book cover container div here */
float: left;
width: 98px;
height: 160px;
padding: 0px 0px;
overflow: hidden;
}
</style>


<div id="gr_grid_widget_1542761241">
<!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->

<div class="gr_grid_container">

<div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://images.gr-assets.com/books/1499718864m/34272565.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="The Book of Why: The New Science of Cause and Effect" rel="nofollow" href="https://www.goodreads.com/book/show/36204378-the-book-of-why"><img alt="The Book of Why: The New Science of Cause and Effect" border="0" src="https://images.gr-assets.com/books/1516890908m/36204378.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="Cybernetics: or the Control and Communication in the Animal and the Machine" rel="nofollow" href="https://www.goodreads.com/book/show/294941.Cybernetics"><img alt="Cybernetics: or the Control and Communication in the Animal and the Machine" border="0" src="https://images.gr-assets.com/books/1347245279m/294941.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="Digital Ethnography: Principles and Practice" rel="nofollow" href="https://www.goodreads.com/book/show/27424160-digital-ethnography"><img alt="Digital Ethnography: Principles and Practice" border="0" src="https://images.gr-assets.com/books/1446614664m/27424160.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="Smart Things: Ubiquitous Computing User Experience Design" rel="nofollow" href="https://www.goodreads.com/book/show/7983620-smart-things"><img alt="Smart Things: Ubiquitous Computing User Experience Design" border="0" src="https://images.gr-assets.com/books/1396366890m/7983620.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="Machine Learning for Designers" rel="nofollow" href="https://www.goodreads.com/book/show/30650012-machine-learning-for-designers"><img alt="Machine Learning for Designers" border="0" src="https://images.gr-assets.com/books/1466351841m/30650012.jpg" /></a></div>

<div class="gr_grid_book_container"><a title="Machine Art in the Twentieth Century" rel="nofollow" href="https://www.goodreads.com/book/show/29889465-machine-art-in-the-twentieth-century"><img alt="Machine Art in the Twentieth Century" border="0" src="https://images.gr-assets.com/books/1479234932m/29889465.jpg" /></a></div>

  </div>

  </div>

<script src="https://www.goodreads.com/review/grid_widget/89166601.Saira's%20designing-with-extended-intelligenc%20book%20montage?cover_size=medium&hide_link=true&hide_title=true&num_books=20&order=a&shelf=designing-with-extended-intelligenc&sort=date_added&widget_id=1542761241" type="text/javascript" charset="utf-8"></script>

</br>
</br>
