# 12.Design Dialogues - Research



## <center>Mechanical metamaterials</center>



**Hasso Plattner Institute (HCI)** Potsdam, Germany have developed some 3D printed metamaterial mechanical devices which you can see below.

They also developed a 'Metamaterial Mechanisms Editor' which is open in the other window on this screen

  <iframe width="560" height="315" src="https://www.youtube.com/embed/lsTiWYSfPck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

  <iframe width="900" height="600" src="https://www.thingiverse.com/HassoPlattnerInstitute_HCI/collections/metamaterial-mechanisms" allowfullscreen></iframe>

  You read their paper [here](http://alexandraion.com/wp-content/uploads/2016UIST-Metamaterial-Mechanisms-authors-copy.pdf) :

## <center>Digital Mechanical Metamaterials</center>

Using "mechanical computation" in 3D printed objects, i.e., no electronic sensors, actuators, or controllers typically used for this purpose. The resulting objects can be 3D printed in one piece and thus do not require assembly.

  <iframe width="560" height="315" src="https://www.youtube.com/embed/A0lJUzcKDsY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## <center>Transformable metamaterials</center>

Transformable metamaterials enable structures to change shape.

* Hasso Plattner Institut are looking into transforming the surfaces of 3D structures:

    <iframe width="560" height="315" src="https://www.youtube.com/embed/t12kHV001hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Origami strategies
  A number of researchers are materialising strategies from origami using digital fabrication methods.

**Johannes T.B. Overvelde** has made some notable recent breakthroughs in this field

* Extruded cubes put together to utilise hinges and stiffness to make a range of shapes. 'A three-dimensional actuated origami-inspired transformable metamaterial with multiple degrees of freedom.' Johannes T.B. Overvelde, Twan A. de Jong, Yanina Shevchenko, Sergio A. Becerra, George M. Whitesides, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2016) Nature Communications

    <iframe width="560" height="315" src="https://www.youtube.com/embed/AbCb1TtYTbg?start=14" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* 'Rational design of reconfigurable prismatic architected materials' Johannes T. B. Overvelde, James C. Weaver, Chuck Hoberman & Katia Bertoldi (2017)  Nature

    <iframe width="560" height="315" src="https://www.youtube.com/embed/7A_jPky3jRY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  '

  * 'Topological kinematics of origami metamaterials' Bin Liu, Jesse L. Silverberg, Arthur A. Evans, Christian D. Santangelo, Robert J. Lang, Thomas C. Hull & Itai Cohen (2018)  Nature Physics

  * 'Geometry of Transformable Metamaterials Inspired by Modular Origami' Yunfang Yang and Zhong You (2017) Journal of Mechanisms and Robotics


## <center>Wave Interaction</center>

In the 1960s 'negative refractive index' was proposed - could we get light to bend the opposite way to how it does in nature?

The structure of materials interact with waves that travel through materials (like air and water) and electromagnetic wave radiation (that doesn't a need material).
Perfect lenses were proposed in the late 1990s and Sir John Pendry demonstrated this in early 2000 using a metamaterial - structures much smaller than the wavelengths of light - in the world of **nanotech**.

<center><iframe src="https://giphy.com/embed/3oD3YLxau5rFE0ZpIs" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/harvard-3oD3YLxau5rFE0ZpIs"></a></p></center>

## <center>Acoustic metamaterials</center>

**University of Sussex's Interact Lab** looked into:

* a [3D printable modular acoustic metamaterial building system in 2017](http://www.sussex.ac.uk/broadcast/read/39354)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/BcVxRyvipcU?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* They also looked into dynamic acoustic control using metamaterials and transducers.

  <iframe width="560" height="315" src="https://www.youtube.com/embed/zixzyeF25SY?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


[**Disney Research Labs**  and **Columbia University** have been trying to incorporate Modular Acoustic Filters using Acoustic Voxels into everyday objects](http://www.cs.columbia.edu/cg/lego/)  https://www.disneyresearch.com/publication/acoustic-voxels/

  <iframe width="560" height="315" src="https://www.youtube.com/embed/7JbN9vXxGYE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


**Steve Cummer's team at Duke University** looked into Acoustic Holograms in 2016:
["it should be possible to manufacture entire 3D printed fields of sound that don’t rely on a transducer array controlled by electronics, but rather a grouping of cells that work together to control the acoustic waves."](https://3dprint.com/154311/duke-3dp-acoustic-holograms/)

* ['Acoustic Holographic Rendering with Two-dimensional Metamaterial-based Passive Phased Array' - Yangbo Xie, Chen Shen, Wenqi Wang, Junfei Li, Dingjie Suo, Bogdan-Ioan Popa, Yun Jing, and Steven Cummer (2016)](https://arxiv.org/ftp/arxiv/papers/1607/1607.06014.pdf)

* The printed material is acrylonitrile butadiene styrene plastic with density of 1180 kg/m3  ['Systematic design and experimental demonstration of bianisotropic metasurfaces for scattering-free manipulation of acoustic wavefronts' Junfei Li, Chen Shen, Ana Díaz-Rubio, Sergei A. Tretyakov & Steven A. Cummer (2018) Nature Communications](https://www.nature.com/articles/s41467-018-03778-9)

    <iframe width="560" height="315" src="https://www.youtube.com/embed/pNsnvRHNfho" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This year a team at the **University of Strathclyde** tried using 'A Helmholtz resonator' configuration in their acoustic metamaterial. A Helmholtz oscillator is a container of gas (usually air) with an open hole (or neck or port). A volume of air in and near the open hole vibrates because of the 'springiness' of the air inside - like a glass bottle when you blow across it. The team concluded that this type of acoustic metamaterials for sound control could be integrated in electroacoustic devices such as headphones and hearing aids. **"In-ear headphones** could be 3D printed with a shape corresponding to the listener external ear".

  * ['Enhancing the Sound Absorption of Small-Scale 3D Printed Acoustic Metamaterials Based on Helmholtz Resonators' Cecilia Casarini,  Ben Tiller, Carmelo Mineo, Charles N. MacLeod, James F.C. Windmill and Joseph C. Jackson (2018)](https://pureportal.strath.ac.uk/files-asset/82476858/Casarini_etal_IEEE_Sensors_2018_Enhancing_the_sound_absorption_of_small_scale_3D_printed_acoustic_metamaterials.pdf)


## <center>Magnetically controlled metamaterial shapes</center>

**MIT** 2018

  <iframe width="560" height="315" src="https://www.youtube.com/embed/MUt1YKtn6kM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This year, a team led by **USC Viterbi** developed 3-D printed acoustic metamaterials that can be switched on and off remotely using a magnetic field](https://viterbischool.usc.edu/news/2018/04/3-d-printed-active-metamaterials-for-sound-and-vibration-control/) https://www.3dnatives.com/en/metamaterials-3d-printed180420184/

* ['Magnetoactive Acoustic Metamaterials' Kunhao Yu  Nicholas X. Fang  Guoliang Huang  Qiming Wang (2018) ](https://onlinelibrary.wiley.com/doi/abs/10.1002/adma.201706348)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/aV07hCF7-AQ?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Also this year, **University of California** looked into ReTunable Field responsive mechanical metamaterials (FRMMs) made of polymeric tubes filled with 'magnetorheological fluid'

  * ['Field responsive mechanical metamaterials'- Julie A. Jackson et al (2018)](http://advances.sciencemag.org/content/4/12/eaau6419)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/iNaMxIad7Io" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## <center>Optical and EM metamaterials</center>

[Invisibility cloaking as a use of negative refraction has been the holy grail of metamaterials largely pushing interest in metamaterials](https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html).
However metamaterials have to be of the nano scale in order to use their geometry to bend light. Currently methods to manufacture these Optical or Photonic metamaterials are very expensive and difficult to do for the visible light range but for longer wavelengths applications include:

  1. Making radar satellite dishes cheaper and flatter
  2. Optimising solar collection
  3. Optimising IR (heat) collection or dissipation

* https://phys.org/news/2016-02-invisibilityengineering-metamaterials.html

### <center>Diatom frustules (nanoscopic algae-made greenhouses) as optical metamaterials</center>

>"The interaction of diatom frustules with light consists of three main phenomena: **light conﬁnement, selective transmission and photoluminescence.**

> "It seems likely that the structure of the frustule provides the diatom with capabilities, not only to **focus light, but also to tune the reception of light in an optimal way**. It aligns with novel developments in antenna technology where electromagnetic bandgap structures are used to create low loss dielectric structures based on periodicity of super shapes to prevent propagation of certain frequencies, and increase isolation between antennas, and this can be tailored on angle of incidence and polarization.


<a href="https://www.researchgate.net/figure/Schematic-representation-of-the-interplay-between-genes-and-molecules-geometry-and_fig2_318567831"><img src="https://www.researchgate.net/profile/Edoardo_De_Tommasi/publication/318567831/figure/fig2/AS:519601444356097@1500894046591/Schematic-representation-of-the-interplay-between-genes-and-molecules-geometry-and.png" alt="Schematic representation of the interplay between genes and molecules, geometry and physics, pivotal to understand form, function and development."/></a>

*from ['Diatom Frustule Morphogenesis and Function: A Multidisciplinary Survey' - Edoardo De Tommasi, Johan Gielis, Alessandra Rogato (2017)](https://www.researchgate.net/publication/318567831_Diatom_Frustule_Morphogenesis_and_Function_A_Multidisciplinary_Survey)*

#### <center>[3d printing diatom frustule structures](https://community.ultimaker.com/files/file/2082-diatom-frustule/)</center>


© Fractal Teapot 2018
  ![3d diatom build](assets/w12/diatom-build.jpg)
  ![3d printed diatom](assets/w12/3dprinteddia.jpg)

  <iframe width="560" height="315" src="https://www.youtube.com/embed/vOOvdheuAFs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


  ![Diatom Frustules](assets/w12/diatoms.jpg)
  ![Diatom Frustules](assets/w12/emdiatom.jpg)
  ![Diatom Frustules](assets/w12/diatom3.jpg)

  © Fractal Teapot 2018  https://www.fractalteapot.com/portfolio/3d-printing-carbon-cycle/


**References:**

* http://cfb.unh.edu/phycokey/Choices/Bacillariophyceae/Bacillario_page/bacillario_key.html

* 'Structure-based optical filtering by the silica microshell of the centric marine diatom Coscinodiscus wailesii' K. Kieu, C. Li, Y. Fang, G. Cohoon, O. D. Herrera, M. Hildebrand, K. H. Sandhage, and R. A. Norwood

* 'Biologically enabled sub-diffractive focusing' E. De Tommasi, A. C. De Luca, L. Lavanga, P. Dardano, M. De Stefano, L. De Stefano, C. Langella, I. Rendina, K. Dholakia, and M. Mazilu

* 'Comparing optical properties of different species of diatoms'(2015)  Maibohm, Christian; Friis, Søren Michael Mørk; Su, Y.; Rottwitt, Karsten

* 'Numerical and experimental investigation of light trapping effect of nanostructured diatom frustules'
(2015) Xiangfan Chen, Chen Wang, Evan Baker, Cheng Sun

## <center>Other things to explore</center>

* Deep learning to design metamaterials:
  Itzik Malkiel, Michael Mrejen, Achiya Nagler, Uri Arieli, Lior Wolf, Haim Suchowski. Plasmonic nanostructure design and characterization via Deep Learning. Light: Science & Applications, 2018





<!--  ### Symbolism and Geometry
Logos and Branding
In the age of mass production and consumerism being the driving force of major economies, ever more sophisticated marketing and branding use symbols to signify identity. Brands produce tribes of consumers.*/

Nike
Adidas
Louis Vuitton     
--!>
