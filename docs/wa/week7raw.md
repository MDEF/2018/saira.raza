# 07 How things work

This week I felt we learned about communication processing  - whether it be through electronic circuits or wifi signals or language.


Mad Max
physical Interfaces
appropriation of existing technical solutions
fast prototyping and sketching technical
linking tools
PYTHON!
sensing and actuating remotely (wifi)
raspberry pi servers

fritzing to sketch circuits
circuits.oi
Some of these processors are very cheap now - wifi proceessor

not reinventing the wheel
pointillium language for visualising - close to python.
javascript!  work in browasers.

connecting sensors to softwares.
infrared camera on xbox 360- - 3d array of light points - claudias idea

you can use that a s a camera

off the shelf - saves time to enable more experimentation

api are interfaces for machines (less human friendly)


node red  - - connecting comunication between Machines

interfaces and metaphors and  analogy become more and more
important because we need physical analogy


always start with why?

its getting harder to shape the technology around us.

sensing the environment

internet of things - inte -rnet of gadegets - like futurama when we need to be thinking aout systemsof information (like week 5)

## Computers

Computers are calculating (information processing) machines.
Computers are not binary by default and silicon transistor based computers are not the only type of computers. You can make analogue computers with gears
![Babbbage's Analytical Engine](assets/w7/babbage.jpg)

*Babbage's Analytical Engine

We chose binary because we can represent 2 states.
We used to use the decimal system (working with 10 states) but the voltages required could not be reproduced accurately
Quantum Computers use quantum bits or qubits, which can be in superpositions of states.

### A short history of Computers

Collosus ww2
1943 onwrds

is a record player a computer?

general  v special purpose

PCBs are an arrangement of components

digitiising a sine wave makes squre waves
information theory section in documentation folder.

electronic v electromechanical
VALVES and then transistors.
1960s move to silicone/
CPU in the 1970 were contained in one chip.
comodore 64
windows 95
96 mobiles - pushed multifunctionality
2017 - 5 dollar raspberry pi



<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

<!-- The Timeline -->

<ul class="timeline">

	<!-- Item 1 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Freelancer</span>
				<span class="time-wrapper"><span class="time">2013 - present</span></span>
			</div>
			<div class="desc">My current employment. Way better than the position before!</div>
		</div>
	</li>

	<!-- Item 2 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag">Apple Inc.</span>
				<span class="time-wrapper"><span class="time">2011 - 2013</span></span>
			</div>
			<div class="desc">My first employer. All the stuff I've learned and projects I've been working on.</div>
		</div>
	</li>

	<!-- Item 3 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Harvard University</span>
				<span class="time-wrapper"><span class="time">2008 - 2011</span></span>
			</div>
			<div class="desc">A description of all the lectures and courses I have taken and my final degree?</div>
		</div>
	</li>

</ul>






pcb- printed circuit board - printed with copper


programme... cpu... data  are 3 elements of a computers

opertive system alllows you to run many Languages
fir,ware interfaces directly with wires on the board.

os  - does scheduling

dieter rams game for electronics from braun

processing code


## Sensors

complex motion from simple motion (brian eno pendulum)

textiles the you can run current throughout
pneumatics

arduino can count in  illiseconds -
it can amplif and calibrate tiny currents - tiny motions and changes


## Information
we touched on the concept of information again.
information  presents a lesson by brit cruise

binary info is closer to languages.

information systems contain ideas such as
compression
error
ram..

its really tempting to draw analogies to human thought.

digitising is quantifying analogue data
then putting a number to it then putting that into binary.

## Networking

with fibre optics you can packet information from people label it and send down  the fibre optics (like cars rather than trains?
  package switching
  what are the best standards to do this?
  )
  internet protocall
  request for commons?
  package switch networks in unis in the us.

  mainframes - timesharing.. runs a bit of everyones applications
   the had a protocol between east and west coasts eventually.

   isp companies - have a network in a country.
   at exchange point all providers come together in one town.

   googel etc have their own networks

   google will have their own wires.

   providers specialise in home delivery, intercntinental. data centre to these points.
   how does it ,

   isp companies decide the path. they buy bandwidth.

   politics of this physical network?
   like building the channel tunnel.



   understanding media the extensions of man - mrshall mcluhan#


   email protocols are the same as the 90s.
   email is a network of servers.


   cern - frist server? tim berners lee to save  documenttation! like us
   he thought of hyperlinking.

he wrote html.
he created the http protocol,
storage server,
browser

he decided to opensource html which was key to improving it - mosaic, explorer, firefox chrome.
unlike adobe.

in the 80s computers got smaller but harder
computers were built out of parts
windows 95 spread the idea of pcs.
.
interenet allows
bi directionsal communication.

tv was first thought as a 2 way communication  tools.

connectivitiy. developing world.

bd tech awareness of what gthe internert is.


most use through mobiles

more devices

### A timeline of modern computers


<font size="2">- **1940s:** Electromechanical computers used electric switches to drive mechanical relays</font>
<font size="2">- **1943–1945**  the world's first electronic digital programmable computer Colossus was developed by codebreakers using used thermionic valves (vacuum tubes) to perform Boolean and counting operations.</font>
<font size="2">- **1950s** semiconductor transistors replaced vacuum tubes</font>
<font size="2">- **1960s** move to electronic circuits on silicon - integrated circuit (or microchip). Earliest uses by NASA and the military</font>
<font size="2">- **1970** CPU's were contained in one chip this led to small, low-cost computers known as Microcomputers.</font>
<font size="2">- **1982** Comodore 64</font>
<font size="2">- **1995** Windows 95 operating system</font>
<font size="2">- **1996** Mobile phones push</font> multifunctionality</font>
<font size="2">- **2017** 5 dollar raspberry pi</font>



### the cloud is not a cloud!
types of networks:
servers close and information gets lost.
centralised > decentralised (with nodes > distributed network (IPFS) like bittorrent)

TRansmitting code:
paper network analogy
communicating a point on a paper

class paper example.

bits and compression (notes)

network info contains what is coming, what is going, who is sending.

layers of data

applications
transport
networks
network interface.

format of a package of data.
Eth ? - ip - tcp ?- data (payload).

We are going to set up servers
raspberr pi to teach kids computers science and distribute computers in the developing word. not for profit.
connected to monitors
20 million distributed.
hdmi video output usp, wifi bluetooth.

pins are exposed so you can connect anything - lcds and cameras.
mqtt - set of rules - to send messages http://mqtt.org/
access point for wifi.

wifi antenna to ethernet bank to raspberry pi.

runs on opensource linux.
opensource operating system.
linux give you the C code. you can read and change it.
in the 80s , in universities, plotters and smaller devices had drivers to use on computers.
( built on top of a system called UNix.) giant servers run on Linux based on unix.

#### OPensource
property over intellectual things (in tech) why should they be different to copyright in the arts? why are they?
the idea of the common good.

copyleft rules: - most of the great works of humans werent motivtaed by oney
0. start at zero: freedom to run programme as you wish for any purpose.
1. freedom to study how the programme works - access to source code mandatory and you can modify as you wish. (like prose , motifs in arts)
2. freedom to redistribute to help your neighbours. you can control and not distribute? you can sell it too
3. freedom to copy.can distribute other peoples work.

helps things move fast and solve problems quickly - efficient  -INCLUSIVE
thoughts on inclusion.
thoughts on music publishing


linux os - can be packaged differently to suit your needs - ubuntu distribution is a system running on linux
deviant? also.
freedom comes with choices
Raspbian is the operating system for a pi.


bigest companies use Linux operating systems  - google, insta, duck duck, tvs tesla cars, github, aquired by ibm.

open decision making systems

servers and embedders - things without screens - routers.

[pi 5-35 euros

chinese versions orange-pi, banana- pi

tiny box computers
 - headless - no interface?

arduini 2kbytes
 chips programmable megabytes

 these can all connect to wifi tho.

network layout


mqtt protocol
ip addresses
  8 sections of 8 bits each will define any computer in a network.

  we create local networks that have gateways .

  192.168.2 - is the code for all local newtworks.
  its the last 2 or 3 digits that are machine identifiers.
  so it changes depending on where you are.

  pis run 3d printers!

internet was invented in Unix so its a powerful tool.
Lots of teaching tools online about linux.

from your computer you can open command line.
open the command window
check computer is networked and responsive:
pi05  192.168.2.151
pi03 227
screen 219
ping  (ip address of the networked computer) in you command line sends through network.
%traceroute to give path of information through network.


## Musical Actuators

https://en.wikipedia.org/wiki/Hydraulophone

Vocal chords
https://www.youtube.com/watch?v=lOJAWOK1RTs


https://www.youtube.com/watch?v=8L8hY8siUMQ

## References
https://www.youtube.com/watch?v=F99sAApGySk


## Connected communities

https://hackoustic.org/
