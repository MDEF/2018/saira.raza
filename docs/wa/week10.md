<style>

w10

.w10 img {
	width:auto;
	height:auto;
	margin:0.5em;
	display:inline-block;
	background-position:50% 50%;
}







.dog {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAZCAMAAAD63NUrAAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAAFZJREFUeNqdzksKwDAIAFHH+x+6lIYOVPOhs5OHJnES/5UkYKEkU7xjijSIm50iFh4fAXgYDd/yumVVRSwsqq/nRA3xVK0oo06d5U6DpQZ7PV7lMxH7LkaQAbYFwryzAAAAAElFTkSuQmCC),auto;
}
.cactus {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAaCAMAAABigZc2AAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAAFpJREFUeNp9jUEOwEAIAhn+/+imjbs1bC0XzQioSUAAYZs0mcVY051RbGXfFsyxibTVHXhuhXYlbuRGPbe7kz3g0wf679OuizDBdCaberLE4AsjGliD02eNugA+MQFAPqynHQAAAABJRU5ErkJggg==),auto;
}
.egg {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAMAAAC6V+0/AAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAAEdJREFUeAF90FEKgDAMwFDT+x9awYCRwjL282CM9joFsGie/swYy6oiENW8RaJ5PBvxvCpqxfwetTOqeyo+Rgx3JjnrCxNa3TwuAY6NyoMhAAAAAElFTkSuQmCC),auto;
}
.paint {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAATCAMAAAB4HKeYAAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAADRJREFUeAFjwAsYsfMYQYgRLsLICKWhskxMTDAmCEMAwhQmEEDho8szUiqPaR+xfIR7sQMAR6QAjICqvMYAAAAASUVORK5CYII=),auto;
}
.planet {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAPCAMAAAAxmgQeAAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAAD5JREFUGNN1j9EKADAIAnP//9GjFq6w7iU4DNRsB06cb44TJ+UzYUcniqnqoA6ag/62JhjKIU0rxx1lKveuXJ5UAQs/G/2vAAAAAElFTkSuQmCC),auto;
}
.tv {
	cursor:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAMAAACeyVWkAAAACVBMVEX///8AAAD///9+749PAAAAAXRSTlMAQObYZgAAAEZJREFUeAGt0EEKgDAUA1En9z+0qEENhA+FvlUZQqE9Bnynlmlr2iVkSY66AHrg+MddSUiU6VuxqMhYquzcCvO6PZjhx5oTQ+8Bs3EshqIAAAAASUVORK5CYII=),auto;
}
</style>

# 10. An Introduction to Futures

with Elisabet Roselló Román

<center>![Interests](assets/lightcone.gif)</center>

## Geometries of time - frameworks for the future

### Linear

The epochs of evolution are a modern western idea

![Epochs](assets/w10/epochs.jpg)

<font size="2"> *http://labman.phys.utk.edu* </font>

![Spiral](assets/w10/time_spiral_cyclicsml.jpg)

### Network

![Network](assets/w10/graphs-diff-dimensions.png)

<font size="2">*Above: Stephen Wolfram's model of time as a network*</font>

### Spacetime Surfaces

#### Light cone from special relativity
![Light Cone](assets/w10/Rellightcone.gif)

We are observing from the vertices between two cones where the top cone is the future and the big bang is the bottom cone.

#### Other future cones

The Cone of Plausibility originated from Charles Taylor (1990), the ‘futures cone’ model by Hancock and Bezold (1994) was based on taxonomy of futures by Henchey (1978) and adapted by   Joseph Voros (2003) for the Generic Foresight Process, Dunne & Raby for Speculative Everything and Stuart Candy as a Taxonomy of Futures.

![Voros Cone](assets/w10/futurescone-voros.png)

<font size="2">*Above: The “cone of plausibility” Charles Taylor (1990), ‘futures cone’ model by Hancock and Bezold (1994) based on taxonomy of futures by Henchey (1978) adapted here for Generic Foresight Process (Joseph Voros 2003)*</font>

![Possibility Cone by Dunne and Raby ](assets/w10/dunneandrabycone 350n.png)![Stuart Candy's Taxonomy of Futures Cone](assets/w10/stuartcandycone350n.jpg)

<font size="2">*Above left: Dunne & Raby's Possibility Cone showing a range of Dystopian and Utopian possibilities*</font>
<font size="2">*Above right: Stuart Candy's Taxonomy of Futures cone adapted from Dunne & Raby's Possibility Cone, indicating a 'preferable' area*</font>

<font size="2">*Below: Adapted Voros Cone by Sjef van Gaalen*</font>

![Voros section](assets/w10/voros section.jpg)

#### De Sitter and Anti-de Sitter spacetime

![De Sitter spacetime](assets/w10/desittersmall.png)

<font size="2">*Above: De Sitter spacetime - 'On the dynamical emergence of de Sitter spacetime' - Christian Marinoni, Heinrich Steigerwald*</font>

![Anti-de Sitter spacetime](assets/w10/antisit.png) ![Anti-de Sitter spacetime 2](assets/w10/antismaller.png)

<font size="2">*Above left: Anti De-Sitter Spacetime - 'The Multiple Entanglement
Renormalization Ansatz and the Anti-de-Sitter/Conformal Field Theory'- J.C. Borger*</font>

<font size="2">*Above right: (2+1)-dimensional anti-de Sitter space. Based on a similar picture in "The Illusion of Gravity" Maldacena (2005)*</font>


### Quantum theory and chaos - time as a physical process

![Quantum time](assets/w10/uncertaintycrop.jpg)

<font size="2">*Above: from http://quantumartandpoetry.blogspot.com*</font>

![Quantum time2 ](assets/w10/symmetrysmaller.jpg)

<font size="2">*Above: from http://quantumartandpoetry.blogspot.com*</font>

![Time curve ](assets/w10/time-curve2.jpg)

<font size="2">*Above: from http://quantumartandpoetry.blogspot.com*</font>

#### Time Crystals

Time crystals repeat themselves in time as well as space, leading the crystal to change from moment to moment.

![Time crystals](assets/w10/timecrystalsmall.png) ![Time crystals](assets/w10/timecrystals.jpg)

<font size="2">*Above left: from 'Time crystals: a review' - Sacha, Krzysztof et al. Rept.Prog.Phys. 81 (2018)</font>
<font size="2">Above right: from 'Space-Time Crystals of Trapped Ions' T. Li et al., Phys. Rev. Lett. (2012)*</font>

### Multidimensional spacetime

There is not a general acceptance of what time looks like because we are in it. It is probably multidemensional.
![Dimensions of spacetime by Max Tegmark](assets/w10/dimensions.png)

<font size="2">*Above: from Max Tegmarks Dimensions of time*</font>

**References:**

* [Dr Joseph Voros' Voroscope](https://thevoroscope.com/)
* Speculative Everything - Dunne & Raby
* [Max Tegmark (1997)'On the dimensionality of spacetime' Letter to the Editor, Classical and Quantum Gravity, ](https://space.mit.edu/home/tegmark/dimensions.pdf)
* [How different cultures understand time - Richard Lewis]( https://www.businessinsider.com/how-different-cultures-understand-time-2014-5?IR=T)
* [Nesta on Speculative Design](https://www.nesta.org.uk/blog/speculative-design-a-design-niche-or-a-new-tool-for-government-innovation/)
* [What Is Spacetime, Really? - Stephan Wolfram 2015]( https://blog.stephenwolfram.com/2015/12/what-is-spacetime-really/)
* ['Operationalizing Design Fiction with Anticipatory Ethnography'JOSEPH LINDLEY, DHRUV SHARMA,
ROBERT POTTS](https://anthrosource.onlinelibrary.wiley.com/doi/pdf/10.1111/1559-8918.2015.01040)
* [Representations of the Future with Graphs - Near Future Laboratory](http://blog.nearfuturelaboratory.com/2010/09/26/representations-of-the-future-with-graphs/)
* [Sjef van Gaalen on Theory of Change and the Futures Cone.](https://sjef.nu/theory-of-change-and-the-futures-cone/)
* Network Cosmology - Dmitri Krioukov, Maksim Kitsak, Robert S. Sinkovits, David Rideout, David Meyer & Marián Boguñá (2012)
* The Multiple Entanglement Renormalization Ansatz and the
Anti-de-Sitter/Conformal Field Theory J.C. Borger

**Further reading**

* A Brain-like Computer Made of Time Crystal: Could a Metric of Prime Alone Replace a User and Alleviate Programming Forever? Reddy S. et al. (2018) . In: Ray K., Pant M., Bandyopadhyay A. (eds) Soft Computing Applications. Studies in Computational Intelligence.
* Fractal Information Theory (FIT)-Derived Geometric Musical Language (GML) for Brain-Inspired Hypercomputing. In: Pant M., Ray K., Sharma T., Rawat S., Bandyopadhyay A. (eds) Soft Computing: Theories and Applications. Advances in Intelligent Systems and Computing. Agrawal L. et al. (2018)
* [For There We Are Captured—The Geometry of Spacetime - The Physics Mill](http://www.thephysicsmill.com/2013/03/17/for-there-we-are-captured-the-geometry-of-spacetime/)
*[How Quantum Pairs Stitch Space-Time - Quanta magazine 2015](https://www.quantamagazine.org/tensor-networks-and-entanglement-20150428/)

## Chaotic times

We touched on the outcomes of our concept of time.
Languages reveal our understanding of time for example most written languages are linear. Mariana mentioned the film Arrival in week 9 and how in that film language was cyclical or radial and as a result of understanding this language, the protagonist was able to experience time cyclically - experiencing the past and future as easily as the present.

Complexity and chaos was suggested as terms that characterise our times.

A system is complex if it has components that relate to eachother in different ways. Complex systems cannot be addressed in linear ways.

My reflections throughout this term have taught me that chaos is determined through events in the past as exemplified by the Butterfly Effect where it is said that the beat of a butterfly's wings can create a storm across the ocean. This week we were shown how trivial things can wreak havoc in a city.



## Imagining the ridiculous - Black Swans and Black Jellyfish


> Dators 2nd Law: "Any useful idea about the futures should appear to be ridiculous."" - Jim Dator

This week I was encouraged to dream big and put my imagination into warp drive because:

1. The thing you'll least expect is the unexpected - the **black jellyfish** - a ridiculous small thing that you never thought would be relevant causes a big thing.
2. You could create self fulfilling prophecies by obsessing about one scenario and not get to make an informed decision.
3. We are currently in a VUCA state: of volatility, uncertainty, complexity and ambiguity. Here no normals exist - old data and stereotypes will not be as useful as they might have been in the past. This is a space where **black swans** appear - unpredictable appearances that provoke changes and new ideas that are not phenomenological and extend over many systems.
(Climate change keeps producing black swans like ice shelves melting. Other examples include the Occupy movement and the 2008 economic crash)
During the emergence of chaos from complexity, imagination becomes more useful than analysis - foretelling becomes hard to achieve but we can decide what to take seriously and assess potentials.
4. Structures of our understanding of time and space change and the tools we haven't perfected ways for understanding change.
5. Images and narratives we have of the future are stereotyped and outdated.

> Dators First Law: "The future" cannot be "predicted" because "the future" does not exist - Jim Dator






> "The future" can not be "predicted", but "desirable futures" can and should be visualized, invented, implemented, continually evaluated, revised and re-projected. " - http://www.postfuturear.com


<center>![Jellyfish](assets/w10/jelly2.png)</center>



## Practicing thinking about the future

### The Polak game - finding your narrative on how things are changing and what you think you can do about it

We lined ourselves up on a scale of if we think things are improving/worsening; and whether or not we thought  people can/can't do something about the future. I found out that everyone in the class were more optimistic than me. HOWEVER I'm really positive that we can change the future if we want to. By the end of the week I realised that I was 'extending the present' and that this stops me investing my imagination into the future. if more of us were to think of fantastic futures that could be - we could incite some synchronicity and change our direction - change our self-fulfilling prophecies.


### Thinking beyond usual narratives - thinking outside of an 'Extended Present'

<font size="3" color="blue">A non-dystopian Posthuman post-identity techno-cult near future</font>

**Exercise:**

- ⌛ The year where we are projecting is 2050
- 👩‍🚀The aim of this session is learning to think beyond usual narratives, as beyond the tendency to try to “predict better” the future
- ⛔Dystopian or apocalyptical thinking is forbidden
- 👽“Any useful idea about the future should appear to be ridiculous”
- 💡Scenarios and images of the future are used as a guide, or for provoking debate and new insights

The starting points we were given were:

  * an object: "Book"
  * the crossing of 2 axes of development: Post Human and Queer Axes


  <img src="https://gitlab.com/MDEF/saira.raza/raw/b14c13303d4b7d988b764ad3df8dc869229e78e3/docs/wa/assets/w10/posthumanaxis.jpg"  width="200" height="300">   <img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/wa/assets/w10/Queeraxis.jpg" width="200" height="300" >

**Outcomes:**

1. Propose a book that would exist in the scenario:

  We proposed a changed religious text because a) religious texts are the most widely read (and therefore significant) and b) because religion has been the main objecting philosophies to exploration of both synthetic biology and non-binary gender.
  We also considered that a repository of posthuman-gender experiences might be something people would find useful when trying to navigate this new world.

2. How is the governance system of this world?
    * Rights to technology to augment your body will be linked to gender rights
    * Dominance systems would somehow be created by the wealthy unless there was an overhaul of the economic system.
    * With religions being undermined by gender and body rights the post human world would create new cults around technology as their sense of self is so tied in with what these companies produce.

3. How is the Economy?
      * the concept of copyright is being dismantled to cater for people's rights to construct the bosy they feel they need.
      * Richer people will still have more access to technology and the changing social norms that come with them.

4. Description of the world

    'A day in the Apple Monastery'

      Tech monasteries become hubs of belonging and self exploration.
      Programmers are priests
      Apple computers are literally apples (biocomputers) that grow on trees, soon to be distributed around the world.

5. From 2019 what key events have happened to create this new world:
    * first genderless religious literature
    * genderless marriage rights
    * Wearable tech start merging with humans
    * Decline of religion
    * Techno-cults rise
    * Wearable tech is cheap and commonplace

  <img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/wa/assets/w10/map.jpg" width="340"> <img src="https://gitlab.com/MDEF/saira.raza/raw/master/docs/wa/assets/w10/Wired_FutureOfChurch.jpg" width="385" height >

  <font size="2">*Above right: Stuart Candy's The Future of Church*</font>

I found this exercise to be really enriching and gave me permission to think about things that may be deemed frivolous otherwise. I felt we created something of a world where interesting things could happen in narratives and aesthetics.

<iframe width="560" height="315" src="https://www.youtube.com/embed/g6BK5Q_Dblo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Representation in imagined futures
The narratives in science fiction are too old, too repetitive and not representing authentic original relevant voices. Science fiction is such a rich device to explore identity and experience as well as strategy and it feels like people think they need permission to be part of it or to put something of their uniqueness into it. I am really looking forward to contributing something to science fiction as part of this course.

### Afrofuturism through music - defiance, imagination and agency

I enjoyed being reminded how Afrofuturism blossomed through music and of the diverse range of imaginative and exceptional music it has produced. As well as breaking new ground in music technique, style and subject, Afrofuturism has also produced vivid imagery and compelling narratives around music.

![Sun Ra](assets/w10/sun-ra-3.jpg)

Sun Ra's personal mythology based on a perceived experience of travelling to Saturn framed his own past and future with a story of otherworldliness and not belonging. Although Sun Ra was an alienated and lonely individual, as a black man from Alabama during the Jim Crow era reclaiming his narrative in such a way was politically defiant.

Here's a full lecture on Afrofuturism by Sun Ra at UC Berkeley in 1971

<audio
   controls
   loop
   crossorigin="anonymous"
   id="audio"
   src="https://ubusound.memoryoftheworld.org/ra_sun/Ra-Sun_Berkeley-Lecture_1971.mp3">
</audio>

Inspired by Sun Ra, George Clinton's Parliament and Funkadelic formed their own mythology by weaving together a range of Afrofuturist narratives illustrated by their stunning  album covers.

![Parliament](assets/w10/Parliament 385.jpg) ![Parliament](assets/w10/parliament 3 385.jpg) ![Parliament](assets/w10/parliament tromb 385.jpg) ![Parliament](assets/w10/osmium 385.jpg)


With band with names like Cybertron, The Martian and Galaxy 2 Galaxy, and labels names like Red Planet, images and narratives of the future and other worlds featured heavily in Detroit Techno where mythology was again used as a tool to explore pasts and futures. Co-founder of the influential Underground Resistance label "Mad" Mike Banks had in fact played as a studio musician with Parliament/ Funkadelic and the influence of Afrofuturism was woven into the Detroit Techno genre early on.

> It's a very strong belief that we're not from here. That we're from the planet Sirius, in the constellation of Orion. if you are Afro-American and you are in a country where your relatives were not able to practice the culture from where they were from because they were slaves brought over from Africa you adapt in other ways. You recreate your universe. It was done a long time ago out of necessity to stay sane in an insane world.

*Jeff Mills, Detroit techno pioneer*

Notably from the Detroit Techno camp, Drexciya used a mythology which drew a direct line from the underwater world they created through their music to slavery and a shared past of many African-Americans. Album notes explain that this world is inhabited by the children of slaves thrown overboard slave ships that mutated to survive underwater.

<iframe src="https://www.indiegogo.com/project/the-book-of-drexciya-volume-one/embedded" width="222px" height="445px" frameborder="0" scrolling="no"></iframe>

![Drexciya](assets/w10/drexciya 300.jpg) ![Model 500](assets/w10/model500 300.jpg) ![Subject label](assets/w10/subject.jpg)

<font size="2">*Above left to right: Drexciya graphic; Model 500 album cover; Subject Detroit label's planet logo.*</font>


These examples show how Afrofuturism uses narratives to constructively express painful pasts and redefine the present and future to give agency.  Today, The [Afrotopia](http://www.afrotopiaisnow.com/) arts programme in Detroit consciously utilises these methods to create "imaginative and effective solutions to address social challenges by fusing elements of science fiction, magical realism, ancient history, and non-western cosmologies"


I think it is also significant that female artists like Grace jones and Missy Elliot used Afrofuturist imagery to depict bold, powerful images of femininity - a far cry from the tame vision of [Sophia the Saudi citizen robot](https://www.wired.co.uk/article/sophia-robot-citizen-womens-rights-detriot-become-human-hanson-robotics), who ended up being used as a (marketing) tool for her male masters - a trope that has been well explored in science fiction.

![Grace Jones](assets/w10/gracejoneshead.jpg)  ![Missy Elliot](assets/w10/missy 360.png)

<font size="2">*Above left: Grace Jones still from 'Slave to the Rhythm video'*</font>
<font size="2">*Above right: Missy Elliot from 'Work it' video*</font>

Controlling destiny is an important goal of Afrofuturism and this remains as poignant as ever today.
A Tribe Called Quest's last album, released in the wake of the 2016 U.S election opened with an Afrofuturist anthem "Space Programme"
calling for us to "Make something Happen". The video pays homage to a scene from Sun Ra's film Space is the Place.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qTrqmNieVKI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> ![Space is the place](assets/w10/sunrachess.jpg)

### The inclusive nature of punk
I've found punk outlooks and methods coming up through discussions over the past 12 weeks.  DIY in a perceived dystopian "no future" world characterises this ideology adapted since is popularisation by Cyberpunk, Steampunk and Afropunk.  I find Punk's  DIY  approach to aesthetics and music-making admirable in that they are inclusive by design. Punk practices resonated with me during our exploration of Design by Experience and fast prototyping.

![Mad Max 2](assets/w10/madmax 300.jpg)  ![Tank Girl](assets/w10/tank girl 300.jpg)  ![The Matrix 3Tank Girl](assets/w10/matrix 300.jpg)

**Reading List**

* [Afrofuturism 2.0 & The Black Speculative Art Movement Notes on a Manifesto Reynaldo Anderson](https://monoskop.org/images/a/a9/Anderson_Reynaldo_2016_Afrofuturism_2.0_and_the_Black_Speculative_Arts_Movement.pdf)
* More Brilliant Than the Sun: Adventures in Sonic Fiction - Kodwo Eshun
* Vessels of Transfer: Allegories of Afrofuturism in Jeff Mills and Janelle Monáe- Tobias C. van Veen (2013)

**Other websites**

* Afrotopia: http://www.afrotopiaisnow.com/
* EOS Detroit for how blockchain can help detroit: https://eosdetroit.io/
* Detroit Scifi Generator: https://detroitscifigenerator.wordpress.com/
* Samba Rhino Techno Resistance and Black Futures festival 2017 site https://www.sambarhino.com/techno-resistance-and-black-futures-2017

** Articles**

* [Detroit Activist on why blockchain needs afrofuturism](https://breakermag.com/why-this-detroit-activist-thinks-blockchain-needs-afrofuturism/)
* [Alan Oldham: The Art of Techno Futurism](http://daily.redbullmusicacademy.com/2014/11/alan-oldham-feature)
*['Underwater electronic futurism, in the words of James Stinson (Drexciya)'- Peter Kirn (2018)](http://cdm.link/2018/11/underwater-futurism-drexciya/)

## Applying this weeks learning

### Using futures theory and tools in research projects.
Futures theory and tools would help my research projects in emergency water and sanitation. I would like to see if this is a path the NGO sector has explored yet.

### Tying research with experiences using speculative design

There is a clear research stage in speculative design. I could use speculative design as a path for my research practice as a means to gather data.

#### Experiential Futures
From the offset I have been trying to link experiences with the measurable, data and theory. The "character" of a place - when a place feels like it has a personality is something I find compelling. Connected to this are the explorations of JG Ballard on the psychological effects of technology and environments and of Psychogeography on utilising subjective "emotive resistance" to environments to re-design them.
Experiential Futures is something I'd like to explore as it ties together these interests with my interests in using light, music and stories to create an engaging experience.
Some experiences I would like to explore include:
* multimedia - interesting syntheses of film and music
* immersive theatre
* film in 3d
* documentaries in 3d

**References**

* [Design Fiction Field Guide - Bruce Stirling](https://www.wired.com/beyond-the-beyond/2017/06/design-fiction-field-guide-ethnographic-experiential-futures/)

#### Design Fictions

![design fictions](assets/w10/desfic.jpg)

> “(Design fiction is) the deliberate use of diegetic prototypes to suspend disbelief about change.”

> "Diegetic" is from film and theatre studies. A movie has a story, but it also has all the commentary, scene-setting, props, sets and gizmos to support that story. Design fiction doesn't tell stories - instead, it designs prototypes that imply a changed world. "Suspending disbelief" means that design fiction has an ethics.... The adept of design fiction comes to realise that every object, even a common fork or dad's boring tie, is "diegetic". They're all background props in some grander story..

*[Sci-Fi Writer Bruce Sterling]( https://slate.com/technology/2012/03/bruce-sterling-on-design-fictions.html)*

Design fictions combine research methods with Speculative techniques. By producing prototypes the audience is put into future scenarios and is forced to consider the experience

It can also be cheap and easy.


**References:**

* 'Science fiction prototypes: Visionary technology narratives between futures' Article in Futures 50:15–24 · Frances Bell et al (2013)
* ['THE NEAR FUTURE LABORATORY DESIGN FICTION PRODUCT DESIGN WORK KIT 0-TBD-D012' Bruce Sterling (2014)](https://www.wired.com/beyond-the-beyond/2014/03/near-future-laboratory-design-fiction-product-design-work-kit-0-tbd-d012/)
* https://www.wired.co.uk/article/patently-untrue
* Speculative Everything: Design, Fiction and Social Dreaming by Dunne and Raby


### The importance of exploring Unknown Unknowns.
It was a revelation to hear about Unknown unknowns this week. The theory behind "futuring" could be a really engaging way to teach critical thinking.




<img class="cactus" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/w10/graphs-diff-dimensions.png">
<img class="egg" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/w10/antismaller.png">
<img class="dog" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/w10/timecrystalsmall.png">
<img class="paint" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/lightcone.gif">
<img class="planet" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/w10/epochs.jpg">
<img class="tv" src="https://mdef.gitlab.io/2018/saira.raza/wa/assets/w10/time_spiral_cyclicsml.jpg">



<!-- ### Geometry and Dynamics and Systems

### The role of contradictions in complex systems
> Contradictions, which are irreconcilable views and perspectives, cannot be resolved: they can only be transcended. In other words, contradictions have to be synthesized and necessitate the formulation of a new position that incorporates most of the various different positions. Contradiction often provide the first signs that a system is moving towards complexity, chaos and eventually postnormality.

* https://postnormaltim.es/essentials/3cs

#### Behaviour change theory and non linearity


--------------


## History of futures

We were taken on a journey trough how the future has been interpreted from Ancient futures right up to emerging cultural trends like Left Accelerationism / Alt Woke

### Ancient futures

MAYA
expectations / desires and fears
oracles
4 horsemen of the apocalypse  - british museum images

### Modern Futures
* different understanding of timelines no words for future - Arrival
* the concept of progress
* utopias 500 year old concepts - political construct
 - thomas moore - new world utopias of the puritans? pilgrim fathers. submarine inventor was a socialist utopian.

### 2nd industrial revolution
 jules verne
 Albert rorids
 the world beyond - william morris - critique of modernisatoin - rural

 paleofutures.com

### The advent of cinema
segundo chomon el hotel electrico
collective fears and hopes after wars changed
dark side of robots starts after ww1 and 2
futurism in italy - like transhuman
soviet films
metropolis
city of skyyscrapers 1925  - bcn references
sexual robots

### golden age of sci fi
aesthetic gets ramped up  
los angeles airport
jetsons  
dune
lovecraft
j.g ballard high rise

### postfuturism
cyber punks
actual Punks
philip k dick
bruce sterling -  

### Posthumanism
ghost in the shell   -
matrix cyber punks

### Retrofuturism
steampunk
la cite des enfents perdu

### year 2000
plasticy decadence

### technoutopianism
the future needs tech

### Transhumanism
transcending our boundaries
x men?
singularity
anti aging
a worker for google is spearheading Transhumanism movement

### Digital nostaliga, vapourwave and fashwave
current Retrofuturism
neuromancer
last decade transhumanism is stronger

### Postcolonial Futures

#### Afrofuturism
working more with music

<iframe width="560" height="315" src="https://www.youtube.com/embed/Z8nppTSY-Rs?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

last decade its grown a lot


#### Gulf futurism
sopohia al Mariana
reza negrani
fatima al qadiri


#### Queer afrofuturism
alien metaphor


existenz!  or anything by cronenberg

#### pakistani


 afrofuturism
miles davis, sun ra


#### Futures as prototypes
kevin Kelly
https://kk.org/

#### Design Fictions!!
superflux


### Posthumanism

rosi braidotti

"A Cyborg Manifesto" is an essay written by Donna Haraway

Cyborg for empowerment beyond gender

movement from philosophy
connected to speculative realism
Thinking outside of anthropocentrism  

postcoloniality - non western countries reclaiming their roots.

gentrification is a bit like this.

 - trying to be more autonomous.
de-colonialisation

new bdies

jason hopkins freaky bodies


### left accelerationism alt woke
 - post capitalism fututre

 -

** Magic
arthur c clarke and Magic
magic machines!
the electric hotel - objects move like Magic **


wendell bell 1997

1. time is Continuous - the future is going to be different.. its about change

Eleanora mMssini

in the 1990s the cone was proposed
 dunne and raby  vesrion  updated this

 http://cargocollective.com/wongxianshun/following/all/wongxianshun/We-Are-As-Gods

 joseph voros blog

 quantum cones?  http://www.dileepnanotech.com/quantum_entanglement_quantum_communication3.html

we cannot imagine something we do not know.

from circles to cone "unknown unknowns"
probability v possibilities
desirability v palusibility

acknowledged or possibilities - knowing and ignoring

xeno - futures (xeno means strange  - fear o f the strange)
http://www.postfuturear.com/


postfururear cones show importance of Narratives


knowledge and unknown

known unknown is climate change
elephant in the room
unknown unknowns -  asteroid / cthulu


## Narratives

Narratives are something I've been thinking about in a [side project on Fiction](https://mdef.gitlab.io/saira.raza/fictions/).

It was mentioned by a guest speaker that stories give us strategies so narratives give us a constructive way to think of what to do in relation to the future.

There is generally a lack of consensus between narratives so we cant do anything.
2001 for a solution states

### Types of narratives

#### Narratives provoke strategies
structures:
* structures mega Narratives
* grand Narratives
* images
* hype and advised
* scientific knowledge is a narrative!
* interesting field - **hyperstitions** (really new - narratives that have agencies ) , hauntology e.f slenderman Alex Williams . creepypasta - like big foot - specifically  fiction feeds off reality really fast.

### Tools of narratives

#### utopias and dystopias

Eleanora Masini
https://www.sciencedirect.com/science/article/pii/S0016328706000462

#### narratives of hanges go in waves.
  patterns are breaking down in waves.
  weather forecasting is breaking down

  hype cycle

  dr wendy schultz

#### Ethnographic Futures

####   **experiential futures** and speculative design  
ex futures - creates an experience includes smells and lights
stuart candy situation lab
https://situationlab.org/about/

Diegesis
e.g methril, sonic scredrivers .. the object doesnt explain the story

**Design Fictions**
conveys research processes with speculative techniques.

**spec design**  - more close to artistic goals - generating discussion after
dunne & raby
united microkingdoms

![Speculative Design Map](specdesignmap.jpg)
*A mapping of the Speculative Design field by Elliott P Montgomery*

superflux  

the goal is the object not the process.
provocative objects.
includes films?

critical design - more critical not commercially oriented.

dunne and raby manifesto.
https://www.researchgate.net/figure/Dunne-Raby-A-B-A-Manifesto-2009_fig4_282460352
Sophia is a speculative design object.

wizard of oz

dunbar coin



**References**

* [TOWARDS A THEORY OF THE INVESTED OBJECT THESIS (Submitted) - Sarah Groff Hennigh-Palermo](https://static1.squarespace.com/static/57d697372994ca7d09b61e2c/t/5860325fb3db2b35c240f68c/1482699384261/sarahghp_thesis_toPrint.pdf) also http://www.lost-time.club/the-paper/

* [The Young Designer’s Guide to Speculative and Critical Design - Leon Karlsen Johannessen](https://www.ntnu.edu/documents/139799/1279149990/16+TPD4505.leon.johannessen.pdf/1c9221a2-2f1b-42fe-ba1f-24bb681be0cd)


## Change

who studied:
Hegel, Marx, Foucault
future studies
intelligence studies
indirectly
Sociology and anthro
economy
phys
phil
theory of change

**change has been interpreted as waves and lines**

### Diffusion of innovations
1962
Innovators
early adopters
early majority
late majority
laggards

> The future is already here - its just not evenly distributed

- William Gibson, Science Fiction Writer (The Economist 2003)


Steward Brand  - father of hacking
graph of change
Updated by Grantcraft



Culture is a slow changer
link to changes of social norms and **behaviour change**
confirmation bias - openness to accept things we believe. we find proof to back our beliefs
culture is hard to change

evolution
innovations are mixed with previous innovations
mixing different sources not linear
**non-linearity**
betul kacar future shaped by pasts that could have been
the same happens


### Trends
cultures, behavioural phenomena
representation of complex elements of **dynamics** in human and non human systems. You have to be familiar to what trends look like so you have tools to work with. and be critical of them.

Hype comes from hyperbole
Don't believe the HYpe
Terry Flew definition of hype

#### Fad
emerges and dies really quickly

#### Taxonomies of trends
secular and cyclical trends
 - by shape - cycles, ven diagramms - cool graphics online to check out
 - micro and macrotrends
 - duration
 - they have interrelations
  - drivers (triggers, catalysts, constructors, bloacker) you also get laws that do this
  - signals or general indicators (innovations, unexpected changes, black swans)


#### Dynamics pf trends or change

Emergence
growth / survival
Stagnation
Death



### Systems thinking
Nets nodes and dependencies

In design there has been a shift from user-centred to complexity thinking
useful to do before interventions to get the desired net effects. Probably as an effect of the increasing complexity of the times.

what is a system?
set of elements that are interelated
ecosystems - systems of systems - 1990s misunderstanding of the net effects of these.

Dick Gently? Douglas Adams


" A sound of thunder" Ray Bradbury
butterfly effect
theres a shit film called the butterfly effect.

* feedback loops
* Hierarchies
* behavior over time

### Horizon scanning
POlitics
INformation and communication
Nature
Conflicts
health
arts& cultre


### Mega trends

non everything global is good

* Oligoppolisations
Big companies are getting bigger

 * State-Nation Crises (catalonia, scotland) - nation-state Brexit, Criticism of the UN
* non-state actors (or power)
companies, NGOs (conspiracy theories?) like amazon have more wealth than some countries. Some people do too
* feminism
* Queeer movement
* Climate Change
* Environmental awareness
extinction rebellion (really current) in London
* Hyper reality -
* Post-truth era
personal trust over science or experts
chnanging politics
* user, human-centrism
anthropocism
* Transhumanism
* new intimacy and privacy structures
 selfies
* postdigial - moderating our digital presence


seems to be a breakdown of trust in general - megatrends intersecting (post-truth, state-nation crises)  into conspiracy theories rampant confirmation bias and weird symbol guides



extended present
 - nothing new, old ideas (yesterdays tomorrowa, predictions. deterministic.
   future studies isn ot this. it is not predications it is forescast - probabilisitic. essentially pluralistic.
   we become relevant if we look at more than one future.
   it gives us choice
   dont worry about it because we are taking care of it.
    population exponential growth
    moores law

    to make a forecast:
    what do we know
    what do we NOT know
    what do we need to learn about them
    - all about learning
the above is about finding trends
a measurment of change over time
has to be quantifyable
there has to be time frequesncy.

trends:
* emergence
* take off
*slow down up to structural limits
* stagnates / rebirth/ decline

at emergence and stagnation qualitative stuff happens

structural limits coincide with  material / information shifts?


#### Future scenarios

how a futures comes to be and the effects
* consistent parts
* coherent

we dont make a scenario to get it right - its to make you understand ge implications

* plausibile - -without using the present as a criteria
the less likely futur is one that is like the prssnent
* questions the present.

mont fleur, south africa
 logic of scenarios


 pick 2 drivers as axes for forecasting but its reductive

 what kind of changes are we considering:
 * stability
 * Constant change


betting money just on trends is risky and dangerous - sitting ducks
the future is more than sum of it parts
 there might be bias at work.

 black elephants - capacity to believe what we're already being told  - matching beliefs - climate change denial, changing economic models.
the oil industry


 belief v data that affects our decisions about now affect our ideas about the future
 brainwashing
 conditioning
 e.g immigration

 colonising futures
 people need to be able to make their own choices

people dont often want to take responsibility for their future collectively.



#### Familiar Futures

future image:
jetsons
daisy -

Blade runner / her /

novelty rather than radical change

Data is overated when talking about the future

Where does novelty come from?

museum of water


during emrgence imagination becomes more useful than analysis  - not to foretell but to DECIDE WHAT TO TAKE SERIOUSLY. working on assessing potential.


### outliers are not noise

they may be a black swan
focus doesnt identify black swans and black elephants
they can be oppootunities

cognitive biases support existing models

our brains save us from thinking
#### and use linearity
sapiens - harare

non linear thinking?

#### dichotomy
if something is true the opposite is false

but there is quantum states - both and none
multiplicity

#### induction
finding patterns in what you see

but what about what you dont see - black swans

the death of death - a book
jose luis disortno? futurist -


women robots
sophia


Alpha go - netflix
after deep blue defeated kasparoff

posthumanism asks us to reasses what it means to be human.

cyborgs

machines are not cruel as far as we know

Unthought futures
the things we create devolve into the exact opposit - e.g the EU
- how is our prerception of the future be limted
- does our worldview change the understanding of change? brains are physically changed by learning languages.
we learn that time is linear through language.
world views created by cultures save us from thinking. we teach kids to form lines.
-


contradictions

complexity - cannot be addressed in a linear way
short term influence
a system is complex if it has components that relate in different ways

chaos
something trival can bring havc to a city.


jellyfish - like warm acid water - great for them is climate change
blocking powerplants
they kill fish

jellyfish a weird.


hyperconnectivity - a tool for the seeds of chaos - we are global.
the worldview has changed
impacts scaled up.
there are thresholds of impact.
that diagram about opportunities (tomas' model)

accelerated
expansive
escalating
simultaneous

postnormal change:
speed
scope
scale


the future can be cliche
could we need to unlearn knowledge
do you think because of what you see or do you observe what you know.

watch out for confirmation of your expectations.

challenge why you think, ask and say stuff

### Unthought
trump is the unthought.
what lies outside our world view

but it is not unthinkable.

beliefs sit in worldviews
the belief in always being the hero
the belief in honor


some mindtools.
strategies for futuring:
start with
* extended present - look for black elephant
* familiar futures - black swan
* unthought futures (worldviews) - black jellyfishes
and repeat and look for them throughout the process
and look for chaos contradictions and complexities throughout.


futures for muslim societies

profiling is failing.

black jellyfish - escalators under certain conditions that we dont believe they exist.

practical dynamics


### Research in Design for futures

 - product centred
 - methodic design emergence of participation in design, 60s onwards
 - user-centred since the 80s
 - human collective-centrred enzo manzini 00s onwards
 -service design
 -strategic design - transition design


### Types of reasoning
- deductive - top down logic - sciencce
- inductive reasoning -
 - abductive

experiential futures

 liam young

Research in design for futures.
speculative:
 - an emerging issue, a critic trend
 small research
 prection of scenarios
 prototype development
 exhibition
 data gathering

 A

 Causal Layered Analysis
 links myths/images to discourse to science to litany (official)
Problem > causes > worldview >

#### Scenario Building

* 2 axis matrix or shoemaker

* Dators 4 futures
continuation
transformation
discipline/saturation
decline or collapse

* simple extrapolation of trends i alinear way


Old Narratives

### Apocalyptical thinking - Thatcher and Mad Max

Elisabet talked about how within our lifetimes apocalypse has come to signify the end of Capitalism. During Margaret Thatchers leadership in the UK Communism was still pushed as a dystopian system. "there is no alternative" international
communism was seen as dystopia
capitalism drove apocolyptic visioms
the other side were elysieum (movie)

When the Berlin wall fell down, for a generation it might have felt easier to imagine the end of the world rather than the end of Capitalism.
The aesthetic of overrun industrial buildings can be fetishised into a symbol of the failure of capitalism.

abandonned things in detroit

The same can be said from a post USSR point of view of

https://www.newstatesman.com/culture/art-design/2016/08/ruin-porn-art-world-s-awkward-obsession-abandoned-soviet-architecture

#### The worst version of humans collectivised - hunger games, 1984, the handmaids tale

#### Transhumanism - agenda and idealism we have a moral need to transform human evolution  - bionic arms, cyborgs elon musk

#### Posthumanism

#### Retrofuturism
People were more in love with the future in the past

-----

## Resources

### Articles

### Websites

* postnormaltim.es

* nearfuturelab


### Books



--!>
