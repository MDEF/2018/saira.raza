# 06. Designing with Extended Intelligence

## What is intelligence?
**The nature of intelligence** Plato took a top-down approach to intelligence based on a hierarchy of symbol allocation whereas Aristotle took a bottom-up approach based on a hierarchy of experiences.

**Testing whether or not a system is intelligent** Ways of testing whether or not a system is intelligent question whether or not self awareness is necessary for intelligence. Turing's test relied on using human experience to rate whether or not a system simulates human intelligence whereas Searle's questioned whether this method was merely symbol matching not awareness of the language. We questioned whether or not a system needs to be self aware (or conscious) in order to be intelligent.

**The Symbol Grounding Problem** is a fundamental problem in how to objectively define “the content of how something is” when it is subjectively created? There is no way of measuring what experiences things cause in individuals because the possibilities are endless because each readers system of reality is unique. How can we prove that someone else knows / experiences what we know/experience?

** Groundwork for exploring AI **At The Dartmouth Conference in the 1950s a collection of leading thinkers across fields of neuroscience and engineering tried to define how to research Artificial Intelligence. This produced topics to explore:
* Automatic computers
* How can a computer be programmed to use a language
* Neuron Nets
* Theory of the size of a calculation
* Self-Improvement
* Abstractions
* Randomness and creativity


### Cybernetics
Norbert Wiener defined Cybernetics in his book published in 1948 as the "the scientific study of control and communication in the animal and the machine." This enabled a visualisation of the elements needed for intelligence which include: databases, AI decision-making and machine learning.


## Describing ML methods
Machine learning is all about labelling with symbols, comparing and clustering.
The chain (of symbols) is only as strong as the weakest link - the weakest symbol you apply to a concept.


### supervised learning
kmeans - minimising distance betweek similar objects
PCA - factoring for significance
regression
decision trees - lines of best fit to Categoriesdecision trees to implement logic - deduce properites

### Unsupervised learning discovers emergent properties
labels come at the end not at the beginning.

### reinforcement learning
learns by example


## Neural networks
It was really interesting to go through the logic of neural networks and see the kind of results it can produce. The neural network is an exhaustive run of possibilities which can produce zero manipulation if un tuned

https://playground.tensorflow.org


### Bias
The process of fragmenting information, running these pieces through a probability machine and then tuning the machine to produce desired results reminded me of the process of bias in humans.

## Machine Learning
Machine learning uses labelling with symbols, comparing and clustering.
The chain (of symbols) is only as strong as the weakest link - the weakest symbol you apply to a concept processed by the system determines the limit of usefulness.

### Supervised Learning
k-means - minimising distance between similar objects
PCA - factoring for significance
regression -
decision trees - lines of best fit to Categories and decision trees to implement logic - deduce properites

### Unsupervised Learning
Labels come at the end not at the beginning. discovers emergent properties

### Reinforcement Learning
This is learning by example






## Studies into Artificial Intelligence
A history of Artifical * ** An early map for exploring AI **At The Dartmouth Conference in the 1950s a collection of leading thinkers across fields of neuroscience and engineering tried to define how to research Artificial Intelligence. This produced topics to explore:
  * Automatic computers
  * How can a computer be programmed to use a language
  * Neuron Nets
  * Theory of the size of a calculation
  * Self-Improvement
  * Abstractions
  * Randomness and creativity

### Cybernetics
Norbert Wiener defined Cybernetics in his book published in 1948 as the "the scientific study of control and communication in the animal and the machine." This enabled a visualisation of the elements needed for intelligence which include: databases, AI decision-making and machine learning.



## Breaking down an AI system into elements
On groups We devised hypothetical AI systems. We were advised that AI tasks tend fall somewhere on a spectrum between Analysis and Design tasks.

Our group talked briefly about an **embodied AI nanobot** that could be put inside the human body to perform medical tasks. This reminded me of natural glass frustule structures of dead diatom algae which are being investigated as housing for nanobots.

We then explored a concept of an **AI jury**.
Here is a summary of our thinking:

|   Elements of the AI   | Thoughts | Critique |
| ------------- |-------------| -----|
| Goals | To reduce prejudices in the gathering of evidence, presenting of evidence and in decision making by juries and due to social pressure, social psychology, emotions and risk of bribery - by replacing juries and lawyers and questioning police | |
| Tasks | 1. To ask and record information from defendants and witnesses in a methodical manner | Language recording requires a level of machine learning (and therefore data and processes) |
| | 2. To ascertain inconsistencies in evidence using a set of questions that form a logical framework | Language interpretation requires a level of machine learning (and therefore data and processes) |
|  |  3. Critical analysis to find the probability of guilt through logic and machine learning of previous  rulings based on similar evidence | Highly complex tasks as language will need to be interpreted, labelled then processed in two ways - in the framework of evidence in the current case and then through machine learning using data from past cases|
| Inputs  | 1. Responses (verbal and physical in the case of lie detector tests) on questioning of the accused and witnesses | As with the current legal system, the defendant's vocabulary and emotions in the situation  can their affect their ability to be clear and avoid misinterpretation. To offset this there could be an AI that could interpret the defendants case in the most positive light possible (an AI lawyer) and an AI prosecuting lawyer that does the opposite |
|  | 2. A repository of data on: Laws, evidence from previous cases and outcomes of those cases. Data on questioning of the accused and witnesses | The preposition that the law is currently executed with conscious and unconscious prejudice will pass on the prejudices of the past in calculating the probability of guilt. It was proposed that we could run a large number of cases without the machine learning from past cases before going live and this would offset the "amount" of decisions that were likely prejudiced |
| Outputs | neutral presentation of evidence, a probability of guilt and a reasoning behind the probability for the judge | The lack trust of such technological systems will need to be developed for humans to want to use it as explanations given by machine learning especially can be full of misinterpretation |
| Context | Implementing laws to remove unfair prejudices | 1. Someone asked to consider if all prejudices and bias are bad - bias exists because of a reason  2. It is hard to spot bias in language with an AI, so is it likely that it can spot mere inconsistencies based on language?  |
| Other thoughts| 1. The system would need to be as secure from hacking as possible so would not be online. It would also need to be written in a unique or heavily encrypted language | |
| | 2. We wondered if the inverse of this system would be a blockchain jury | ||


We realised that we are a long way from this being something that could be trialled or be socially accepted.

Other groups in our class explored:
*  networked ideas involving distributed sensors and touch points.
* trying to measure immeasurable quantities such as the happiness of a pig contained in a meat product or loneliness.

## Symbols

> "If you can objectify information and attribute a symbol to it you can process that thing artificially"

Taxonomy, classification and symbols are key to processing information.


## Artists using Machine Learning

### Kyle McDonald

Art can allow us to comprehend our interactions with data and how anything can be seen as data.

Stories can be made out of Data.

Machine Learning is learning by example.
It uses labels to predict categories and produce neural networks.

It can take a representation of a thing and try to  invert it.

it can find similarity and label items as similar and categorise theme

### Brian Eno  - generative music

oblique strategy cards.
 reflections app
 https://www.youtube.com/watch?v=4__AGU2AlJU
 http://artssantamonica.gencat.cat/en/detall/Lightforms---Soundforms
 https://www.soundonsound.com/people/brian-eno


### Steve Reich
3 tales
https://www.youtube.com/watch?v=ff11eik33xI


### Charlemaigne Palestine
 in his drone pieces uses actual music actual instruments which seems to produce a musical analogy of a neural networks

Erkki Kurenniemi

 ## Sound
 The schematic of the neural network reminded me of an electronic music hardware set ups with knobs modulating sound waves and also of valves.

 MOdular synthesis


 Valve amplifiers were used to amplify sounds

 Can neural network be used to pick out certain parts of sounds and play them like an instrument?

 Here are some instruments that use neural networks:
 https://magenta.tensorflow.org/demos/web/
https://www.youtube.com/watch?v=iTXU9Z0NYoU&feature=youtu.be


 https://deepmind.com/blog/wavenet-generative-model-raw-audio/



### Machines making art

There have been several experiments of machines being programmed to make art. It dends up just reflecting on the thoughts of the programmer in a disengaging way
Sony - beatles songwriting machine
https://youtu.be/LSHZ_b05W7o



## Machine-like Learning seen in humans

### Is the internet introducing machine learning elements into human culture?

Is the internet is presenting information more suitable for what an AI is designed to process than what a human is designed to process?
Can this drive humans to learn to process the information differently?
Are humans processing information more and more like AIs and can we see this in more creative and social activities?

* Evidence
  - The homogenisation of popular music and film writing is data driven


 Reasoning:

 - large amounts of **compressed discretely packaged information** from the internet from different sources that lack context is being used to make decisions.
 - the internet gives opportunities for **more and faster feedback signals** which can be unrepresentative and disproportionate to the outputs that trigger them and can go viral. These encourage risk-averse social or cultural decisions.
 - **Misinformation** is rampant and words are being misused and pushed as buzz words to unify and identify people around political causes rather than for debate. This degrades words as symbols representing reality as information is lost and with it some potential understanding of reality.
 - In business, **with a proliferation of stored data, reverse engineering is easier** to find the most widely, easily accepted products (for music, for films).




![How data saved music](assets/w6/How-data-saved-music.jpg)

*"How Data saved Music" by Valerio Pellegrini, Ben Fraser, F. Scott Schafer, Kevin Gray from Wired UK, May 2015 Issue*

 ### The internet's effect on human learning and understanding

 #### Is the internet driving machine learning into human and cultural behaviour?
 * Evidence
     - The dramatic rise in populism (matching),
     - the homogenisation (matching) of popular music
 * Reasoning:
     - condensed information lacking complexity and context is being used to make decisions
     - numerous and fast feedback loops which go viral and can be disproportionate to the outputs (as they dont have feedback loops of their own) encourage risk averse (literally more conservative) social or cultural decisions. In business, reverse engineering to find the most widely, easily accepted products (for music, for films)

 #### Changing or loss of meaning to words and taxonomy is leading to mismatching of reality to our idea of it
      Although humans have evolved to be far more intelligent than machine learning, is there evidence that the changing of taxonomy and symbols is leading to a lack of intelligence - ability to learn and adapt and monitor our environment. There is evidence that people are starting to use words that people don't know the meaning of


#### Machine Learning concepts affecting politics

Adam Curtis proposes in his 3 part documentary that at some point in designing machines we decided we were machines ourselves and gives evidence in this through examples in philosophy and politics. Cyberutopians of silicon valley believed that with Computer and networks   human beings were linked they could create their own kind of order.  the feedback connected by nodes system.
in the network would create a stable (power) governance system. This was the thinking behind the stock market

<iframe frameborder="0" width="480" height="270" src="https://www.dailymotion.com/embed/video/x5u8bgk" allowfullscreen allow="autoplay"></iframe>




In the field of ITCs and gender studies, project data describes technology to be gendered in design and use.
"Despite the lack of gender-specifc quantitative
data, project-level qualitative data have established
that ICTs are not gender neutral. ICTs impact men
and women differentially" -





### Human Intelligence and Artificial Intelligence
> “The best material model for a cat is another, or preferably the same cat” - *Arturo Rosenblueth and Norbert Wiener (founders of cybernetics, 1945)*

It has been tempting to look for analogies between human and cultural intelligence systems and the AI processes we learned about however it would be foolish to ignore the glaring difference between computers and humans - materiality - the stuff we use to process information (neurones) living and dying in a human body system are very different to silicone crystals sat icomputers view of a train journey

We assessted the ethics of our system. It was responding to an ethical problem so this process has been interestingg to find out the ethical mechanics of AI.

We decided that using a system that has no awareness of its own logic and compute probability is what bias is. So using a bias to counter bias seems a futile excersise.
If the system were to operatoe on 100% machine learning it would require huge amounts of data to compute sentences and compare them to laws based in human languages. It would require a large bank of comparable interviews with a wide variety of people which in of itself would take a lot of time.
And in the end decisions made by computers are not easily culturally or politically accepted.
The point that Bias is a learning tool in humans (a short cut) to judge probabilities of behaviours.
In the end it would take a lot less resources and would be much more sustainable to try and make people conscious of their own biases. Creating effective communication interventions to help people do this might help.

## References


All watched over by Machines of loving Grace (2011) - Adam Curtis

Hafkin, N. (2002, July). “Is ICT gender neutral? A gender analysis of six case studies of multi-donor ICT projects.”

http://www.generativemusic.com/

How Anticipatory Design Will Challenge Our Relationship
with Technology Joël van Bodegraven

http://moralmachine.mit.edu/

http://aiweirdness.com/post/178619746932/imaginary-worlds-dreamed-by-biggan?fbclid=IwAR2afmCLrAJizWA0MfMkCTCJBU8qWgPSnv11n2cRAfJKGNAuuOAOSQOoXwg

http://aiweirdness.com/

Research Papers:
* Computer Machinery and Intelligence - Turing (1950)
* Minds Brains and Programs -Searle (1980)
* A PROPOSAL FOR THE DARTMOUTH SUMMER RESEARCH PROJECT ON * ARTIFICIAL INTELLIGENCE J. McCarthy , M. L. Minsky , N. Rochester, C.E. Shannon 1955
* How Anticipatory Design Will Challenge Our Relationship
with Technology Joël van Bodegraven

The Book of Why: The New Science of Cause and Effect  – May 15, 2018 Judea Pearl  (Author), Dana Mackenzie  (Author)

<style type="text/css" media="screen">
  .gr_grid_container {
    /* customize grid container div here. eg: width: 500px; */
  }

  .gr_grid_book_container {
    /* customize book cover container div here */
    float: left;
    width: 98px;
    height: 160px;
    padding: 0px 0px;
    overflow: hidden;
  }
</style>
<div id="gr_grid_widget_1542518100">
  <!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->
    <div class="gr_grid_container">
<div class="gr_grid_book_container"><a title="The Book of Why: The New Science of Cause and Effect" rel="nofollow" href="https://www.goodreads.com/book/show/36204378-the-book-of-why"><img alt="The Book of Why: The New Science of Cause and Effect" border="0" src="https://images.gr-assets.com/books/1516890908m/36204378.jpg" /></a></div>
<noscript><br/>Share <a rel="nofollow" href="/">book reviews</a> and ratings with Saira, and even join a <a rel="nofollow" href="/group">book club</a> on Goodreads.</noscript>
</div>

</div>
<script src="https://www.goodreads.com/review/grid_widget/89166601.Saira's%20designing-with-extended-intelligenc%20book%20montage?cover_size=medium&hide_link=true&hide_title=true&num_books=20&order=a&shelf=designing-with-extended-intelligenc&sort=date_added&widget_id=1542518100" type="text/javascript" charset="utf-8"></script>
