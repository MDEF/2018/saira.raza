# <center>Resources</center>

## Books

<style type="text/css" media="screen">
  .gr_grid_container {
    /* customize grid container div here. eg: width: 500px; */
  }

  .gr_grid_book_container {
    /* customize book cover container div here */
    float: left;
    width: 98px;
    height: 160px;
    padding: 0px 0px;
    overflow: hidden;
  }
</style>
<div id="gr_grid_widget_1564321380">
  <!-- Show static html as a placeholder in case js is not enabled - javascript include will override this if things work -->
    <div class="gr_grid_container">
<div class="gr_grid_book_container"><a title="Elements of a Philosophy of Technology: On the Evolutionary History of Culture" rel="nofollow" href="https://www.goodreads.com/book/show/39732746-elements-of-a-philosophy-of-technology"><img alt="Elements of a Philosophy of Technology: On the Evolutionary History of Culture" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="Design Unbound: Designing for Emergence in a White Water World: Ecologies of Change" rel="nofollow" href="https://www.goodreads.com/book/show/39644238-design-unbound"><img alt="Design Unbound: Designing for Emergence in a White Water World: Ecologies of Change" border="0" src="https://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png" /></a></div>
<div class="gr_grid_book_container"><a title="Waste Is Information: Infrastructure Legibility and Governance" rel="nofollow" href="https://www.goodreads.com/book/show/34540074-waste-is-information"><img alt="Waste Is Information: Infrastructure Legibility and Governance" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1502112021l/34540074._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Speculative Everything: Design, Fiction, and Social Dreaming" rel="nofollow" href="https://www.goodreads.com/book/show/17756296-speculative-everything"><img alt="Speculative Everything: Design, Fiction, and Social Dreaming" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1381287521l/17756296._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Philosophy, Technology, and the Environment (MIT Press Book 2)" rel="nofollow" href="https://www.goodreads.com/book/show/35516778-philosophy-technology-and-the-environment"><img alt="Philosophy, Technology, and the Environment" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1498443362l/35516778._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Life 3.0: Being Human in the Age of Artificial Intelligence" rel="nofollow" href="https://www.goodreads.com/book/show/34272565-life-3-0"><img alt="Life 3.0: Being Human in the Age of Artificial Intelligence" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1499718864l/34272565._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Control: Digitality as Cultural Logic" rel="nofollow" href="https://www.goodreads.com/book/show/26263175-control"><img alt="Control: Digitality as Cultural Logic" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1442815654l/26263175._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="The Book of Why: The New Science of Cause and Effect" rel="nofollow" href="https://www.goodreads.com/book/show/36204378-the-book-of-why"><img alt="The Book of Why: The New Science of Cause and Effect" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1516890908l/36204378._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Geometry of Meaning: Semantics Based on Conceptual Spaces" rel="nofollow" href="https://www.goodreads.com/book/show/18699199-geometry-of-meaning"><img alt="Geometry of Meaning: Semantics Based on Conceptual Spaces" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1392025960l/18699199._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Quantum Language and the Migration of Scientific Concepts" rel="nofollow" href="https://www.goodreads.com/book/show/36722595-quantum-language-and-the-migration-of-scientific-concepts"><img alt="Quantum Language and the Migration of Scientific Concepts" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1541090340l/36722595._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" rel="nofollow" href="https://www.goodreads.com/book/show/34523271-designing-reality"><img alt="Designing Reality: How to Survive and Thrive in the Third Digital Revolution" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1493825166l/34523271._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" rel="nofollow" href="https://www.goodreads.com/book/show/17239972-how-matter-matters"><img alt="How Matter Matters: Objects, Artifacts, and Materiality in Organization Studies" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1363584198l/17239972._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" rel="nofollow" href="https://www.goodreads.com/book/show/12923329-the-future-of-post-human-geometry"><img alt="The Future of Post-Human Geometry: A Preface to a New Theory of Infinity, Symmetry, and Dimensionality" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1348319287l/12923329._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Mismatch: How Inclusion Shapes Design" rel="nofollow" href="https://www.goodreads.com/book/show/39644200-mismatch"><img alt="Mismatch: How Inclusion Shapes Design" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1538365830l/39644200._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Staying with the Trouble: Making Kin in the Chthulucene" rel="nofollow" href="https://www.goodreads.com/book/show/28369185-staying-with-the-trouble"><img alt="Staying with the Trouble: Making Kin in the Chthulucene" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1458692014l/28369185._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Design, When Everybody Designs: An Introduction to Design for Social Innovation" rel="nofollow" href="https://www.goodreads.com/book/show/23461349-design-when-everybody-designs"><img alt="Design, When Everybody Designs: An Introduction to Design for Social Innovation" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1421710040l/23461349._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Hyperobjects: Philosophy and Ecology after the End of the World" rel="nofollow" href="https://www.goodreads.com/book/show/17802065-hyperobjects"><img alt="Hyperobjects: Philosophy and Ecology after the End of the World" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1367340523l/17802065._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Humankind: Solidarity with Non-Human People" rel="nofollow" href="https://www.goodreads.com/book/show/32686994-humankind"><img alt="Humankind: Solidarity with Non-Human People" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1499221893l/32686994._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Vibrant Matter: A Political Ecology of Things" rel="nofollow" href="https://www.goodreads.com/book/show/7547895-vibrant-matter"><img alt="Vibrant Matter: A Political Ecology of Things" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1328807459l/7547895._SX98_.jpg" /></a></div>
<div class="gr_grid_book_container"><a title="Vibrant Architecture: Matter as a Codesigner of Living Structures" rel="nofollow" href="https://www.goodreads.com/book/show/22793403-vibrant-architecture"><img alt="Vibrant Architecture: Matter as a Codesigner of Living Structures" border="0" src="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1410163737l/22793403._SX98_.jpg" /></a></div>
</div>
</div>
<script src="https://www.goodreads.com/review/grid_widget/89166601.Saira's%20project%20book%20montage?cover_size=medium&hide_link=true&hide_title=true&num_books=50&order=a&shelf=project&sort=date_added&widget_id=1564321380" type="text/javascript" charset="utf-8"></script>

<br>
</br>


## Great journals, magazines, communities and projects

   * MIT Media Lab's essential [Journal of Design and Science (JoDS)](https://jods.mitpress.mit.edu/)
   * Haus der Kulturen der Welt's beautiful and brilliant [Technosphere Magazine](http://technosphere-magazine.hkw.de/)
   * Max Planck Institute for the History of Science and Haus der Kulturen der Welt's [Anthropocene Curriculum](anthropocene-curriculum.org) project experimenting with the culture of knowledge surrounding the Anthropocene and technosphere
   * [Mismatch.design](https://mismatch.design/about-this-site/) community and a digital magazine dedicated to advancing inclusive design
   * [Anthropocene magazine](http://www.anthropocenemagazine.org)
   * [Community and resources about Metadesign](https://metadesigners.org/HomePage) linked to the The Metadesign Research Centre planned to be launches at Swansea College of Art & Design into 2019
   * Terre des Hommes Human Fablabs blog on [Fablabs for the humanitarian world](http://humanfablabs.org/)
   * [Thingiverse](https://www.thingiverse.com) online 3D printing community
   * Our internet underlords - [IAM](https://www.internetagemedia.com/)
   * The Institute of Idea's [culturewars.org](http://www.culturewars.org.uk/) on how ideas filter through to culture and how the arts in influence politics and culture
   * https://monoskop.org/
   * Peter Haff's [blog on Being Human in the Anthropocene](https://blogs.nicholas.duke.edu/anthropocene/)

   <br>

## Updates from people doing interesting things

   <a class="twitter-timeline" data-lang="en" data-width="400" data-height="600" data-theme="light" data-link-color="#00ff00" href="https://twitter.com/srazamataz/lists/mdef-interests?ref_src=twsrc%5Etfw"></a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Tools

### Mapping / visualisation tools

* Kumu.io
* Draw.io
* http://arborjs.org/halfviz/#/python-grammar
* https://pp.bme.hu/ar/article/view/8215
* https://gephi.org/
* https://rawgraphs.io/
* http://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3 map colouring tool
* https://cmap.ihmc.us/cmaptools/cmaptools-download/
* https://idyll-lang.org/

</p>
<br>

### Drawing / CAD  tools

* [Rhino 3D modelling](https://www.rhino3d.com/)
* Grasshopper
* [Grasshopper plugins](https://www.food4rhino.com/app/dodo#downloads_list)
* [Austodesk 3D modelling](https://www.autodesk.com/products/eagle/free-download)
* Adobe photoshop or InDesign

### Video tools

* Adobe after effects

### Coding tools

* [Atom web editor](https://atom.io/)
* [Gitlab](https://gitlab.com)
* [Arduino IDE](https://www.arduino.cc/en/Main/Software)

### Networking tools
* https://nodered.org/about/

## Control interfaces
* Repetier
* chilipeppr

## AI Tools

* https://modelzoo.co/
* https://ai.google/tools/datasets/
* https://blueprints.creaidai.com/

## Simulation tools

* https://www.lumerical.com/
* https://nraynaud.github.io/webgcode/
* https://www.simscale.com
* https://npm.runkit.com/d3-force - forces

## Computation Material Science and Tools

* a reference for tools available https://www.researchgate.net/publication/326608140_Machine_learning_for_molecular_and_materials_science
* https://www.materialscloud.org/work/tools/options
* https://www.materialscloud.org/work/tools/sycofinder - for metal organic frameworks
* https://github.com/marvel-nccr/quantum-mobile/releases/
* https://www.quantum-espresso.org/

<br>
