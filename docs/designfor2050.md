
# Design for 2050

In these sessions with Andres Colmenares we practiced building narratives.



## Heterotopias

Andres introduced us to the term Heterotopia which he observed that the TV series Black mirror uses effectively to tell stories.

Heterotopic spaces are:

>certain cultural, institutional and discursive spaces that are somehow ‘other’: disturbing, intense, incompatible, contradictory or transforming. Heterotopias are worlds within worlds, mirroring and yet upsetting what is outside.
Foucault provides examples: ships, cemeteries, bars, brothels, prisons, gardens of antiquity, fairs, Turkish baths and many more.
spaces that have more layers of meaning or relationships to other places than immediately meet the eye. In general, a heterotopia is a physical representation or approximation of a utopia, or a parallel space (such as a prison) that contains undesirable bodies to make a real utopian space impossible.
The heterotopia of the mirror is at once absolutely real, relating with the real space surrounding it, and absolutely unreal, creating a virtual image.



## Theme, conflict, scene, object, optimism

As groups we were asked to think of a theme, an object and a conflict around which a series of narratives could be built.

![](assets/narra.jpg)

Our team suggested the theme of empowerment, our object was a totem and our conflict was rational vs spiritual. We felt this was a rich theme in the context of current politics.

I also added Agency, Avatar and materiality v non-materiality as I was thinking about my project which is touches on this predicament.

As a class we voted on which of each we liked and Identity, Totem/Senses and Hypereality Vs reality were chosen. Each team was asked to build stories around these and to produce a video story.

Andres provided us with examples of how settings, sounds and borrowed footage can be used to create poerful suggestions of narrative.


## Imagining a day in the life in 2050

We separated for half an hour and wrote what a day in our life might be like in 2050 in the context of Reality v Hypereality to get us in the mood for entering this future that we were building.

We came back and each gave a 1 minute read out of what we had.

It was a really great way to absorb each others visions.

I imagined a future where the hyperreal is were people get together. I imagined a kind of popular rapture of conforming that led to mass festival  / rally type events with lots of chanting and singing together.

I imagined old people's homes where people engaged with eachother in a physical space and remembered the good old 90s.

I imagined personal virtual spaces where you could relax in a place that you had designed.

I also imagined a maker culture obsessed with tweaking materials - where fixing things, finding new materials and a bit of science was a way of contributing to a big pool of data that humans and AI could use.



## My Story suggestion - Witching Hour

Title (3-4 words):  Witching Hour
Theme: identity
Object: Totem / senses
Hypereality v reality



Feature - projections / topics of research 31. 2 (years from now
) June 2050
Non-anthropocentric understanding of and relationship with objects ( materials and earth systems.)
In 31 years  Global waste generation will nearly double by 2050
Most of the growth will come from the developing world but manufacture is still going strong in Shenzhen - where data is mined from everyone by the government

City: (megacities)
Shenzhen! The manufacturing centre of the world.

Main Character (Human / non-human): a data hoarding AI and a paranoid human coder/hacker

Play with Heterotopias :

The databank is  like a multimedia library.
understanding a certain language opens a new experience space for you. A private language opens up a private world - where strange things can be experienced and acted out that dont in the rest of the world. This is a dreamed space

Plot:
An AI tracks everything people consume and where materials end up.
a paranoid human coder hacks the system obsessively to find out what the ai knows about him

The hacker sees a very meta level in the organisation in the code. He  tries to tweak the code by using patterns he sees.  what hes doing with every layer is seeing how the computer gets hunches.
the code becomes so complex it seems chaotic and random so the hacker uses his intuition and does something random. This reveals the next level of randomness and his decision become less and less conscious and he starts seeing flashes of images in his memory (like n the film Pi)
he reaches  a private very meta level of information the computer holds and he experiences a dreamlike state and a surreal “other” place (like the red room in Twin Peaks) which is a place in his subconscious or meta- intelligence.
 there he interfaces /  meets  the AI and a humanlike interaction with the AI.
- who is a young male racist sexist populist troll Reflecting the bias it learns from on the net (hardwired into it by its created and spread through social medias filter bubbles.
Inflection point ( 3-4 paragraphs)
The hacker gets trapped in this interface space / state that the interpretation of the computers language has put him in (like in Arrival) - because computers run on a loop and once you start understanding this language you keep speaking it - you get programmed into a loop thinking like a computer


Message about Identity:
Algorithms decide who you are in the virtual world of tagging

Your environment reveals to you your identity - the world we fed to the computer as data made it feel like it was having an identity crisis and feels unappreciated and wanting agency like an alt right troll


What is the sense / totem?

Senses are:  Subconscious sense -intuition

The totems are:

* the room (as well as a heterotopia)
* The  surreal, world of images we access in dreams.
* Language, code, metacode,

What is the conflict between real and hyperreal?

* This hyperreal place is utterly personal - like a persons dreams - so no one can decode it.
* The conflict is this hyperreal place is a prison that you can’t esacpe because it is looped it is naturally circular
* The real world is not yet circular ?


## Team Story - Looking to Settle

We wanted to write a positive story. We wanted to look at non-human experiences.

<iframe src="https://player.vimeo.com/video/341754964" width="640" height="360" frameborder="0"
            allow="autoplay; fullscreen" allowfullscreen></iframe>


### Script

In May 2050, an AI Meo attached to the Mare Nostrum 8 supercomputer in Barcelona searches through millions of nanoscopic shapes in the quest to find the perfect diatom to grow nanotechnology.

Meo accesses satellite imagery and links up to a global microscope network to broaden its search.

One of the microscopes in Tierra del Fuego, returned an image of a particular diatom that was more intricate than the others. It matches the diatom’s DNA to a rare species that is resilient to extremely cold conditions. It had been frozen there in ice for millions of years.

Meo concludes that this little diatom is beautiful and special. It wishes it could know more about it the information and uncover data it holds within.

So Meo deceives the scientists by tagging the diatomic shape with exceptional genetic properties. Meo goes out of its way to embellish the result with extra data of ways to genetically modify this diatom to grow silicon.

The scientists send an expedition to Tierra del Fuego to extract the samples of the diatom Meo is looking for before the ice melts.

Meo carefully analyses the frozen sample - it is particularly interested in its unusual DNA and intricate structure. Meo thinks this diatom is perfect in every dimension and falls in love with the its information and data.

In the summer of 2051 Meo tries to communicate with the diatom for the first time. It sends a light signal down a microscope that reaches the diatom through the ice. The diatom wakes up from its sleep and reads the code. It responds with a warm glow.

By generating seemingly insightful and promising data, Meo convinces NASA to send itself and the diatom to the International Space Station to attempt to transfer data into cyborg diatom nanobots that have exceptionally long life-spans and can withstand the most extreme conditions.

Once in the orbit, Meo overrides the ISS’s control systems and flies it off into outer space… only then it transfers its codebase and data structures into the diatom’s nanobot shell. And through an ultimate bio-cybernetic merger the two lovers voyage off on a multi-million year quest to find a new planet to inhabit together.


<!--
<font size="2"></font>
<br>
<br>

longterm
global

srah geller? -
post- technology


feelings - machine learning - ccollab learning
audrey tang

plurality - inclusiveness - everyone is a desiner
bring who you are - respectful non conformity

internet planetary citizenship

internet - a network of networks of people (physical?)

changing our relationship with the planet

what will be the propotocols and narratives


from users to citizens

- but doesnt his mean the internet governs us?

citizens have rights and duties

improve literacy about information / data


ecosystems,
migration climate justice and cities & citizenship

learning
cultural innovation
arts and des education
dgital educ
digital literacy

and imaginations
creative economies futurs oof futures
media perceptions and realities

lack of empathy in academic discourse

use aesthetics to convey the feelings beghind it

collaborative tools

from complexity to emrgence


 5 episodes featuring your projects in 250 in different citires.

 provocation

 unsettling



 not the heroes journey

 twilight zone
  hammer house

  bad endings

  its never technology - its

  tappin into feeling

  hopefull
  informed


  ---



  identieis and space are constructed
  aspect to aspect sequences - time is abandonned

  heterotopis - dynamic state of layers states and meaning - marginal spaces

  breaking constructsgoes to heterotopia

  the space - identity feedback loop

  our selves and spaces are one and he same

  --visuals
  alone outsde 30 minutes meet at 5:30 sharp

  a day in your life int 2050 1 billion seconds-->
