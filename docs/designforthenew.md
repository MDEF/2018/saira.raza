
# <center>Design for the new</center>

<!--- Derivables:
- Bitacora describing your learning process and how are you integrating the course work into your final project. Including:
- Desired practice analysis
- Current Practice analysis
- Intervention transition pathway
  Note: We are looking for something more than the pure transcript of the exercices.  We will value that you include your personal reflections and learnings.
- Intervention prototype + documented learning process  
- 1st documented version of prototype. Different possible formats: storyboard, transmedia, performance, multimedia, roleplay, etc.
- Documented learnings from intervention rehearsal - not mandatory
- New version of intervention prototype (blueprint,media, etc) - not mandatory
-->

## Social practice theory

We first learned the concept of Social practices as a tools to analyse cultures. A social practice is:
<div class="blockquote">
‘A routinized type of behaviour which consists of several elements, interconnected to one other: forms of <b>bodily activities</b>, forms of <b>mental activities</b>, <b>‘things’</b> and their use, a background knowledge in the form of <b>understanding, know-how, states of emotion and motivational knowledge.</b>’

<div class="blockquote-footer">Reckwitz (2002)</div>
</div>

###  Stuff, Images, Skills

Social practices are made up of three components: stuff, images and skills.

<div class="blockquote">
According to Shove et al., practices comprise material artefacts, conventions and competences (sometimes and henceforth in this paper called ‘stuff’, ‘image’ and ‘skill’). Here ‘stuff’ includes technologies, artefacts, spaces, bodies, structures, formats, compositions and ingredients. ‘Image’ represents the social and personal meaning attempted or achieved through practices, including emotion, aspiration, belief, identity and aesthetics. ‘Skill’ includes understanding, taste, competence, know-how or ‘procedures’ for accomplishment of a practice as learned socially and through performance.

<div class="blockquote-footer">Higginson et al (2015) Diagramming social practice theory: An interdisciplinary experiment exploring practices as networks</div>
</div>

These definitions were difficult to grasp at first as the words "image" and "practice" are used to mean something they don't usually mean. Also these three elements contained many different phenomena which I struggled sometimes to categorise.

It took me a while to realise that Social Practice Theory is deeply philosophical and that you have to spend some time adjusting to the level of abstraction at which you have to look at things and speak of things - to be able to not jump to categorise skills as inherently human and materials as non-human.

It helped me to think of a social practice like an electrical circuit - where 'image' is the voltage or driving potential or motivation of electricity (the set up), material 'stuff' is material circuit and 'skill' is like current or the flow of charge - the movement of energy.


### Everydayness

The definition of social practices relates to the everydayness of things people do when they are part of a society. It enables us to break down things we think are social practices (that happen because we live in a society) into their everydayness elements or to build new ones or find new possible elements and connections that we can work as interventions.

### Practice-as-entity and Practice-as-performance


<div class="blockquote">
The practice-as-entity refers to the practice as a structured organisation, i.e., capturing how the elements and their links which specify ‘how actions (including speech acts) ought to be carried out,...'' (Schatzki 2001).  Practices-as-entities evidently endure over space and time. This is why they are recognisable as practices.

<div class="blockquote-footer">

<a href="https://www.researchgate.net/publication/266247132_Implications_of_Social_Practice_Theory_for_Sustainable_Design">Lenneke Kuijer (2014)</a>
</div>
</div>


Social practices are things that people do as part of a social code or norm.
However social practices are carried out as different performances belonging to the practice. People perform social practices with different constituent parts of the practice - for example showering before work and showering after exercise are different performances of the practice of showering but with different but linked motivations and different showers.

<div class="blockquote">
Given a single set of performances, there is more than one way of expressing the practice they exemplify. Performances therefore underdetermine practices
<div class="blockquote-footer">
<a href="https://books.google.co.uk/books?isbn=0199838178">Harold Kincaid (2012) The Oxford Handbook of Philosophy of Social Science</a>
</div>
</div>

Examining performances of practices broadens the focus from interactions and allows you to identify less obvious interventions.


## Finding opportunities for interventions towards desired changes

<!--- Actor-network needs mapping  Actants: both objects, discourses and people Which actants affect or are affected by your project? Which are their needs, demands or requirement?-->
### Defining desired impacts

We started off thinking about the changes we want to make.
My Desired impacts are:

* A sense of guardianship of non-renewable materials
* Wider participation in making and innovation
* Decentralised, opensource, free networks of learning and innovation.
* Inclusive education in science, technology, design and making
* A global public awareness of science and technology involved in design and making to prevent technologies being
* Design and making literacy to move out of the niche
* Wider inclusion in design and codesign methods
* Upskilling more people from all sorts of backgrounds to co-design and problem-solve.

This exercise helped to distil intuitions however the act of putting this intuition at the centre of the intervention felt the opposite of embodied design where you work and respond only with what you know at the time without presuming to know what the end result will be..

### Widening the space of practices and interventions for impact

Next we considered the actants (anything with agency) connected to these impacts.  Actor Network Theory considers the relationships between anything that has agency to affect the practice in question. Again this required avoiding the temptation to think of only humans as having agency.

<div class="blockquote">
ANT maintains that in order to study any phenomenon, all pre-existing theories must be abandoned. The observer cannot, and must not, have an a priori list of theories in which they try and fit the actors behaviour –  the actors must be allowed to make their own way and decide for themselves what their world is made of; to use an actual ANT slogan, you have “to follow the actors themselves”.

<div class="blockquote-footer"><a href="https://www.academia.edu/542543/What_is_Actor-Network_Theory">Farzana Dudhwala (year not given) What is Actor-Network Theory?</a></div>
</div>

<div class="blockquote">
Microbes, neutrinos of DNA are at the same time natural, social and discourse. They are real, human and semiotic entities in the same breath.
<div class="blockquote-footer">Bruno Latour (1990) On actor-network theory. A few clarifications plus more than a few complications</div>
</div>

#### We can find actants by:

1) first considering the thing(s) we want to affect

2) then consider, to achieve the thing..

  - what behaviours or **qualities** we need
  - what **things** can we have
  - what **actions** we can do
  - and environments or situations we can make to cause **interactions** of the previous elements

3) We then connect these actants by cause and effect:

  - what they need (cause)
  - and what actors they can affect (effect)

I found some guidance on how to build an Actor Network theory from [this site by Lewis & Clark College](https://jimproctor.us/envs/mapping-actors-processes/)

This took a lot of unpicking and I realised that I wanted multiple impacts that are connected. The exercise helped to differentiate causes from effects. It was difficult to categorise some actors as either things or opportunities for interactions - because all things are opportunities for interactions.

Often one opportunity for interaction applied to many desired impacts. There were also instances where an environment for interaction produced new ideas for things that could sit in them.

It was a lengthy, searching exercise in semantics for me where ideas and rules revealed themselves gradually. I think this process is one that could take me months to refine.

<center>![](assets/actornetwork.jpg)</center>


I have been relying heavily on mapping to understanding the practice of the MDEF course as well as my own interests and how they might come together to deliver needs of both, so I found the breaking down of actants and connecting them by impact an interesting lens to look at my previous maps.

Here is a map of ideas surrounding my intervention that  I made at the end term 2:

<br>
<iframe
  src="https://embed.kumu.io/be6ded396c07ce531095260278aed74e"
  width="940" height="600" frameborder="0"></iframe>
<br>

<!--- Which social practices can you identify from themap we have built?
➔ Mapping related social practices
➔ Locating your interventions/projects
Where does your interventions lie in the map?
Which are the main disruptions that your intervention is suggesting?
-->

### Identifying social practices connected to the desired impact

The current social practices I found in the actor network that are connected to my desired impacts were:

- STEM Education
- Human-AI relationships
- Design culture
- Making / Digital fabrication

This helped me to narrow down the focus of my project and words I use to describe it.


### Examining histories of social practices connected to impact

STEM education is a topic that I have experienced as a student and as a communicator. Although a lot of effort has been put into trying to make STEM more accessible to more students there are wider social norms surrounding who should and should not invest time in knowing about technology. Just as it helps to grow up in a culture where reading is something people do for pleasure as well as to pass exams, growing up around discourse on science and technology helps to demystify and engage interest in these topics. Many people lack these opportunities.

There are also generational problems with a current lack of science teachers being felt in UK which makes it even harder to provide these spaces for inspiring people to pursue science.

Engineering in particular is taught in a modular bottom-up structure which requires a lot of theory before you can begin to approach design problems and this can act as a barrier to think about innovation which is a niche not the norm of engineering.

<br>
<iframe
  src="https://embed.kumu.io/9a5fdf1c08a089ab8d4439a1a1c15ad4"
  width="940" height="600" frameborder="0"></iframe>
<br>

## Locating my intervention in todays social practices

My intervention includes:

- Demonstrating through an artefact how far a willing maker can get with making metamaterials with AI on their own today
- Documenting learnings from embodied design metamaterials and tools to test them.
- a map of 2nd hand research on metamaterials
- a repository of AI tools available to makers
- a roadmap to resources and processes between Machine Learning for material science, Making and design.

The social practice I situated my intervention in is Making/ Digital fabrication. I wanted to make an artefact that aspired to produce data that an AI could use.

It has turned into an investigation on linking practical knowledge with theory and mapping and diagramming has produced more constructive insight than physical making.


## Desired social practices of the future

We imagined and mapped everyday images, things and skills that might form a desired future practice.
The social practice my intervention is situated in is Making/ Digital fabrication.


This became a sprawling cloud of information very quickly revealing many paths towards change.
Thinking through the elements of this desired future social practice provoked a more fleshed out vision of the everyday experiences people might have participating in this practice.
<br>
<br>
<iframe
  src="https://embed.kumu.io/bbf1f1ca016e84cbba39779ed278891b"
  width="940" height="600" frameborder="0"></iframe>
<br>



## Designing an interventions for impact

<div class="blockquote">
‘practices change when new elements are introduced or when existing elements are combined in new ways’
<div class="blockquote-footer">Shove et al</div>
</div>

Interventions as Unfamiliar events

![](assets/unfamiliar.PNG)

*<a href="https://www.researchgate.net/publication/266247132_Implications_of_Social_Practice_Theory_for_Sustainable_Design">Lenneke Kuijer (2014)</a>*

<br>

<div class="blockquote">
Facilitating extensive reconfiguration in a desirable direction therefore requires:
• The introduction of unfamiliar elements and/or links
• Improvisation or experimentation
• Bodily performance
• Repetition, iteration and learning
• Continued monitoring and involvement

<div class="blockquote-footer">
<a href="https://www.researchgate.net/publication/266247132_Implications_of_Social_Practice_Theory_for_Sustainable_Design">Lenneke Kuijer (2014)</a>
</div>
</div>

### Practice-oriented design - definitions

<div class="blockquote">
practice-oriented design is inherently participatory. This position follows the observation by McHardy et al. (2010) that design ‘does not lie in the hands of a single actor, but is instead distributed between multiple participants’.  

what a product is, its meaning and how it is used is partly determined in practice. In this sense, the term participatory design, when used in a practice-oriented context, is a tautology.

prototyping as ‘a series of interactions between the designer and the design medium — sketching on paper, shaping clay, building with foam core’, and view it as an essential part of successful product design. According to them, prototyping ‘affords unexpected realizations that a designer could not have arrived at without producing a concrete artefact’.  

In practice-oriented design, this concept is extended towards proto-practices, which include stuff, but also images and skills. A proto-practice or practice prototype is a worked out idea or suggestion of how things could be (in terms of configurations of images, skills and stuff). A practice prototype is performed, not used  

<div class="blockquote-footer">
<a href="https://www.researchgate.net/publication/266247132_Implications_of_Social_Practice_Theory_for_Sustainable_Design">Lenneke Kuijer (2014)</a>
</div>
</div>

### Intervention transition pathway - scene setting

We were asked to imagine transition pathways from the present to desired future everydayness through tangible project/ initiative milestones.
This felt like birds-eye prototyping - imagining on micro, meso and macro levels the scenarios and performances that may lead to the desired state.
The exercise required assumptions and made me feel exposed to reveal my ignorance, naivety and biases. The exercise helped to form the scene in which a prototype could be situated. You have to start somewhere.
<br>
<iframe
  src="https://embed.kumu.io/ef5390bda442b6cd676c55d54f072517"
  width="940" height="600" frameborder="0"></iframe>
<br>
<br>

### Intervention prototype


In social practice theory, 'intervention prototype' means the performing within scenarios of a speculative intervention in order to gather data.
This is another term which has a specific meaning in social practice theory which can be confusing.

![](assets/practpro.PNG)

I began trying to promote valuing materials and sharing knowledge by trying to reverse-engineer a speculative intervention from the social practice mapping of my project that:
- Rewards being a link or a node in an open, transdisciplinary design network (something like reddit karma points).
- Equates investment with material-heavy things.
- Equates (the availability of) atoms with value.

However I felt the most significant "stuff" of a new transdisciplinary, non-anthropocentric co-design practice would be the human - non-human interfaces and was curious to think through what the functions of these interfaces would be.

### An AI design interface story board prototype
<br>
<center><iframe width="600" height="400" src="https://sraza.kumu.io/i-am-the-shape-machine" allowfullscreen></iframe></center>
<br>
When I tried this 'prototype' with several people - I found that:

1. it's linearity puts people off and did not engage them for long
2. its not intuitive to use through a screen as it doesn't feel like its about materials.
3. it would probably work better as an immersive experience.
4. A lot more fleshing out and skill is needed to make this interface presentation engaging and thought provoking.







<!--
### System Acupuncture

“to ‘interact’ with one another: they are one another, or, better, they own one another to begin with, since every item listed to define one entity might also be an item in the list defining another agent (Tarde 1903; 1999 [1895]). In other words, association is not what happens after individuals have been defined with few properties, but what characterize entities in the first place (Dewey 1927). It is even possible to argue that the very notion of ‘interaction’ as an occasional encounter among separated agents is a consequence of limited information on the attributes defining the individuals (Latour 2010).”


STUFF

Stuff refers to the tangible, material elements deployed in the practice. Shove et al. (2012) summarize them as objects, infrastructures, tools, hardware and the body itself.

SKILLS

Skills are learned bodily and mental routines, including know-how, levels of competence and ways of feeling and doing. The important point here is that in this approach, ways of feeling about and appreciating things and situations is seen as part of the practice, as learned through doing.

IMAGES

Images are socially shared ideas or concepts associated with the practice that give meaning to it; reasons to engage in it, reasons what it is for, or as Shove et al. put it, ‘the social and symbolic significance of participation at
any one moment’ (Shove et al. 2012:22).

LINKS

Moreover, since ‘practices emerge, persist and disappear as links between their defining elements are made and broken’ (Shove et al. 2012:21), these links are important for understanding change in practices

| PRACTICE | STUFF | SKILLS | IMAGES | LINKS |
|---| ---- |---------| -----|----|
| |  |  | ||
| |  |  | ||
| |  |  | |||

## Locating your intervention

new element, new practices, new relationships?

the structure of your "practice/"intervention elements, practices, relationships

how does it influence everydayness

social practice is everydayness
how does it engage with their lives

## Today

intersections between fab labs and material design

energy to bits
sensors

atoms to bits
documentation

bits to atoms
digital fabricatoin

atoms to shapes
documenting cad, material sceince libraries

energy to bits to shapes
visualisation of data

### ordinary practices today by people

> making and material optimising artefact design through shapes

## In the future

### ordinary practices in the future

>building a total participatory design channels and interfaces by humans an objects through sharing shape data

> total participatory design systems

>  users and non-humans collecting, sharing data and experiences through shapes


## Transition pathways
establish experimental practices
tensions:
problems
solutions (there are many to complex problems)
facts
futures multiplicity
desired futures can guide you with a vision
every experiences of "people"
**systems** this is our focus

### System Acupuncture - where you can intervene
stuff that you like and the system needs
deisgn interventions

waiting and observing

reframing present and future

### spatio temporal matrix for different levels of intervention

in micro, everydayness
meso,
macro

past
presnt
future


wicked problem is in the middle
the future vision is in the middle row right - the problem is resolves


what kind of microinterventions might affect the wicked problem?

### how transition happens


- experience need for change
- diagnose he system
- create pioneering practices -
- enabling tipping point - (interfaces, education, )
- sustain the transition -
- set the rules of the new mainstream - what are these?

how to describe the experience of the intevention i the future



## deconstructing practices

social needs
practices
new needs

interventions can be of stuff images and skills - hw to create a new image about a prqctices and the relationship between stuff and skills of using tools - education




## tools for pivoting

### images

#### building narratives

archetyoe toolkits
structures of narratives


#### overton window

a window of opinons of acceptability
evolves with time

social movements

### skills


#### workshop

constructionism

>you cannot separate learning from experience
Seymour Papert

really linked with the maker movement


### stuff

stuff affects practices things that you do


### images

linked to imaginaion?
images help us accessing habits

## looking ahead

building the future practices - things we do

## social practice
shared understood and shared practices between people things that we can share

the commonality of behaviours

desiging

skills - as different to behaviours

shared things that we do


so to change the social practice of designing artefacts
you can change the objects of design - the tools the interfaces the objects we use
you can change the skills  - the things we can do
the ideas (narratives) we have about design


## draw the transition plan - pathway


to go from present practice s to future ones

find the changes in stuff images and skills from futur and present

pick them

draw a transition path of each a skill an object and image
9mental model)


object - design interfaces- physics tunnel environment
catalogues and shops of stuff - using stuff you already have - reusable material
skill  - drawing and modelling - drawing in a physics tunnel  - -live drawing
mental model - anthropocentric  non anthropocentric


then describe the interventions micro, meso and macro


|  O/ S / I    |   Now   |   Future  | Macro / meso / micro  |  |
| ------------- |-------------| -----|----|
|      |  | | |
|     |      |   | |
|  |     |   | | |

putting yourself in the scene


ask questions around the intervention - different directions

what do you w



build narrative
choose a character
what props do they have
choose one setup and scenery

script and conflict

## deliverable

### bitcora learning proocess
  - documentation of process
      -current future practice
      - transition path of interventions

  - prototype intervention

  - documented learnings from testing.


  <!---
  Design
  >It is impossible to understand how a society works without appreciating how design shapes, conditions, facilitates and makes possible everyday sociality. Viewed as a type of connector, not as a separate cold domain of material relations, design’s investigation might shed light on other types of non-social ties that are brought together to make the social durable.
  * [Albena Yaneva (2009) Making the Social Hold: Towards an Actor-Network Theory of Design. Available](https://www.researchgate.net/publication/233606873_Making_the_Social_Hold_Towards_an_Actor-Network_Theory_of_Design) *
  -->
