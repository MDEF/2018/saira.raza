
<style>
img {
  float: left;
}

.gradient
{
	text-transform: uppercase;
	background: linear-gradient(blue 0%, #a3d2ff 100%);
	-webkit-background-clip: text;
  -webkit-text-fill-color: blue;
/*-webkit-text-stroke-width: 2px;
-webkit-text-stroke-color: linear-gradient(to top, #lime 0%, blue 100%);*/
		font-size: 2vw;
    line-height: 1.0;
		font-family:  'Lucida', sans-serif;
    font-weight: 400;
}
</style>

<body>
<body style="background-color:blue;">
<font color="white">
<br>
<br>
 <font face="Archivo Black" color="white" size="10">SAIRA RAZA</font>

<p><img src="https://media0.giphy.com/media/t0XXqlRWJ8uek/source.gif" alt="wave" style="width:366px;height:244px;margin-right:15px;">
<body>
<br>

I am a desk researcher with a background in mechanical engineering and setting up, running and reporting on projects for NGOs and the media sector.</p>

<p>I enjoy hoarding and mapping data, joining dots and finding gaps and where possible - finding information through making things (often by breaking things first).</p>

</p>

I'm interested in inclusive and transdisciplinary knowledge-building practices at all levels, especially to help more people contribute to the development of technology.

<!---<p>Projects I have worked on have included co-designing water and sanitation facilities with people affected by natural disasters, co-designing online communities of practice, and researching how mobile phones are used to deliver emergency public health information. Doing these and other jobs in implementation and reporting have given me a respect for accumulating and archiving data and developing tools to navigate it.</p>This year I have been looking at the relationship between geometry, materials and technology-->

<p>This year I have been researching the data driven design and digital fabrication of metamaterials. I have also been looking at the intriguing material properties of diatom frustules.</p>

<right><strong>Contact: saira.raza@iaac.net</strong></right>
</font>
</p>
</body>
