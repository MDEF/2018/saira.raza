
var lastScrollTop = 0;

$(window).scroll(function() {

  var st = window.pageYOffset || document.documentElement.scrollTop;

  if (st > lastScrollTop) {

    // scroll down
    $('#container').removeClass('scroll-up').removeClass('scroll-down');
    $('#container').addClass('scroll-down');

  }
  else {

    // scroll up
    $('#container').removeClass('scroll-up').removeClass('scroll-down');
    $('#container').addClass('scroll-up');

  }

  clearTimeout($.data(this, 'scrollTimer'));
  $.data(this, 'scrollTimer', setTimeout(function() {
	   $('#container').removeClass('scroll-up').removeClass('scroll-down');
	}, 100));

  lastScrollTop = st;

});
-----------------------------------
